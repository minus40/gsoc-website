<?php

/**
 * These are you image sizes for your site.
 * Register as many as required. Some sensible defaults have been set.
 */

return [

    'min_width' => 400,
    'min_height' => 400,

    'sizes' => [

        'small-square' => [
            'width' => 400,
            'height' => 400,
            'crop' => true
        ],

        'small-landscape' => [
            'width' => 400,
            'height' => 225,
            'crop' => true
        ],

        'small-portrait' => [
            'width' => 225,
            'height' => 400,
            'crop' => true
        ],


        'medium-square' => [
            'width' => 720,
            'height' => 720,
            'crop' => true
        ],

        'medium-landscape' => [
            'width' => 720,
            'height' => 405,
            'crop' => true
        ],

        'medium-portrait' => [
            'width' => 405,
            'height' => 720,
            'crop' => true
        ],

        'large-square' => [
            'width' => 1280,
            'height' => 1280,
            'crop' => true
        ],

        'large-landscape' => [
            'width' => 1280,
            'height' => 720,
            'crop' => true
        ],

        'large-portrait' => [
            'width' => 720,
            'height' => 1280,
            'crop' => true
        ],

        'xlarge-square' => [
            'width' => 1280,
            'height' => 1280,
            'crop' => true
        ],

        'xlarge-landscape' => [
            'width' => 1280,
            'height' => 720,
            'crop' => true
        ],

        'xlarge-portrait' => [
            'width' => 720,
            'height' => 1280,
            'crop' => true
        ],

        'full-hd' => [
            'width' => 1920,
            'height' => 1080,
            'crop' => true
        ],
    ]

];