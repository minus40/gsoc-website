<?php

return
[
    'host' => env('DB_HOST', 'localhost'),
    'username' => env('DB_USER'),
    'password' => env('DB_PASSWORD'),
    'database' => env('DB_NAME'),
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci'
];