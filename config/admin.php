<?php

    return [


        'login' => [

            /*
            |--------------------------------------------------------------------------
            | Site Name (Login)
            |--------------------------------------------------------------------------
            |
            | The name of the site that appears on the login screen Usually this will
            | be the name of the client's website.
            */

            'site_name' => 'Hailstone',

            /*
            |--------------------------------------------------------------------------
            | Site URL (Login)
            |--------------------------------------------------------------------------
            |
            | The URL you go to when you click the logo on the login screen. Usually
            | this will be the URL of the website. By default it uses the value set in
            | your .env file. Change it below if you need to.
            */

            'site_url' => env('APP_URL', '/'),

        ],


        /*
        |--------------------------------------------------------------------------
        | Footer Text
        |--------------------------------------------------------------------------
        |
        | The text for the "byline" in the footer of the Admin panel.
        | You can include HTML here.
        */

        'footer_text' => 'Website by <a href="http://minus40.co">@minus40co</a>',
    ];