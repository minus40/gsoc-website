<?php

return [
    'google' => [
        'analyticsTag' => env('ANALYTICS_TAG')
    ],
    'fromEmail' =>
    [
    	'name' => 'Garda Ombudsman',
    	'email' => 'website@gsoc.ie',
    	'reply-to' => 'infos@gsoc.ie'
    ],
    'notificationEmails' =>
    [	
    	0 => 
    	[
    		'name' => 'Minus40',
    		'email' => 'support@minus40.co'
    	]
    ]
];