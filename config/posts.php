<?php

return [

    'article' => [
        'enabled' => true,
        'provider' => \Hailstone\Core\Providers\ArticleServiceProvider::class,
        'alias' => [
            'Article' => \Hailstone\Core\Facades\ArticleFacade::class
        ]
    ],

    'audio' => [
        'enabled' => false,
        'provider' => \Hailstone\Core\Providers\AudioServiceProvider::class,
        'alias' => [
            'Audio' => \Hailstone\Core\Facades\AudioFacade::class
        ]
    ],

    'career' => [
        'enabled' => true,
        'provider' => \Hailstone\Core\Providers\CareerServiceProvider::class,
        'alias' => [
            'Career' => \Hailstone\Core\Facades\CareerFacade::class
        ]
    ],

    'case-study' => [
        'enabled' => true,
        'provider' => \Hailstone\Core\Providers\CaseStudyServiceProvider::class,
        'alias' => [
            'CaseStudy' => \Hailstone\Core\Facades\CaseStudyFacade::class
        ]
    ],

    'chart' => [
        'enabled' => true,
        'provider' => \Hailstone\Core\Providers\ChartServiceProvider::class,
        'alias' => [
            'Chart' => \Hailstone\Core\Facades\ChartFacade::class
        ]
    ],

//    'event' => [
//        'enabled' => true,
//        'provider' => \Hailstone\Core\Providers\EventServiceProvider::class,
//        'alias' => [
//            'Event' => \Hailstone\Core\Facades\EventFacade::class
//        ]
//    ],

    'faq' => [
        'enabled' => true,
        'provider' => \Hailstone\Core\Providers\FAQServiceProvider::class,
        'alias' => [
            'FAQ' => \Hailstone\Core\Facades\FAQFacade::class
        ]
    ],

    'location' => [
        'enabled' => true,
        'provider' => \Hailstone\Core\Providers\LocationServiceProvider::class,
        'alias' => [
            'Location' => \Hailstone\Core\Facades\LocationFacade::class
        ]
    ],

    'people' => [
        'enabled' => true,
        'provider' => \Hailstone\Core\Providers\PeopleServiceProvider::class,
        'alias' => [
            'People' => \Hailstone\Core\Facades\PeopleFacade::class
        ]
    ],

    'statistic' => [
        'enabled' => true,
        'provider' => \Hailstone\Core\Providers\StatisticServiceProvider::class,
        'alias' => [
            'Statistic' => \Hailstone\Core\Facades\StatisticFacade::class
        ]
    ],

//    'testimonial' => [
//        'enabled' => true,
//        'provider' => \Hailstone\Core\Providers\TestimonialServiceProvider::class,
//        'alias' => [
//            'Testimonial' => \Hailstone\Core\Facades\TestimonialFacade::class
//        ]
//    ],

    'video' => [
        'enabled' => true,
        'provider' => \Hailstone\Core\Providers\VideoServiceProvider::class,
        'alias' => [
            'Video' => \Hailstone\Core\Facades\VideoFacade::class
        ]
    ],

];