<?php

define( 'WP_POST_REVISIONS', 5 );

return
[
    'debug' => env('APP_DEBUG', false),

    'directories' => [
        'content' => '/app',
        'public' => '/public',
        'config' => '/config',
        'wordpress' => 'wp'
    ],

    'url' => env('APP_URL', 'http://localhost'),


    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [
        \Hailstone\Core\Providers\AppServiceProvider::class,
        \Hailstone\Core\Providers\TimberServiceProvider::class,
        \Hailstone\Core\Providers\OptionsServiceProvider::class,
        \Hailstone\Core\Providers\MainContentServiceProvider::class,
        \Hailstone\Core\Providers\FeatureCarouselServiceProvider::class,
        \Hailstone\Core\Providers\DocumentLibraryServiceProvider::class,
        \Hailstone\Core\Providers\GoogleMapsServiceProvider::class,
        \Hailstone\Core\Providers\HomePageServiceProvider::class,
        \Hailstone\Core\Providers\RouteServiceProvider::class,
    ],



    'aliases' => [
        'Image' => \Hailstone\Core\Facades\ImageFacade::class,

        'Page' => \Hailstone\Core\Facades\PageFacade::class,

        'Attachment' => \Hailstone\Core\Facades\AttachmentFacade::class,
        'User' => \Hailstone\Core\Facades\UserFacade::class,
    ]

];