<?php

use Illuminate\Support\Str;

if (! function_exists('env')) {
    /**
     * Gets the value of an environment variable. Supports boolean, empty and null.
     *
     * @param  string  $key
     * @param  mixed   $default
     * @return mixed
     */
    function env($key, $default = null)
    {
        $value = getenv($key);

        if ($value === false) {
            return value($default);
        }

        switch (strtolower($value)) {
            case 'true':
            case '(true)':
                return true;
            case 'false':
            case '(false)':
                return false;
            case 'empty':
            case '(empty)':
                return '';
            case 'null':
            case '(null)':
                return;
        }

        if (strlen($value) > 1 && Str::startsWith($value, '"') && Str::endsWith($value, '"')) {
            return substr($value, 1, -1);
        }

        return $value;
    }
}

if (! function_exists('app')) {
    /**
     * Get the available container instance.
     *
     * @param  string  $make
     * @param  array   $parameters
     * @return mixed|\Illuminate\Foundation\Application
     */
    function app($make = null, $parameters = [])
    {
        global $app;

        if (is_null($make)) {
            return $app;
        }

        return $app->make($make);
    }
}


if (! function_exists('config')) {
    function config($key = null, $default = null)
    {

        if (is_null($key)) {
            return app()->getConfig();
        }

        if ($key)
        {
            return app()->findValueInConfig($key);   
        }
    }
}


if (! function_exists('option')) {

    /**
     * @param $key
     *
     * @return mixed|void
     */
    function option($key)
    {
        return (new \Hailstone\Core\PostTypes\Options\Option())->get($key);
    }

};

require __DIR__ . "/../vendor/illuminate/support/helpers.php";