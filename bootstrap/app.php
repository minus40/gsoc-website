<?php

$app = new \Hailstone\Core\Foundation\Application(
    realpath(__DIR__.'/../')
);

return $app;