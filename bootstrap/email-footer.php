<?php

// function to validate form elements quckly
function runPostChecks($str, $maxlength = 48)
{
	return (isset($_POST[$str]) && strlen($_POST[$str]) > 3 && strlen($_POST[$str]) < $maxlength ) ? filter_var($_POST[$str], FILTER_SANITIZE_STRING) : false;
}

function formatLocalPhoneNumberForHref($number, $countryCode = '00353')
{
    $countryCodeLength = strlen($countryCode);

    // general clean up for spaces and pluses
    $number = str_replace(' ', '', $number); // remove spaces
    $number = str_replace('+', '00', $number); // exchange + for 0
    $number = str_replace('(0)', '', $number); // remove optional (0)

    // now, if the number begins with 0 but doesn't begin with country code, prefix with country code
    if(substr( $number, 0, $countryCodeLength ) !== $countryCode && substr($number, 0, 1)  === "0"):
         $number = preg_replace('/0/', $countryCode, $number, 1);
    endif;


    return ($number);
}

// only display the result if it is submitted within last 20 minutes
if (isset($_POST['make']) && is_numeric($_POST['make']) && $_POST['make'] < time() && $_POST['make'] > (time() - (20 * 60) ) ):

	$name = runPostChecks('name');
	$title = runPostChecks('title');
	$email = runPostChecks('email', 126);
	$tel = runPostChecks('tel', 16);
	$mob = runPostChecks('mob', 16);

	// might be ssl rendered
	$protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') === FALSE ? 'http://' : 'https://';


?>

<table cellpadding="0" cellspacing="0" border="0" width="420">

	<tr valign="top">
		<td align="left" style="text-align:left">
            <a href="https://www.gardaombudsman.ie" target="_blank">
                <img src="https://www.gardaombudsman.ie/app/themes/hailstone/img/layout/logo-email-footer.png" alt="Garda Ombudsman" vspace="0" hspace="0" width="180" height="56" border="0" style="max-width:180px;" />
            </a>
		</td>
	</tr>
    <tr>
        <td align="left" style="text-align:left">

        <?php if ($name): ?>
	        <h3 color="#831323" style="color:#831323;font-size:18px;font-weight:bold;line-height:1.5;margin:0px;margin-top:15px;padding:0px;font-family: 'calibri', Helvetica, Arial, sans-serif;">
	            <font size="3" color="#831323" face="'calibri', Helvetica, Arial, sans-serif" style="color:#831323;font-size:18px;font-weight:bold;line-height:1.5;font-family: 'calibri', Helvetica, Arial, sans-serif;">
	            <?=$name;?>
	            </font>
	        </h3>
	    <?php endif;?>

        <?php if ($title): ?>
	        <h4 color="#64666A" style="color:#64666A;font-size:14px;font-weight:bold;line-height:1.5;margin:0px;padding:0px;font-family: 'calibri', Helvetica, Arial, sans-serif;">
	            <font size="2" color="#64666A" face="'calibri', Helvetica, Arial, sans-serif" style="color:#64666A;font-size:14px;font-weight:bold;line-height:1.5;font-family: 'calibri', Helvetica, Arial, sans-serif;">
	             <?=$title;?>
	            </font>
	        </h4>
	    <?php endif;?>   

	        <p color="#64666A" style="color:#64666A;font-size:14px;font-weight:normal;line-height:1.5;margin:0px;margin-bottom:10px;padding:0px;font-family: 'calibri', Helvetica, Arial, sans-serif;">
	            <font size="2" color="#64666A" face="'calibri', Helvetica, Arial, sans-serif" style="color:#64666A;font-size:14px;font-weight:normal;line-height:1.5;font-family: 'calibri', Helvetica, Arial, sans-serif;">
	             Garda S&iacute;och&aacute;na Ombudsman Commission
	             <br />
	             150 Upper Abbey Street, Dublin 1
	             <br />
	             D01 FT73
	            </font>
	        </p>


	    </td>
    </tr>
    <tr>
        <td>
            <table cellpadding="0" cellspacing="0" border="0" width="100%">

            	<?php if ($tel): ?>

                <tr>
                    <td align="left" style="text-align:left">
				        <p color="#64666A" style="color:#64666A;font-size:14px;font-weight:normal;line-height:1.5;margin:0px;padding:0px;font-family: 'calibri', Helvetica, Arial, sans-serif;">
					        <span color="#831323" style="width:20px; display:inline-block;color:#831323;font-size:14px;font-weight:bold;line-height:1.5;margin:0px;padding:0px;font-family: 'calibri', Helvetica, Arial, sans-serif;">
					            <font size="2" color="#831323" face="'calibri', Helvetica, Arial, sans-serif" style="color:#831323;font-size:14px;font-weight:bold;line-height:1.5;font-family: 'calibri', Helvetica, Arial, sans-serif;">
					             T&nbsp;
					            </font>
					        </span>
				            <a href="tel:<?=formatLocalPhoneNumberForHref($tel);?>" style="color:#64666A;font-size:14px;font-weight:normal; text-decoration:none; margin:0px;padding:0px;font-family: 'calibri', Helvetica, Arial, sans-serif;">
					            <font size="2" color="#64666A" face="'calibri', Helvetica, Arial, sans-serif" style="text-decoration:none;color:#64666A;font-size:14px;font-weight:normal;line-height:1.5;font-family: 'calibri', Helvetica, Arial, sans-serif;">
					             <?=$tel;?>
					            </font>
					    	</a>
				        </p>
                    </td>
                </tr>

                <?php endif;?>
            	<?php if ($mob): ?>
                
                <tr>
                    <td align="left" style="text-align:left">                    
				        <p color="#64666A" style="color:#64666A;font-size:14px;font-weight:normal;line-height:1.5;margin:0px;padding:0px;font-family: 'calibri', Helvetica, Arial, sans-serif;">
					        <span color="#831323" style="width:20px; display:inline-block;color:#831323;font-size:14px;font-weight:bold;line-height:1.5;margin:0px;padding:0px;font-family: 'calibri', Helvetica, Arial, sans-serif;">
					            <font size="2" color="#831323" face="'calibri', Helvetica, Arial, sans-serif" style="color:#831323;font-size:14px;font-weight:bold;line-height:1.5;font-family: 'calibri', Helvetica, Arial, sans-serif;">
					             M&nbsp;
					            </font>
					        </span>
				            <a href="tel:<?=formatLocalPhoneNumberForHref($mob);?>" style="color:#64666A;font-size:14px;font-weight:normal; text-decoration:none; margin:0px;padding:0px;font-family: 'calibri', Helvetica, Arial, sans-serif;">
					            <font size="2" color="#64666A" face="'calibri', Helvetica, Arial, sans-serif" style="text-decoration:none; color:#64666A;font-size:14px;font-weight:normal;line-height:1.5;font-family: 'calibri', Helvetica, Arial, sans-serif;">
					             <?=$mob;?>
					            </font>
					    	</a>
				        </p>
                    </td>
                </tr>
                
                <?php endif;?>
            	<?php if ($email): ?>

                <tr>
                    <td align="left" style="text-align:left">
				        <p color="#64666A" style="color:#64666A;font-size:14px;font-weight:normal;line-height:1.5;margin:0px;padding:0px;font-family: 'calibri', Helvetica, Arial, sans-serif;">
					        <span color="#831323" style="width:20px; display:inline-block;color:#831323;font-size:14px;font-weight:bold;line-height:1.5;margin:0px;padding:0px;font-family: 'calibri', Helvetica, Arial, sans-serif;">
					            <font size="2" color="#831323" face="'calibri', Helvetica, Arial, sans-serif" style="color:#831323;font-size:14px;font-weight:bold;line-height:1.5;font-family: 'calibri', Helvetica, Arial, sans-serif;">
					             E&nbsp;
					            </font>
					        </span>
				            <a href="mailto:<?=$email;?>" style="color:#64666A;font-size:14px;font-weight:normal; text-decoration:none; margin:0px;padding:0px;font-family: 'calibri', Helvetica, Arial, sans-serif;">
					            <font size="2" color="#64666A" face="'calibri', Helvetica, Arial, sans-serif" style="text-decoration:none; color:#64666A;font-size:14px;font-weight:normal;line-height:1.5;font-family: 'calibri', Helvetica, Arial, sans-serif;">
					             <?=$email;?>
					            </font>
					    	</a>
				        </p>
                    </td>
                </tr>

                <?php endif;?>

                <tr>
                    <td align="left" style="text-align:left">
				        <p color="#64666A" style="color:#64666A;font-size:14px;font-weight:normal;line-height:1.5;margin:0px;padding:0px;font-family: 'calibri', Helvetica, Arial, sans-serif;">
					        <span color="#831323" style="width:20px; display:inline-block;color:#831323;font-size:14px;font-weight:bold;line-height:1.5;margin:0px;padding:0px;font-family: 'calibri', Helvetica, Arial, sans-serif;">
					            <font size="2" color="#831323" face="'calibri', Helvetica, Arial, sans-serif" style="color:#831323;font-size:14px;font-weight:bold;line-height:1.5;font-family: 'calibri', Helvetica, Arial, sans-serif;">
					             W&nbsp;
					            </font>
					        </span>
				            <a href="https://www.gardaombudsman.ie" target="_blank" style="color:#64666A;font-size:14px;font-weight:normal; text-decoration:none; margin:0px;padding:0px;font-family: 'calibri', Helvetica, Arial, sans-serif;">
					            <font size="2" color="#64666A" face="'calibri', Helvetica, Arial, sans-serif" style="text-decoration:none; color:#64666A;font-size:14px;font-weight:normal;line-height:1.5;font-family: 'calibri', Helvetica, Arial, sans-serif;">
					            	https://www.gardaombudsman.ie
					            </font>
					    	</a>
				        </p>
                    </td>
                </tr>

            </table>
        </td>

    </tr>
    <tr>
        <td>
	        <p color="#64666A" style="color:#64666A;font-size:10px;font-weight:normal;line-height:1.5;margin:0px;margin-top:15px;padding:0px;font-family: 'calibri', Helvetica, Arial, sans-serif;">
	            <font size="1" color="#64666A" face="'calibri', Helvetica, Arial, sans-serif" style="color:#64666A;font-size:1-px;font-weight:normal;line-height:1.5;font-family: 'calibri', Helvetica, Arial, sans-serif;">
					This email and any files transmitted with it are confidential and intended solely for the use of the individual or entity to whom they are addressed. If you have received this email in error please notify the sender.
					<br /><br />
					This email has been scanned for the presence of viruses but this does not guarantee that the email or its attachments is virus-free.
	            </font>
	        </p>
        </td>
    </tr>
</table>


<?php
// if form not submitted yet
else:
?>

<html>

<head>
	<title>Email Footer Generator</title>
	<meta name="robots" content="noindex, nofollow">
	<style>
	body
	{
		font-family: "'calibri', Helvetica, Arial, sans-serif";
	}
	div.form_row
	{
		clear:both;
		margin-bottom:15px;
		margin-top:5px;
	}
	label
	{
		display:block;
		float:left;
		width:100px;
		margin-bottom:10px;
		margin-right:15px;
		font-weight:bold;
		font-size:14px;
		margin-top:4px;
		text-transform: uppercase;
	}
	input[type="text"]
	{
		float:left;
		width:300px;
		margin-bottom:20px;
		font-size:14px;
	}
	input[type=submit] {
	    padding:5px 15px; 
	    background:#831323; 
	    color:#fff;
	    border:0 none;
	    cursor:pointer;
	    -webkit-border-radius: 5px;
	    border-radius: 5px; 
	    font-size:14px;
	    text-transform: uppercase;
	}
	fieldset { 
		border:1px solid #64666A; 
		max-width:460px;
	}

	legend 
	{
	  padding: 5px 15px;
	  border: 1px solid #64666A;
	  color:#831323; 
	  text-align:left;
	}
	h3
	{
		color:#831323; 
		text-transform: uppercase;
	}
	h3, p, li
	{
		line-height:1.2;
	}

	</style>

</head>

<body>

	<h3>
	Personalised Email Footer Generator
	</h3>

	<p>This tool will allow you to generate a standardised footer for your Garda Ombudsman email account. The output can then be copied into your Outlook Email settings.</p>

	<p><b>To use the tool:</b></p>
	<ol>
		<li>Complete the form accurately below, and press submit.</li>
		<li>When the page reloads, it will contain only the contents your personalised footer.</li>
		<li>You should copy (CTRL+C) the contents to your clipboard.</li>
		<li>[INSTRUCTIONS FROM GSOC IT TO FOLLOW]</li>
	</ol>

	<p>Note that all fields are optional, but the more information you provide, the more useful your personalised footer becomes for recipients.</p>

	<p><small>After you submit, if you notice any mistakes with your personalised footer, press the <i>back</i> button on your browser to restart the process.</small></p>

	<form name="generator_form" id="generator_form" method="post" action="<?= $_SERVER["PHP_SELF"];?>">

	<fieldset>

			<legend>Your Details</legend>

			<div class="form_row">
				<label for="name">Name</label>
				<input type="text" name="name" id="name" placeholder="your full name" maxlength="48" />
			</div>
			<div class="form_row">
				<label for="name">Job Title</label>
				<input type="text" name="title" id="title" placeholder="your official job title" maxlength="48" />
			</div>		
			<div class="form_row">
				<label for="email">Email</label>
				<input type="text" name="email" id="email" placeholder="your email address" maxlength="126" />
			</div>
			<div class="form_row">
				<label for="tel">Landline</label>
				<input type="text" name="tel" id="tel" placeholder="With no spaces e.g. 018716727" maxlength="14" />
			</div>
			<div class="form_row">
				<label for="mob">Mobile</label>
				<input type="text" name="mob" id="mob" placeholder="with no spaces e.g. 086123456" maxlength="14" />
			</div>
			<div class="form_row">
				<input type="hidden" name="make" value="<?=time();?>" />
				<input type="submit" name="submit" value="Submit" />
			</div>
		</fieldset>
	</form>

</body>
</html>

<?php
endif;
?>