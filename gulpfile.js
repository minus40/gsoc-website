import Gulp from 'gulp';
import GulpAutoprefixer from 'gulp-autoprefixer';
import GulpChanged from 'gulp-changed';
import GulpCleanCSS from 'gulp-clean-css';
import GulpConcat from 'gulp-concat';
import GulpIf from 'gulp-if';
import GulpPlumber from 'gulp-plumber';
import GulpRename from 'gulp-rename';
import GulpSass from "gulp-sass";
import GulpSourcemaps from 'gulp-sourcemaps';
import GulpTerser from 'gulp-terser';

import * as allSass from "sass";

const src = Gulp.src;
const dest = Gulp.dest;
const series = Gulp.series;
const watch = Gulp.watch;

const sass = GulpSass(allSass);


const inputScssFolder = './resources/assets/scss/';
const inputJsFolder = './resources/assets/js/';

const outputCssFolder = './public/app/themes/hailstone/css/';
const outputJsFolder = './public/app/themes/hailstone/js/';
const outputLibraryFolder = './public/app/themes/hailstone/library/';

const compileLibraries = false;

const processCSS = (source, outputFolder, outputFileName, noMap) =>
{
    return src(source)
        .pipe(GulpPlumber())
        .pipe(GulpIf(!noMap, GulpSourcemaps.init()))
        .pipe(GulpChanged(outputFolder))
        .pipe(sass({}, false).on('error', sass.logError))
        .pipe(GulpAutoprefixer())
        .pipe(GulpConcat(outputFileName + '.css'))
        .pipe(GulpCleanCSS({debug: true}))
        .pipe(GulpIf(!noMap, GulpSourcemaps.write('./maps')))
        .pipe(dest(outputFolder));
}

const processScriptJS = (source, outputFolder, outputFileName) =>
{
    return src(source)
        .pipe(GulpPlumber())
        .pipe(GulpSourcemaps.init())
        .pipe(GulpTerser())
        .pipe(GulpConcat(outputFileName + '.js'))
        .pipe(GulpSourcemaps.write('./maps'))
        .pipe(dest(outputFolder));
}

const copyAndMinimizeCSS = (source, outputFolder) =>
{
    return src(source)
        .pipe(GulpPlumber())
        .pipe(GulpSourcemaps.init())

        //copy
        .pipe(GulpChanged(outputFolder))
        .pipe(dest(outputFolder))

        //minimize
        .pipe(sass({}, false).on('error', sass.logError))
        .pipe(GulpAutoprefixer())
        .pipe(GulpCleanCSS({debug: true}))
        .pipe(GulpRename((path) => {
            path.basename += '.min';
            return path;
        }))
        .pipe(GulpSourcemaps.write('./maps'))
        .pipe(dest(outputFolder));
}

const copyAndMinimizeJS = (source, outputFolder) =>
{
    return src(source)
        .pipe(GulpPlumber())

        //copy
        .pipe(dest(outputFolder))

        //minimize
        .pipe(GulpSourcemaps.init())
        .pipe(GulpRename((path) => {
            path.basename += '.min';
            return path;
        }))
        .pipe(GulpTerser())
        .pipe(GulpSourcemaps.write('./maps'))
        .pipe(dest(outputFolder));
}

const processFiles = (source, outputFolder) =>
{
    return src(source)
        .pipe(GulpPlumber())
        .pipe(GulpChanged(outputFolder))
        .pipe(dest(outputFolder));
}

const appCSS = (cb) =>
{
    let src = processCSS(
        inputScssFolder +'app.scss',
        outputCssFolder,
        'app'
    );
    cb();
    return src;
}

const scriptJS = (cb) =>
{
    let src = processScriptJS(
        [
            './resources/assets/js/script/**/*.js'
        ],
        outputJsFolder,
        'script'
    );
    cb();
    return src;
}

const bootstrapCustomisedCSS = (cb) => {
    const src = processCSS(
        inputScssFolder + 'bootstrap-customised.scss',
        outputCssFolder,
        'bootstrap-customised'
    );
    cb();
    return src;
}

const loginCSS = (cb) =>
{
    let src = processCSS(
        inputScssFolder +'login.scss',
        outputCssFolder,
        'login'
    );
    cb();
    return src;
}

const printCSS = (cb) =>
{
    let src = processCSS(
        inputScssFolder +'print.scss',
        outputCssFolder,
        'print'
    );
    cb();
    return src;
}

const dashboardCSS = (cb) =>
{
    let src = processCSS(
        inputScssFolder +'dashboard.scss',
        outputCssFolder,
        'dashboard'
    );
    cb();
    return src;
}

const trackingJS= (cb) =>
{
    let src = processScriptJS(
        inputJsFolder + 'tracking/**/*.js',
        outputJsFolder,
        'tracking'
    );
    cb();
    return src;
}

const mapHelpersJS = (cb) =>
{
    let src = processScriptJS(
        inputJsFolder + 'map-helpers/**/*.js',
        outputJsFolder,
        'map-helpers'
    );
    cb();
    return src;
}

const adminJS = (cb) =>
{
    let src = processScriptJS(
        inputJsFolder + 'admin/**/*.js',
        outputJsFolder,
        'admin'
    );
    cb();
    return src;
}

const library = (cb) =>
{

    const chosenJS = (cb) =>
    {
        const css = (cb) =>
        {
            let src = copyAndMinimizeCSS(
                './node_modules/chosen-js/chosen.css',
                outputLibraryFolder + 'chosen-js/'
            )
            cb();
            return src;
        }

        const js = (cb) =>
        {
            let src = copyAndMinimizeJS(
                './node_modules/chosen-js/chosen.jquery.js',
                outputLibraryFolder + 'chosen-js/'
            );
            cb();
            return src;
        }

        const image = (cb) =>
        {
            let src = processFiles(
                [
                    './node_modules/chosen-js/chosen-sprite.png',
                    './node_modules/chosen-js/chosen-sprite@2x.png'
                ],
                outputLibraryFolder + 'chosen-js/'
            );
            cb();
            return src;
        }

        let src = series(
            css,
            js,
            image
        )();
        cb();
        return src;
    }

    const flatpickr = (cb) =>
    {
        const css = (cb) =>
        {
            let src = copyAndMinimizeCSS(
                './node_modules/flatpickr/dist/flatpickr.css',
                outputLibraryFolder + 'flatpickr/'
            );
            cb();
            return src;
        }

        const js = (cb) =>
        {
            let src = copyAndMinimizeJS(
                './node_modules/flatpickr/dist/flatpickr.js',
                outputLibraryFolder + 'flatpickr/'
            );
            cb();
            return src;
        }

        let src = series(
            css,
            js
        )();
        cb();
        return src;
    }

    const slickCarousel= (cb) =>
    {
        const css = (cb) =>
        {
            let src = copyAndMinimizeCSS(
                [
                    './node_modules/slick-carousel/slick/slick.css',
                    './node_modules/slick-carousel/slick/slick-theme.css'
                ],
                outputLibraryFolder + 'slick-carousel/'
            );
            cb();
            return src;
        }

        const js = (cb) =>
        {
            let src = copyAndMinimizeJS(
                './node_modules/slick-carousel/slick/slick.js',
                outputLibraryFolder + 'slick-carousel/'
            );
            cb();
            return src;
        }

        const image = (cb) =>
        {
            let src = processFiles(
                './node_modules/slick-carousel/slick/ajax-loader.gif',
                outputLibraryFolder + 'slick-carousel/'
            );
            cb();
            return src;
        }

        const font = (cb) =>
        {
            let src = processFiles(
                './node_modules/slick-carousel/slick/fonts/**/*',
                outputLibraryFolder + 'slick-carousel/fonts/'
            );
            cb();
            return src;
        }

        let src = series(
            css,
            js,
            image,
            font
        )();
        cb();
        return src;
    }

    const lazysizes = (cb) =>
    {

        let src = copyAndMinimizeJS(
            './node_modules/lazysizes/lazysizes.js',
            outputLibraryFolder + 'lazysizes/'
        );
        cb();
        return src;
    }

    const fancybox = (cb) =>
    {
        const css = (cb) =>
        {
            let src = copyAndMinimizeCSS(
                './node_modules/fancybox/dist/css/jquery.fancybox.css',
                outputLibraryFolder + 'fancybox/css/'
            );
            cb();
            return src;
        }

        const js = (cb) =>
        {
            let src = copyAndMinimizeJS(
                './node_modules/fancybox/dist/js/jquery.fancybox.js',
                outputLibraryFolder + 'fancybox/js/'
            );
            cb();
            return src;
        }

        const image = (cb) =>
        {
            let src = processFiles(
                './node_modules/fancybox/dist/img/**/*',
                outputLibraryFolder + 'fancybox/img/'
            );
            cb();
            return src;
        }

        let src = series(
            css,
            js,
            image
        )();
        cb();
        return src;
    }

    const vanillaCookieconsent = (cb) =>
    {
        const css = (cb) =>
        {
            let src = copyAndMinimizeCSS(
                './node_modules/vanilla-cookieconsent/src/cookieconsent.css',
                outputLibraryFolder + 'vanilla-cookieconsent/'
            );
            cb();
            return src;
        }

        const js = (cb) =>
        {
            let src = copyAndMinimizeJS(
                './node_modules/vanilla-cookieconsent/src/cookieconsent.js',
                outputLibraryFolder + 'vanilla-cookieconsent/'
            );
            cb();
            return src;
        }

        let src = series(
            css,
            js
        )();
        cb();
        return src;
    }


    let src = series(
        chosenJS,
        flatpickr,
        slickCarousel,
        lazysizes,
        fancybox,
        vanillaCookieconsent
    )();
    cb();
    return src;
}

const watchFiles = (cb) =>
{
    watch(inputScssFolder, appCSS);
    watch(inputJsFolder + 'script/', scriptJS);
    watch(inputScssFolder + 'base/', bootstrapCustomisedCSS);
    watch(inputScssFolder + 'login.scss', loginCSS);
    watch(inputScssFolder + 'print.scss', printCSS);
    watch(inputScssFolder + 'dashboard.scss', dashboardCSS);
    watch(inputJsFolder + 'tracking/', trackingJS);
    watch(inputJsFolder + 'map-helpers/', mapHelpersJS);
    watch(inputJsFolder + 'admin/', adminJS);
    cb();
}

let processTasks;
if(!compileLibraries){
    processTasks = series(
        appCSS,
        scriptJS,
        bootstrapCustomisedCSS,
        loginCSS,
        printCSS,
        dashboardCSS,
        trackingJS,
        mapHelpersJS,
        adminJS
    );
} else {
    processTasks = series(
        appCSS,
        scriptJS,
        bootstrapCustomisedCSS,
        loginCSS,
        printCSS,
        dashboardCSS,
        trackingJS,
        mapHelpersJS,
        adminJS,
        library
    );
}

// noinspection JSUnusedGlobalSymbols
export default series(processTasks, watchFiles);