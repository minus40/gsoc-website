# Hailstone

Hailstone is a custom built Wordpress platform based on Composer, and Timber. It contains several common modules which can be used in your projects, including custom post types and API integrations.

This documentation outlines the basics of how to use the platform.

## Getting Started
[Installation](getting-started/INSTALLATION.md)  
Configuration  
Directory Structure  
Errors & Logging  

## Core Concepts
Facades