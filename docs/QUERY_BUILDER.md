# Query Builder

A basic Wordpress Query builder has been included to make querying post types easier ansd more expressive.

Here's an example: 

~~~~
$articles = Article::wherePostNotIn([1,2,3])->orderBy('title')->get();
~~~~

You can chain as many elements together as you wish.

~~~~
$articles = Article::wherePostNotIn([1,2,3])
                ->whereStatus('publish')
                ->orderBy('title')
                ->paged(2)
                ->limit(5)
                ->get();
~~~~