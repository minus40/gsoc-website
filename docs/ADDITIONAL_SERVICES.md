
# Additional Services

### Mailchimp

A Mailchimp class has been integrated for easy subscriptions to mailing lists. To use it, you should update the Mailchimp API Key, and List ID in your .env file.

~~~~
MAILCHIMP_API_KEY=yourAPIKey
MAILCHIMP_LIST_ID=e1234567
~~~~

Once you've updated your API details, you can subscribe users to the list very simply - 

~~~~
$mailchimp = new Hailstone\Core\Services\Mailchimp();
$result = $mailchimp->subscribe('someone@testing.com', 'Jane', 'Doe');
~~~~

The $result variable will give you details of whether or not the subscriptions was a success.