# Custom Post Types

A number of custom post types have been included for you to use and configure out of the box. The files for these are located in:

/public/app/themes/hailstone/core/PostTypes/

The following types are available for you to use:

* Articles/Article (News)
* Attachments/Attachment (Media)
* FAQs/FAQ
* Media/Library (Media Taxonomy)
* Options/Option
* Pages/Page
* Testimonials/Testimonial
* Videos/Video

These post types have attributes that are unique to them, and are defined in more detail below.

## Posts

This project includes a Hailstone\Core\PostTypes\Post file which can be used to query posts. In essence, it's just an extension of the Timber Post.php class, but will allow us to include generic functionality if needed. Here's how you can use it:

~~~~
// single.php, see connected twig example

use Hailstone\Core\PostTypes\Post;

$context = Timber::get_context();
$context['post'] = Post();
Timber::render('single.twig', $context);
~~~~

~~~~
{# single.twig #}
<article>
    <h1 class="headline">{{post.title}}</h1>
    <div class="body">
        {{post.content}}
    </div>
</article>
~~~~

The full list of attributes on the Post object is available [here](http://timber.github.io/timber/#timber-post), but here are a few common ones:

**post.author**
The post author, as a Timber [User](http://timber.github.io/timber/#timber-user) Object
E.g. {{ post.author.id }} {{ post.author.first_name }} {{ post.author.avatar }} 

**post.thumbnail**
The post Thumbnail as a Timber [Image](http://timber.github.io/timber/#timber-image) object
E.g. {{ post.thumbnail.src }} {{ post.thumbnail.src }} {{ post.thumbnail.src('large-square') }}

**post.content**
The content of the post.

**post.link**
The URL of the current post.

**post.categories**
An array of all the Terms associated with this post. Returns an array of Timber [Term](http://timber.github.io/timber/#timber-term) objects.

**post.parent**
The post's parent as a Timber [Post](http://timber.github.io/timber/#timber-post)
E.g. {{ post.parent.title }} {{ post.parent.link }}

## Articles

An article will inherit all of the basic Post attributes above.

### All Articles

You can easily retrieve all Articles

~~~~
use Hailstone\Core\PostTypes\Articles\Article;

$articles = Article::all();
~~~~

It's also simple to paginate these if you wish. Here's a full example of a News listing page:

~~~~
// page-news.php

global $paged;

use Hailstone\Core\PostTypes\Articles\Article;

$context = Timber\Timber::get_context();

/* This is crucial for pagination */
query_posts([
    'post_type' => 'article',
    'posts_per_page' => 9,
    'paged' => !isset($paged) || !$paged ? 1 : $paged
]);

$context['articles'] = Article::all();
$context['pagination'] = Timber\Timber::get_pagination();

Timber::render('page-news.twig', $context);
~~~~

The basic template - 

~~~~
// views/page-news.twig
   
{% for article in articles %}

    <article>
        <div class="row">
            <div class="medium-3 columns">
                {% if article.thumbnail %}
                <figure>
                    <a href="{{ article.link }}" class="thumbnail">
                        <img src="{{ article.thumbnail.src | resize(300, 150) }}">
                    </a>
                    <figcaption>Photo caption</figcaption>
                </figure>
                {% endif %}
            </div>
            <div class="medium-9 columns">
                <h3><a href="{{ article.link }}">{{ article.title }}</a></h3>
                <p>
                    {{ article.content }}
                </p>
            </div>
        </div>
    </article>
{% endfor %}

{% include 'partials/pagination.twig'  with { 'pagination': pagination } %}
~~~~

The pagination template - 

~~~~
// views/partials/pagination.twig

<ul class="pagination" role="navigation" aria-label="Pagination">
    {% if pagination.prev %}
        <li class="pagination-previous {{pagination.prev.link|length ? '' : 'disabled'}}"><a href="{{pagination.prev.link}}" class="prev {{pagination.prev.link|length ? '' : 'invisible'}}">Previous <span class="show-for-sr">page</span></a></li>
    {% endif %}

    {% for page in pagination.pages %}
        <li>
            {% if page.link %}
                <a href="{{page.link}}" aria-label="{{page.title}}" class="{{page.class}}">{{page.title}}</a>
            {% else %}
                <span class="{{page.class}}">{{page.title}}</span>
            {% endif %}
        </li>
    {% endfor %}

    {% if pagination.next %}
        <li class="pagination-next {{pagination.next.link|length ? '' : 'disabled'}}"><a href="{{pagination.next.link}}" class="next {{pagination.next.link|length ? '' : 'invisible'}}">Next <span class="show-for-sr">page</span></a></li>
    {% endif %}
</ul>
~~~~

### Article Categories

Article categories are defined as part of the post type registration. They can be used as follows:

~~~~
<ul>
{% for category in article.categories %}
    <li><a href="{{ category.link }}">{{ category.name }}</a></li>
{% endfor %}
</ul>
~~~~

## Videos

A video will inherit all of the basic Post attributes above.

Additionally there are a number of custom functions on the video object to make our lives easier.

~~~~
// Retrieve the full link to the video. Automatically returns the correct link for both Youtube and Vimeo videos.
{{ video.url }}

// Returns the embed code for the video from the correct provider (Supports Youtube & Vimeo)
{{ video.embed }}

// Returns a large 'thumbnail' image URL for the video
{{ video.thumbnail }} - e.g <img src="{{ video.thumbnail }}" alt="My video" />
~~~~

### All Videos

If you wish to retrieve All Videos, you can do the following: 

~~~~
$videos = Video::all()
~~~~
