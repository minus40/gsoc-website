# Facades

Facades are a development tool that allow us to access namespaced classes through a static interface, giving us an easier to read and more expressive way to access our methods.

For example, we can use a Facade to find all Articles, without having to specify a full namespace or import a class. - 
~~~
Article::all();
~~~

All of the Facades are created in Hailstone\Core\Facades, and then registered in the config/app.php file.