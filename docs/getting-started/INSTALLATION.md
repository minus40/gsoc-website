## Hailstone: Installation

### Installing the Project

To install this project, first clone the repository to your local machine:

~~~~
git clone (gitURL) my-project-name
~~~~

Once the project has been cloned, you should install the project dependencies.

~~~~
composer install
sudo npm install
bower install
~~~~

### Vaprobash - Virtual Machine

Vaprobash is included by default. It tends to be the preferred VM configuration for Minus40. You have to change the hostname to your desired project in the Vagrantfile.

~~~~
16.     hostname        = "hailstone.dev"
~~~~

There are many other settings available in Vaprobash - some sensible defaults for this project have already been chosen, but you may need to configure additional settings depending on the project.

Once you've updated the Vagrantfile, you can boot the machine.

~~~~
vagrant up
~~~~

#### Environment Settings

In the root of your project, there's a file called **.env.example**, which contains the basic settings for your project. At this stage, all you need to do is update your App URL. This should match your hostname that you set in your Vagrant file earlier.

~~~~
APP_URL=http://myproject.dev
~~~~

#### Database

Before you get started, you'll need to create a database for the project. We tend to use SequelPro for this. The details are:

~~~~
MySQL Host: [Your Hostname - e.g. myproject.dev]
Username: root
Password: troot
Database: [Blank]

SSH Host: [Your Hostname - e.g. myproject.dev]
SSH User: Vagrant
SSH Password: Vagrant
~~~~

Once you're connected, you'll need to create a database for the project. Generally it's worth keeping this obvious, like myproject, or myproject_db.

Once the database has been create, you'll need to update your .env file (in the root of your project) to reflect these changes)*[]: 

~~~~
DB_NAME=myproject_db
DB_USER=root
DB_PASSWORD=root
DB_HOST=localhost
DB_PREFIX=myprfx_
~~~~

Don't forget to choose a reasonable prefix that is fairly short, but difficult to remember. Don't ever use the default 'wp_' prefix, as this poses a security problem.
