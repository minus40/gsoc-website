## Directory Structure

The Hailstone directory structure is designed to provide a simple, clean starting point for a new website.

### The Root Directory

##### The Bootstrap Directory

The `bootstrap` directory - TBC

##### The Public Directory

The `public` directory is where the main WordPress code is store. It's separated out into two directories - `app` and `wp`

### The Public Directory