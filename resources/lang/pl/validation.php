<?php

return [

    'mismatch' => "Nie ma reguły walidacji dla tego pola.",
    'validate_required' => "To pole jest wymagane",
    'validate_required_if' => "To pole jest wymagane",
    'validate_valid_email' => "To pole musi zawierać prawidłowy adres e-mail",
    'validate_min_len' => "Minimalna liczba znaków w tym polu to :param",
    'validate_max_len' => "Maksymalna liczba znaków w tym polu to :param",
    'validate_exact_len' => "Pole :attribute field mieć dokładnie :param znaków",
    'validate_alpha' => "Pole :attribute field może zawierać jedynie znaki alfabetu (a–z)",
    'validate_alpha_space' => "Pole :attribute field może zawierać jedynie znaki alfabetu (a–z) i spacje",
    'validate_alpha_numeric' => "Pole :attribute może zawierać jedynie znaki alfanumeryczne",
    'validate_alpha_dash' => "Pole :attribute może zawierać jedynie znaki alfanumeryczne i myślniki",
    'validate_numeric' => "To pole może zawierać tylko znaki numeryczne",
    'validate_integer' => "To pole może zawierać tylko wartość liczbową",
    'validate_boolean' => "To pole może zawierać tylko wartość prawda lub fałsz",
    'validate_valid_url' => "To pole musi zawierać prawidłowy URL",
    'validate_url_exists' => "Atrybut adres URL nie istnieje",
    'validate_valid_ip' => "Pole :attribute musi zawierać ważny adres IP",
    'validate_contains' => "Pole :attribute musi zawierać jedną z następujących wartości: :params,",
    'validate_contains_list' => "Pole :attribute musi zawierać wartość z listy rozwijanej",
    'validate_doesnt_contain_list' => "Pole :attribute zawiera wartość, która nie jest akceptowana",
    'validate_street_address' => "Pole :attribute musi zawierać aktualną nazwę ulicy",
    'validate_date' => "To pole musi zawierać prawidłową datę (DD/MM/RRRR)",
    'custom_date' => "To pole musi zawierać prawidłową datę (DD/MM/RRRR)",
    'validate_min_numeric' => "Pole :attribute musi mieć wartość numeryczną, równą :param lub wyższą",
    'validate_max_numeric' => "Pole :attribute musi mieć wartość numeryczną, równą :param lub niższą",
    'validate_min_age' => "To pole musi zawierać wiek większy lub równy :param",
    'validate_phone_number' => "Pole :attribute musi zawierać aktualny numer telefonu",
    'validate_equalsfield' => "Adresy e-mail nie są zgodne",
    'validate_contact' => "Nie podałeś nam numeru telefonu ani adresu e-mail, co utrudni nam skontaktowanie się z Tobą w celu skutecznego rozpatrzenia Twojej skargi. Wróć i wprowadź swoje dane teraz, jeśli chcesz, abyśmy mogli się z Tobą skontaktować."

];