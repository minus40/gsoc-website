<?php

return
    [

        'page' =>
            [
                'title' => 'Jak złożyć skargę',
                'error_msg' => 'Prosimy sprawdzić błędy zaznaczone na czerwono poniżej przed wysłaniem formularza.',
                'yes' => 'Tak',
                'no' => 'Nie',
                'max_200_words' => 'Maksymalnie 200 słów.'
            ],

        'sections' =>
            [

                'complainant_introduction' =>
                    '
            <div>
                <p>Jeśli składasz tę skargę, jesteś <b>“skarżącym”</b>.</p>
               
                <p>Należy podać wszystkie informacje wymagane w niniejszym formularzu, w przeciwnym razie rozpatrzenie skargi może zostać opóźnione.</p>

                <p><b>Przed rozpoczęciem:</b></p>
                <ul class="vertical-icons">
                    <li><a href="/make-a-complaint/before-you-complain/">Dowiedz się, czy GSOC jest właściwą organizacją do rozpatrzenia Twojej skargi.</a></li>
                    <li>Możesz również zapoznać się z naszą Polityką <a href="/site-pages/website-terms/privacy-and-cookies/">prywatności</a> i <a href="/about-gsoc/data-protection-freedom-of-information/data-protection/">ochrony danych</a>.</li>
                </ul>
            </div>
        ',

                'representative_introduction' =>
                    '
            <div class="explainer">
                <h3>Przedstawiciel</h3>

                <p>Jeśli składasz skargę w imieniu innej osoby lub firmy, musisz przedłożyć zgodę tej osoby lub dyrektora firmy, którą reprezentujesz.</p>
                <ul class="vertical-icons">
                    <li><a href="#" class="consent-form-link" target="_blank">Formularz zgody</a> znajduje się tutaj.</li>
                </ul>

                <h3>Osoby poniżej 18 roku życia</h3>

                <p>Osoby poniżej 18 roku życia mogą złożyć skargę samodzielnie lub inna osoba może złożyć skargę w ich imieniu.</p>

                <p>Jeśli poprosisz inną osobę o złożenie skargi w Twoim imieniu, musisz udzielić jej zgody na złożenie skargi. </p>

                <ul class="vertical-icons">
                    <li><a href="#" class="consent-form-link" target="_blank">Formularz zgody</a> znajduje się tutaj.</li>
                </ul>

                <p><i>Przedstawiciel musi podać wszystkie informacje wymagane w tym formularzu. Jeśli nie podasz wszystkich potrzebnych informacji, może to opóźnić rozpatrzenie Twojej skargi lub spowodować, że GSOC nie podejmie żadnych dalszych działań. </i></p>
            </div>
        ',



                'complaint_garda_members_description' =>
                    '
            <div class="explainer mb-5">
                <p>Na przykład jego:</p>
                <ul class="vertical-icons">
                    <li>Imię i nazwisko</li>
                    <li>Numer odznaki</li>
                    <li>Posterunek</li>
                    <li>Pojazd</li>
                    <li>Numer rejestracyjny pojazdu</li>
                <ul>
            </div>
        ',
                'complaint_description' => '
            <div class="explainer mb-5">
                <p>Na przykład co:</p>
                <ul class="vertical-icons">
                    <li>Powiedział?</li>
                    <li>Zrobił?</li>
                    <li>Czego nie zrobił?</li>
                </ul>
                <p>Powinna to być krótka relacja z incydentu.</p>
            </div>
        ',
                'evidence_description' =>
                    '
            <div class="explainer mb-5">
                <p>Na przykład:</p>
                <ul class="vertical-icons">
                    <li>monitoring CCTV lub inne nagrania video</li>
                    <li>dokumenty</li>
                </ul>
            </div>
        ',
                'injuries_description' =>
                    '
            <div class="explainer mb-5">
                <p>Prosimy nie przesyłać żadnych raportów medycznych ani zdjęć urazu wraz z tym formularzem. Poprosimy Państwa o nie w razie potrzeby.</p>
            </div>
        ',


                'child_protection_description' => '<p>Dobro dziecka jest najważniejsze. Jeśli Rzecznik Garda ma jakiekolwiek obawy dotyczące ochrony lub dobra dzieci, ma obowiązek udostępnić te informacje Agencji ds. Dziecka I Rodziny TUSLA. O ile to możliwe, najpierw omówimy to z Tobą. Więcej informacji na temat przepisów o ochronie dzieci I Agencji TUSLA można znaleźć na stronie <a href="https://www.tusla.ie" target="_blank">www.TUSLA.ie</a>.</p>',

                'declaration_description' => '<p>Podanie GSOC “fałszywych lub wprowadzających w błąd informacji” jest przestępstwem. W przypadku podejrzenia takiego czynu możesz zostać oskarżony. Może to skutkować grzywną i/lub karą pozbawienia wolności.
<br /><br />Mówiąc krótko, podawane informacje muszą być zgodne z prawdą i nie mogą mieć na celu wprowadzenia GSOC w błąd podczas badania skargi lub prowadzenia dochodzenia. Podanie nieprawdziwych lub wprowadzających w błąd informacji może skutkować karą grzywny w wysokości do 2 500 EUR lub karą pozbawienia wolności do 6 miesięcy, lub obiema karami łącznie.
<br /><br />Prosimy o zaznaczenie poniższego pola, aby potwierdzić zapoznanie się z powyższymi informacjami i ich zrozumienie.</p>',

                'information_description' => '<p>Twoje dane mogą zostać ujawnione stronom trzecim, takim jak Garda Síochána, w celu umożliwienia nam skutecznego zbadania skarg lub w przypadku, gdy jesteśmy do tego prawnie zobowiązani. Ma to na celu zapewnienie skutecznego rozpatrzenia skargi.
<br /><br />Tam, gdzie jest to konieczne i uzasadnione, możemy udostępniać informacje innym stronom we wszelkich powiązanych postępowaniach karnych lub cywilnych. W odniesieniu do postępowania karnego obejmuje to udostępnianie informacji dyrektorowi prokuratury, który może następnie udostępnić te informacje innym stronom, takim jak świadek lub inne osoby wskazane w skardze. Informacje można znaleźć na stronie <a href="https://www.gardaombudsman.ie/about-gsoc/non-party-disclosure/">Ujawnianie informacji osobom trzecim</a>.
<br /><br />W powiązanym postępowaniu cywilnym obejmuje to udostępnianie informacji innym stronom postępowania, co może być wymagane przez prawo, w tym przez sąd.
<br /><br />Aby uzyskać więcej informacji, skorzystaj z poniższych linków do naszej <a href="https://www.gardaombudsman.ie/about-gsoc/data-protection-freedom-of-information/data-protection/">polityki ochrony danych</a> oraz <a href="https://www.gardaombudsman.ie/site-pages/website-terms/privacy-and-cookies/">polityki prywatności i plików cookie</a> lub wyślij wiadomość e-mail na adres <a href="mailto:data.protection@gsoc.ie">data.protection@gsoc.ie</a>.</p>',

            ],


        'sidebars' =>
            [
                'change_language_title' => 'Zmień język',
                'proof_title' => 'Dlaczego potrzebujemy tych informacji?',
                'proof_content' => 'Jeśli nie otrzymamy dowodu wydania zgody lub wyjaśnień, prawdopodobnie nie będziemy w stanie rozpatrzyć skargi.',
                'help_title' => 'Potrzebujesz pomocy?',
                'help_content' => 'Zadzwoń do nas pod numer 0818 600 800, jeśli potrzebujesz pomocy w wypełnieniu formularza skargi. Pracownicy są dostępni telefonicznie w dni powszednie rano. W innych godzinach można zostawić wiadomość z prośbą o oddzwonienie lub umówienie spotkania z pracownikiem. ',
                'complainant_contact_title' => 'Pozostańmy w kontakcie',
                'complainant_contact_content' => 'Musimy mieć możliwość kontaktowania się z Tobą podczas rozpatrywania Twojej skargi. Prosimy więc o poinformowanie nas jeśli dane kontaktowe ulegną zmianie, wysyłając wiadomość e-mail na adres <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>.',
                'representative_contact_title' => 'Dlaczego potrzebujemy tych informacji?',
                'representative_contact_content' => 'Musimy mieć możliwość kontaktowania się z Tobą podczas rozpatrywania Twojej skargi. Prosimy więc o poinformowanie nas jeśli dane kontaktowe ulegną zmianie, wysyłając wiadomość e-mail na adres <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>.',

            ],


        'fields' =>
            [
                'complainant_legend' => 'Dane osobowe skarżącego',
                'complainant_name' => 'Imię i nazwisko/nazwa',
                'complainant_forename' => 'Imię',
                'complainant_surname' => 'Nazwisko',
                'complainant_primary_address_street' => 'Wiersz adresu 1',
                'complainant_primary_address_street2' => 'Wiersz adresu 2',
                'complainant_primary_address_street3' => 'Wiersz adresu 3 (opcjonalnie)',
                'complainant_primary_address_town' => 'Miejscowość',
                'complainant_primary_address_county' => 'Hrabstwo',
                'complainant_primary_address_postcode' => 'Kod eircode',
                'complainant_primary_address_country' => 'Kraj',
                'complainant_date_of_birth' => 'Data urodzenia (DD/MM/RRRR)',
                'complainant_primary_address_mobile' => 'Numer telefonu komórkowego',
                'complainant_primary_address_phone' => 'Inny numer telefonu (domowy lub służbowy) (opcjonalnie)',
                'complainant_primary_address_email' => 'Adres e-mail',
                'complainant_primary_address_email_confirm' => 'Potwierdź adres e-mail',

                'representative_legend' => 'Dane osobowe przedstawiciela',
                'representative_required_legend' => 'Czy potrzebujesz przedstawiciela?',
                'has_consent_form_legend' => 'Czy skarżący podpisał <a href="#" class="consent-form-link" target="_blank">formularz zgody</a>?',
                'third_party_representative_legend' => 'Jaka jest Twoja relacja ze skarżącym?',
                'third_party_representative_type_guardian' => 'Opiekun',
                'third_party_representative_type_legal_representative' => 'Przedstawiciel prawny',
                'third_party_representative_type_parent' => 'Rodzic',
                'third_party_representative_type_social_worker' => 'Pracownik socjalny',
                'third_party_representative_type_other' => 'Inne',
                'third_party_representative_description_other' => 'Opisz “inne”',
                'third_party_representative_name' => 'Imię i nazwisko/nazwa',
                'third_party_representative_forename' => 'Imię',
                'third_party_representative_surname' => 'Nazwisko',
                'third_party_representative_date_of_birth' => 'Data urodzenia (DD/MM/RRRR)',
                'third_party_representative_mobile' => 'Numer telefonu komórkowego',
                'third_party_representative_email' => 'Adres e-mail',
                'third_party_representative_email_confirm' => 'Potwierdź adres e-mail',

                'correspondence_legend' => 'Jeśli chcesz, aby Rzecznik Garda korespondował z Tobą pod innym adresem niż ten podany dla skarżącego, zaznacz odpowiednie pole.',
                'correspondence_type_1' => 'Adres taki sam jak skarżącego',
                'correspondence_type_2' => 'Inny adres',

                'complainant_secondary_address_legend' => 'Adres',

                'complainant_secondary_address_street' => 'Wiersz adresu 1',
                'complainant_secondary_address_town' => 'Miejscowość',
                'complainant_secondary_address_county' => 'Hrabstwo',
                'complainant_secondary_address_postcode' => 'Kod Eircode',
                'complainant_secondary_address_country' => 'Kraj',
                'complainant_secondary_address_mobile' => 'Numer telefonu komórkowego',
                'complainant_secondary_address_phone' => 'Inny numer telefonu (domowy lub służbowy) (opcjonalnie)',
                'complainant_secondary_address_email' => 'Adres e-mail',
                'complainant_secondary_address_email_confirm' => 'Potwierdź adres e-mail',

                'complaint_legend' => 'Skarga',

                'complaint_late_submission_legend' => 'Szczegóły dotyczące późnego zgłoszenia',

                'complaint_date' => 'Data incydentu (DD/MM/RRRR)',
                'complaint_time_reason' => 'Jeśli incydent miał miejsce ponad 12 miesięcy temu, prosimy powiedzieć, dlaczego nastąpiło opóźnienie w jego zgłoszeniu.',
                'complaint_time' => 'Godzina incydentu',
                'complaint_location' => 'Miejsce incydentu',
                'complaint_garda_members' => 'Czy posiadasz jakiekolwiek informacje, które mogłyby pomóc w zidentyfikowaniu funkcjonariusza Garda, na którego chcesz złożyć skargę?',
                'complaint_details' => 'Prosimy o opisanie konkretnego niewłaściwego zachowania zarzucanego funkcjonariuszowi Garda.',
                'complaint_lead_up' => 'Co wydarzyło się przed incydentem?',
                'complaint_motive' => 'Jak myślisz, dlaczego funkcjonariusz Garda zachował się w ten sposób?',

                'complaint_witnesses_legend' => 'Czy są jacyś świadkowie incydentu?',
                'complaint_witnesses' => 'Jeśli “Tak”: Prosimy o podanie imion i nazwisk oraz danych kontaktowych wszystkich świadków.',
                'complaint_evidence_legend' => 'Czy są jakieś inne dowody?',
                'complaint_evidence' => 'Jeśli “Tak”: jakie to są dowody?',
                'complaint_injuries_legend' => 'Czy odniosłeś jakieś obrażenia lub potrzebowałeś opieki medycznej po incydencie?',
                'complaint_injuries' => 'Jeśli “Tak”: Opisz wszelkie obrażenia lub leczenie, które otrzymałeś po incydencie.',

                'child_protection_legend' => 'Ochrona dzieci i obowiązki GSOC w zakresie opieki społecznej',
                'child_protection' => 'Rozumiem obowiązki GSOC w zakresie ochrony i opieki nad dziećmi.',

                'declaration_legend' => 'Sekcja 110 ustawy o Garda Síochána z 2005 r.',
                'declaration' => 'Rozumiem sekcję 110 ustawy o Garda Síochána z 2005 r.',

                'information_legend' => 'Wykorzystanie informacji',
                'information' => 'Przyjmuję do wiadomości, na jakiej podstawie GSOC wykorzystuje moje informacje i w jaki sposób może je udostępniać.',

                'submit' => 'WYŚLIJ',

            ],

        'additional' =>
            [
                'behalf_note' => 'If you are making this complaint on behalf of another person or a company, you will need to provide the consent of the person or a director of the company you are representing.',
                'date_of_birth' => 'Date of birth',
                'other_phone' => 'Other phone number',
                'date_of_incident' => 'Date of incident',
                'child_protection_description_simplified' => 'Kebajikan kanak-kanak adalah yang paling utama. Jika Garda Ombudsman mempunyai sebarang kebimbangan tentang perlindungan atau kebajikan seseorang kanak-kanak, Garda Ombudsman mempunyai kewajipan untuk berkongsi maklumat ini dengan TUSLA, Agensi Kanak-kanak dan Keluarga. Setakat yang mungkin, kami akan membincangkannya dengan anda terlebih dahulu.',

                'declaration_description_simplified' => 'Memberi "maklumat palsu atau mengelirukan" kepada GSOC dengan sengaja adalah suatu kesalahan jenayah. Anda boleh didakwa jika nampaknya anda telah melakukan ini. Ini boleh menyebabkan denda dan/atau hukuman penjara dikenakan.<br /><br />
Secara ringkasnya, maklumat yang anda berikan mestilah benar dan tidak bertujuan untuk mengelirukan GSOC dalam sebarang aduan atau siasatan.  Jika anda memberikan maklumat palsu atau mengelirukan, anda mungkin perlu membayar denda sehingga € 2,500 atau dipenjarakan sehingga 6 bulan, atau kedua-duanya. Sila tandakan kotak di bawah untuk menunjukkan bahawa anda telah membaca perkara di atas dan memahaminya.',
                'information_description_simplified' => 'Maklumat anda mungkin didedahkan kepada pihak ketiga seperti Garda Síochána untuk membolehkan kami menyiasat aduan dengan berkesan atau di mana kami diwajibkan oleh undang-undang untuk berbuat demikian. Ini adalah untuk memastikan penyiasatan yang berkesan terhadap aduan anda.
<br /><br />Sekiranya perlu dan berkadar untuk berbuat demikian, kami mungkin berkongsi maklumat dengan pihak lain dalam mana-mana prosiding jenayah atau sivil yang berkaitan. Berhubung dengan pendakwaan jenayah, ini termasuk berkongsi maklumat dengan Pengarah Pendakwaan Awam yang kemudiannya boleh berkongsi maklumat ini dengan pihak lain seperti saksi atau orang lain yang dikenal pasti dalam aduan anda.
<br /><br />Dalam prosiding sivil yang berkaitan, ini termasuk berkongsi maklumat dengan pihak lain yang mungkin diperlukan oleh undang-undang termasuk oleh Mahkamah Perundangan.',
            ]


    ];