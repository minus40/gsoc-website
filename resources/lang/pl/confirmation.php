<?php

return [
    'title' => 'Potwierdzenie',
    'paragraph_1' => 'Dziękujemy za złożenie skargi do Rzecznika Garda.',
    'paragraph_2' => 'Po opuszczeniu tej strony nie będzie można ponownie wrócić do przesłanego formularza.',
    'cta_button' => 'Zapisz PDF',
    'subtitle' => 'Następne kroki',
    'list' => ' <ul>
                    <li>W ciągu następnego tygodnia wyślemy Ci pismo potwierdzające, że Twoja skarga została zarejestrowana i że zostanie podjęta decyzja co do tego, czy może ona zostać przyjęta i rozpatrzona przez GSOC, zgodnie z kryteriami podanymi w sekcji 87 ustawy o Garda Síochána z 2005 r.</li>
                    <li>Gdy decyzja zostanie podjęta, poinformujemy Cię o niej. Jeśli przyjmiemy Twoją skargę, poinformujemy Cię, w jaki sposób zostanie ona rozpatrzona. Przeczytaj o możliwych sposobach rozpatrywania skarg.</li>
                    <li>Ukończenie procesu rozpatrywania skargi zajmuje zwykle od 3 do 9 miesięcy. W tym czasie najprawdopodobniej skontaktuje się z Tobą funkcjonariusz GSOC lub funkcjonariusz Garda Síochána zajmujący się Twoją sprawą, aby poprosić o dalsze informacje i przekazać aktualne informacje o postępach w sprawie.</li>
                    <li>Na koniec napiszemy i poinformujemy Cię o ustaleniach i wszelkich dalszych działaniach, które powinny zostać podjęte. Jeżeli Twoje dane kontaktowe ulegną zmianie, poinformuj nas o tym przez e-mail pod adresem <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a></li>
                </ul>'
];