<?php

return [

    'mismatch' => "此欄位無驗證規則。",
    'validate_required' => "此欄位為必填",
    'validate_required_if' => "此欄位為必填",
    'validate_valid_email' => "此欄位須為有效的電子郵件地址",
    'validate_min_len' => "本欄位字數至少為:param",
    'validate_max_len' => "本欄位字數至多為:param",
    'validate_exact_len' => "The :attribute field 欄位長度必須正好是 :param 個字元",
    'validate_alpha' => "The :attribute field 欄位只能包含字母字元 (a-z)",
    'validate_alpha_space' => "The :attribute field 欄位只能包含字母字元 (a-z) 和空格",
    'validate_alpha_numeric' => "The :attribute field 欄位只能包含英數字元",
    'validate_alpha_dash' => "The :attribute field 欄位只能包含字母字元和短破折號",
    'validate_numeric' => "此欄位僅能使用數字",
    'validate_integer' => "此欄位僅能使用一個數字的值",
    'validate_boolean' => "此欄位僅能使用 True 或 False",
    'validate_valid_url' => "此欄位須為有效的網址",
    'validate_url_exists' => "The :attribute field 網址不存在",
    'validate_valid_ip' => "The :attribute field 欄位必須包含有效的 IP 位址",
    'validate_contains' => "The :attribute field 欄位必須包含下列其中一個值：:params",
    'validate_contains_list' => "The :attribute field 欄位必須包含其下拉式清單中的一個值",
    'validate_doesnt_contain_list' => "The :attribute field 欄位包含一個不被接受的值",
    'validate_street_address' => "The :attribute field 欄位必須是有效的街道地址",
    'validate_date' => "此欄位須為有效日期（日/月/年）",
    'custom_date' => "此欄位須為有效日期（日/月/年）",
    'validate_min_numeric' => "The :attribute field 欄位必須是等於或大於 :param 的數值",
    'validate_max_numeric' => "The :attribute field 欄位必須是等於或小於 :param 的數值",
    'validate_min_age' => "此欄位的年紀須大於或等於：param",
    'validate_phone_number' => "The :attribute field 欄位必須是有效的電話號碼",
    'validate_equalsfield' => "電子郵件地址不相符",
    'validate_contact' => "您並未提供電話號碼或電子郵件地址給我們，這樣會造成我們聯繫上的困難，無法有效處理您的申訴。如果您希望我們能夠聯繫您，請立即返回填寫資料。"

];