<?php

return [
    'title' => '確認',
    'paragraph_1' => '感謝您向警政監察使委員會 (Garda Ombudsman) 提出申訴。',
    'paragraph_2' => '一旦離開本網站，您將無法再存取已經送出的表格。',
    'cta_button' => '儲存PDF 檔',
    'subtitle' => '下一步',
    'list' => ' <ul>
                    <li>我們會於下周內寄發一封郵件給您，確認愛爾蘭警政監察使委員會 (GSOC) 已經登錄您的申訴，並將根據 2005 年《愛爾蘭和平衛隊法案 (Garda Síochána Act)》第 87 條的標準來決議是否由愛爾蘭警政監察使委員會 (Garda Ombudsman) 受理。</li>
                    <li>當決議出來時，我們會寫信告知您。若我們可受理您的申訴，我們會告知您這件事的處理辦法。請詳閱可能的處理辦法。</li>
                    <li>申訴結案通常需要三到九個月的時間。在這段期間，為處理這起申訴，愛爾蘭警政監察使委員會 (Garda Ombudsman) 的職員或愛爾蘭和平衛隊的職員很可能會聯繫您，向您要求進一步的資料，並告知您當前處理的最新進度。</li>
                    <li>最後我們會寫信告訴您調查結果，以及未來要採取的進一步行動。如果您的聯繫方式有更動，請寫信至 <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a> 告知我們。</li>
                </ul>'
];