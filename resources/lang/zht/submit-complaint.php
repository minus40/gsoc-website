<?php

return
    [

        'page' =>
            [
                'title' => '如何提出投訴',
                'error_msg' => '在交出表格前，請查看下方以紅色標示的錯誤。',
                'yes' => '是',
                'no' => '否',
                'max_200_words' => '最多 200 個字。'
            ],

        'sections' =>
            [

                'complainant_introduction' =>
                    '
            <div>
                <p>如果提出投訴，即是「投訴人」。</p>
               
                <p>您必須填寫本表單上的所有資訊，否則可能會因此延誤投訴處理。</p>

                <p><b>在開始之前：</b></p>
                <ul class="vertical-icons">
                    <li><a href="/make-a-complaint/before-you-complain/">請確認 GSOC 是否是處理該投訴的適當機關。</a></li>
                    <li>或是參閱本公司的隱<a href="/site-pages/website-terms/privacy-and-cookies/">私權</a>與<a href="/about-gsoc/data-protection-freedom-of-information/data-protection/">個人資料保護政策</a></li>
                </ul>
            </div>
        ',

                'representative_introduction' =>
                    '
            <div class="explainer">
                <h3>代表人</h3>

                <p>如果您代表他人或公司進行投訴，需要提供所代表的個人或公司董事的同意。</p>
                <ul class="vertical-icons">
                    <li><a href="#" class="consent-form-link" target="_blank">同意書可在</a>此處下載。</li>
                </ul>

                <h3>年齡未滿 18 歲</h3>

                <p>如果年齡未滿 18 歲，可以自行提出投訴，或由其他人代為投訴。</p>

                <p>如果委託他人代您投訴，需要提供投訴同意書。</p>

                <ul class="vertical-icons">
                    <li><a href="#" class="consent-form-link" target="_blank">同意書可在</a>此處下載。</li>
                </ul>

                <p><i>代表人必須填寫此表單中的所有資訊。如果未完成填寫，可能會因此延遲處理投訴，或導致GSOC 不採取進一步行動。</i></p>
            </div>
        ',



                'complaint_garda_members_description' =>
                    '
            <div class="explainer mb-5">
                <p>例如，警察的：</p>
                <ul class="vertical-icons">
                    <li>姓名</li>
                    <li>肩章編號</li>
                    <li>警局</li>
                    <li>警車</li>
                    <li>車牌號碼</li>
                <ul>
            </div>
        ',
                'complaint_description' => '
            <div class="explainer mb-5">
                <p>例如，他們：</p>
                <ul class="vertical-icons">
                    <li>說了什麼？</li>
                    <li>做了什麼？</li>
                    <li>沒做什麼？</li>
                </ul>
                <p>簡要說明案發經過。</p>
            </div>
        ',
                'evidence_description' =>
                    '
            <div class="explainer mb-5">
                <p>例如：</p>
                <ul class="vertical-icons">
                    <li>影像監視器或其他影片</li>
                    <li>文件</li>
                </ul>
            </div>
        ',
                'injuries_description' =>
                    '
            <div class="explainer mb-5">
                <p>請勿使用本表格送交任何醫療報告或傷口的照片。如有必要，我們會向您索取這些資料。</p>
            </div>
        ',


                'child_protection_description' => '<p>兒童的福利是首要的。如果愛爾蘭警察事務投訴委員會對兒童的保護或福利有任何疑慮，有義務將這些資訊分享給兒童家庭機構 (TUSLA)。在可能的情況下，我們將首先與您討論這個問題。如果想了解《兒童優先法》(Children First Legislation) 和兒童家庭機構 (TUSLA) 更多相關資訊，請參訪網站：<a href="https://www.tusla.ie" target="_blank">www.TUSLA.ie</a>。</p>',

                'declaration_description' => '<p>故意向 GSOC 提供「不實或誤導性資訊」屬於刑事犯罪行為。如果發現您有這種行為，可能會因此遭到起訴，而導致罰款和/或入獄。
<br /><br />總之，您必須提供真實的資訊，不能在任何投訴或調查中企圖誤導 GSOC。如果您提供不實或誤導性資訊，可能需要支付最高 2500 歐元的罰款，或判處最多 6 個月有期徒刑，或兩者並處。
<br /><br />請勾選此項表示已閱讀並理解上述內容。</p>',

                'information_description' => '<p>您的資料可能會被揭露給第三方，例如愛爾蘭國家警察，以便有效調查投訴或履行法律義務。這是為了確保對您的投訴進行有效調查。
<br /><br />就必要性與相稱性原則之要求而言，我們可能會在任何有關的刑事或民事訴訟中與其他第三方分享資訊。就刑事訴訟而言，這包括與檢察官分享資訊，後者可能會分享給其他第三方，如證人或在投訴中需確認身份的其他人士。有關資訊揭露的詳細資訊，請參閱<a href="https://www.gardaombudsman.ie/about-gsoc/non-party-disclosure/">非當事人揭露</a>。
<br /><br />在相關民事訴訟中，這包括依法律要求，向法院等其他第三方分享資訊。
<br /><br />如欲了解更多資訊，請參閱我們的<a href="https://www.gardaombudsman.ie/about-gsoc/data-protection-freedom-of-information/data-protection/">資料保護政策</a>、<a href="https://www.gardaombudsman.ie/site-pages/website-terms/privacy-and-cookies/">隱私權與 Cookie 政策</a>，或發送電子郵件至<a href="mailto:data.protection@gsoc.ie">data.protection@gsoc.ie</a>。</p>',

            ],


        'sidebars' =>
            [
                'change_language_title' => '變更語言',
                'proof_title' => '我們為什麼需要這些資訊？',
                'proof_content' => '如果我們未收到同意證明或解釋，可能無法處理投訴。',
                'help_title' => '需要協助嗎？',
                'help_content' => '如果在填寫投訴表單時需要協助，請撥打 0818 600 800。平日上午會有個案工作人員可以提供協助。其餘時間，可以留言要求回電，或與個案工作人員預約時間。',
                'complainant_contact_title' => '與我們保持聯繫',
                'complainant_contact_content' => '在處理您的投訴期間，我們需要與您保持聯繫。因此，如果您的聯絡方式有任何變動，請發送電子郵件至<a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>通知我們。',
                'representative_contact_title' => '我們為什麼需要這些資訊？',
                'representative_contact_content' => '在處理您的投訴期間，我們需要與您保持聯繫。因此，如果您的聯絡方式有任何變動，請發送電子郵件至<a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>通知我們。',

            ],


        'fields' =>
            [
                'complainant_legend' => '投訴人資料',
                'complainant_name' => '姓名',
                'complainant_forename' => '名字',
                'complainant_surname' => '姓氏',
                'complainant_primary_address_street' => '地址 1',
                'complainant_primary_address_street2' => '地址 2',
                'complainant_primary_address_street3' => '地址 3（非必填）',
                'complainant_primary_address_town' => '區域',
                'complainant_primary_address_county' => '郡',
                'complainant_primary_address_postcode' => '郵遞區號',
                'complainant_primary_address_country' => '國家',
                'complainant_date_of_birth' => '出生年月日（年/月/日）',
                'complainant_primary_address_mobile' => '手機號碼',
                'complainant_primary_address_phone' => '其他聯繫電話（住家或工作）（非必填）',
                'complainant_primary_address_email' => '電子郵件地址',
                'complainant_primary_address_email_confirm' => '確認電子郵件地址',

                'representative_legend' => '代表人的個人資料',
                'representative_required_legend' => '請問是否需要代表人？',
                'has_consent_form_legend' => '投訴人是否簽署<a href="#" class="consent-form-link" target="_blank">了同意書</a>？',
                'third_party_representative_legend' => '您與投訴人是什麼關係？',
                'third_party_representative_type_guardian' => '監護人',
                'third_party_representative_type_legal_representative' => '法定代理人',
                'third_party_representative_type_parent' => '父母',
                'third_party_representative_type_social_worker' => '社會工作者',
                'third_party_representative_type_other' => '其他關係',
                'third_party_representative_description_other' => '請說明「其他」關係：',
                'third_party_representative_name' => '姓名',
                'third_party_representative_forename' => '名字',
                'third_party_representative_surname' => '姓氏',
                'third_party_representative_date_of_birth' => '出生年月日（年/月/日）',
                'third_party_representative_mobile' => '手機號碼',
                'third_party_representative_email' => '電子郵件地址',
                'third_party_representative_email_confirm' => '確認電子郵件地址',

                'correspondence_legend' => '如果希望愛爾蘭警察事務投訴委員會 (Garda Ombudsman) 使用不同於投訴人提供的地址與您通信，請勾選此選項。',
                'correspondence_type_1' => '同投訴者人',
                'correspondence_type_2' => '其他地址',

                'complainant_secondary_address_legend' => '地址',

                'complainant_secondary_address_street' => '地址 1',
                'complainant_secondary_address_town' => '區域',
                'complainant_secondary_address_county' => '郡',
                'complainant_secondary_address_postcode' => '郵遞區號',
                'complainant_secondary_address_country' => '國家',
                'complainant_secondary_address_mobile' => '手機號碼',
                'complainant_secondary_address_phone' => '其他聯繫電話（住家或工作）（非必填）',
                'complainant_secondary_address_email' => '電子郵件地址',
                'complainant_secondary_address_email_confirm' => '確認電子郵件地址',

                'complaint_legend' => '投訴',

                'complaint_late_submission_legend' => '遲交詳情',

                'complaint_date' => '案發日期（年/月/日）',
                'complaint_time_reason' => '若意外發生時間點已超過 12 個月，請告訴我們未能準時回報意外的原因。',
                'complaint_time' => '案發時間',
                'complaint_location' => '案發地點',
                'complaint_garda_members' => '是否有任何資訊能夠幫助識別要投訴的警察？',
                'complaint_details' => '請說明欲指控的警察之具體不當行為。',
                'complaint_lead_up' => '案發前發生了什麼事？',
                'complaint_motive' => '為什麼您認為警察會有如此的行為？',

                'complaint_witnesses_legend' => '案發現場有目擊者嗎？',
                'complaint_witnesses' => '如果「是」的話，請提供目擊者的全名和聯絡方式。',
                'complaint_evidence_legend' => '還有其他的證據嗎？',
                'complaint_evidence' => '如果「是」的話，請說明是什麼證據？',
                'complaint_injuries_legend' => '案發後是否受傷或需要送醫治療？',
                'complaint_injuries' => '如果「是」的話，請說明案發後受到的任何傷害或接受的治療。',

                'child_protection_legend' => 'GSOC 的兒童保護與福利義務',
                'child_protection' => '我理解 GSOC 的兒童保護與福利義務。',

                'declaration_legend' => '2005 年《愛爾蘭國家警察法》第 110 條',
                'declaration' => '我理解 2005 年《愛爾蘭國家警察法》第 110 條。',

                'information_legend' => '資料的使用',
                'information' => '我了解 GSOC 使用個人資料之依據以及分享方式。',

                'submit' => '送出表單',

            ],

        'additional' =>
            [
                'behalf_note' => '如果您代表他人或公司進行投訴，需要提供所代表的個人或公司董事的同意。',
                'date_of_birth' => '出生年月日',
                'other_phone' => '其他聯繫電話',
                'date_of_incident' => '案發日期',
                'child_protection_description_simplified' => '兒童的福利是首要的。如果愛爾蘭警察事務投訴委員會對兒童的保護或福利有任何疑慮，有義務將這些資訊分享給兒童家庭機構 (TUSLA)。在可能的情況下，我們將首先與您討論這個問題。',

                'declaration_description_simplified' => '故意向 GSOC 提供「不實或誤導性資訊」屬於刑事犯罪行為。如果發現您有這種行為，可能會因此遭到起訴，而導致罰款和/或入獄。
<br /><br />總之，您必須提供真實的資訊，不能在任何投訴或調查中企圖誤導 GSOC。如果您提供不實或誤導性資訊，可能需要支付最高 2500 歐元的罰款，或判處最多 6 個月有期徒刑，或兩者並處。',

                'information_description_simplified' => '您的資料可能會被揭露給第三方，例如愛爾蘭國家警察，以便有效調查投訴或履行法律義務。這是為了確保對您的投訴進行有效調查。
<br /><br />就必要性與相稱性原則之要求而言，我們可能會在任何有關的刑事或民事訴訟中與其他第三方分享資訊。就刑事訴訟而言，這包括與檢察官分享資訊，後者可能會分享給其他第三方，如證人或在投訴中需確認身份的其他人士。
<br /><br />在相關民事訴訟中，這包括依法律要求，向法院等其他第三方分享資訊。',
            ]


    ];