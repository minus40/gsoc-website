<?php

return [
    'title' => 'Confirmación',
    'paragraph_1' => 'Gracias por presentar su queja ante el Defensor del Pueblo de Garda.',
    'paragraph_2' => 'Una vez que abandone este sitio web, no podrá volver a acceder al formulario enviado.',
    'cta_button' => 'Guardar PDF',
    'subtitle' => 'Próximos pasos',
    'list' => ' <ul>
                    <li>Durante la próxima semana, le enviaremos una carta reconociendo que su queja se ha registrado y que se debe tomar una decisión sobre si GSOC puede admitirla y abordarla, de acuerdo con los criterios del artículo 87 de la Ley de Garda Síochána de 2005.</li>
                    <li>Cuando se tome esta decisión, le escribiremos para informarle de ella. Si podemos admitir su queja, le diremos cómo se va a tratar. Lea sobre las posibles formas en que podría tratarse.</li>
                    <li>Por lo general, se tarda de 3 a 9 meses en cerrar una queja. Lo más probable es que el oficial de GSOC o el oficial de Garda Síochána que se ocupa de su caso se comunique con usted durante este tiempo para solicitar más información y darle nueva información sobre su progreso.</li>
                    <li>Al final, le escribiremos para comunicarle los hallazgos y cualquier otra acción que deba tomar. Háganos saber, enviando un correo electrónico a <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>, si sus datos de contacto cambian.</li>
                </ul>'
];