<?php

return
    [

        'page' =>
            [
                'title' => 'Cómo presentar una queja',
                'error_msg' => 'Comprueba los errores siguientes marcados en rojo antes de enviar el formulario.',
                'yes' => 'Sí',
                'no' => 'No',
                'max_200_words' => 'Máx. 200 palabras.'
            ],

        'sections' =>
            [

                'complainant_introduction' =>
                    '
            <div>
                <p>Si está presentando esta queja, usted es el <b>"denunciante"</b>.</p>
               
                <p>Debe completar toda la información en este formulario o de lo contrario el procesamiento de su queja puede retrasarse.</p>

                <p><b>Antes de empezar</b></p>
                <ul class="vertical-icons">
                    <li><a href="/make-a-complaint/before-you-complain/">Averigüe si GSOC es la organización adecuada para tratar su queja/a></li>
                    <li>También puede consultar nuestra <a href="/site-pages/website-terms/privacy-and-cookies/">Política de privacidad</a> y <a href="/about-gsoc/data-protection-freedom-of-information/data-protection/">protección de datos</a>.</li>
                </ul>
            </div>
        ',

                'representative_introduction' =>
                    '
            <div class="explainer">
                <h3>Un representante</h3>

                <p>Si presenta esta queja en nombre de otra persona o de una empresa, deberá proporcionar el consentimiento de la persona o de un director de la empresa a la que representa.</p>
                <ul class="vertical-icons">
                    <li>Aquí se proporciona un <a href="#" class="consent-form-link" target="_blank">formulario de consentimiento</a></li>
                </ul>

                <h3>Menores de 18 años</h3>

                <p>Si es menor de 18 años, puede presentar una queja usted mismo u otra persona puede presentar esta queja por usted.</p>

                <p>Si solicita a otra persona que presente esta queja por usted, deberá proporcionarle su consentimiento para presentar su queja. </p>

                <ul class="vertical-icons">
                    <li>Aquí se proporciona un <a href="#" class="consent-form-link" target="_blank">formulario de consentimiento</a>.</li>
                </ul>

                <p>El representante debe rellenar toda la información de este formulario. Si no completa toda la información que necesitamos, puede retrasar el procesamiento de su queja o que como consecuencia GSOC no tome ninguna acción al respecto. </i></p>
            </div>
        ',



                'complaint_garda_members_description' =>
                    '
            <div class="explainer mb-5">
                <p>Por ejemplo, su:</p>
                <ul class="vertical-icons">
                    <li>Nombre</li>
                    <li>Número de agente</li>
                    <li>Comisaría</li>
                    <li>Coche</li>
                    <li>Número de registro de coche</li>
                <ul>
            </div>
        ',
                'complaint_description' => '
            <div class="explainer mb-5">
                <p>Por ejemplo, ¿qué:</p>
                <ul class="vertical-icons">
                    <li>Dijeron?</li>
                    <li>Hicieron?</li>
                    <li>No hicieron?</li>
                </ul>
                <p>Debe incluir un breve recuento del incidente.</p>
            </div>
        ',
                'evidence_description' =>
                    '
            <div class="explainer mb-5">
                <p>Por ejemplo:</p>
                <ul class="vertical-icons">
                    <li>CCTV u otras imágenes de vídeo</li>
                    <li>documentos</li>
                </ul>
            </div>
        ',
                'injuries_description' =>
                    '
            <div class="explainer mb-5">
                <p>No envíes ningún informe médico ni fotos de la lesión en este formulario. Si es necesario, te los solicitaremos.</p>
            </div>
        ',


                'child_protection_description' => '<p>El bienestar de un niño es primordial. Si el Defensor del Pueblo de Garda tiene alguna sospecha con respecto a la protección o el bienestar de un niño, tiene la obligación de compartir esta información con TUSLA, la Agencia de Niños y Familias. En la medida de lo posible, lo discutiremos primero con usted. Más información sobre la Leigslación de Children First y TUSLA está disponible en <a href="https://www.tusla.ie" target="_blank">www.TUSLA.ie</a>.</p>',

                'declaration_description' => '<p>Es un delito penal proporcionar a sabiendas "información falsa o engañosa" a GSOC. Puede ser procesado si resulta aparente que lo ha hecho, lo cual podría resultar en una multa y/o una sentencia de prisión.<br /><br />
En resumen, la información que proporcione debe ser veraz y no tratar de engañar a GSOC en ninguna queja o investigación. Si da información falsa o engañosa, es posible que tenga que pagar una multa de hasta 2.500 € o cumplir una pena de prisión de hasta 6 meses, o ambas cosas.<br /><br />
Marque la casilla a continuación para demostrar que ha leído lo anterior y lo entiende.
</p>',
                'information_description' => '<p>Su información puede divulgarse a terceros, como Garda Síochána, para que podamos investigar eficazmente las quejas o cuando estemos legalmente obligados a hacerlo. Tiene como intención garantizar una investigación efectiva de su queja.
<br /><br />Cuando sea necesario y proporcionado hacerlo, podemos compartir información con otras partes en cualquier procedimiento penal o civil relacionado. En relación con un enjuiciamiento penal, esto incluye compartir información con el fiscal general del Estado (Director of Public Prosecutions), quien luego puede compartir esa información con otras partes, como un testigo u otras personas identificadas en su denuncia. Para obtener información, visite <a href="https://www.gardaombudsman.ie/about-gsoc/non-party-disclosure/">Divulgación de información de partes no interesadas</a>.
<br /><br />En un procedimiento civil relacionado, esto incluye compartir información con otras partes según lo requiera la ley, incluido un tribunal de justicia.
<br /><br />Para más información, siga estos enlaces a nuestra <a href="https://www.gardaombudsman.ie/about-gsoc/data-protection-freedom-of-information/data-protection/">Política de protección de datos</a> y <a href="https://www.gardaombudsman.ie/site-pages/website-terms/privacy-and-cookies/">Política de privacidad y cookies</a> o envíe un correo electrónico a <a href="mailto:data.protection@gsoc.ie">data.protection@gsoc.ie</a>.</p>',

            ],


        'sidebars' =>
            [
                'change_language_title' => 'Cambiar idioma',
                'proof_title' => '¿Por qué necesitamos esta información?',
                'proof_content' => 'Si no recibimos ninguna prueba de consentimiento o explicación, probablemente no podremos tratar la queja.',
                'help_title' => '¿Necesita ayuda?',
                'help_content' => 'Llámenos al 0818 600 800 si necesita ayuda para completar el formulario de queja. Los trabajadores sociales están disponibles por teléfono entre semana por la mañana. En otras ocasiones, puede dejar un mensaje solicitando una devolución de llamada o una cita con un trabajador social.',
                'complainant_contact_title' => 'Mantengámonos en contacto',
                'complainant_contact_content' => 'Tendremos que poder ponernos en contacto con usted mientras tratamos su queja. Así que háganos saber, enviando un correo electrónico a <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>, si sus datos de contacto cambian.',
                'representative_contact_title' => '¿Por qué necesitamos esta información?',
                'representative_contact_content' => 'Tendremos que poder ponernos en contacto con usted mientras tratamos su queja. Así que háganos saber, enviando un correo electrónico a <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>, si sus datos de contacto cambian.',

            ],


        'fields' =>
            [
                'complainant_legend' => 'Datos personales del denunciante',
                'complainant_name' => 'Nombre',
                'complainant_forename' => 'Nombre de pila',
                'complainant_surname' => 'Apellidos',
                'complainant_primary_address_street' => 'Línea de dirección 1',
                'complainant_primary_address_street2' => 'Línea de dirección 2',
                'complainant_primary_address_street3' => 'Línea de dirección 3 (opcional)',
                'complainant_primary_address_town' => 'Localidad',
                'complainant_primary_address_county' => 'Condado',
                'complainant_primary_address_postcode' => 'Código postal',
                'complainant_primary_address_country' => 'País',
                'complainant_date_of_birth' => 'Fecha de nacimiento (DD/MM/AAAA)',
                'complainant_primary_address_mobile' => 'Número de teléfono móvil',
                'complainant_primary_address_phone' => 'Otro número de teléfono (casa o trabajo) (opcional)',
                'complainant_primary_address_email' => 'Correo electrónico',
                'complainant_primary_address_email_confirm' => 'Confirme su dirección de correo electrónico',

                'representative_legend' => 'Datos personales del representante',
                'representative_required_legend' => '¿Necesita un representante?',
                'has_consent_form_legend' => '¿Ha firmado el denunciante el <a href="#" class="consent-form-link" target="_blank">formulario de consentimiento</a>?',
                'third_party_representative_legend' => '¿Cuál es su relación con el denunciante?',
                'third_party_representative_type_guardian' => 'Tutor',
                'third_party_representative_type_legal_representative' => 'Representante legal',
                'third_party_representative_type_parent' => 'Padre/madre',
                'third_party_representative_type_social_worker' => 'Trabajador/a social',
                'third_party_representative_type_other' => 'Otro',
                'third_party_representative_description_other' => 'Por favor, describa "otro"',
                'third_party_representative_name' => 'Nombre',
                'third_party_representative_forename' => 'Nombre de pila',
                'third_party_representative_surname' => 'Apellidos',
                'third_party_representative_date_of_birth' => 'Fecha de nacimiento (DD/MM/AAAA)',
                'third_party_representative_mobile' => 'Número de teléfono móvil',
                'third_party_representative_email' => 'Correo electrónico',
                'third_party_representative_email_confirm' => 'Confirme su dirección de correo electrónico',

                'correspondence_legend' => 'Si desea que el Defensor del Pueblo de Garda se comunique con usted en una dirección diferente a la proporcionada para el demandante, marque la casilla correspondiente.',
                'correspondence_type_1' => 'La misma que el denunciante',
                'correspondence_type_2' => 'Dirección diferente',

                'complainant_secondary_address_legend' => 'Dirección',

                'complainant_secondary_address_street' => 'Línea de dirección 1',
                'complainant_secondary_address_town' => 'Localidad',
                'complainant_secondary_address_county' => 'Condado',
                'complainant_secondary_address_postcode' => 'Código postal',
                'complainant_secondary_address_country' => 'País',
                'complainant_secondary_address_mobile' => 'Número de teléfono móvil',
                'complainant_secondary_address_phone' => 'Otro número de teléfono (casa o trabajo) (opcional)',
                'complainant_secondary_address_email' => 'Correo electrónico',
                'complainant_secondary_address_email_confirm' => 'Confirme su dirección de correo electrónico',

                'complaint_legend' => 'La queja',

                'complaint_late_submission_legend' => 'Detalles de envío tardío',

                'complaint_date' => 'Fecha del incidente (DD/MM/AAAA)',
                'complaint_time_reason' => 'Si el incidente se produjo hace más de 12 horas, indícanos por qué se ha informado con retraso.',
                'complaint_time' => 'Hora del incidente',
                'complaint_location' => 'Lugar del incidente',
                'complaint_garda_members' => '¿Tiene alguna información que pueda ayudar a identificar a la Garda de la que desea quejarse?',
                'complaint_details' => 'Describa el mal comportamiento específico que alega que Garda llevó a cabo.',
                'complaint_lead_up' => '¿Qué ocurrió antes del incidente?',
                'complaint_motive' => '¿Por qué cree que Garda se comportó así?',

                'complaint_witnesses_legend' => 'En caso afirmativo: ¿Hay testigos del incidente?',
                'complaint_witnesses' => 'Proporcione los nombres completos y los datos de contacto de cualquier testigo aquí.',
                'complaint_evidence_legend' => '¿Hay alguna otra prueba?',
                'complaint_evidence' => 'En caso afirmativo: ¿qué pruebas hay?',
                'complaint_injuries_legend' => '¿Sufrió alguna lesión o necesitó tratamiento médico después del incidente?',
                'complaint_injuries' => 'En caso afirmativo: Describa cualquier lesión o tratamiento médico que haya recibido después del incidente.',

                'child_protection_legend' => 'Obligaciones de protección y bienestar infantil del GSOC',
                'child_protection' => 'Entiendo las Obligaciones de Protección y Bienestar Infantil de GSOC',

                'declaration_legend' => 'Artículo 110 de la Ley de Garda Síochána, 2005',
                'declaration' => 'Entiendo el Artículo 110 de la Ley Garda Síochána, 2005',

                'information_legend' => 'Uso de su información',
                'information' => 'Reconozco las bases por las que GSOC utiliza mi información y cómo se puede compartir',

                'submit' => 'Enviar',

            ],

        'additional' =>
            [
                'behalf_note' => 'Si presenta esta queja en nombre de otra persona o de una empresa, deberá proporcionar el consentimiento de la persona o de un director de la empresa a la que representa.',
                'date_of_birth' => 'Fecha de nacimiento',
                'other_phone' => 'Otro número de teléfono',
                'date_of_incident' => 'Fecha del incidente',

                'child_protection_description_simplified' => 'El bienestar de un niño es primordial. Si el Defensor del Pueblo de Garda tiene alguna sospecha con respecto a la protección o el bienestar de un niño, tiene la obligación de compartir esta información con TUSLA, la Agencia de Niños y Familias. En la medida de lo posible, lo discutiremos primero con usted.',

                'declaration_description_simplified' => 'Es un delito penal proporcionar a sabiendas "información falsa o engañosa" a GSOC. Puede ser procesado si resulta aparente que lo ha hecho, lo cual podría resultar en una multa y/o una sentencia de prisión.<br /><br />
En resumen, la información que proporcione debe ser veraz y no tratar de engañar a GSOC en ninguna queja o investigación. Si da información falsa o engañosa, es posible que tenga que pagar una multa de hasta 2.500 € o cumplir una pena de prisión de hasta 6 meses, o ambas cosas.
',
                'information_description_simplified' => 'Su información puede divulgarse a terceros, como Garda Síochána, para que podamos investigar eficazmente las quejas o cuando estemos legalmente obligados a hacerlo. Tiene como intención garantizar una investigación efectiva de su queja.
<br /><br />Cuando sea necesario y proporcionado hacerlo, podemos compartir información con otras partes en cualquier procedimiento penal o civil relacionado. En relación con un enjuiciamiento penal, esto incluye compartir información con el fiscal general del Estado (Director of Public Prosecutions), quien luego puede compartir esa información con otras partes, como un testigo u otras personas identificadas en su denuncia.
<br /><br />En un procedimiento civil relacionado, esto incluye compartir información con otras partes según lo requiera la ley, incluido un tribunal de justicia.'
            ]


    ];