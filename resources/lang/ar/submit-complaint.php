<?php

return [

    'page' =>
        [
            'title' => 'كيفية تقديم شكوى',
            'error_msg' => 'يرجى التحقق من الأخطاء باللون الأحمر أدناه، قبل تقديم النموذج الخاص بك.',
            'yes' => 'نعم',
            'no' => 'لا',
            'max_200_words' => '200 كلمة بحد أقصى.'
        ],

    'sections' =>
        [

            'complainant_introduction' =>
                '
        <div>
            <p>إذا كنت أنت مَن يقدم الشكوى، فأنت \'المُشتكي\'.</p>
           
           <p>يجب عليك استكمال جميع المعلومات المطلوبة في هذا النموذج، وإلا فقد يتأخر النظر في شكواك.</p>

            <p>قبل البدء في إجراءات تقديم الشكوى:</p>
            <ul class="vertical-icons">
                    <li><a href="/make-a-complaint/before-you-complain/">تأكد مما إذا كان ديوان المظالم التابع للشرطة الآيرلندية هو المؤسسة المختصة بالنظر في شكواك.</a></li>
                    <li>أيضًا في الاطلاع على سياسة <a href="/site-pages/website-terms/privacy-and-cookies/">الخصوصية</a> وحماية ا<a href="/about-gsoc/data-protection-freedom-of-information/data-protection/">حماية البيانات</a>بيانات المُعتمَدة لدينا.</li>
            </ul>
        </div>
    ',

            'representative_introduction' =>
                '
        <div class="explainer">
            <h3>الممثل</h3>

            <p>إذا كنت تقدم هذه الشكوى نيابةً عن شخص آخر أو شركة أخرى، فإنه يجب عليك تقديم موافقة من الشخص الذي تُمثِّله أو مدير الشركة التي تمثلها.</p>
            <ul class="vertical-icons">
                يمكنك العثور على <a href="#" class="consent-form-link" target="_blank">نموذج الموافقة</a> هنا.
            </ul>

            <h3>لمن هم أقل من 18 عامًا</h3>

            <p>إذا كنت دون سن 18 عامًا، فإنه يمكنك تقديم الشكوى بنفسك أو يمكن لشخص آخر تقديمها نيابةً عنك.</p>

            <p>إذا طلبت من شخص آخر تقديم هذه الشكوى نيابةً عنك، يجب عليك تزويده بموافقتك على تفويضه في تقديم شكواك.</p>

            <ul class="vertical-icons">
                يمكنك العثور على <a href="#" class="consent-form-link" target="_blank">نموذج الموافقة</a> هنا.
            </ul>

            <p><i>يجب على الممثل ملء جميع المعلومات المطلوبة في هذا النموذج. إذا لم تُكمل جميع المعلومات التي نطلبها، فقد يترتب على ذلك تأخير النظر في شكواك أو عدم اتخاذ ديوان المظالم التابع للشرطة الآيرلندية أي إجراء بشأنها.</i></p>
        </div>
    ',


            'complaint_garda_members_description' =>
                '
        <div class="explainer mb-5">
            <p>على سبيل المثال:</p>
            <ul class="vertical-icons">
                <li>الاسم</li>
                <li> رقم الهُوية</li>
                <li>المركز</li>
                <li>السيارة</li>
                <li>رقم لوحة السيارة</li>
            <ul>
        </div>
    ',
            'complaint_description' => '
        <div class="explainer mb-5">
            <p>على سبيل المثال، ماذا فعل:</p>
            <ul class="vertical-icons">
                <li>ماذا قال؟</li>
                <li>ماذا فعل؟</li>
                <li>هل امتنع عن العمل؟</li>
            </ul>
            <p>يجب تقديم نُبذة موجزة عن الحادث.</p>
        </div>
    ',
            'evidence_description' =>
                '
        <div class="explainer mb-5">
            <p>على سبيل المثال:</p>
            <ul class="vertical-icons">
                <li>لقطات لكاميرا مراقبة أو مقاطع فيديو أخرى</li>
                <li>مستندات</li>
            </ul>
        </div>
    ',
            'injuries_description' =>
                '<div class="explainer mb-5"><p>من فضلك، لا تقدم أي تقارير طبية أو صور للإصابة مع هذا النموذج. إن كانت ضرورية، سنطلبها منك. تفاصيل تأخر التقديم
</p></div>',


            'child_protection_description' => '<p>رعاية الأطفال ورفاهيتهم من الأمور المُهمة. إذا كان لدى ديوان المظالم التابع للشرطة الآيرلندية أي مخاوف تتعلق بحماية الأطفال أو رعايتهم، فإنه مُلزم بمشاركة هذه المعلومات مع وكالة الطفل والأسرة (توسلا). وبقدر الإمكان، سنناقش هذا الأمر معك أولًا. يمكنك الاطلاع على المزيد من المعلومات عن تشريعات "الأطفال أولًا" ووكالة الطفل والأسرة (توسلا) على الموقع الإلكتروني (<a href="https://www.tusla.ie" target="_blank">www.TUSLA.ie</a>).</p>',

            'declaration_description' => '<p>إن تقديم "معلومات خاطئة أو مُضللة" عن قصد إلى ديوان المظالم التابع للشرطة الآيرلندية يُعد جريمة، وقد يتم مُقاضاتك إذا تبين أنك قمت بذلك عامدًا، وقد يترتب على ذلك فرض غرامة أو عقوبة السجن أو كلتا العقوبتين.<br /><br />وخلاصة القول، يجب عليك تقديم معلومات صحيحة، وعدم تضليل ديوان المظالم التابع للشرطة الآيرلندية في أي شكوى أو تحقيق. إذا قدمت معلومات خاطئة أو مُضللة، فقد تتعرض لغرامة تصل إلى 2,500 يورو أو السجن لمدة تصل إلى 6 أشهر، أو كلتا العقوبتين.
<br /><br />يُرجى النقر على المربع أدناه تأكيدًا منك أنك قد قرأت ما ورد أعلاه وفهمته.</p>',
            'information_description' => '

<p>
            
قد يتم الكشف عن معلوماتك لأطراف أخرى مثل الشرطة الآيرلندية لتمكيننا من التحقيق الفعَّال في الشكاوى أو عندما نكون مُلزمين قانونًا بذلك. يهدف هذا الإجراء إلى ضمان التحقيق الفعَّال في شكواك.

<br /><br />

قد نشارك معلوماتك مع أطراف أخرى فيما يتعلق بأي دعوى جنائية أو مدنية متعلقة بك إذا كان ذلك ضروريًّا وبالقدر الذي يتناسب مع طبيعة هذه الدعوى. عندما يتعلق الأمر بملاحقة جنائية، فإن هذا الإجراء يشمل مشاركة المعلومات مع مدير النيابات العامة، والذي بدوره قد يشارك هذه المعلومات مع أطراف أخرى مثل الشهود أو غيرهم من الأشخاص الذين أشرت إليهم في شكواك. لمزيدٍ من المعلومات، يُرجى زيارة (<a href="https://www.gardaombudsman.ie/about-gsoc/non-party-disclosure/">عمليات الإفصاح من أطراف خارجية</a>).

<br /><br />


عندما يتعلق الأمر بدعوى مدنية متعلقة بك، فإن هذا الإجراء يشمل مشاركة المعلومات مع أطراف أخرى -من بينها المحاكم- وفقًا لما يقتضيه القانون.


<br /><br />


للمزيد من المعلومات، يُرجى النقر على هذه الروابط للاطلاع على <a href="https://www.gardaombudsman.ie/about-gsoc/data-protection-freedom-of-information/data-protection/">سياسة حماية البيانات</a> وسياسة <a href="https://www.gardaombudsman.ie/site-pages/website-terms/privacy-and-cookies/">الخصوصية وملفات تعريف الارتباط</a> أو التواصل عبر البريد الإلكتروني (<a href="mailto:data.protection@gsoc.ie">data.protection@gsoc.ie</a>).

</p>
            
            ',

        ],


    'sidebars' =>
        [
            'change_language_title' => 'تغيير اللغة',
            'proof_title' => 'لماذا نحتاج إلى هذه المعلومات؟',
            'proof_content' => 'إذا لم نحصل على دليل يثبت الموافقة أو أي توضيح نحتاج إليه، فلن نتمكن على الأرجح من التعامل مع الشكوى.',
            'help_title' => 'هل تحتاج إلى مساعدة؟',
            'help_content' => 'اتصل بنا على 800 600 0818 إذا كنت في حاجة إلى مساعدة لملء نموذج الشكوى. يمكنك التواصل مع الأخصائيين الاجتماعيين عبر الهاتف صباحًا خلال أيام العمل. وفي الأوقات الأخرى، يمكنك ترك رسالة تطلب فيها مُعاودة الاتصال بك أو تحديد موعد مع الأخصائي الاجتماعي.',
            'complainant_contact_title' => ' فلتبقَ على اتصال معنا',
            'complainant_contact_content' => 'سنحتاج إلى التواصل معك خلال نظرنا في شكواك. ولذلك، يُرجى إخبارنا عن طريق البريد الإلكتروني (<a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>) إذا طرأ أي تغيير في بيانات الاتصال الخاصة بك.',
            'representative_contact_title' => 'لماذا نحتاج إلى هذه المعلومات؟',
            'representative_contact_content' => 'سنحتاج إلى التواصل معك خلال نظرنا في شكواك. ولذلك، يُرجى إخبارنا عن طريق البريد الإلكتروني (<a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>) إذا طرأ أي تغيير في بيانات الاتصال الخاصة بك.'

        ],


    'fields' =>
        [
            'complainant_legend' => 'البيانات الشخصية للمُشتكي',
            'complainant_name' => 'الاس',
            'complainant_forename' => 'الاسم الأول ',
            'complainant_surname' => 'اللقب',
            'complainant_primary_address_street' => 'العنوان - السطر الأول',
            'complainant_primary_address_street2' => 'العنوان - السطر الثاني',
            'complainant_primary_address_street3' => 'العنوان - السطر الثالث (اختياري)',
            'complainant_primary_address_town' => 'البلدة أو المدينة',
            'complainant_primary_address_county' => 'المقاطعة',
            'complainant_primary_address_postcode' => 'Eircode',
            'complainant_primary_address_country' => 'دولة',
            'complainant_date_of_birth' => 'تاريخ الميلاد (يوم/ شهر/ سنة)',
            'complainant_primary_address_mobile' => 'رقم الهاتف المحمول',
            'complainant_primary_address_phone' => 'رقم هاتف آخر (المنزل أو العمل) (اختياري)',
            'complainant_primary_address_email' => 'عنوان البريد الإلكتروني',
            'complainant_primary_address_email_confirm' => 'تأكيد عنوان البريد الإلكتروني',

            'representative_legend' => 'البيانات الشخصية للممثل',
            'representative_required_legend' => ' هل تحتاج إلى ممثل؟',
            'has_consent_form_legend' => 'هل وقَّع المُشتكي على <a href="#" class="consent-form-link" target="_blank">نموذج الموافقة</a>؟',
            'third_party_representative_legend' => 'ما علاقتك بالمُشتكي؟',
            'third_party_representative_type_guardian' => 'وصيٌّ قانوني عليه',
            'third_party_representative_type_legal_representative' => 'ممثل قانوني',
            'third_party_representative_type_parent' => 'ولي أمره',
            'third_party_representative_type_social_worker' => 'أخصائي اجتماعي',
            'third_party_representative_type_other' => 'غير ذلك',
            'third_party_representative_description_other' => 'إذا كانت الإجابة \'غير ذلك\' يُرجى التوضيح ',
            'third_party_representative_name' => 'الاسم',
            'third_party_representative_forename' => 'الاسم الأول',
            'third_party_representative_surname' => 'اللقب',
            'third_party_representative_date_of_birth' => 'تاريخ الميلاد (يوم/ شهر/ سنة)',
            'third_party_representative_mobile' => 'رقم الهاتف المحمول',
            'third_party_representative_email' => 'عنوان البريد الإلكتروني',
            'third_party_representative_email_confirm' => 'تأكيد عنوان البريد الإلكتروني',

            'correspondence_legend' => 'إذا كنت ترغب في أن يتواصل معك ديوان المظالم التابع للشرطة الآيرلندية على عنوان آخر غير ذلك الخاص بالمُشتكي، يُرجى النقر على المربع المعنيِّ.',
            'correspondence_type_1' => 'نفس عنوان المُشتكي',
            'correspondence_type_2' => 'عنوان مختلف',

            'complainant_secondary_address_legend' => 'العنوان',

            'complainant_secondary_address_street' => 'العنوان - السطر الأول',
            'complainant_secondary_address_town' => 'البلدة أو المدينة',
            'complainant_secondary_address_county' => 'المقاطعة',
            'complainant_secondary_address_postcode' => 'Eircode',
            'complainant_secondary_address_country' => 'دولة',
            'complainant_secondary_address_mobile' => 'رقم الهاتف المحمول',
            'complainant_secondary_address_phone' => 'رقم الهاتف المحمول (المنزل أو العمل) (اختياري)',
            'complainant_secondary_address_email' => 'عنوان البريد الإلكتروني',
            'complainant_secondary_address_email_confirm' => 'تأكيد عنوان البريد الإلكتروني',

            'complaint_legend' => 'الشكوى',

            'complaint_late_submission_legend' => 'تفاصيل تأخر التقديم',

            'complaint_date' => 'تاريخ الحادث (يوم/ شهر/ سنة)',
            'complaint_time_reason' => 'إن حدثت الواقعة قبل أكثر من 12 شهرًا، من فضلك أخبرنا لماذا تأخر الإبلاغ عنها.',
            'complaint_time' => 'وقت الحادث',
            'complaint_location' => 'مكان الحادث',
            'complaint_garda_members' => 'هل لديك أي معلومات قد تساعد في تحديد هُوية الشرطي الذي ترغب في تقديم الشكوى ضده؟',
            'complaint_details' => 'يُرجى توضيح سوء السلوك الذي تزعم أن الشرطي قد ارتكبه.',
            'complaint_lead_up' => 'ماذا حدث قبل الحادث؟',
            'complaint_motive' => 'لماذا تعتقد أن الشرطي تصرف بهذه الطريقة؟',

            'complaint_witnesses_legend' => 'هل هناك شهود على الحادث؟',
            'complaint_witnesses' => 'إذا كانت الإجابة \'نعم\': يُرجى ذكر الأسماء الكاملة وبيانات الاتصال الخاصة بجميع الشهود هنا.',
            'complaint_evidence_legend' => 'هل توجد أي أدلة أخرى؟',
            'complaint_evidence' => 'إذا كانت الإجابة \'نعم\': فما الدليل؟',
            'complaint_injuries_legend' => 'هل أُصبتَ بأي أذى أو احتجت إلى علاج طبي بعد الحادث؟',
            'complaint_injuries' => 'إذا كانت الإجابة \'نعم\': يُرجى توضيح الإصابات التي عانيت منها أو العلاج الطبي الذي تلقيته بعد الحادث.',

            'child_protection_legend' => 'التزامات ديوان المظالم التابع للشرطة الآيرلندية تجاه حماية الأطفال ورعايتهم',
            'child_protection' => 'أقر بأنني أفهم التزامات ديوان المظالم التابع للشرطة الآيرلندية تجاه حماية الأطفال ورعايتهم',

            'declaration_legend' => 'البند (110) من قانون الشرطة الآيرلندية لعام 2005',
            'declaration' => 'أقر بأنني أفهم البند (110) من قانون الشرطة الآيرلندية لعام 2005.',

            'information_legend' => 'استخدام المعلومات',
            'information' => 'أُقرُّ بموافقتي على الأسس والقواعد التي يتبعها ديوان المظالم التابع للشرطة الآيرلندية في استخدام معلوماتي وقواعد مشاركته لهذه المعلومات مع الأطراف الأخرى.',

            'submit' => 'إرسال',

        ],

    'additional' =>
        [
            'behalf_note' => 'يجب عليك استكمال جميع المعلومات المطلوبة في هذا النموذج، وإلا فقد يتأخر النظر في شكواك.',
            'date_of_birth' => 'تاريخ الميلاد',
            'other_phone' => 'رقم هاتف آخر',
            'date_of_incident' => 'تاريخ الحادث',
            'child_protection_description_simplified' => 'رعاية الأطفال ورفاهيتهم من الأمور المُهمة. إذا كان لدى ديوان المظالم التابع للشرطة الآيرلندية أي مخاوف تتعلق بحماية الأطفال أو رعايتهم، فإنه مُلزم بمشاركة هذه المعلومات مع وكالة الطفل والأسرة (توسلا). وبقدر الإمكان، سنناقش هذا الأمر معك أولًا.',

            'declaration_description_simplified' => 'إن تقديم "معلومات خاطئة أو مُضللة" عن قصد إلى ديوان المظالم التابع للشرطة الآيرلندية يُعد جريمة، وقد يتم مُقاضاتك إذا تبين أنك قمت بذلك عامدًا، وقد يترتب على ذلك فرض غرامة أو عقوبة السجن أو كلتا العقوبتين.<br /><br />وخلاصة القول، يجب عليك تقديم معلومات صحيحة، وعدم تضليل ديوان المظالم التابع للشرطة الآيرلندية في أي شكوى أو تحقيق. إذا قدمت معلومات خاطئة أو مُضللة، فقد تتعرض لغرامة تصل إلى 2,500 يورو أو السجن لمدة تصل إلى 6 أشهر، أو كلتا العقوبتين.',
            'information_description_simplified' => 'قد يتم الكشف عن معلوماتك لأطراف أخرى مثل الشرطة الآيرلندية لتمكيننا من التحقيق الفعَّال في الشكاوى أو عندما نكون مُلزمين قانونًا بذلك. يهدف هذا الإجراء إلى ضمان التحقيق الفعَّال في شكواك.
<br /><br />
قد نشارك معلوماتك مع أطراف أخرى فيما يتعلق بأي دعوى جنائية أو مدنية متعلقة بك إذا كان ذلك ضروريًّا وبالقدر الذي يتناسب مع طبيعة هذه الدعوى. عندما يتعلق الأمر بملاحقة جنائية، فإن هذا الإجراء يشمل مشاركة المعلومات مع مدير النيابات العامة، والذي بدوره قد يشارك هذه المعلومات مع أطراف أخرى مثل الشهود أو غيرهم من الأشخاص الذين أشرت إليهم في شكواك.
<br /><br />
عندما يتعلق الأمر بدعوى مدنية متعلقة بك، فإن هذا الإجراء يشمل مشاركة المعلومات مع أطراف أخرى -من بينها المحاكم- وفقًا لما يقتضيه القانون.'
        ]


];