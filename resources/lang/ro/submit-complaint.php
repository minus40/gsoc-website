<?php

return
    [

        'page' =>
            [
                'title' => 'Cum puteți face o reclamație',
                'error_msg' => 'Vă rugăm să verificați erorile de mai jos marcate cu roșu înainte de a trimite formularul.',
                'yes' => 'Da',
                'no' => 'Nu',
                'max_200_words' => 'Maximum 200 de cuvinte.'
            ],

        'sections' =>
            [

                'complainant_introduction' =>
                    '
            <div>
                <p>Dacă reclamația este a dvs., atunci sunteți <b>”reclamantul”</b>.</p>
               
                <p>Trebuie să completați toate informațiile de pe acest formular, în caz contrar procesarea reclamației poate dura o perioadă mai lungă.</p>

                <p><b>Înainte de a începe:</b></p>
                <ul class="vertical-icons">
                    <li><a href="/make-a-complaint/before-you-complain/">Asigurați-vă că GSOC este organizația porivită pentru a se ocupa cu reclamația dvs.</a></li>
                    <li>Dacă doriți, puteți citi și politicile noastre cu privire la <a href="/site-pages/website-terms/privacy-and-cookies/">Confidențialitate</a> și <a href="/about-gsoc/data-protection-freedom-of-information/data-protection/">Protecția Datelor</a>.</li>
                </ul>
            </div>
        ',

                'representative_introduction' =>
                    '
            <div class="explainer">
                <h3>Un reprezentant</h3>

                <p>În cazul în care faceți această reclamație în numele altei persoane sau al unei companii, va trebui să furnizați consimțământul persoanei sau al directorului companiei pe care o reprezentați.</p>
                <ul class="vertical-icons">
                    <li>Puteți găsi aici un <a href="#" class="consent-form-link" target="_blank">formular de consimțământ</a>.</li>
                </ul>

                <h3>Sub 18 ani</h3>

                <p>Dacă aveți mai puțin de 18 ani, puteți fie să faceți reclamația personal sau altcineva o poate face în numele dvs. </p>

                <p>Dacă cereți altcuiva să facă această reclamație pt dvs., va trebui să le oferiți consimțământul dvs. de a face această reclamație.</p>

                <ul class="vertical-icons">
                    <li>Puteți găsi aici un <a href="#" class="consent-form-link" target="_blank">formular de consimțământ</a>.</li>
                </ul>

                <p><i>Reprezentantul trebuie să completeze toate datele pe acest formular. Dacă nu completați toate detaliile necesare, acest lucru poate întârzia procesul reclamației sau poate duce la a nu se lua nicio acțiune din partea GSOC.</i></p>
            </div>
        ',



                'complaint_garda_members_description' =>
                    '
            <div class="explainer mb-5">
                <p>De exemplu:</p>
                <ul class="vertical-icons">
                    <li>Nume</li>
                    <li>Numărul de pe umăr</li>
                    <li>Stația de poliție</li>
                    <li>Mașina</li>
                    <li>Numărul de înregistrare al mașinii</li>
                <ul>
            </div>
        ',
                'complaint_description' => '
            <div class="explainer mb-5">
                <p>De exemplu, ce a:</p>
                <ul class="vertical-icons">
                    <li>Spus?</li>
                    <li>Făcut?</li>
                    <li>Nu a făcut?</li>
                </ul>
                <p>Oferiți o scurtă descriere a incidentului.</p>
            </div>
        ',
                'evidence_description' =>
                    '
            <div class="explainer mb-5">
                <p>De exemplu:</p>
                <ul class="vertical-icons">
                    <li>Înregistrare CCTV sau alt fel de înregistrare</li>
                    <li>Documente</li>
                </ul>
            </div>
        ',
                'injuries_description' =>
                    '
            <div class="explainer mb-5">
                <p>Vă rugăm să nu trimiteți niciun raport medical sau poze ale rănilor în acest formular. Dacă este nevoie, vă vom cere acestea.</p>
            </div>
        ',


                'child_protection_description' => '<p>Bunăstarea unui copil este deosebit de importantă. În cazul în care Garda Ombudsman are îngrijorări cu privire la protecția sau bunăstarea unui copil, are obligația de a împărtăși aceste îngrijorări cu TUSLA, Agenția pentru Copil și Familie. Pe cât posibil, vom discuta acest lucru cu dvs. întâi. Mai multe informații despre Legislația Copilul are Prioritate, și TUSLA sunt disponibile la <a href="https://www.tusla.ie" target="_blank">www.TUSLA.ie</a>.</p>',

                'declaration_description' => '<p>Este o infracțiune să furnizați cu bună știință ”informații false sau înșelătoare” către GSOC. Puteți fi pus/ă sub urmărire penală dacă se pare că ați făcut acest lucru. Aceasta poate duce la primirea unei amenzi și/sau pedeapsă cu închisoarea.
<br /><br />Pe scurt, informațiile pe care le oferiți trebuie să fie verdice și să nu urmăriți să induceți în eroare GSOC cu nicio reclamație sau investigație. Dacă oferiți informații false sau înșelătoare, este posibil să fie nevoie să plătiți o amendă de până la 2.500 euro sau să fiți închis/ă până la 6 luni, sau ambele.
<br /><br />Vă rugăm să bifați căsuța pentru a arăta că ați citit și înțeles cele de mai sus.</p>',

                'information_description' => '<p>Informațiile dvs. pot fi dezvăluite unei terțe părți, cum ar fi Garda Síochána, pentru a ne ajuta să investigăm în mod eficient reclamațiile, sau în situația în care suntem obligați prin lege să o facem. Acest lucru este pentru a asigura o investigație eficientă a reclamației dvs. 
<br /><br />Acolo unde este necesar sau proportional să facem acest lucru, putem împărtăși informații cu alte părți în orice procedură penală sau civilă. În cazul unei urmăriri penale, aceasta include împărtășirea de informații cu Directorul Procuraturii Publice, care poate apoi împărtăși această informație cu alte părți, cum ar fi un martor sau alte persoane identificate din reclamația dvs. Pentru alte informații, accesați <a href="https://www.gardaombudsman.ie/about-gsoc/non-party-disclosure/">Non-Party Disclosure</a>.
<br /><br />Într-o procedură civilă conexă, aceasta include împărtășirea de informații cu alte părți, deoarece poate fi necesar prin lege, inclusive  de către o instanță de judecată.
<br /><br />Pentru mai multe informații, vă rugăm să accesați aceste link-uri către <a href="https://www.gardaombudsman.ie/about-gsoc/data-protection-freedom-of-information/data-protection/">data protection policy</a> (politica cu privire la protecția datelor) și <a href="https://www.gardaombudsman.ie/site-pages/website-terms/privacy-and-cookies/">privacy and cookie policy</a> (politica cu privire la confidențialitate și cookie-uri) , sau sa ne trimiteți un email la <a href="mailto:data.protection@gsoc.ie">data.protection@gsoc.ie</a></p>',
            ],


        'sidebars' =>
            [
                'change_language_title' => 'Schimbă limba',
                'proof_title' => 'De ce avem nevoie de aceste informații?',
                'proof_content' => 'Dacă nu primim dovada consimțământului sau o explicație, probabil că nu vom putea să procesăm reclamația.',
                'help_title' => 'Aveți nevoie de ajutor?',
                'help_content' => 'Sunați-ne la 0818 600 800 dacă aveți nevoie de ajutor la completarea formularului de reclamație. Asistenții noștri de caz sunt disponibili la telefon în dimineața zilelor lucrătoare. Alteori, puteți lăsa mesaj pentru a solicita să fiți sunat/ă înapoi, sau o întâlnire cu un asistent de caz.',
                'complainant_contact_title' => 'Haideți să păstrăm legătura',
                'complainant_contact_content' => 'Va trebui să vă putem contacta în timp ce procesăm reclamația dvs. Așa că vă rugăm să ne anunțați orice schimbare de date de contact printr-un email trimis la <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>.',
                'representative_contact_title' => 'De ce avem nevoie de aceste informații?',
                'representative_contact_content' => 'Va trebui să vă putem contacta în timp ce procesăm reclamația dvs. Așa că vă rugăm să ne anunțați orice schimbare de date de contact printr-un email trimis la <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>.',

            ],


        'fields' =>
            [
                'complainant_legend' => 'Detaliile personale ale reclamantului',
                'complainant_name' => 'Nume',
                'complainant_forename' => 'Prenume',
                'complainant_surname' => 'Nume de familie',
                'complainant_primary_address_street' => 'Adresă',
                'complainant_primary_address_street2' => 'Adresă continuare',
                'complainant_primary_address_street3' => 'Adresă continuare (optional)',
                'complainant_primary_address_town' => 'Oraș',
                'complainant_primary_address_county' => 'District',
                'complainant_primary_address_postcode' => 'Cod EIR',
                'complainant_primary_address_country' => 'Țară',
                'complainant_date_of_birth' => 'Data nașterii (zi/lună/an)',
                'complainant_primary_address_mobile' => 'Nr. de telefon mobil',
                'complainant_primary_address_phone' => 'Alt nr. de telefon (de acasă sau de la serviciu) (optional)',
                'complainant_primary_address_email' => 'Adresă de email',
                'complainant_primary_address_email_confirm' => 'Confirmați adresa de email',

                'representative_legend' => 'Detaliile personale ale reprezentantului',
                'representative_required_legend' => 'Aveți nevoie de un representant?',
                'has_consent_form_legend' => '<a href="#" class="consent-form-link" target="_blank">Formularul de consimțământ</a> a fost semnat de către reclamant?',
                'third_party_representative_legend' => 'Care este relația dvs. cu reclamantul?',
                'third_party_representative_type_guardian' => 'Tutore',
                'third_party_representative_type_legal_representative' => 'Reprezentant legal',
                'third_party_representative_type_parent' => 'Părinte',
                'third_party_representative_type_social_worker' => 'Lucrător social',
                'third_party_representative_type_other' => 'Altul',
                'third_party_representative_description_other' => 'Vă rugăm să descrieți ”altul”',
                'third_party_representative_name' => 'Nume',
                'third_party_representative_forename' => 'Prenume',
                'third_party_representative_surname' => 'Nume de familie',
                'third_party_representative_date_of_birth' => 'Data nașterii (zi/lună/an)',
                'third_party_representative_mobile' => 'Nr. de telefon mobil',
                'third_party_representative_email' => 'Adresă de email',
                'third_party_representative_email_confirm' => 'Confirmați adresa de email',

                'correspondence_legend' => 'Dacă doriți ca Garda Ombudsman să vă trimită corespondența la altă adresă decât cea furnizată pentru reclamant, vă rugăm să bifați căsuța potrivită.',
                'correspondence_type_1' => 'Aceeași ca a reclamantului',
                'correspondence_type_2' => 'Adresă diferită',

                'complainant_secondary_address_legend' => 'Adresă',

                'complainant_secondary_address_street' => 'Adresă',
                'complainant_secondary_address_town' => 'Oraș',
                'complainant_secondary_address_county' => 'District',
                'complainant_secondary_address_postcode' => 'Cod EIR',
                'complainant_secondary_address_country' => 'Țară',
                'complainant_secondary_address_mobile' => 'Nr. de telefon mobil',
                'complainant_secondary_address_phone' => 'Alt nr. de telefon (de acasă sau de la serviciu) (optional)',
                'complainant_secondary_address_email' => 'Adresă de email',
                'complainant_secondary_address_email_confirm' => 'Confirmați adresa de email',

                'complaint_legend' => 'Reclamația',

                'complaint_late_submission_legend' => 'Detalii trimiteri întârziate',

                'complaint_date' => 'Data incidentului (zi/lună/an)',
                'complaint_time_reason' => 'Dacă incidentul a avut loc în urmă cu mai mult de 12 luni, spuneți-ne de ce îl raportați cu întârziere.',
                'complaint_time' => 'Ora incidentului',
                'complaint_location' => 'Locul incidentului',
                'complaint_garda_members' => 'Aveți informații care ne-ar putea ajuta să identificăm ofițerul de poliție pentru care faceți reclamația?',
                'complaint_details' => 'Vă rugăm să descrieți comportamentul neadecvat pe care pretindeți că l-a comis agentul de poliție.',
                'complaint_lead_up' => 'Ce s-a întâmplat înainte de incident?',
                'complaint_motive' => 'De ce credeți că agentul de poliție a acționat în acest fel?',

                'complaint_witnesses_legend' => 'Există martori ai incidentului?',
                'complaint_witnesses' => 'Dacă ”Da”: Vă rugăm să scrieți aici numele complet și detaliile de contact ale martorilor.',
                'complaint_evidence_legend' => 'Există alte dovezi?',
                'complaint_evidence' => 'Dacă ”Da”:  Care sunt acestea?',
                'complaint_injuries_legend' => 'Ați fost rănit/ă sau ați avut nevoie de tratament medical în urma incidentului?',
                'complaint_injuries' => 'Dacă ”Da”:  Descrieți orice rănire sau tratament medical pe care l-ați primit în urma incidentului',

                'child_protection_legend' => 'Obligațiile cu privire la Protecția și Bunăstarea Copilului ale GSOC',
                'child_protection' => 'Înțeleg Obligațiile cu Privire la Protecția și Bunăstarea Copilului ale GSOC.',

                'declaration_legend' => 'Secțiunea 110 din Legea Garda Síochána, 2005',
                'declaration' => 'Am înțeles Secțiunea 110 din Legea Garda Síochána, 2005',

                'information_legend' => 'Folosirea informațiilor',
                'information' => 'Recunosc bazele prin care GSOC îmi poate folosi informațiile, și cum pot acestea fi împărtășite.',

                'submit' => 'TRIMITE',

            ],

        'additional' =>
            [
                'behalf_note' => 'În cazul în care faceți această reclamație în numele altei persoane sau al unei companii, va trebui să furnizați consimțământul persoanei sau al directorului companiei pe care o reprezentați.',
                'date_of_birth' => 'Data nașterii',
                'other_phone' => 'Alt nr. de telefon',
                'date_of_incident' => 'Data incidentului',
                'child_protection_description_simplified' => 'Bunăstarea unui copil este deosebit de importantă. În cazul în care Garda Ombudsman are îngrijorări cu privire la protecția sau bunăstarea unui copil, are obligația de a împărtăși aceste îngrijorări cu TUSLA, Agenția pentru Copil și Familie. Pe cât posibil, vom discuta acest lucru cu dvs. întâi.',

                'declaration_description_simplified' => 'Este o infracțiune să furnizați cu bună știință ”informații false sau înșelătoare” către GSOC. Puteți fi pus/ă sub urmărire penală dacă se pare că ați făcut acest lucru. Aceasta poate duce la primirea unei amenzi și/sau pedeapsă cu închisoarea.
<br /><br />Pe scurt, informațiile pe care le oferiți trebuie să fie verdice și să nu urmăriți să induceți în eroare GSOC cu nicio reclamație sau investigație. Dacă oferiți informații false sau înșelătoare, este posibil să fie nevoie să plătiți o amendă de până la 2.500 euro sau să fiți închis/ă până la 6 luni, sau ambele.',

                'information_description_simplified' => 'Informațiile dvs. pot fi dezvăluite unei terțe părți, cum ar fi Garda Síochána, pentru a ne ajuta să investigăm în mod eficient reclamațiile, sau în situația în care suntem obligați prin lege să o facem. Acest lucru este pentru a asigura o investigație eficientă a reclamației dvs. 
<br /><br />Acolo unde este necesar sau proportional să facem acest lucru, putem împărtăși informații cu alte părți în orice procedură penală sau civilă. În cazul unei urmăriri penale, aceasta include împărtășirea de informații cu Directorul Procuraturii Publice, care poate apoi împărtăși această informație cu alte părți, cum ar fi un martor sau alte persoane identificate din reclamația dvs.
<br /><br />Într-o procedură civilă conexă, aceasta include împărtășirea de informații cu alte părți, deoarece poate fi necesar prin lege, inclusive  de către o instanță de judecată.',
            ]


    ];