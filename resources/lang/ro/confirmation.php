<?php

return [
    'title' => 'Confirmare',
    'paragraph_1' => 'Vă mulțumim că ați trimis o reclamație către Garda Ombudsman.',
    'paragraph_2' => 'Odată ce ieșiți de pe website, nu veți mai putea accesa formularul trimis.',
    'cta_button' => 'Salvează PDF',
    'subtitle' => 'Următorii pași:',
    'list' => ' <ul>
                    <li>În săptămâna ce urmează, vă vom trimite o scrisoare prin care se vă vom comunica faptul că reclamația dvs. a fost înregistrată și că se va lua o decizie cu privire la aceasta, dacă poate fi admisă și procesată de către GSOC, conform criteriilor din secțiunea 87 a Legii  Garda Síochána 2005.</li>
                    <li>După ce se va lua această decizie, vă vom scrie să vă comunicăm decizia. Dacă vă putem admite reclamația, vă vom spune cum ne vom ocupa de aceasta. Citiți despre posibilele moduri în care aceasta poate fi tratată.</li>
                    <li>De obicei durează între 3 și 9 luni să finalizăm o reclamație. Cel mai sigur, veți fi contactat/ă în acest timp de către un ofițer GSOC sau de către ofițerul  Garda Síochána care se ocupă de cazul dvs., pentru a cere mai multe informații și pentru a vă oferi actualizări cu privire la procedurile urmate.</li>
                    <li>La final, vă vom scrie să pentru a vă comunica despre rezultate și ce măsuri se vor lua. Vă rugăm să ne comunicați printr-un email la <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a> dacă vi se schimbă datele de contact. </li>
                </ul>'
];