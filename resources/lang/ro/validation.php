<?php

return [

    'mismatch' => "Nu există reguli de validare pentru acest câmp.",
    'validate_required' => "Acest câmp trebuie completat.",
    'validate_required_if' => "Acest câmp trebuie completat.",
    'validate_valid_email' => "Acest câmp trebuie să fie o adresă de email validă.",
    'validate_min_len' => "Numărul minim de caractere pentru acest câmp este :param",
    'validate_max_len' => "Numărul maxim de caractere pentru acest câmp este :param",
    'validate_exact_len' => "Câmpul The :attribute field trebuie să aibă exact :param caractere",
    'validate_alpha' => "Câmpul The :attribute field poate conține doar caractere alfa (a-z)",
    'validate_alpha_space' => "Câmpul The :attribute field poate conține doar caractere alfa (a-z) și spații",
    'validate_alpha_numeric' => "Câmpul The :attribute field poate conține doar caractere alfanumerice",
    'validate_alpha_dash' => "Câmpul The :attribute field poate conține doar caractere alfanumerice și linii",
    'validate_numeric' => "Acest câmp poate conține doar caractere numerice",
    'validate_integer' => "Acest câmp poate conține doar o valoare numerică",
    'validate_boolean' => "Acest câmp poate conține doar o valoare adevărat sau fals",
    'validate_valid_url' => "Acest câmp trebuie să fie un URL acceptat",
    'validate_url_exists' => "URL-ul The :attribute field nu există",
    'validate_valid_ip' => "Câmpul The :attribute field trebuie să conțină o adresă IP valabilă",
    'validate_contains' => "Câmpul The :attribute field trebuie să conțină una dintre aceste valori: :param,",
    'validate_contains_list' => "Câmpul The :attribute field trebuie să conțină o valoare din lista verticală",
    'validate_doesnt_contain_list' => "Câmpul The :attribute field conține o valoare care nu este acceptată",
    'validate_street_address' => "Câmpul The :attribute field trebuie să fie o adresă poștală valabilă",
    'validate_date' => "Acest câmp trebuie să fie o dată validă (ZZ/LL/AAAA)",
    'custom_date' => "Acest câmp trebuie să fie o dată validă (ZZ/LL/AAAA)",
    'validate_min_numeric' => "Câmpul The :attribute field trebuie să fie o valoare numerică egală cu sau mai mare de :param",
    'validate_max_numeric' => "Câmpul The :attribute field trebuie să fie o valoare numerică egală cu sau mai mică de :param",
    'validate_min_age' => "Acest câmp trebuie să conțină o vârsta mai mare sau egală cu :param",
    'validate_phone_number' => "Câmpul The :attribute field trebuie să fie un număr de telefon valabil",
    'validate_equalsfield' => "Adresele de email nu se potrivesc",
    'validate_contact' => "Nu ne-ați dat un număr de telefon sau adresă de email, făcând dificilă contactarea dvs. de către noi pentru a ne ocupa în mod eficient de reclamația dvs. Vă rugăm să vă întoarceți și să le introduceți dacă doriți să vă contactăm."

];