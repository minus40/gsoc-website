<?php

return [
    'title' => 'Dearbhú',
    'paragraph_1' => 'Go raibh maith agat as do ghearán a chur faoi bhráid Ombudsman an Gharda Síochána.',
    'paragraph_2' => 'Ní bheidh tú in ann rochtain a fháil arís ar an bhfoirm a chuir tú isteach nuair a fhágann tú an suíomh gréasáin seo.',
    'cta_button' => 'Sábháil go pdf',
    'subtitle' => 'Na Chéad Chéimeanna Eile',
    'list' => ' <ul>
                    <li>Laistigh den tseachtain seo chugainn, seolfaimid litir chugat ag admháil go bhfuil do ghearán logáilte agus go bhfuil cinneadh le déanamh an féidir le Coimisiún Ombudsman an Gharda Síochána é a glacadh agus déileáil leis, de réir na gcritéar in alt 87 d’Acht an Gharda Síochána 2005.</li>
                    <li>Nuair a dhéanfar an cinneadh seo, scríobhfaimid chun an cinneadh a chur in iúl duit. Más féidir linn do ghearán a glacadh, inseoimid duit conas a ndéileálfar leis. Léigh faoi na bealaí féideartha ina bhféadfaí déileáil leis.</li>
                    <li>De ghnáth tógann sé idir 3-9 mí chun gearán a phróiseáil. Is dócha go rachaidh oifigeach Choimisiún Ombudsman an Gharda Síochána nó an t-oifigeach Garda Síochána atá ag déileáil le do chás i dteagmháil leat le linn an tréimhse seo, chun tuilleadh faisnéise a iarraidh agus chun nuashonruithe a thabhairt duit ar aon dul chun cinn.</li>
                    <li>Ag an deireadh, scríobhfaimid agus cuirfear ar an eolas thú maidir le na torthaí agus aon ghníomh eile a bheidh le déanamh. Má tá athrú ar do shonraí teagmhála cuir é sin in iúl dúinn le do thoil, trí ríomhphost a sheoladh chuig <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>,</li>
                </ul>'
];