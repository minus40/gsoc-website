<?php

return [

    'mismatch' => "Níl aon riail bailíochtaithe ann don réimse seo.",
    'validate_required' => "Tá an réimse seo ag teastáil",
    'validate_required_if' => "Tá an réimse seo ag teastáil",
    'validate_valid_email' => "Ní mór don réimse seo a bheith ina sheoladh ríomhphoist bailí",
    'validate_min_len' => "Is é seo an líon íosta carachtar don réimse seo:param",
    'validate_max_len' => "Is é seo an líon uasta carachtar don réimse seo:param",
    'validate_exact_len' => "Ní mór an réimse :attribute a bheith :param carachtar go díreach ar fad",
    'validate_alpha' => "Ní fhéadfaidh ach carachtair alfa (a-z) bheith sa réimse :attribute",
    'validate_alpha_space' => "Ní fhéadfaidh ach carachtair alfa (a-z) agus spásanna bheith sa réimse :attribute",
    'validate_alpha_numeric' => "Ní fhéadfaidh ach carachtair alfa-uimhriúla bheith sa réimse :attribute",
    'validate_alpha_dash' => "Ní fhéadfaidh ach carachtair alfa agus fleasca bheith sa réimse :attribute",
    'validate_numeric' => "Ní fhéadfaidh ach carachtair uimhriúla a bheith sa réimse seo",
    'validate_integer' => "Ní fhéadfaidh ach luach uimhriúil a bheith sa réimse seo",
    'validate_boolean' => "Ní fhéadfaidh ach fíorluach nó luach bréagach a bheith sa réimse seo",
    'validate_valid_url' => "Ní mór don réimse seo a bheith ina URL bailí",
    'validate_url_exists' => "Ní ann don URL :attribute",
    'validate_valid_ip' => "Ní mór seoladh IP bailí a bheith ann sa réimse :attribute",
    'validate_contains' => "Ní mór ceann amháin de na luachanna seo a bheith ann sa réimse :attribute: :params,",
    'validate_contains_list' => "Ní mór luach óna liosta anuas a bheith ann sa réimse :attribute",
    'validate_doesnt_contain_list' => "Tá luach nach nglactar leis ann sa réimse :attribute",
    'validate_street_address' => "Ní mór seoladh sráide bailí a bheith sa réimse :attribute",
    'validate_date' => "Ní mór dáta bailí a bheith sa réimse seo (LL/MM/BBBB)",
    'custom_date' => "Ní mór dáta bailí a bheith sa réimse seo (LL/MM/BBBB)",
    'validate_min_numeric' => "Ní mór luach uimhriúil atá níos mó ná nó cothrom le :param a bheith sa réimse :attribute",
    'validate_max_numeric' => "Ní mór luach uimhriúil atá níos ísle ná nó cothrom le :param a bheith sa réimse :attribute",
    'validate_min_age' => "Caithfidh aois níos mó ná nó cothrom le :param a bheith sa réimse seo",
    'validate_phone_number' => "Ní mór uimhir theileafóin bhailí a bheith sa réimse :attribute",
    'validate_equalsfield' => "Ní hionann na seoltaí ríomhphoist",
    'validate_contact' => "Níor thug tú uimhir theileafóin ná seoladh ríomhphoist dúinn, rud a cíallaíonn go mbeidh sé deacair teagmháil a dhéanamh leat chun déileáil go héifeachtach le do ghearán. Téigh ar ais agus cuir uimhir theileafóin isteach anois, más mian leat go mbeimid in ann teagmháil a dhéanamh leat."

];