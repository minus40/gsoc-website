<?php

return
    [

        'page' =>
            [
                'title' => 'Conas gearán a dhéanamh',
                'error_msg' => 'Seiceáil le do thoil na hearráidí arna marcáil i ndearg thíos, roimh duit an fhoirm a chur isteach.',
                'yes' => 'Tá',
                'no' => 'Níl',
                'max_200_words' => '200 focal ar a mhéad.'
            ],

        'sections' =>
            [

                'complainant_introduction' =>
                    '
            <div>
                <p>Má tá an gearán seo á dhéanamh agat, is tusa an . <b>\'gearánach\'</b>.</p>
               
                <p>Ní mór duit an t-eolas ar fad ar an bhfoirm seo a líonadh isteach nó d’fhéadfadh sé go gcuirfí moill ar phróiseáil do ghearáin.</p>

                <p><b>Sula dtosaíonn tú:</b></p>
                <ul class="vertical-icons">
                    <li><a href="/make-a-complaint/before-you-complain/">Faigh amach an é GSOC an eagraíocht cheart chun déileáil le do ghearán.</a></li>
                    <li>B’fhéidir gur mhaith leat breathnú freisin ar ár bpolasaí <a href="/site-pages/website-terms/privacy-and-cookies/">Príobháideachta</a> agus <a href="/about-gsoc/data-protection-freedom-of-information/data-protection/">Cosanta Sonraí</a>.</li>
                </ul>
            </div>
        ',

                'representative_introduction' =>
                    '
            <div class="explainer">
                <h3>Ionadaí</h3>

                <p>Má tá an gearán seo á dhéanamh agat thar ceann duine eile nó cuideachta, beidh ort toiliú a thabhairt ón duine nó ó stiúrthóir na cuideachta a bhfuil tú ag déanamh ionadaíochta uirthi.</p>
                <ul class="vertical-icons">
                    <li>Tá <a href="#" class="consent-form-link" target="_blank">foirm toilithe</a> ar fáil anseo.</li>
                </ul>

                <h3>Faoi bhun 18 mbliana d’aois</h3>

                <p>Má tá tú faoi bhun 18 mbliana d’aois, is féidir leat gearán a dhéanamh tú féin nó is féidir le duine eile an gearán seo a dhéanamh duit.</p>

                <p>Má iarrann tú ar dhuine eile an gearán seo a dhéanamh duit, beidh ort toiliú a thabhairt dóibh do ghearán a dhéanamh.</p>

                <ul class="vertical-icons">
                    <li>Tá <a href="#" class="consent-form-link" target="_blank">foirm toilithe</a> ar fáil anseo.</li>
                </ul>

                <p><i>Ní mór don ionadaí an fhaisnéis ar fad ar an bhfoirm seo a líonadh isteach. Mura líonann tú isteach an t-eolas go léir a theastaíonn uainn, d’fhéadfadh sé moill a chur ar phróiseáil do ghearáin nó ní dhéanfaidh GSOC aon ghníomh eile dá bharr.</i></p>
            </div>
        ',



                'complaint_garda_members_description' =>
                    '
            <div class="explainer mb-5">
                <p>Mar shampla:</p>
                <ul class="vertical-icons">
                    <li>Ainm</li>
                    <li>Uimhir ghualainn</li>
                    <li>Stáisiún</li>
                    <li>Carr</li>
                    <li>Uimhir chlárúcháin an chairr</li>
                <ul>
            </div>
        ',
                'complaint_description' => '
            <div class="explainer mb-5">
                <p>Mar shampla, cad a rinne siad:</p>
                <ul class="vertical-icons">
                    <li>Cad a bhí le rá acu?</li>
                    <li>Cad a bhí déanta acu?</li>
                    <li>Cad nach raibh déanta acu?</li>
                </ul>
                <p>Ba chóir go mbeadh sé seo cuntas gairid ar an eachtra.</p>
            </div>
        ',
                'evidence_description' =>
                    '
            <div class="explainer mb-5">
                <p>Mar shampla:</p>
                <ul class="vertical-icons">
                    <li>TCI nó scannáin físe eile</li>
                    <li>doiciméid</li>
                </ul>
            </div>
        ',
                'injuries_description' =>
                    '
            <div class="explainer mb-5">
                <p>Le do thoil, ná cuir isteach aon tuairiscí leighis ná fótagraif den ghortú in éineacht leis an bhfoirm seo.  Má bhíonn siad seo ag teastáil, lorgóimid uait iad.</p>
            </div>
        ',


                'child_protection_description' => '<p>Tá leas an linbh fíorthábhachtach. Má tá aon imní ar Ombudsman an Gharda Síochána maidir le cosaint nó leas linbh, tá dualgas air an t-eolas seo a roinnt le TUSLA, an Ghníomhaireacht um Leanaí agus an Teaghlach. A mhéid is féidir, pléifimid é seo leat ar dtús. Tá tuilleadh eolais ar Reachtaíocht Tús Áite do Leanaí agus ar TUSLA ar fáil ag <a href="https://www.tusla.ie" target="_blank">www.TUSLA.ie</a>.</p>',

                'declaration_description' => '<p>Is cion coiriúil é “faisnéis bhréagach nó mhíthreorach” a sholáthar go feasach do GSOC. D’fhéadfaí tú a ionchúiseamh má dhealraíonn sé go bhfuil sé seo déanta agat. D’fhéadfadh fíneáil agus/nó pianbhreith phríosúin a bheith mar thoradh air sin.
<br /><br />Go hachomair, ní mór don fhaisnéis a thugann tú a bheith fírinneach agus gan féachaint le GSOC a chur amú in aon ghearán nó imscrúdú. Má thugann tú faisnéis bhréagach nó mhíthreorach, féadfaidh sé go mbeidh ort fíneáil suas le EUR 2,500 a íoc nó a bheith i bpríosún ar feadh suas le 6 mhí, nó iad araon.
<br /><br />Cuir tic sa bhosca thíos chun a thaispeáint go bhfuil an méid thuas léite agat agus é a thuiscint.</p>',

                'information_description' => '<p>Is féidir do chuid faisnéise a nochtadh do thríú páirtithe ar nós an Gharda Síochána chun cur ar ár gcumas gearáin a imscrúdú go héifeachtach nó sa chás go bhfuil dualgas dlíthiúil orainn é sin a dhéanamh. Déantar é seo chun imscrúdú éifeachtach ar do ghearán a chinntiú. 
<br /><br />I gcás ina bhfuil sé riachtanach agus comhréireach é sin a dhéanamh, féadfaimid faisnéis a roinnt le páirtithe eile in aon imeachtaí coiriúla nó sibhialta gaolmhara. Maidir le hionchúiseamh coiriúil, áirítear leis seo faisnéis a roinnt leis an Stiúrthóir Ionchúiseamh Poiblí agus féadfaidh an Stiúrthóir an fhaisnéis sin a roinnt ansin le páirtithe eile cosúil le finné nó daoine eile a aithnítear i do ghearán. Chun tuilleadh eolais a fháil, tabhair cuairt ar <a href="https://www.gardaombudsman.ie/about-gsoc/non-party-disclosure/">Nochtadh Neamh-Pháirtí</a>.
<br /><br />In imeacht sibhialta gaolmhar, áirítear leis sin faisnéis a roinnt le páirtithe eile de réir mar is gá de réir an dlí, lena n-áirítear Cúirt Dlí.
<br /><br />Chun tuilleadh faisnéise a fháil, lean na naisc seo chuig ár <a href="https://www.gardaombudsman.ie/about-gsoc/data-protection-freedom-of-information/data-protection/">mbeartas cosanta sonraí</a> agus ár <a href="https://www.gardaombudsman.ie/about-gsoc/data-protection-freedom-of-information/data-protection/">mbeartas príobháideachais agus fianán</a> nó seol ríomhphost chuig <a href="mailto:data.protection@gsoc.ie">data.protection@gsoc.ie</a>.</p>',

            ],


        'sidebars' =>
            [
                'change_language_title' => 'Athraigh an teanga',
                'proof_title' => 'Cén fáth go dteastaíonn an fhaisnéis seo uainn?',
                'proof_content' => 'Mura bhfaighimid cruthúnas ar thoiliú nó míniú, is dócha nach mbeimid in ann déileáil leis an ngearán.',
                'help_title' => 'An bhfuil cabhair uait?',
                'help_content' => 'Cuir glaoch orainn ar 0818 600 800 má tá cabhair uait chun an fhoirm ghearáin a chomhlánú. Tá oibrithe cáis ar fáil ar an bhfón maidin na seachtaine. Ag amanna eile, is féidir leat teachtaireacht a fhágáil ag iarraidh glaoch ar ais, nó coinne a dhéanamh le cás-oibrí.',
                'complainant_contact_title' => 'Lig dúinn a bheith i dteagmháil',
                'complainant_contact_content' => 'Ní mór dúinn a bheith in ann teagmháil a dhéanamh leat agus muid ag déileáil le do ghearán. Mar sin, cuir in iúl dúinn, ag R-pHosting <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>, má athraíonn do shonraí teagmhála.',
                'representative_contact_title' => 'Cén fáth go dteastaíonn an fhaisnéis seo uainn?',
                'representative_contact_content' => 'Ní mór dúinn a bheith in ann teagmháil a dhéanamh leat agus muid ag déileáil le do ghearán. Mar sin, cuir in iúl dúinn, ag R-pHosting <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>, má athraíonn do shonraí teagmhála.',

            ],


        'fields' =>
            [
                'complainant_legend' => 'Sonraí pearsanta an ghearánaigh',
                'complainant_name' => 'Ainm',
                'complainant_forename' => 'Céadainm',
                'complainant_surname' => 'Sloinne',
                'complainant_primary_address_street' => 'Seoladh Líne 1',
                'complainant_primary_address_street2' => 'Seoladh Líne 2',
                'complainant_primary_address_street3' => 'Seoladh Líne 3 (Roghnach)',
                'complainant_primary_address_town' => 'Baile nó Cathair',
                'complainant_primary_address_county' => 'Contae',
                'complainant_primary_address_postcode' => 'Éircód',
                'complainant_primary_address_country' => 'Tír',
                'complainant_date_of_birth' => 'Dáta breithe (LL/MM/BBBB)',
                'complainant_primary_address_mobile' => 'Uimhir fóin póca',
                'complainant_primary_address_phone' => 'Uimhir fón eile (sa bhaile nó ag obair) (roghnach)',
                'complainant_primary_address_email' => 'Seoladh R-phost',
                'complainant_primary_address_email_confirm' => 'Deimhnigh R-phost Seoladh',

                'representative_legend' => 'Sonraí pearsanta an ionadaí',
                'representative_required_legend' => 'An dteastaíonn ionadaí uait?',
                'has_consent_form_legend' => 'An bhfuil an <a href="#" class="consent-form-link" target="_blank">fhoirm toilithe</a> sínithe ag an ngearánach?',
                'third_party_representative_legend' => 'Cén gaol atá agat leis an ngearánach?',
                'third_party_representative_type_guardian' => 'Caomhnóir',
                'third_party_representative_type_legal_representative' => 'Ionadaí Dlí',
                'third_party_representative_type_parent' => 'Tuismitheoir',
                'third_party_representative_type_social_worker' => 'Oibrí Sóisialta',
                'third_party_representative_type_other' => 'Eile',
                'third_party_representative_description_other' => 'Déan cur síos ar ‘eile’',
                'third_party_representative_name' => 'Ainm',
                'third_party_representative_forename' => 'Céadainm',
                'third_party_representative_surname' => 'Sloinne',
                'third_party_representative_date_of_birth' => 'Dáta breithe (LL/MM/BBBB)',
                'third_party_representative_mobile' => 'Uimhir fóin póca',
                'third_party_representative_email' => 'Seoladh R-phost',
                'third_party_representative_email_confirm' => 'Deimhnigh R-phost Seoladh',

                'correspondence_legend' => 'Más mian leat go bhfreagródh Ombudsman an Gharda Síochána ag seoladh eile seachas an seoladh a cuireadh ar fáil don ghearánach, cuir tic sa bhosca cuí.',
                'correspondence_type_1' => 'Mar an gcéanna leis an ngearánach',
                'correspondence_type_2' => 'Seoladh Eile',

                'complainant_secondary_address_legend' => 'Seoladh',

                'complainant_secondary_address_street' => 'Seoladh Líne 1',
                'complainant_secondary_address_town' => 'Baile nó Cathair',
                'complainant_secondary_address_county' => 'Contae',
                'complainant_secondary_address_postcode' => 'Éircód',
                'complainant_secondary_address_country' => 'Tír',
                'complainant_secondary_address_mobile' => 'Uimhir fóin póca',
                'complainant_secondary_address_phone' => 'Uimhir fón eile (sa bhaile nó ag obair) (roghnach)',
                'complainant_secondary_address_email' => 'Seoladh R-phost',
                'complainant_secondary_address_email_confirm' => 'Deimhnigh R-phost Seoladh',

                'complaint_legend' => 'An Gearán',

                'complaint_late_submission_legend' => 'Sonraí maidir le cur isteach déanach ',

                'complaint_date' => 'Dáta an Teagmhais (LL/MM/BBBB)',
                'complaint_time_reason' => 'Má tharla an teagmhas níos mó ná 12 mí ó shin, abair linn le do thoil céard a chuir moill ar chur isteach na tuairisce ina thaobh. ',
                'complaint_time' => 'Am an Teagmhais',
                'complaint_location' => 'Suíomh na Teagmhas',
                'complaint_garda_members' => 'An bhfuil aon fhaisnéis agat a d’fhéadfadh cabhrú leis an nGarda a aithint ar mian leat gearán a dhéanamh fúthu?',
                'complaint_details' => 'Déan cur síos, le do thoil, ar an mí-iompar ar leith atá á líomhain agat I leith an Garda.',
                'complaint_lead_up' => 'Cad a tharla roimh an eachtra?',
                'complaint_motive' => 'Cén fáth, dar leat, a d’iompaigh an Garda Síochána ar an mbealach sin?',

                'complaint_witnesses_legend' => 'An bhfuil aon fhinnéithe ar an eachtra?',
                'complaint_witnesses' => 'Mas “Tá” : Tabhair ainmneacha iomlána agus sonraí teagmhála aon duine atá ina fhinné anseo.',
                'complaint_evidence_legend' => 'An bhfuil aon fhianaise eile ann?',
                'complaint_evidence' => 'Más ‘Tá’: cad é an fhianaise?',
                'complaint_injuries_legend' => 'An raibh aon gortuithe agat nó an raibh cóir leighis de dhíth ort tar éis na heachtra?',
                'complaint_injuries' => 'Más ‘Tá’: Déan cur síos ar aon gortuithe nó cóireáil leighis a fuair tú tar éis na heachtra.',

                'child_protection_legend' => 'Oibleagáidí an GSOC maidir le Cosaint agus Leas Leanaí',
                'child_protection' => 'Tuigim na hOibleagáidí um Chosaint agus Leas Leanaí atá ag GSOC',

                'declaration_legend' => 'Alt 110 Acht an Gharda Síochána, 2005',
                'declaration' => 'Tuigim Alt 110 d’Acht an Gharda Síochána, 2005',

                'information_legend' => 'Úsáid Faisnéise',
                'information' => 'Admhaím an bonn lena n- úsáideann GSOC m’eolas agus conas is féidir é a roinnt',

                'submit' => 'CUIR ISTEACH',

            ],

        'additional' =>
            [
                'behalf_note' => 'Má tá an gearán seo á dhéanamh agat thar ceann duine eile nó cuideachta, beidh ort toiliú a thabhairt ón duine nó ó stiúrthóir na cuideachta a bhfuil tú ag déanamh ionadaíochta uirthi.',
                'date_of_birth' => 'Dáta breithe',
                'other_phone' => 'Uimhir fón eile',
                'date_of_incident' => 'Dáta an Teagmhais',
                'child_protection_description_simplified' => 'Tá leas an linbh fíorthábhachtach. Má tá aon imní ar Ombudsman an Gharda Síochána maidir le cosaint nó leas linbh, tá dualgas air an t-eolas seo a roinnt le TUSLA, an Ghníomhaireacht um Leanaí agus an Teaghlach. A mhéid is féidir, pléifimid é seo leat ar dtús.',

                'declaration_description_simplified' => 'Is cion coiriúil é “faisnéis bhréagach nó mhíthreorach” a sholáthar go feasach do GSOC. D’fhéadfaí tú a ionchúiseamh má dhealraíonn sé go bhfuil sé seo déanta agat. D’fhéadfadh fíneáil agus/nó pianbhreith phríosúin a bheith mar thoradh air sin.
<br /><br />Go hachomair, ní mór don fhaisnéis a thugann tú a bheith fírinneach agus gan féachaint le GSOC a chur amú in aon ghearán nó imscrúdú. Má thugann tú faisnéis bhréagach nó mhíthreorach, féadfaidh sé go mbeidh ort fíneáil suas le EUR 2,500 a íoc nó a bheith i bpríosún ar feadh suas le 6 mhí, nó iad araon.',

                'information_description_simplified' => 'Is féidir do chuid faisnéise a nochtadh do thríú páirtithe ar nós an Gharda Síochána chun cur ar ár gcumas gearáin a imscrúdú go héifeachtach nó sa chás go bhfuil dualgas dlíthiúil orainn é sin a dhéanamh. Déantar é seo chun imscrúdú éifeachtach ar do ghearán a chinntiú. 
<br /><br />I gcás ina bhfuil sé riachtanach agus comhréireach é sin a dhéanamh, féadfaimid faisnéis a roinnt le páirtithe eile in aon imeachtaí coiriúla nó sibhialta gaolmhara. Maidir le hionchúiseamh coiriúil, áirítear leis seo faisnéis a roinnt leis an Stiúrthóir Ionchúiseamh Poiblí agus féadfaidh an Stiúrthóir an fhaisnéis sin a roinnt ansin le páirtithe eile cosúil le finné nó daoine eile a aithnítear i do ghearán.
<br /><br />In imeacht sibhialta gaolmhar, áirítear leis sin faisnéis a roinnt le páirtithe eile de réir mar is gá de réir an dlí, lena n-áirítear Cúirt Dlí.'
            ]


    ];