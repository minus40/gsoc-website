<?php

return [

    'mismatch' => "Il n’y a pas de règle de validation pour ce champ.",
    'validate_required' => "Ce champ est obligatoire.",
    'validate_required_if' => "Ce champ est obligatoire.",
    'validate_valid_email' => "Ce champ doit contenir une adresse électronique valide.",
    'validate_min_len' => "Le nombre minimal de caractères pour ce champ est :param",
    'validate_max_len' => "Le nombre maximal de caractères pour ce champ est :param",
    'validate_exact_len' => ":attribute field doit comprendre exactement :param caractères",
    'validate_alpha' => ":attribute doit comprendre uniquement des caractères alphabétiques (a-z)",
    'validate_alpha_space' => ":attribute doit comprendre uniquement des caractères alphabétiques (a-z)",
    'validate_alpha_numeric' => ":attribute doit comprendre uniquement des caractères alphanumériques",
    'validate_alpha_dash' => ":attribute doit comprendre uniquement des caractères alphabétiques et des tirets",
    'validate_numeric' => "Ce champ ne peut contenir que des caractères numériques.",
    'validate_integer' => "Ce champ ne peut contenir qu’une valeur numérique.",
    'validate_boolean' => "Ce champ ne peut contenir qu’une valeur « vrai » ou « faux »",
    'validate_valid_url' => "Ce champ doit contenir une URL valide.",
    'validate_url_exists' => "L'URL :attribute n'existe pas",
    'validate_valid_ip' => ":attribute doit comprendre une adresse IP valide",
    'validate_contains' => ":attribute doit comprendre l'une de ces valeurs: :params",
    'validate_contains_list' => ":attribute doit comprendre une valeur provenant de la liste déroulante",
    'validate_doesnt_contain_list' => ":attribute comprend une valeur qui n'est pas acceptée",
    'validate_street_address' => ":attribute doit comprendre une adresse postale valide",
    'validate_date' => "Ce champ doit contenir une date valide (JJ/MM/AAAA).",
    'custom_date' => "Ce champ doit contenir une date valide (JJ/MM/AAAA).",
    'validate_min_numeric' => ":attribute doit être une valeur numérique égale ou supérieure à :param",
    'validate_max_numeric' => ":attribute doit être une valeur numérique égale ou inférieure à :param",
    'validate_min_age' => "Ce champ doit contenir un âge égal ou supérieur à :param",
    'validate_phone_number' => ":attribute doit être un numéro de téléphone valide",
    'validate_equalsfield' => "Les adresses électroniques ne correspondent pas.",
    'validate_contact' => "Vous ne nous avez pas communiqué de numéro de téléphone ni d’adresse électronique, ce qui nous empêchera de vous contacter afin de traiter efficacement votre plainte. Veuillez revenir en arrière et saisir un numéro de téléphone ou une adresse électronique maintenant, si vous voulez que nous puissions vous contacter."

];