<?php

return
    [

        'page' =>
            [
                'title' => 'Comment déposer plainte',
                'error_msg' => 'Veuillez vérifier les erreurs marquées en rouge ci-après, avant de soumettre votre formulaire.',
                'yes' => 'Oui',
                'no' => 'Non',
                'max_200_words' => '200 mots maximum.'
            ],

        'sections' =>
            [

                'complainant_introduction' =>
                    '
            <div>
                <p>Si vous déposez cette plainte, vous êtes le <b>« plaignant »</b> (ou la <b>« plaignante »</b>).</p>
               
                <p>Vous devez remplir tous les champs de ce formulaire, faute de quoi le traitement de votre plainte pourrait être retardé.</p>

                <p><b>Avant de commencer</b></p>
                <ul class="vertical-icons">
                    <li><a href="/make-a-complaint/before-you-complain/">Déterminez si la GSOC est l’organisation appropriée pour traiter votre plainte.</a></li>
                    <li>Vous pouvez également consulter notre <a href="/site-pages/website-terms/privacy-and-cookies/">politique de confidentialité</a> et de <a href="/about-gsoc/data-protection-freedom-of-information/data-protection/">protection des données></a>.</li>
                </ul>
            </div>
        ',

                'representative_introduction' =>
                    '
            <div class="explainer">
                <h3>Représentant(e)</h3>

                <p>Si vous déposez cette plainte pour le compte d’une autre personne ou d’une entreprise, vous devez fournir le consentement de la personne ou d’un dirigeant de l’entreprise que vous représentez.</p>
                <ul class="vertical-icons">
                    <li>Un <a href="#" class="consent-form-link" target="_blank">formulaire de consentement</a> est disponible ici.</li>
                </ul>

                <h3>Personnes de moins de 18 ans</h3>

                <p>Si vous avez moins de 18 ans, vous pouvez déposer plainte vous-même ou une autre personne peut le faire pour vous.</p>

                <p>Si vous demandez à une autre personne de déposer cette plainte pour vous, vous devez lui donner votre consentement.</p>

                <ul class="vertical-icons">
                    <li>Un <a href="#" class="consent-form-link" target="_blank">formulaire de consentement</a> est disponible ici.</li>
                </ul>

                <p>Le (la) représentant(e) doit remplir tous les champs de ce formulaire. Si vous ne renseignez pas toutes les informations dont nous avons besoin, le traitement de votre plainte pourrait être retardé ou la GSOC pourrait ne pas y donner suite.</i></p>
            </div>
        ',



                'complaint_garda_members_description' =>
                    '
            <div class="explainer mb-5">
                <p>Par exemple :</p>
                <ul class="vertical-icons">
                    <li>Nom</li>
                    <li>Matricule</li>
                    <li>Commissariat</li>
                    <li>Véhicule</li>
                    <li>Plaque d’immatriculation du véhicule</li>
                <ul>
            </div>
        ',
                'complaint_description' => '
            <div class="explainer mb-5">
                <p>Par exemple :</p>
                <ul class="vertical-icons">
                    <li>Qu’a-t-il (elle) dit ?</li>
                    <li>Qu’a-t-il (elle) fait ?</li>
                    <li>Que n’a-t-il (elle) pas fait ?</li>
                </ul>
                <p>Décrivez brièvement l’incident.</p>
            </div>
        ',
                'evidence_description' =>
                    '
            <div class="explainer mb-5">
                <p>Par exemple :</p>
                <ul class="vertical-icons">
                    <li>Vidéosurveillance ou autre enregistrement vidéo</li>
                    <li>Documents</li>
                </ul>
            </div>
        ',
                'injuries_description' =>
                    '
            <div class="explainer mb-5">
                <p>Veuillez ne pas joindre de rapport médical ou de photos de la blessure à ce formulaire. Nous vous les demanderons si cela est nécessaire.</p>
            </div>
        ',


                'child_protection_description' => '<p>Le bien-être d’un enfant est primordial. Si la commission Garda Ombudsman a des préoccupations concernant la protection ou le bien-être d’un enfant, elle a l’obligation de partager ces informations avec TUSLA, l’agence irlandaise pour l’enfance et la famille. Dans la mesure du possible, nous en discuterons d’abord avec vous. De plus amples informations sur la législation en faveur des enfants et sur TUSLA sont disponibles à l’adresse <a href="https://www.tusla.ie" target="_blank">www.TUSLA.ie</a>.</p>',

                'declaration_description' => '<p>Fournir délibérément des « informations fausses ou trompeuses » à la GSOC représente une infraction pénale. Si nous constatons que vous avez agi ainsi, vous pouvez faire l’objet de poursuites. Cette pratique est passible d’une amende et/ou d’une peine de prison.<br /><br />
En résumé, les informations que vous transmettez doivent être véridiques et ne doivent pas chercher à induire la GSOC en erreur dans le cadre d’une plainte ou d’une enquête. Si vous fournissez des informations fausses ou trompeuses, vous êtes passible d’une amende pouvant aller jusqu’à 2 500 € ou d’une peine de prison d’une durée maximale de 6 mois, ou des deux.<br /><br />
Veuillez cocher la case indiquant que vous avez lu et compris les informations ci-dessus.
</p>',
                'information_description' => '<p>Les renseignements que vous nous avez fournis peuvent être divulgués à des tiers tels que la Garda Síochána pour nous permettre d’enquêter efficacement sur les plaintes ou lorsque nous sommes légalement tenus de le faire. L’objectif est de mener une enquête efficace sur votre plainte.
<br /><br />Lorsque cela est nécessaire et approprié, nous pouvons partager des informations avec d’autres parties dans le cadre d’une procédure civile ou pénale connexe. En cas de poursuites pénales, cela inclut notamment le partage d’informations avec le procureur général, lequel peut ensuite transmettre ces renseignements à d’autres parties, comme un témoin ou d’autres personnes identifiées dans votre plainte. Pour plus d’informations, consultez la page <a href="https://www.gardaombudsman.ie/about-gsoc/non-party-disclosure/">Divulgation à des tiers</a>.
<br /><br />Dans le cadre d’une procédure civile connexe, cela inclut la communication d’informations à d’autres parties si la loi, et notamment une cour de justice, l’exige.
<br /><br />Pour obtenir de plus amples d’informations, veuillez cliquer sur les liens suivants qui vous renverront à notre <a href="https://www.gardaombudsman.ie/about-gsoc/data-protection-freedom-of-information/data-protection/">politique de protection des données</a> et à notre <a href="https://www.gardaombudsman.ie/site-pages/website-terms/privacy-and-cookies/">politique en matière de confidentialité et de cookies</a> ou veuillez envoyer un courrier électronique à <a href="mailto:data.protection@gsoc.ie">data.protection@gsoc.ie</a>.</p>',

            ],


        'sidebars' =>
            [
                'change_language_title' => 'Changer de langue',
                'proof_title' => 'Pourquoi avons-nous besoin de ces informations ?',
                'proof_content' => 'Si nous ne recevons pas de preuve de consentement ni d’explication, nous serons probablement dans l’incapacité de traiter votre plainte.',
                'help_title' => 'Besoin d’aide ?',
                'help_content' => 'Si vous rencontrez des difficultés pour remplir le formulaire de plainte, appelez-nous au 1890 600 800. Des conseillers sont à votre disposition par téléphone, tous les matins, du lundi au vendredi. En dehors de ces créneaux horaires, vous pouvez laisser un message nous demandant de vous rappeler ou de vous proposer un rendez-vous avec un conseiller.',
                'complainant_contact_title' => 'Restons en contact',
                'complainant_contact_content' => 'Nous devrons être en mesure de vous contacter pendant la durée du traitement de votre plainte. Veuillez nous indiquer, par courrier électronique à <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>, tout changement de coordonnées',
                'representative_contact_title' => 'Pourquoi avons-nous besoin de ces informations ?',
                'representative_contact_content' => 'Nous devrons être en mesure de vous contacter pendant la durée du traitement de votre plainte. Veuillez nous indiquer, par courrier électronique à <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>, tout changement de coordonnées',

            ],


        'fields' =>
            [
                'complainant_legend' => 'Renseignements personnels du (de la) plaignant(e)',
                'complainant_name' => 'Nom',
                'complainant_forename' => 'Prénom',
                'complainant_surname' => 'Nom',
                'complainant_primary_address_street' => 'Ligne d’adresse 1',
                'complainant_primary_address_street2' => 'Ligne d’adresse 2',
                'complainant_primary_address_street3' => 'Ligne d’adresse 3 (facultative)',
                'complainant_primary_address_town' => 'Ville',
                'complainant_primary_address_county' => 'Comté',
                'complainant_primary_address_postcode' => 'Code postal',
                'complainant_primary_address_country' => 'Pays',
                'complainant_date_of_birth' => 'Date de naissance (JJ/MM/AAAA)',
                'complainant_primary_address_mobile' => 'Numéro de téléphone portable',
                'complainant_primary_address_phone' => 'Autre numéro de téléphone (domicile ou travail) (facultatif)',
                'complainant_primary_address_email' => 'Adresse électroniqu',
                'complainant_primary_address_email_confirm' => 'Confirmer l’adresse électronique',

                'representative_legend' => 'Renseignements personnels du (de la) représentant(e)',
                'representative_required_legend' => 'Avez-vous besoin d’un(e) representant(e) ?',
                'has_consent_form_legend' => 'Le (la) plaignant(e) a-t-il (elle) signé le <a href="#" class="consent-form-link" target="_blank">formulaire de consentement</a> ?',
                'third_party_representative_legend' => 'Quel est votre lien avec le (la) plaignant(e) ?',
                'third_party_representative_type_guardian' => 'Tuteur(-trice)',
                'third_party_representative_type_legal_representative' => 'Représentant(e) légal(e)',
                'third_party_representative_type_parent' => 'Parent',
                'third_party_representative_type_social_worker' => 'Assistant(e) social(e)',
                'third_party_representative_type_other' => 'Autre',
                'third_party_representative_description_other' => 'Veuillez préciser la réponse « Autre »',
                'third_party_representative_name' => 'Nom',
                'third_party_representative_forename' => 'Prénom',
                'third_party_representative_surname' => 'Nom',
                'third_party_representative_date_of_birth' => 'Date de naissance (JJ/MM/AAAA)',
                'third_party_representative_mobile' => 'Numéro de téléphone portable',
                'third_party_representative_email' => 'Adresse électroniqu',
                'third_party_representative_email_confirm' => 'Confirmer l’adresse électronique',

                'correspondence_legend' => 'Si vous souhaitez que la commission Garda Ombudsman corresponde avec vous à une adresse différente de celle fournie pour le (la) plaignant(e), veuillez cocher la case appropriée.',
                'correspondence_type_1' => 'Même adresse que le (la) plaignant(e)',
                'correspondence_type_2' => 'Adresse différente',

                'complainant_secondary_address_legend' => 'Adresse',

                'complainant_secondary_address_street' => 'Ligne d’adresse 1',
                'complainant_secondary_address_town' => 'Ville',
                'complainant_secondary_address_county' => 'Comté',
                'complainant_secondary_address_postcode' => 'Code postal',
                'complainant_secondary_address_country' => 'Pays',
                'complainant_secondary_address_mobile' => 'Numéro de téléphone portable',
                'complainant_secondary_address_phone' => 'Autre numéro de téléphone (domicile ou travail) (facultatif)',
                'complainant_secondary_address_email' => 'Adresse électroniqu',
                'complainant_secondary_address_email_confirm' => 'Confirmer l’adresse électronique',

                'complaint_legend' => 'La plainte',

                'complaint_late_submission_legend' => 'Informations concernant les soumissions tardives',

                'complaint_date' => 'Date de l’incident (JJ/MM/AAAA)',
                'complaint_time_reason' => 'Si l’incident s’est produit il y a plus de 12 mois, veuillez nous indiquer les raisons pour lesquelles il y a eu un retard dans son signalement.',
                'complaint_time' => 'Heure de l’incident',
                'complaint_location' => 'Lieu de l’incident',
                'complaint_garda_members' => 'Veuillez renseigner toute information qui aidera à identifier le (la) garda contre qui vous souhaitez porter plainte.',
                'complaint_details' => 'Veuillez décrire le comportement spécifique que vous reprochez au (à la) garda.',
                'complaint_lead_up' => 'Que s’est-il passé avant l’incident ?',
                'complaint_motive' => 'Selon vous, pourquoi le (la) garda s’est-il (elle) comporté(e) de la sorte ?',

                'complaint_witnesses_legend' => 'Des témoins ont-ils assisté à l’incident ?',
                'complaint_witnesses' => 'Si « Oui » : veuillez indiquer les noms complets et les coordonnées des témoins.',
                'complaint_evidence_legend' => 'Avez-vous d’autres preuves ?',
                'complaint_evidence' => 'Si « Oui » : quelles sont les preuves ?',
                'complaint_injuries_legend' => 'Avez-vous été blessé(e) ou avez-vous eu besoin d’un traitement médical à la suite de l’incident ?',
                'complaint_injuries' => 'Si « Oui » : décrivez vos blessures ou le traitement médical que vous avez reçu à la suite de l’incident.',

                'child_protection_legend' => 'Obligations de la GSOC en matière de protection et de bien-être des enfants',
                'child_protection' => 'Je comprends les obligations de la GSOC en matière de protection et de bien-être des enfants.',

                'declaration_legend' => 'Article 110 de la loi de 2005 sur la Garda Síochána',
                'declaration' => 'Je comprends l’article 110 de la loi de 2005 sur la Garda Síochána.',

                'information_legend' => 'Utilisation des renseignements',
                'information' => 'Je reconnais la base sur laquelle la GSOC utilise mes informations et la manière dont elles peuvent être partagées.',

                'submit' => 'Soumettre',

            ],

        'additional' =>
            [
                'behalf_note' => 'Si vous déposez cette plainte pour le compte d’une autre personne ou d’une entreprise, vous devez fournir le consentement de la personne ou d’un dirigeant de l’entreprise que vous représentez.',
                'date_of_birth' => 'Date de naissance',
                'other_phone' => 'Autre numéro de téléphone',
                'date_of_incident' => 'Date de l’incident',
                'child_protection_description_simplified' => 'Le bien-être d’un enfant est primordial. Si la commission Garda Ombudsman a des préoccupations concernant la protection ou le bien-être d’un enfant, elle a l’obligation de partager ces informations avec TUSLA, l’agence irlandaise pour l’enfance et la famille. Dans la mesure du possible, nous en discuterons d’abord avec vous.',

                'declaration_description_simplified' => 'Fournir délibérément des « informations fausses ou trompeuses » à la GSOC représente une infraction pénale. Si nous constatons que vous avez agi ainsi, vous pouvez faire l’objet de poursuites. Cette pratique est passible d’une amende et/ou d’une peine de prison.<br /><br />
En résumé, les informations que vous transmettez doivent être véridiques et ne doivent pas chercher à induire la GSOC en erreur dans le cadre d’une plainte ou d’une enquête. Si vous fournissez des informations fausses ou trompeuses, vous êtes passible d’une amende pouvant aller jusqu’à 2 500 € ou d’une peine de prison d’une durée maximale de 6 mois, ou des deux.',
                'information_description_simplified' => 'Les renseignements que vous nous avez fournis peuvent être divulgués à des tiers tels que la Garda Síochána pour nous permettre d’enquêter efficacement sur les plaintes ou lorsque nous sommes légalement tenus de le faire. L’objectif est de mener une enquête efficace sur votre plainte.
<br /><br />Lorsque cela est nécessaire et approprié, nous pouvons partager des informations avec d’autres parties dans le cadre d’une procédure civile ou pénale connexe. En cas de poursuites pénales, cela inclut notamment le partage d’informations avec le procureur général, lequel peut ensuite transmettre ces renseignements à d’autres parties, comme un témoin ou d’autres personnes identifiées dans votre plainte.
<br /><br />Dans le cadre d’une procédure civile connexe, cela inclut la communication d’informations à d’autres parties si la loi, et notamment une cour de justice, l’exige.',
            ]


    ];