<?php

return [
    'title' => 'Confirmation',
    'paragraph_1' => 'Merci d’avoir soumis votre plainte à la commission Garda Ombudsman.',
    'paragraph_2' => 'Une fois que vous aurez quitté ce site, vous ne pourrez plus accéder au formulaire que vous avez soumis.',
    'cta_button' => 'Sauvegarder le PDF',
    'subtitle' => 'Prochaines étapes',
    'list' => ' <ul>
                    <li>Dans le courant de la semaine prochaine, nous vous enverrons une lettre vous informant que votre plainte a été enregistrée et qu’une décision va être prise pour déterminer si elle peut être acceptée et traitée par la GSOC, conformément aux critères énoncés à l’article 87 de la loi de 2005 sur la Garda Síochána.</li>
                    <li>Lorsque cette décision aura été prise, nous vous écrirons pour vous en informer. Si nous pouvons accepter votre plainte, nous vous préciserons comment elle sera traitée. Lisez les différentes manières dont la plainte peut être traitée.</li>
                    <li>Le temps nécessaire au traitement d’une plainte, jusqu’à sa clôture, est de trois à neuf mois. Pendant cette période, vous serez probablement contacté(e) par le responsable de la GSOC ou l’agent de la Garda Síochána qui s’occupe de votre dossier, pour vous demander des informations supplémentaires et vous informer des avancées.</li>
                    <li>À la fin, nous vous écrirons pour vous faire part de nos conclusions et des éventuelles mesures à prendre. Si vos coordonnées changent, veuillez nous en informer en envoyant un courriel à l’adresse <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>.</li>
                </ul>'
];