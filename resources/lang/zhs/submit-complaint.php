<?php

return
    [

        'page' =>
            [
                'title' => '如何投诉',
                'error_msg' => '在提交表格前，请先检查以下标记为红色的错误。',
                'yes' => '是',
                'no' => '否',
                'max_200_words' => '最多200个单词。'
            ],

        'sections' =>
            [

                'complainant_introduction' =>
                    '
            <div>
                <p>如果由您提出此投诉，则您是“投诉人”。</p>
               
                <p>请您务必填写本表中的所有信息，否则可能会导致我们延迟处理您的投诉。</p>

                <p><b>开始之前：</b></p>
                <ul class="vertical-icons">
                    <li><a href="/make-a-complaint/before-you-complain/">请了解GSOC是否适合处理您的投诉</a></li>
                    <li>您也可以查看我们的《<a href="/site-pages/website-terms/privacy-and-cookies/">隐私</a>和<a href="/about-gsoc/data-protection-freedom-of-information/data-protection/">数据保护</a>》政策。</li>
                </ul>
            </div>
        ',

                'representative_introduction' =>
                    '
            <div class="explainer">
                <h3>代表人</h3>

                <p>如果您代表其他人或某公司提出此投诉，您需要提供您所代表的个人或公司的一名董事出具的同意书。</p>
                <ul class="vertical-icons">
                    <li>此处<a href="#" class="consent-form-link" target="_blank">是同意书</a>模版。</li>
                </ul>

                <h3>18岁以下</h3>

                <p>如果您未满18岁，您可以自己提出投诉，也可以由其他人代您提出投诉。</p>

                <p>如果您请其他人代您提出投诉，则需要向其提供由您签署的同意书，然后才能提出投诉。</p>

                <ul class="vertical-icons">
                    <li>此处<a href="#" class="consent-form-link" target="_blank">是同意书</a>模版。</li>
                </ul>

                <p><i>代表人必须填写此表格中的所有信息。如果您未填写我们需要的所有信息，则可能会导致我们延迟处理您的投诉，或导致GSOC不再采取进一步行动。</i></p>
            </div>
        ',



                'complaint_garda_members_description' =>
                    '
            <div class="explainer mb-5">
                <p>例如，其：</p>
                <ul class="vertical-icons">
                    <li>姓名</li>
                    <li>肩章编号</li>
                    <li>警署</li>
                    <li>汽车</li>
                    <li>汽车牌号</li>
                <ul>
            </div>
        ',
                'complaint_description' => '
            <div class="explainer mb-5">
                <p>例如，其在当时：</p>
                <ul class="vertical-icons">
                    <li>说了什么？</li>
                    <li>做了什么？</li>
                    <li>没做什么？</li>
                </ul>
                <p>这应该是对事件的简要说明。</p>
            </div>
        ',
                'evidence_description' =>
                    '
            <div class="explainer mb-5">
                <p>例如：</p>
                <ul class="vertical-icons">
                    <li>闭路电视或其他视频片段</li>
                    <li>文档</li>
                </ul>
            </div>
        ',
                'injuries_description' =>
                    '
            <div class="explainer mb-5">
                <p>请不要与本表格同时提交任何医疗报告或伤情图片。如有需要，我们会另外要求您提交。</p>
            </div>
        ',


                'child_protection_description' => '<p>儿童的福利至关重要。如果警察事务申诉委员会认为儿童未受到良好的保护或儿童福利出现任何问题，便有义务与儿童和家庭机构TUSLA分享这一信息。如果可能，我们将首先与您讨论此问题。有关“儿童第一的立法”（Children First Legislation）和TUSLA的更多信息，请访问<a href="https://www.tusla.ie" target="_blank">www.TUSLA.ie</a>。</p>',

                'declaration_description' => '<p>故意向 GSOC 提供“虚假或误导性信息”属于刑事犯罪行为。如果有迹象表明您已经这样做了，那么您可能会因此遭到起诉。此种行为可能会导致罚款和/或监禁。
<br /><br />总之，您提供的信息必须是真实的，且不得在任何投诉或调查中试图误导GSOC。如果您提供虚假或误导性信息，则可能需要支付最高2500欧元的罚款或长达6个月的监禁，或两者兼罚。
<br /><br />请勾选下面的方框，表明您已阅读并理解上述内容。</p>',

                'information_description' => '<p>您的信息可能会被披露给包括爱尔兰警察局（Garda Síochána）在内的第三方，以便我们有效地调查投诉，或满足法律义务的要求。这是为了确保对您的投诉进行有效的调查。
<br /><br />在必要且合适的情况下，我们可能会在任何相关的刑事或民事诉讼中与其他方共享信息。就刑事诉讼而言，这包括与检察长（Director of Public Prosecutions）分享信息，而检察长随后可以与证人等其他当事方或您在投诉中确认的其他人分享这些信息。如需了解这方面的信息，请访问非当事方信息披露（<a href="https://www.gardaombudsman.ie/about-gsoc/non-party-disclosure/">Non-Party Disclosure</a>）。
<br /><br />在相关的民事诉讼中，这包括根据法律（包括法院）与其他当事方共享信息。
<br /><br />如需更多信息，请点击以下链接进入我们<a href="https://www.gardaombudsman.ie/about-gsoc/data-protection-freedom-of-information/data-protection/">的数据保护政策</a>和<a href="https://www.gardaombudsman.ie/site-pages/website-terms/privacy-and-cookies/">隐私与cookie政策</a>，或发送电子邮件至<a href="mailto:data.protection@gsoc.ie">data.protection@gsoc.ie</a>。</p>',

            ],


        'sidebars' =>
            [
                'change_language_title' => '更换语言',
                'proof_title' => '我们为什么需要这些资料？',
                'proof_content' => '如果您无法提供同意证明书或相关解释文件，我们便无法处理您的投诉。',
                'help_title' => '需要帮助？',
                'help_content' => '如果您需要协助填写投诉表，请拨打 1890 600 800 联系我们。每个工作日上午都会有工作人员接听您的来电。如遇其他时间，您可以留言要求我们回电，您也可以与工作人员另行约定时间。',
                'complainant_contact_title' => '让我们保持联系',
                'complainant_contact_content' => '我们在处理您的投诉时，需要能够与您取得联系。因此，如果您的联系方式有变，请发送电子邮件至<a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>通知我们。',
                'representative_contact_title' => '我们为什么需要这些资料？',
                'representative_contact_content' => '我们在处理您的投诉时，需要能够与您取得联系。因此，如果您的联系方式有变，请发送电子邮件至<a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>通知我们。',

            ],


        'fields' =>
            [
                'complainant_legend' => '投诉人的个人资料',
                'complainant_name' => '姓名',
                'complainant_forename' => '名字',
                'complainant_surname' => '姓氏',
                'complainant_primary_address_street' => '地址 第1行',
                'complainant_primary_address_street2' => '地址 第2行',
                'complainant_primary_address_street3' => '地址 第3行 (可选)',
                'complainant_primary_address_town' => '镇/市',
                'complainant_primary_address_county' => '县',
                'complainant_primary_address_postcode' => '邮政编码',
                'complainant_primary_address_country' => '国家',
                'complainant_date_of_birth' => '出生日期（DD/MM/YYYY）',
                'complainant_primary_address_mobile' => '手机号码',
                'complainant_primary_address_phone' => '其他联系电话（家庭或工作）（可选）',
                'complainant_primary_address_email' => '电子邮件地址',
                'complainant_primary_address_email_confirm' => '确认电子邮件地址',

                'representative_legend' => '代表人的个人资料',
                'representative_required_legend' => '您是否需要一名代表人？',
                'has_consent_form_legend' => '投诉人是否已经签<a href="#" class="consent-form-link" target="_blank">署了同意书</a>？',
                'third_party_representative_legend' => '您与投诉人之间是什么关系？',
                'third_party_representative_type_guardian' => '监护人',
                'third_party_representative_type_legal_representative' => '法定代表人',
                'third_party_representative_type_parent' => '父母',
                'third_party_representative_type_social_worker' => '社会工作者',
                'third_party_representative_type_other' => '其他',
                'third_party_representative_description_other' => '请对“其他”进行说明',
                'third_party_representative_name' => '姓名',
                'third_party_representative_forename' => '名字',
                'third_party_representative_surname' => '姓氏',
                'third_party_representative_date_of_birth' => '出生日期（DD/MM/YYYY）',
                'third_party_representative_mobile' => '手机号码',
                'third_party_representative_email' => '电子邮件地址',
                'third_party_representative_email_confirm' => '确认电子邮件地址',

                'correspondence_legend' => '如果您希望警察事务申诉委员会（Garda Ombudsman）使用与投诉人不同的地址与您进行通信，请勾选相应的方框。',
                'correspondence_type_1' => '与投诉人相同',
                'correspondence_type_2' => '不同的地址',

                'complainant_secondary_address_legend' => '地址',

                'complainant_secondary_address_street' => '地址 第1行',
                'complainant_secondary_address_town' => '镇/市',
                'complainant_secondary_address_county' => '县',
                'complainant_secondary_address_postcode' => '邮政编码',
                'complainant_secondary_address_country' => '国家',
                'complainant_secondary_address_mobile' => '手机号码',
                'complainant_secondary_address_phone' => '其他联系电话（家庭或工作）（可选）',
                'complainant_secondary_address_email' => '电子邮件地址',
                'complainant_secondary_address_email_confirm' => '确认电子邮件地址',

                'complaint_legend' => '投诉内容',

                'complaint_late_submission_legend' => '请不要与本表格同时提交任何医疗报告或伤情图片。如有需要，我们会另外要求您提交。',

                'complaint_date' => '事件发生日期（DD/MM/YYYY）',
                'complaint_time_reason' => '如果事件发生在12个月之前，请告诉我们您推迟提交该事件报告的原因。',
                'complaint_time' => '事件发生时间',
                'complaint_location' => '事件发生地点',
                'complaint_garda_members' => '请提供能够帮助我们识别您投诉所涉及的警察的所有信息。',
                'complaint_details' => '请描述您指控的警察做出的具体不当行为。',
                'complaint_lead_up' => '导致该事件发生的原因？',
                'complaint_motive' => '如果您认为该名警察做出那样的行为是事出有因，请告诉我们原因。',

                'complaint_witnesses_legend' => '事件是否有目击证人？',
                'complaint_witnesses' => '如果“是”：请在此提供目击证人的全名和联系方式。',
                'complaint_evidence_legend' => '是否还有其他证据？',
                'complaint_evidence' => '如果“是”：该其他证据是什么？',
                'complaint_injuries_legend' => '事件发生后，您是否有任何受伤情况或需要接受治疗？',
                'complaint_injuries' => '如果“是”：请描述您在事件发生后出现的任何受伤情况或接受的任何治疗。',

                'child_protection_legend' => 'GSOC为儿童提供保护和福利的义务',
                'child_protection' => '我明白GSOC在为儿童提供保护和福利方面的义务',

                'declaration_legend' => '2005年《警务法》第110条',
                'declaration' => '我理解2005年《警务法》第110条的内容',

                'information_legend' => '对信息的使用',
                'information' => '我认可GSOC使用我的信息的依据及其分享这些信息的方式',

                'submit' => '提交',

            ],

        'additional' =>
            [
                'behalf_note' => '如果您代表其他人或某公司提出此投诉，您需要提供您所代表的个人或公司的一名董事出具的同意书。',
                'date_of_birth' => '出生日期',
                'other_phone' => '其他联系电话',
                'date_of_incident' => '事件发生日期',
                'child_protection_description_simplified' => '儿童的福利至关重要。如果警察事务申诉委员会认为儿童未受到良好的保护或儿童福利出现任何问题，便有义务与儿童和家庭机构TUSLA分享这一信息。如果可能，我们将首先与您讨论此问题。',

                'declaration_description_simplified' => '故意向 GSOC 提供“虚假或误导性信息”属于刑事犯罪行为。如果有迹象表明您已经这样做了，那么您可能会因此遭到起诉。此种行为可能会导致罚款和/或监禁。
<br /><br />总之，您提供的信息必须是真实的，且不得在任何投诉或调查中试图误导GSOC。如果您提供虚假或误导性信息，则可能需要支付最高2500欧元的罚款或长达6个月的监禁，或两者兼罚。',

                'information_description_simplified' => '您的信息可能会被披露给包括爱尔兰警察局（Garda Síochána）在内的第三方，以便我们有效地调查投诉，或满足法律义务的要求。这是为了确保对您的投诉进行有效的调查。
<br /><br />在必要且合适的情况下，我们可能会在任何相关的刑事或民事诉讼中与其他方共享信息。就刑事诉讼而言，这包括与检察长（Director of Public Prosecutions）分享信息，而检察长随后可以与证人等其他当事方或您在投诉中确认的其他人分享这些信息。
<br /><br />在相关的民事诉讼中，这包括根据法律（包括法院）与其他当事方共享信息。',
            ]


    ];