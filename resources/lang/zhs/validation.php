<?php

return [

    'mismatch' => "此栏没有验证规则。",
    'validate_required' => "此栏为必填字段",
    'validate_required_if' => "此栏为必填字段",
    'validate_valid_email' => "此栏需填写有效的电子邮件地址",
    'validate_min_len' => "该栏最少字符数目是：param",
    'validate_max_len' => "该栏最多字符数目是：param",
    'validate_exact_len' => "The :attribute field 字段长度必须刚好为 :param 个字符",
    'validate_alpha' => "The :attribute field 字段可能仅包含字母字符 (a-z)",
    'validate_alpha_space' => "The :attribute field 字段可能仅包含字母字符 (a-z) 和空格",
    'validate_alpha_numeric' => "The :attribute field 字段可能仅包含字母数字字符",
    'validate_alpha_dash' => "The :attribute field 字段可能仅包含字母字符和破折号",
    'validate_numeric' => "此栏只能包含数字字符",
    'validate_integer' => "此栏只能包含数字值",
    'validate_boolean' => "此栏只能包含TRUE或FALSE值",
    'validate_valid_url' => "此栏需是一个有效的URL",
    'validate_url_exists' => "The :attribute field URL 不存在",
    'validate_valid_ip' => "The :attribute field 字段必须包含一个有效的 IP 地址",
    'validate_contains' => "The :attribute field 字段必须包含下述其中一个值：:params",
    'validate_contains_list' => "The :attribute field 字段必须包含其下拉列表中的一个值",
    'validate_doesnt_contain_list' => "The :attribute field 字段包含一个不被接受的值",
    'validate_street_address' => "The :attribute field 字段必须为有效的街道地址",
    'validate_date' => "此栏需是有效日期（日/月/年）",
    'custom_date' => "此栏需是有效日期（日/月/年）",
    'validate_min_numeric' => "The :attribute field 字段必须为一个等于或大于 :param 的数值",
    'validate_max_numeric' => "The :attribute field 字段必需为一个等于或小于 :param 的数值",
    'validate_min_age' => "此栏的年龄必须大于或等于:param",
    'validate_phone_number' => "The :attribute field 字段必须为有效的电话号码",
    'validate_equalsfield' => "电子邮件地址不匹配",
    'validate_contact' => "您没有向我们提供电话号码或电子邮件地址，这将使我们难以与您取得联系，因此无法有效处理您的投诉。如果您希望我们能够与您取得联系，请立即返回并输入一个联系方式。"

];