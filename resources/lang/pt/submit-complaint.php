<?php

return
    [

        'page' =>
            [
                'title' => 'Como fazer uma queixa',
                'error_msg' => 'Antes de enviar o formulário, verifique os erros assinalados a vermelho.',
                'yes' => 'Sim',
                'no' => 'Não',
                'max_200_words' => 'Máximo de 200 palavras.'
            ],

        'sections' =>
            [

                'complainant_introduction' =>
                    '
            <div>
                <p>Se está a apresentar esta queixa, significa que é o <b>"queixoso"</b>.</p>
               
                <p>Deve preencher todas as informações deste formulário, caso contrário o processamento da sua queixa poderá sofrer atrasos.</p>

                <p><b>Antes de começar:</b></p>
                <ul class="vertical-icons">
                    <li><a href="/make-a-complaint/before-you-complain/">Certifique-se de que a GSOC é a entidade adequada para tratar da sua queixa.</a></li>
                    <li>Consulte também a nossa política de <a href="/site-pages/website-terms/privacy-and-cookies/">privacidade</a> e <a href="/about-gsoc/data-protection-freedom-of-information/data-protection/">proteção de dados</a>.</li>
                </ul>
            </div>
        ',

                'representative_introduction' =>
                    '
            <div class="explainer">
                <h3>Representante</h3>

                <p>Se apresentar esta queixa em nome de outra pessoa ou empresa, terá de apresentar o consentimento dessa pessoa ou do diretor da empresa que está a representar.</p>
                <ul class="vertical-icons">
                    <li>O <a href="#" class="consent-form-link" target="_blank">formulário de consentimento</a> está disponível aqui.</li>
                </ul>

                <h3>Menos de 18 anos de idade</h3>

                <p>Se tiver menos de 18 anos, pode apresentar uma queixa pessoalmente ou outra pessoa pode fazê-lo por si.</p>

                <p>Se pedir a outra pessoa que apresente esta queixa por si, terá de conceder-lhe autorização para apresentar a queixa.</p>

                <ul class="vertical-icons">
                    <li>O <a href="#" class="consent-form-link" target="_blank">formulário de consentimento</a> está disponível aqui.</li>
                </ul>

                <p><i>O representante deve preencher todas as informações deste formulário. O não preenchimento de todas as informações de que necessitamos pode atrasar o processamento da sua queixa ou levar a que não seja tomada qualquer medida por parte da GSOC.</i></p>
            </div>
        ',



                'complaint_garda_members_description' =>
                    '
            <div class="explainer mb-5">
                <p>Por exemplo, o seu:</p>
                <ul class="vertical-icons">
                    <li>Nome</li>
                    <li>Número de identificação</li>
                    <li>Esquadra</li>
                    <li>Carro</li>
                    <li>Número da matrícula do carro</li>
                <ul>
            </div>
        ',
                'complaint_description' => '
            <div class="explainer mb-5">
                <p>Por exemplo, o que é que:</p>
                <ul class="vertical-icons">
                    <li>Disse?</li>
                    <li>Fez?</li>
                    <li>Não fez?</li>
                </ul>
                <p>Deve apresentar um breve resumo do incidente.</p>
            </div>
        ',
                'evidence_description' =>
                    '
            <div class="explainer mb-5">
                <p>Por exemplo:</p>
                <ul class="vertical-icons">
                    <li>Câmaras de vigilância</li>
                    <li>Documentos</li>
                </ul>
            </div>
        ',
                'injuries_description' =>
                    '
            <div class="explainer mb-5">
                <p>Não envie relatórios médicos ou fotografias da lesão juntamente com este formulário. Se for necessário, solicitar-lhe-emos esses documentos.</p>
            </div>
        ',


                'child_protection_description' => '<p>O bem-estar de uma criança é fundamental. Se a Garda Ombudsman tiver alguma informação sobre a proteção ou ao bem-estar de uma criança, tem a obrigação de a partilhar com a TUSLA, a Agência da Criança e da Família. Na medida do possível, abordaremos primeiro esta questão consigo. Para mais informações sobre a legislação "Children First" e a TUSLA consulte <a href="https://www.tusla.ie" target="_blank">www.TUSLA.ie</a>.</p>',

                'declaration_description' => '<p>Fornecer intencionalmente "informação falsa ou enganosa" à GSOC é crime. Pode ser alvo de uma ação judicial se houver suspeitas de que o fez, podendo resultar numa multa e/ou pena de prisão.
<br /><br />Em suma, as informações prestadas devem ser verdadeiras e não devem ter por objetivo induzir em erro a GSOC em qualquer queixa ou inquérito. Se prestar informações falsas ou suscetíveis de induzir em erro, poderá ter de pagar uma coima até 2 500 euros ou cumprir uma pena de prisão até 6 meses, ou ambas.
<br /><br />Assinale a caixa abaixo para indicar que leu e compreendeu o acima exposto.</p>',

                'information_description' => '<p>As suas informações podem ser divulgadas a terceiros, como a Garda Síochána, para nos permitir investigar eficazmente as queixas ou quando formos legalmente obrigados a fazê-lo. Este procedimento destina-se a garantir uma investigação eficaz da sua queixa.
<br /><br />Sempre que for necessário e adequado, partilharemos informações com outras partes em qualquer processo penal ou cível conexo. Relativamente a um processo criminal, isto inclui a partilha de informações com o Ministério Público, que pode depois partilhar essas informações com outras partes, como uma testemunha ou outras pessoas identificadas na sua queixa. Para mais informações, consulte o <a href="https://www.gardaombudsman.ie/about-gsoc/non-party-disclosure/">Acordo de Confidencialidade</a>.
<br /><br />Num processo cível conexo, isto inclui a partilha de informações com outras partes, conforme possa ser necessário por lei, incluindo por um Tribunal de Justiça.
<br /><br />Para mais informações, consulte a nossa <a href="https://www.gardaombudsman.ie/about-gsoc/data-protection-freedom-of-information/data-protection/">política de proteção de dados</a> e <a href="https://www.gardaombudsman.ie/site-pages/website-terms/privacy-and-cookies/">política de privacidade e de cookies</a> ou envie um e-mail para <a href="mailto:data.protection@gsoc.ie">data.protection@gsoc.ie</a>.</p>',

            ],


        'sidebars' =>
            [
                'change_language_title' => 'Mudar idioma',
                'proof_title' => 'Por que razão precisamos desta informação?',
                'proof_content' => 'Se não recebermos a prova de consentimento ou uma explicação, é provável que não possamos tratar da queixa.',
                'help_title' => 'Precisa de ajuda?',
                'help_content' => 'Contacte-nos através do número 1890 600 800, se precisar de ajuda com o preenchimento do formulário de queixa. Assistentes sociais estão disponíveis por via telefónica todos os dias úteis de manhã. Noutras alturas, pode deixar uma mensagem a pedir que lhe liguem de volta ou uma entrevista com um assistente social.',
                'complainant_contact_title' => 'Mantenha o contacto',
                'complainant_contact_content' => 'Precisamos de poder contactá-lo enquanto estivermos a tratar da sua queixa. Assim, se os seus dados de contacto sofrerem alterações, informe-nos por e-mail para <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>.',
                'representative_contact_title' => 'Por que razão precisamos desta informação?',
                'representative_contact_content' => 'Precisamos de poder contactá-lo enquanto estivermos a tratar da sua queixa. Assim, se os seus dados de contacto sofrerem alterações, informe-nos por e-mail para <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>.',

            ],


        'fields' =>
            [
                'complainant_legend' => 'Dados pessoais do queixoso',
                'complainant_name' => 'Nome',
                'complainant_forename' => 'Nome próprio',
                'complainant_surname' => 'Apelido',
                'complainant_primary_address_street' => 'Morada',
                'complainant_primary_address_street2' => 'Morada continuação',
                'complainant_primary_address_street3' => 'Morada continuação (opcional)',
                'complainant_primary_address_town' => 'Vila ou cidade',
                'complainant_primary_address_county' => 'Condado',
                'complainant_primary_address_postcode' => 'Código postal',
                'complainant_primary_address_country' => 'País',
                'complainant_date_of_birth' => 'Data de nascimento (DD/MM/YYYY)',
                'complainant_primary_address_mobile' => 'Número de telemóvel',
                'complainant_primary_address_phone' => 'Outro número de contacto (casa ou trabalho) (opcional)',
                'complainant_primary_address_email' => 'Endereço de e-mail',
                'complainant_primary_address_email_confirm' => 'Confirmar endereço de e-mail',

                'representative_legend' => 'Dados pessoais do representante',
                'representative_required_legend' => 'Necessita de um representante?',
                'has_consent_form_legend' => 'O queixoso assinou o <a href="#" class="consent-form-link" target="_blank">formulário de consentimento</a>?',
                'third_party_representative_legend' => 'Qual é a sua relação com o queixoso?',
                'third_party_representative_type_guardian' => 'Tutor',
                'third_party_representative_type_legal_representative' => 'Representante Legal',
                'third_party_representative_type_parent' => 'Pai ou Mãe',
                'third_party_representative_type_social_worker' => 'Assistente social',
                'third_party_representative_type_other' => 'Outro',
                'third_party_representative_description_other' => 'Defina “outro”',
                'third_party_representative_name' => 'Nome',
                'third_party_representative_forename' => 'Nome próprio',
                'third_party_representative_surname' => 'Apelido',
                'third_party_representative_date_of_birth' => 'Data de nascimento (DD/MM/YYYY)',
                'third_party_representative_mobile' => 'Número de telemóvel',
                'third_party_representative_email' => 'Endereço de e-mail',
                'third_party_representative_email_confirm' => 'Confirmar endereço de e-mail',

                'correspondence_legend' => 'Se desejar que a Garda Ombudsman o contacte para uma morada diferente da indicada para o queixoso, assinale a caixa correspondente.',
                'correspondence_type_1' => 'Mesma morada que o queixoso',
                'correspondence_type_2' => 'Morada diferente',

                'complainant_secondary_address_legend' => 'Morada',

                'complainant_secondary_address_street' => 'Morada',
                'complainant_secondary_address_town' => 'Vila ou cidade',
                'complainant_secondary_address_county' => 'Condado',
                'complainant_secondary_address_postcode' => 'Código postal',
                'complainant_secondary_address_country' => 'País',
                'complainant_secondary_address_mobile' => 'Número de telemóvel',
                'complainant_secondary_address_phone' => 'Outro número de contacto (casa ou trabalho) (opcional)',
                'complainant_secondary_address_email' => 'Endereço de e-mail',
                'complainant_secondary_address_email_confirm' => 'Confirmar endereço de e-mail',

                'complaint_legend' => 'A queixa',

                'complaint_late_submission_legend' => 'Detalhes do envio em atraso.',

                'complaint_date' => 'Data do incidente (DD/MM/YYYY)',
                'complaint_time_reason' => 'Se o incidente ocorreu há mais de 12 meses, indique-nos por que razão se atrasou a comunicá-lo.',
                'complaint_time' => 'Hora do incidente',
                'complaint_location' => 'Local do incidente',
                'complaint_garda_members' => 'Tem alguma informação que possa ajudar a identificar o membro da Garda de quem pretende apresentar queixa?',
                'complaint_details' => 'Descreva especificamente o comportamento incorreto que alega ter sido praticado pelo membro da Garda.',
                'complaint_lead_up' => 'O que é que aconteceu antes do incidente?',
                'complaint_motive' => 'Por que razão acha que o membro da Garda se comportou daquela forma?',

                'complaint_witnesses_legend' => 'Há testemunhas do incidente?',
                'complaint_witnesses' => 'Se respondeu ‘Sim’: Indique aqui os nomes completos e os dados de contacto das testemunhas.',
                'complaint_evidence_legend' => 'Is there any other evidence?',
                'complaint_evidence' => 'Se respondeu ‘Sim’: quais são as provas?',
                'complaint_injuries_legend' => 'Sofreu algum ferimento ou necessitou de tratamento médico na sequência do incidente?',
                'complaint_injuries' => 'Se respondeu ‘Sim’: Descreva os ferimentos ou o tratamento médico que recebeu após o incidente.',

                'child_protection_legend' => 'Proteção das crianças e obrigações sociais da GSOC',
                'child_protection' => 'Compreendo as obrigações da GSOC relativamente à proteção e bem-estar das crianças.',

                'declaration_legend' => 'Secção 110 da Lei da Garda Síochána de 2005',
                'declaration' => 'Compreendo a Secção 110 da Lei da Garda Síochána de 2005',

                'information_legend' => 'Utilização das informações',
                'information' => 'Declaro ter conhecimento sobre a forma como a GSOC utiliza as minhas informações e como estas podem ser partilhadas.',

                'submit' => 'ENVIAR',

            ],

        'additional' =>
            [
                'behalf_note' => 'Se está a apresentar esta queixa, significa que é o "queixoso".',
                'date_of_birth' => 'Data de nascimento',
                'other_phone' => 'Outro número de contacto',
                'date_of_incident' => 'Data do incidente',
                'child_protection_description_simplified' => 'O bem-estar de uma criança é fundamental. Se a Garda Ombudsman tiver alguma informação sobre a proteção ou ao bem-estar de uma criança, tem a obrigação de a partilhar com a TUSLA, a Agência da Criança e da Família. Na medida do possível, abordaremos primeiro esta questão consigo.',

                'declaration_description_simplified' => 'Fornecer intencionalmente "informação falsa ou enganosa" à GSOC é crime. Pode ser alvo de uma ação judicial se houver suspeitas de que o fez, podendo resultar numa multa e/ou pena de prisão.
<br /><br />Em suma, as informações prestadas devem ser verdadeiras e não devem ter por objetivo induzir em erro a GSOC em qualquer queixa ou inquérito. Se prestar informações falsas ou suscetíveis de induzir em erro, poderá ter de pagar uma coima até 2 500 euros ou cumprir uma pena de prisão até 6 meses, ou ambas.',

                'information_description_simplified' => 'As suas informações podem ser divulgadas a terceiros, como a Garda Síochána, para nos permitir investigar eficazmente as queixas ou quando formos legalmente obrigados a fazê-lo. Este procedimento destina-se a garantir uma investigação eficaz da sua queixa.
<br /><br />Sempre que for necessário e adequado, partilharemos informações com outras partes em qualquer processo penal ou cível conexo. Relativamente a um processo criminal, isto inclui a partilha de informações com o Ministério Público, que pode depois partilhar essas informações com outras partes, como uma testemunha ou outras pessoas identificadas na sua queixa.
<br /><br />Num processo cível conexo, isto inclui a partilha de informações com outras partes, conforme possa ser necessário por lei, incluindo por um Tribunal de Justiça.',
            ]


    ];