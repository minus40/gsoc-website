<?php

return [

    'mismatch' => " Não existe nenhuma regra de validação para este campo.",
    'validate_required' => "Este campo é obrigatório",
    'validate_required_if' => "Este campo é obrigatório",
    'validate_valid_email' => "Este campo deve incluir um endereço de e-mail válido",
    'validate_min_len' => "O número mínimo de caracteres para este campo é :param",
    'validate_max_len' => "O número máximo de caracteres para este campo é :param",
    'validate_exact_len' => "O campo :attribute tem de ter exatamente :param caracteres de comprimento",
    'validate_alpha' => "O campo :attribute apenas pode conter caracteres alfabéticos(a-z)",
    'validate_alpha_space' => "O campo :attribute apenas pode conter caracteres alfabéticos(a-z) e espaços",
    'validate_alpha_numeric' => "O campo :attribute apenas pode conter caracteres alfanuméricos",
    'validate_alpha_dash' => "O campo :attribute apenas pode conter caracteres alfabéticos e travessões",
    'validate_numeric' => "Este campo só deve incluir caracteres numéricos",
    'validate_integer' => "Este campo só deve incluir um valor numérico",
    'validate_boolean' => "Este campo só deve incluir a resposta verdadeiro ou falso",
    'validate_valid_url' => "Este campo deve incluir um URL válido",
    'validate_url_exists' => "O URL :attribute não existe",
    'validate_valid_ip' => "O campo :attribute tem de conter um endereço IP válido",
    'validate_contains' => "O campo :attribute tem de conter um destes valores: :params,",
    'validate_contains_list' => "O campo :attribute tem de conter um valor da respetiva lista pendente",
    'validate_doesnt_contain_list' => "O campo :attribute contém um valor que não é aceite",
    'validate_street_address' => "O campo :attribute tem de ser um endereço de rua válido",
    'validate_date' => "Este campo deve incluir uma data válida (DD/MM/AAAA)",
    'custom_date' => "Este campo deve incluir uma data válida (DD/MM/AAAA)",
    'validate_min_numeric' => "O campo :attribute tem de ser um valor numérico igual ou superior a :param",
    'validate_max_numeric' => "O campo :attribute tem de ser um valor numérico igual ou inferior a :param",
    'validate_min_age' => "Este campo deve incluir uma idade igual ou superior :param",
    'validate_phone_number' => "O campo :attribute tem de ser um número de telefone válido",
    'validate_equalsfield' => "Os endereços de e-mail não coincidem",
    'validate_contact' => "Não apresentou um número de telefone nem um endereço de e-mail, o que dificultará o nosso contacto para podermos tratar de forma eficaz a sua queixa. Para podermos entrar em contacto consigo, volte atrás e introduza os dados em falta."

];