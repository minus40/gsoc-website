<?php

return [
    'title' => 'Confirmação',
    'paragraph_1' => 'Obrigado por ter apresentado a sua queixa à Garda Ombudsman.',
    'paragraph_2' => 'Se sair desta página, não poderá voltar a aceder ao formulário enviado.',
    'cta_button' => 'Guardar PDF',
    'subtitle' => 'Próximos passos',
    'list' => ' <ul>
                    <li>Na próxima semana, enviar-lhe-emos uma carta a confirmar que a sua queixa foi registada e que será tomada uma decisão sobre se a mesma pode ser aceite e tratada pela GSOC, de acordo com os critérios previstos na secção 87 da Lei da Garda Síochána de 2005.</li>
                    <li>Quando esta decisão for tomada, informá-lo-emos por escrito. Se aceitarmos a sua queixa, indicar-lhe-emos como será tratada. Leia sobre as formas de tratamento possíveis.</li>
                    <li>Normalmente, são necessários 3 a 9 meses para encerrar uma queixa. É possível que, durante este período, seja contactado pelo funcionário da GSOC ou pelo funcionário da Garda Síochána que está a tratar do seu caso, para lhe pedir mais informações e para o informar sobre o progresso do processo.</li>
                    <li>No final, comunicar-lhe-emos por escrito as conclusões e as eventuais medidas a tomar. Caso os seus dados de contacto sejam alterados, informe-nos por e-mail, para <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>.</li>
                </ul>'
];