<?php

return 
[

    'page' =>
    [
        'title' => 'How to make a complaint',
        'error_msg' => 'Please check the errors marked in red below, before submitting your form.',
        'yes' => 'Yes',
        'no' => 'No',
        'max_200_words' => 'Max 200 words.'
    ],

    'sections' => 
    [

        'complainant_introduction' => 
        '
            <div>
                <p>If you are making this complaint you are the <b>\'complainant\'</b>.</p>
               
                <p>You <b>must</b> fill in <b>all</b> the information on this form or else the processing of your complaint may be delayed.</p>

                <p><b>Before you start</b></p>
                <ul class="vertical-icons">
                    <li><a href="/make-a-complaint/before-you-complain/">Find out if GSOC is the right organisation to deal with your complaint.</a></li>
                    <li>You may also wish to view our <a href="/site-pages/website-terms/privacy-and-cookies/">Privacy</a> and <a href="/about-gsoc/data-protection-freedom-of-information/data-protection/">Data Protection policy</a>.</li>
                </ul>
            </div>
        ',
        
        'representative_introduction' => 
        '
            <div class="explainer">
                <h3>A Representative</h3>

                <p>If you are making this complaint on behalf of another person or a company, you will need to provide the consent of the person or a director of the company you are representing. </p>
                <ul class="vertical-icons">
                    <li>A <a href="#" class="consent-form-link" target="_blank">consent form</a> is provided here.</li>
                </ul>

                <h3>Under 18 Years Of Age</h3>

                <p>If you are under 18 years of age, you can make a complaint yourself or another person can make this complaint for you.</p>

                <p>If you ask another person to make this complaint for you, you will need to provide them with consent to make your complaint.</p>

                <ul class="vertical-icons">
                    <li>A <a href="#" class="consent-form-link" target="_blank">consent form</a> is provided here.</li>
                </ul>

                <p><i>The representative <b>must</b> fill in <b>all</b> the information on this form. If they do not fill in all the information we need, it may delay processing your complaint or will lead to no further action by GSOC.</i></p>
            </div>
        ',
        
        
        
        'complaint_garda_members_description' =>
        '
            <div class="explainer mb-5">
                <p>For example, their:</p>
                <ul class="vertical-icons">
                    <li>name</li>
                    <li>shoulder number</li>
                    <li>station</li>
                    <li>car</li>
                    <li>car registration number</li>
                <ul>
            </div>
        ',
        'complaint_description' => '
            <div class="explainer mb-5">
                <p>For example, what did they:</p>
                <ul class="vertical-icons">
                    <li>say?</li>
                    <li>do?</li>
                    <li>not do?</li>
                </ul>
                <p>This should be a <b>brief account</b> of the incident.</p>
            </div>
        ',
        'evidence_description' => 
        '
            <div class="explainer mb-5">
                <p>For example:</p>
                <ul class="vertical-icons">
                    <li>CCTV or other video footage</li>
                    <li>documents</li>
                </ul>
            </div>
        ',
        'injuries_description' => 
        '
            <div class="explainer mb-5">
                <p>Please, do not submit any medical reports or photos of the injury with this form. If required, we will ask you for these.</p>
            </div>
        ',


    'child_protection_description' => '<p>The welfare of a child is paramount. If the Garda Ombudsman has any concerns regarding the protection or welfare of a child, it has an obligation to share this information with TUSLA, the Child and Family Agency. Insofar as possible, we will discuss this with you first. Further information on Children First Legislation and TUSLA is available at <a href="https://www.tusla.ie" target="_blank">www.TUSLA.ie</a>.</p>',

    'declaration_description' => '<p>It is a criminal offence to knowingly supply "false or misleading information" to GSOC. You may be prosecuted if it appears that you have done this. This could result in a fine and/or a prison sentence.<br /><br />
In summary, the information you give must be truthful and not seek to mislead GSOC in any complaint or investigation.  If you give false or misleading information, you may have to pay a fine of up to &euro;2,500 or be imprisoned for up to 6 months, or both.<br /><br />
Please tick the box below to show that you have read the above and understand it.
</p>',
    'information_description' => '<p>Your information may be disclosed to third parties such as the Garda Síochána to enable us to effectively investigate complaints or where we are legally obliged to do so.  This is to ensure an effective investigation of your complaint.
<br /><br />Where it is necessary and proportionate to do so, we may share information with other parties in any related criminal or civil proceedings.  In relation to a criminal prosecution, this includes sharing information with the Director of Public Prosecutions who may then share this information with other parties like a witness or others identified in your complaint.  For information visit <a href="https://www.gardaombudsman.ie/about-gsoc/non-party-disclosure/">Non-Party Disclosure</a>.
<br /><br />In a related civil proceeding, this includes sharing information with other parties as may be necessitated by law including by a Court of Law.
<br /><br />For further information please follow these links to our <a href="https://www.gardaombudsman.ie/about-gsoc/data-protection-freedom-of-information/data-protection/">data protection policy</a> and <a href="https://www.gardaombudsman.ie/site-pages/website-terms/privacy-and-cookies/">privacy and cookie policy</a> or email <a href="mailto:data.protection@gsoc.ie">data.protection@gsoc.ie</a>.</p>',

    ],


    'sidebars' =>
    [
        'change_language_title' => 'Change language',
        'proof_title' => 'Why do we need this information?',
        'proof_content' => 'If we do not receive proof of consent or an explanation, we will probably not be able to deal with the complaint.',
        'help_title' => 'Need help?',
        'help_content' => 'Call us on 0818 600 800 if you need help filling out the complaint form. Caseworkers are available on the phone every weekday morning. At other times, you can leave a message requesting a call back, or an appointment with a caseworker.',
        'complainant_contact_title' => 'Let us keep in contact',
        'complainant_contact_content' => 'We will need to be able to contact you while we are dealing with your complaint. So please let us know, by e-mailing <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>, if your contact details change.',
        'representative_contact_title' => 'Why do we need this information?',
        'representative_contact_content' => 'We will need to be able to contact you while we are dealing with your complaint. So please let us know, by e-mailing <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>, if your contact details change.',

    ],


    'fields' =>
    [
        'complainant_legend' => 'Complainant\'s personal details',
        'complainant_name' => 'Name',
        'complainant_forename' => 'First name',
        'complainant_surname' => 'Surname',
        'complainant_primary_address_street' => 'Address line 1',
        'complainant_primary_address_street2' => 'Address line 2',
        'complainant_primary_address_street3' => 'Address line 3 (optional)',
        'complainant_primary_address_town' => 'Town or city',
        'complainant_primary_address_county' => 'County',
        'complainant_primary_address_postcode' => 'Eircode',
        'complainant_primary_address_country' => 'Country',
        'complainant_date_of_birth' => 'Date of birth (DD/MM/YYYY)',
        'complainant_primary_address_mobile' => 'Mobile phone number',
        'complainant_primary_address_phone' => 'Other phone number (home or work) (optional)',
        'complainant_primary_address_email' => 'E-mail address',
        'complainant_primary_address_email_confirm' => 'Confirm e-mail address',

        'representative_legend' => 'Representative\'s personal details',
        'representative_required_legend' => 'Do you require a representative?',
        'has_consent_form_legend' => 'Has the complainant signed the <a href="#" class="consent-form-link" target="_blank">consent form</a>?',
        'third_party_representative_legend' => 'What is your relationship to the complainant?',
        'third_party_representative_type_guardian' => 'Guardian',
        'third_party_representative_type_legal_representative' => 'Legal representative',
        'third_party_representative_type_parent' => 'Parent',
        'third_party_representative_type_social_worker' => 'Social worker',
        'third_party_representative_type_other' => 'Other',
        'third_party_representative_description_other' => 'Please describe \'other\'',
        'third_party_representative_name' => 'Name',
        'third_party_representative_forename' => 'First name',
        'third_party_representative_surname' => 'Surname',
        'third_party_representative_date_of_birth' => 'Date of birth (DD/MM/YYYY)',
        'third_party_representative_mobile' => 'Mobile phone number',
        'third_party_representative_email' => 'E-mail address',
        'third_party_representative_email_confirm' => 'Confirm e-mail address',

        'correspondence_legend' => 'If you wish the Garda Ombudsman to correspond with you at a different address to that provided for the complainant, please tick relevant box.',
        'correspondence_type_1' => 'Same as complainant',
        'correspondence_type_2' => 'Different address',

        'complainant_secondary_address_legend' => 'Address',

        'complainant_secondary_address_street' => 'Address line 1',
        'complainant_secondary_address_town' => 'Town or city',
        'complainant_secondary_address_county' => 'County',
        'complainant_secondary_address_country' => 'Country',
        'complainant_secondary_address_postcode' => 'Eircode',
        'complainant_secondary_address_mobile' => 'Mobile phone number',
        'complainant_secondary_address_phone' => 'Other phone number (home or work) (optional)',
        'complainant_secondary_address_email' => 'E-mail address',
        'complainant_secondary_address_email_confirm' => 'Confirm e-mail address',

        'complaint_legend' => 'The complaint',

        'complaint_late_submission_legend' => 'Late submission details',

        'complaint_date' => 'Date of incident (DD/MM/YYYY)',
        'complaint_time_reason' => 'If the incident occurred more than 12 months ago, please tell us why there has been a delay in reporting it.',
        'complaint_time' => 'Time of incident',
        'complaint_location' => 'Location of incident',
        'complaint_garda_members' => 'Do you have any information that could help identify the Garda who you wish to complain about?',
        'complaint_details' => 'Please describe the specific misbehaviour you are alleging the Garda carried out.',
        'complaint_lead_up' => 'What happened before the incident?',
        'complaint_motive' => 'Why do you think the garda behaved that way?',

        'complaint_witnesses_legend' => 'Are there any witnesses to the incident?',
        'complaint_witnesses' => 'Please give the full names and contact details of any witnesses here.',
        'complaint_evidence_legend' => 'Is there any other evidence?',
        'complaint_evidence' => 'If \'Yes\': what is the evidence?',
        'complaint_injuries_legend' => 'Did you have any injuries or need medical treatment following the incident?',
        'complaint_injuries' => 'If \'Yes\': Describe any injuries or medical treatment you received after the incident.',
    
        'child_protection_legend' => 'Child protection and welfare obligation of GSOC',
        'child_protection' => 'I understand the Child Protection and Welfare Obligations of GSOC',

        'declaration_legend' => 'Section 110 Garda Síochána Act, 2005',
        'declaration' => 'I understand Section 110 of Garda Síochána Act',

        'information_legend' => 'Use of your information',
        'information' => 'I acknowledge the basis which GSOC use my information and how it may be shared',

        'submit' => 'Submit',

    ],

    'additional' =>
        [
            'behalf_note' => 'If you are making this complaint on behalf of another person or a company, you will need to provide the consent of the person or a director of the company you are representing.',
            'date_of_birth' => 'Date of birth',
            'other_phone' => 'Other phone number',
            'date_of_incident' => 'Date of incident',
            'child_protection_description_simplified' => 'The welfare of a child is paramount. If the Garda Ombudsman has any concerns regarding the protection or welfare of a child, it has an obligation to share this information with TUSLA, the Child and Family Agency. Insofar as possible, we will discuss this with you first.',
            'declaration_description_simplified' => 'It is a criminal offence to knowingly supply "false or misleading information" to GSOC. You may be prosecuted if it appears that you have done this. This could result in a fine and/or a prison sentence.<br /><br />
In summary, the information you give must be truthful and not seek to mislead GSOC in any complaint or investigation.  If you give false or misleading information, you may have to pay a fine of up to &euro;2,500 or be imprisoned for up to 6 months, or both.<br /><br />',
            'information_description_simplified' => 'Your information may be disclosed to third parties such as the Garda Síochána to enable us to effectively investigate complaints or where we are legally obliged to do so.  This is to ensure an effective investigation of your complaint.
<br /><br />Where it is necessary and proportionate to do so, we may share information with other parties in any related criminal or civil proceedings.  In relation to a criminal prosecution, this includes sharing information with the Director of Public Prosecutions who may then share this information with other parties like a witness or others identified in your complaint.
<br /><br />In a related civil proceeding, this includes sharing information with other parties as may be necessitated by law including by a Court of Law.',
        ]


];