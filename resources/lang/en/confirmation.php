<?php

return [
    'title' => 'Confirmation',
    'paragraph_1' => 'Thank you for submitting your complaint to the Garda Ombudsman.',
    'paragraph_2' => 'Once you leave this website, you will not be able to access your submitted form again.',
    'cta_button' => 'Save PDF',
    'subtitle' => 'Next Steps',
    'list' => ' <ul>
                    <li>Within the next week, we will send you a letter acknowledging that your complaint has been logged and that a decision is to be made on whether it can be admitted and dealt with by GSOC, according to the criteria in section 87 of the Garda Síochána Act 2005.</li>
                    <li>When this decision is made, we will write to let you know the decision. If we can admit your complaint, we will tell you how it is going to be dealt with. Read about the possible ways it could be dealt with.</li>
                    <li>It usually takes 3-9 months to close a complaint. You will most likely be contacted during this time by the GSOC officer or the Garda Síochána officer dealing with your case, to request further information and give you updates on their progress.</li>
                    <li>At the end, we will write and tell you the findings and any further action to be taken. Please let us know, by emailing <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>, if your contact details change.</li>
                </ul>'
];