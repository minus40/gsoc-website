<?php

return [


    'change_language' => 'Change Language',
    'pre_title' => 'How to make a complaint',
    'pre_subtitle' => '',
    'pre_section_1_title' => '',
    'pre_section_1_content' => '',
    'pre_section_2_title' => '',
    'pre_section_2_content' => '
        <p>If you are making this complaint you are the <b>\'complainant\'</b>.</p>
       
        <p>You <b>must</b> fill in <b>all</b> the information on this form or else the processing of your complaint may be delayed.</p>

        <p><b>Before you start</b></p>
        <ul class="vertical-icons">
            <li><a href="/make-a-complaint/before-you-complain/">Find out if GSOC is the right organisation to deal with your complaint.</a></li>
            <li>You can complete the form in stages by saving it and returning to it later.</li>
            <li>You may also wish to view our <a href="/site-pages/website-terms/privacy-and-cookies/">Privacy</a> and <a href="/about-gsoc/data-protection-freedom-of-information/data-protection/">Data Protection policy</a>.</li>
        </ul>',

    'post_section_2_content' => '
<h5><b>A Representative</b></h5>

<p>If you are making this complaint on behalf of another person or a company, you will need to provide the consent of the person or a director of the company you are representing. </p>
<ul class="vertical-icons">
    <li>A <a href="/app/uploads/2017/05/gsoc-consent-form-2017.pdf" target="_blank">consent form</a> is provided here.</li>
</ul>

<h5><b>Under 18 Years Of Age</b></h5>

<p>If you are under 18 years of age, you can make a complaint yourself or another person can make this complaint for you.</p>

<p>If you ask another person to make this complaint for you, you will need to provide them with consent to make your complaint.</p>

<ul class="vertical-icons">
    <li>A <a href="/app/uploads/2017/05/gsoc-consent-form-2017.pdf" target="_blank">consent form</a> is provided here.</li>
</ul>

<p><i>The representative <b>must</b> fill in <b>all</b> the information on this form. If they do not fill in all the information we need, it may delay processing your complaint or will lead to no further action by GSOC.</i></p>
',

    'pre_section_3_title' => '',
    'pre_section_3_subtitle_1' => '',
    'pre_section_3_content_1' => '',
    'pre_section_3_subtitle_2' => '',
    'pre_section_3_content_2' => '',
    'pre_section_3_subtitle_3' => '',
    'pre_section_3_content_3' => '',
    'pre_section_4_title' => '',
    'pre_section_4_content' => '',

    'help_header_1' => 'Why do we need this information?',
    'help_header_2_a' => 'Need help?',
    'help_header_2_b' => 'Let us keep in contact',
    'help_header_3' => 'Why do we need this information?',
    'help_header_4' => 'Why do we need this information?',
    'help_header_5' => 'What information will help identify a garda?',
    'help_header_6' => 'More than one garda?',
    'help_header_7' => 'What counts as evidence?',
    'help_header_7a' => 'What information will we need?',
    'help_header_8' => 'Why do we need this information?',

    'section_1_title' => 'About You',
    'section_1_help' => 'If we do not receive proof of consent or an explanation, we will probably not be able to deal with the complaint.',

    'behalf' => 'Is this complaint being made on behalf of a third party?',
    'behalf_no' => 'No',
    'behalf_yes' => 'Yes',
    'behalf_description' => 'If you are making a complaint on behalf of another person (such as a family member, friend or vulnerable person), you will need to print the <a href="https://gardaombudsman.ie/app/uploads/2017/05/gsoc-consent-form-2017.pdf" title="Consent Form">consent form</a> and ask them to sign it. Please send the signed form to the Garda Ombudsman by post, or scan it and send it as an e-mail attachment to <a href="mailto:complaints@gsoc.ie" title="Send Consent Form By Email">complaints@gsoc.ie</a>. If the person is unable to give their consent, because of age, or a mental or physical condition, please send us some documentation which explains that instead.',

    'section_2_title' => 'Complainant’s personal details',
    'section_2_subtitle' => 'Address',
    'section_2_help_1' => 'Call us on 0818 600 800 if you need help filling out the complaint form. Caseworkers are available on the phone every weekday morning. At other times, you can leave a message requesting a call back, or an appointment with a caseworker.',
    'section_2_help_2' => 'We will need to be able to contact you while we are dealing with your complaint. So please let us know, by e-mailing <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>, if your contact details change.',

    'section_3_title' => 'Representative’s personal details',
    'section_3_subtitle' => 'Address',
    'pre_section_3_content' => '',
    'section_3_help' => 'We will need to be able to contact you while we are dealing with your complaint. So please let us know, by e-mailing <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>, if your contact details change.',

    'behalf_details' =>  'Following section must be completed only where consent is required.<br />If you are acting as a <b>representative</b> of someone who is under the age of 18, please enter your own details in this form.',
    'third_party_representative_type' => 'What is your relationship to the complainant?',
    // Fix Dropdown
    'third_party_representative_type_guardian' => 'Guardian',
    'third_party_representative_type_legal_representative' => 'Legal Representative',
    'third_party_representative_type_parent' => 'Parent',
    'third_party_representative_type_social_worker' => 'Social Worker',
    'third_party_representative_type_other' => 'Other',

    'third_party_representative_description_other' => 'Please describe \'other\'',

    'representative_required_title' => 'Do you require a representative?',
    'representative_required_description' => '',
    'representative_required_yes' => 'Yes',
    'representative_required_no' => 'No',


    'has_consent_form_title' => 'Consent',
    'has_consent_form_description' => 'Has the complainant signed the <a href="/app/uploads/2017/05/gsoc-consent-form-2017.pdf" target="_blank">consent form</a>?',
    'has_consent_form_check' => 'Yes',
    'has_consent_form_false' => 'No',

    // Fix Dropdown
    'correspondence_description' => 'If you wish the Garda Ombudsman to correspond with you at a different address to that provided for the complainant, please tick relevant box.',
    'correspondence_same' => 'Same as complainant',
    'correspondence_different' => 'Different address',

    'section_4_title' => 'The complaint',
    'section_4_help' => 'The Garda Ombudsman can deal with a complaint made outside of the time limit if there is good reason to do so. There are practical reasons for the time limit.  The further back in time we go, the more likely it is that evidence e.g. CCTV footage, no longer exists. Equally importantly, with the passage of time it becomes more likely that gardaí complained about will have retired so the Garda Ombudsman may not have jurisdiction over them.',

    'complaint_time_reason_description' => 'The legal time limit to make a complaint is up to one year after the incident you are complaining about.',
    'complaint_time_reason' => 'If the incident occurred more than 12 months ago, please tell us why there has been a delay in reporting it.',

    'section_5_help' => 'Information such as their name, shoulder number, station, description, car registration.',

    'complaint_garda_members' => 'Do you have any information that could help identify the Garda who you wish to complain about?',
    'complaint_garda_members_description' => '
        <p>For example, their:</p>
        <ul class="vertical-icons">
        <li>name</li>
        <li>shoulder number</li>
        <li>station</li>
        <li>car</li>
        <li>car registration number</li>
        <ul>
    ',

    'complaint_date' => 'Date of Incident (DD/MM/YYYY)*',
    'complaint_time' => 'Time of Incident',
    'complaint_location' => 'Location of Incident',

    'section_6_help' => 'If your complaint relates to more than one garda, please be clear about the behaviour complained of in relation to each one.',

    'complaint_details' => 'Please describe the specific misbehaviour you are alleging the Garda carried out. *',
    'complaint_description' => '
        <p>For example, what did they:</p>
        <ul class="vertical-icons">
        <li>say?</li>
        <li>do?</li>
        <li>not do?</li>
        </ul>
        <p>This should be a <b>brief account</b> of the incident.</p>
    ',


    'complaint_lead_up' => 'What happened before the incident?',
    'complaint_motive' => 'Why do you think the garda behaved that way?',
    'complaint_witnesses' => 'Please give the full names and contact details of any witnesses here.',

    'section_7_help' => 'Examples are CCTV or other video showing the incident, witness statements, medical records, or other documentation which supports your allegations.',
    'section_7a_help' => 'We will need to gather evidence to support your statement, for example photos of any injuries, or medical records from your doctor or hospital. We will ask for your written permission to access your medical records.',

    'complaint_evidence' => 'If \'Yes\': what is the evidence?',
    'complaint_injuries' => 'If \'Yes\': Describe any injuries or medical treatment you received after the incident.',

    'section_8_help' => 'If you have already tried to have this issue dealt with by the Garda Síochána themselves, we will ask them about any actions taken to try to resolve it.',
    'section_8_title' => 'Previous complaint made at a Garda Station (if relevant)',

    'section_9_title_0' => 'Child protection and welfare obligation of GSOC *',
    'section_9_title_1' => 'Section 110 Garda Síochána Act, 2005 *',
    'section_9_title_2' => 'Use of your information *',

    'protection_description' => '<p>The welfare of a child is paramount. If the Garda Ombudsman has any concerns regarding the protection or welfare of a child, it has an obligation to share this information with TUSLA, the Child and Family Agency. Insofar as possible, we will discuss this with you first. Further information on Children First Legislation and TUSLA is available at <a href="https://www.tusla.ie" target="_blank">www.TUSLA.ie</a>.</p>',

    'declaration_description' => '<p>It is a criminal offence to knowingly supply "false or misleading information" to GSOC. You may be prosecuted if it appears that you have done this. This could result in a fine and/or a prison sentence.<br /><br />
In summary, the information you give must be truthful and not seek to mislead GSOC in any complaint or investigation.  If you give false or misleading information, you may have to pay a fine of up to &euro;2,500 or be imprisoned for up to 6 months, or both.<br /><br />
Please tick the box below to show that you have read the above and understand it.
</p>',
    'information_description' => '<p>Your information may be disclosed to third parties such as the Garda Síochána to enable us to effectively investigate complaints or where we are legally obliged to do so.  This is to ensure an effective investigation of your complaint.
<br /><br />Where it is necessary and proportionate to do so, we may share information with other parties in any related criminal or civil proceedings.  In relation to a criminal prosecution, this includes sharing information with the Director of Public Prosecutions who may then share this information with other parties like a witness or others identified in your complaint.  For information visit <a href="https://www.gardaombudsman.ie/about-gsoc/non-party-disclosure/">Non-Party Disclosure</a>.
<br /><br />In a related civil proceeding, this includes sharing information with other parties as may be necessitated by law including by a Court of Law.
<br /><br />For further information please follow these links to our <a href="https://www.gardaombudsman.ie/about-gsoc/data-protection-freedom-of-information/data-protection/">data protection policy</a> and <a href="https://www.gardaombudsman.ie/site-pages/website-terms/privacy-and-cookies/">privacy and cookie policy</a> or email <a href="mailto:data.protection@gsoc.ie">data.protection@gsoc.ie</a>.</p>',

    'submit' => 'Submit',

    'forename' => 'Name*',
    'forename_placeholder' => 'First name',
    'surname_placeholder' => 'Surname',
    'date_of_birth' => 'Date of birth (DD/MM/YYYY)*',
    'third_party_date_of_birth' => 'Date of birth (DD/MM/YYYY)',
    'gender' => 'Gender',
    
    // Fix Dropdown
    'gender_male' => 'Male',
    'gender_female' => 'Female',
    'gender_other' => 'Prefer not to say',
    // Fix Dropdown
    'street' => 'Address Line 1',
    'street2' => 'Address Line 2',
    'street3' => 'Address Line 3 (Optional)',
    'town' => 'Town or city',
    'county' => 'County',
    'postcode' => 'Eircode',
    'country' => 'Country',
    'mobile' => 'Mobile phone number',
    'phone' => 'Other phone number (home or work) (optional)',
    'email' => 'E-mail address*',
    'email_confirm' => 'Confirm e-mail address',




    'has_witnesses_title' => '',
    'has_witnesses_description' => 'Are there any witnesses to the incident?',
    'has_witnesses_true' => 'Yes',
    'has_witnesses_false' => 'No',


    'has_evidence_title' => '',
    'has_evidence_description' => 'Is there any other evidence?',
    'has_evidence_true' => 'Yes',
    'has_evidence_false' => 'No',
    'has_evidence_explainer' => '
        <p>For example:</p>
        <ul class="vertical-icons">
        <li>CCTV or other video footage</li>
        <li>documents</li>
        </ul>
    ',

    'has_injuries_title' => '',
    'has_injuries_description' => 'Did you have any injuries or need medical treatment following the incident?',
    'has_injuries_true' => 'Yes',
    'has_injuries_false' => 'No',
    'has_injuries_explainer' => 'Please, do not submit any medical reports or photos of the injury with this form. If required, we will ask you for these.',
];