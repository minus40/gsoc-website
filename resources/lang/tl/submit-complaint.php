<?php

return
    [

        'page' =>
            [
                'title' => 'Paano gumawa ng reklamo',
                'error_msg' => 'Pakisuri ang mga mali na minarkahan ng pula sa ibaba, bago isumite ang inyong form.',
                'yes' => 'Hindi',
                'no' => 'Oo',
                'max_200_words' => 'Max na 200 salita.'
            ],

        'sections' =>
            [

                'complainant_introduction' =>
                    '
            <div>
                <p>Kung kayo ang gumagawa ng ganitong reklamo, kayo ang <b>‘nagrereklamo’</b>.</p>
               
                <p>Dapat niyong punan ang lahat ng impormasyon sa form na ito o kung hindi ay maaaring maantala ang pagproseso ng inyong reklamo.</p>

                <p><b>Bago kayo magsimula:</b></p>
                <ul class="vertical-icons">
                    <li><a href="/make-a-complaint/before-you-complain/">Alamin kung ang GSOC ay ang tamang organisasyon na mag-aasikaso sa inyong reklamo</a></li>
                    <li>Maaari niyo ring hilingin na tingnan ang aming patakaran sa Proteksyon ng <a href="/site-pages/website-terms/privacy-and-cookies/">Privacy</a> at <a href="/about-gsoc/data-protection-freedom-of-information/data-protection/">Data</a>.</li>
                </ul>
            </div>
        ',

                'representative_introduction' =>
                    '
            <div class="explainer">
                <h3>Isang Kinatawan</h3>

                <p>Kung ginagawa niyo ang reklamong ito sa ngalan ng isa pang tao o isang kumpanya, kakailanganin niyong ibigay ang pahintulot ng tao o ng isang director ng kumpanya na inyong kinakatawan.</p>
                <ul class="vertical-icons">
                    <li>Ang isang <a href="#" class="consent-form-link" target="_blank">form ng pahintulot</a> ay ibinigay dito.</li>
                </ul>

                <h3>Wala pang 18 Taong Gulang</h3>

                <p>Kung wala pa kayong 18 taong gulang, maaaring kayo na ang gumawa ng reklamo o maaaring gawin ng isa pang tao ang reklamong ito para sa inyo.</p>

                <p>Kung hihilingin niyo sa isa pang tao na gawin ang reklamong ito para sa inyo, kakailanganin niyong bigyan sila ng pahintulot para gawin ang inyong reklamo.</p>

                <ul class="vertical-icons">
                    <li>Ang isang <a href="#" class="consent-form-link" target="_blank">form ng pahintulot</a> ay ibinigay dito.</li>
                </ul>

                <p><i>Ang kinatawan ay dapat na punan ang lahat ng impormasyon sa form na ito. Kung hindi niyo pupunan ang lahat ng impormasyon na kailangan namin, maaaring maantala nito ang pagproseso ng inyong reklamo o hahantong sa kawalan ng higit pang aksyon ng GSOC.</i></p>
            </div>
        ',



                'complaint_garda_members_description' =>
                    '
            <div class="explainer mb-5">
                <p>Halimbawa, kanilang:</p>
                <ul class="vertical-icons">
                    <li>Pangalan</li>
                    <li>Shoulder number</li>
                    <li>Istasyon</li>
                    <li>Sasakyan</li>
                    <li>Numero ng rehistro ng sasakyan</li>
                <ul>
            </div>
        ',
                'complaint_description' => '
            <div class="explainer mb-5">
                <p>Halimbawa, ano ang kanilang:</p>
                <ul class="vertical-icons">
                    <li>Sinabi?</li>
                    <li>Ginawa?</li>
                    <li>Hindi ginawa?</li>
                </ul>
                <p>Ito ay dapat na isang maikling salaysay ng insidente.</p>
            </div>
        ',
                'evidence_description' =>
                    '
            <div class="explainer mb-5">
                <p>Halimbawa:</p>
                <ul class="vertical-icons">
                    <li>Kuha ng CCTV o iba pang video</li>
                    <li>Mga dokumento</li>
                </ul>
            </div>
        ',
                'injuries_description' =>
                    '
            <div class="explainer mb-5">
                <p>Pakiusap, huwag magsumite ng anumang mga medikal na ulat o larawan ng pinsala kasama ng form na ito. Kung kakailanganin, hihingin namin ang mga ito sa inyo.</p>
            </div>
        ',


                'child_protection_description' => '<p>Ang kapakanan ng isang bata ay nakahihigit sa lahat. Kung ang Garda Ombudsman ay may anumang pagkabahala patungkol sa proteksyon o kapakanan ng isang bata, may obligasyon itong ibahagi ang impormasyong ito sa TUSLA, ang Ahensya ng Bata at Pamilya. Hangga’t maaari, amin munang tatalakayin ito sa inyo. Ang higit pang impormasyon sa Lehislasyon na Una ang Bata (Children First Legislation) at TUSLA ay makukuha sa <a href="https://www.tusla.ie" target="_blank">www.TUSLA.ie</a>.</p>',

                'declaration_description' => '<p>Isang kriminal na pagkakasala ang sadyang magtustos ng "maling impormasyon o mapanligaw na impormasyon" sa GSOC. Maaari kayong kasuhan kung lumalabas na ginawa niyo ito. Maaari itong magresulta sa isang multa at/o sa isang sentensiya sa bilangguan.
<br /><br />Sa kabuuan, ang impormasyon na inyong ibinibigay ay dapat na makatotohanan at hindi naglalayong iligaw ang GSOC sa anumang reklamo o imbestigasyon. Kung magbibigay kayo ng mali o mapanligaw na impormasyon, maaaring kailanganin niyong magbayad ng multa ng hanggang €2,500 o makulong ng hanggang sa 6 na buwan, o pareho.
<br /><br />Mangyaring kudlitan ang kahon sa ibaba para ipakita na inyo nang nabasa ang nasa itaas at nauunawaan ito.</p>',

                'information_description' => '<p>Ang inyong impormasyon ay maaaring ibunyag sa mga ikatlong partido (third parties) tulad ng Garda Síochána upang magpagana sa amin na imbestigahan nang epektibo ang mga reklamo o kung saan kami ay legal na inoobliga na gawin ito. Ito ay upang matiyak ang isang epektibong imbestigasyon sa inyong reklamo.
<br /><br />Kung saan ay kinakailangan at proporsyonal na gawin ito, maaari kaming magbahagi ng impormasyon sa iba pang mga partido sa anumang nauugnay na kriminal o sibil na paglilitis. Kaugnay ng isang kriminal na pag-uusig, kabilang dito ang pagbabahagi ng impormasyon sa Direktor ng Mga Pampublikong Pag-uusig (Director of Public Prosecutions) na maaaring magbahagi ng impormasyong ito sa iba pang mga partido tulad ng isang saksi o iba pang kinilala sa inyong reklamo. Para sa impormasyon, bisitahin ang <a href="https://www.gardaombudsman.ie/about-gsoc/non-party-disclosure/">Walang Partidong Pagbubunyag</a>.
<br /><br />Sa isang may kaugnayang sibil na paglilitis, kabilang dito ang pagbabahagi ng impormasyon sa iba pang mga partido na maaaring atasan ng batas kabilang ang isang Hukuman ng Batas. 
<br /><br />Para sa higit pang impormasyon, mangyaring sundan ang mga link na ito sa aming <a href="https://www.gardaombudsman.ie/about-gsoc/data-protection-freedom-of-information/data-protection/">patakaran sa proteksyon ng data</a> at <a href="https://www.gardaombudsman.ie/site-pages/website-terms/privacy-and-cookies/">patakaran sa privacy at cookie</a> o i-email ang <a href="mailto:data.protection@gsoc.ie">data.protection@gsoc.ie</a>.</p>',

            ],


        'sidebars' =>
            [
                'change_language_title' => 'Baguhin ang Wika',
                'proof_title' => 'Bakit namin kailangan ang impormasyong ito?',
                'proof_content' => 'Kung hindi kami makakatanggap ng patunay ng pahintulot o isang pagpapaliwanag, marahil ay hindi namin magagawang maasikaso ang reklamo.',
                'help_title' => 'Kailangan ng tulong?',
                'help_content' => 'Tawagan kami sa 0818 600 800 kung kailangan niyo ng tulong sa pagpunan sa form ng reklamo. Ang mga caseworker ay makakausap sa telepono sa umaga sa araw na may pasok. Sa iba pang mga oras, maaari kayong mag-iwan ng mensahe na humihiling ng isang pabalik na tawag, o isang tipanan (appointment) sa isang caseworker.',
                'complainant_contact_title' => 'Manatili tayong nakikipag-ugnayan',
                'complainant_contact_content' => 'Kakailanganin namin na magawang makipag-ugnayan sa inyo habang aming inaasikaso ang inyong reklamo. Kaya’t mangyaring ipaalam sa amin, sa pamamagitan ng pag-e-mail sa <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>, kung ang inyong detalye ng kontak ay magbabago.',
                'representative_contact_title' => 'Bakit namin kailangan ang impormasyong ito?',
                'representative_contact_content' => 'Kakailanganin namin na magawang makipag-ugnayan sa inyo habang aming inaasikaso ang inyong reklamo. Kaya’t mangyaring ipaalam sa amin, sa pamamagitan ng pag-e-mail sa <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>, kung ang inyong detalye ng kontak ay magbabago.',

            ],


        'fields' =>
            [
                'complainant_legend' => 'Mga personal na detalye ng nagrereklamo',
                'complainant_name' => 'Pangalan',
                'complainant_forename' => 'Unang pangalan',
                'complainant_surname' => 'Apelyido',
                'complainant_primary_address_street' => 'Linya ng Address 1',
                'complainant_primary_address_street2' => 'Linya ng Address 2',
                'complainant_primary_address_street3' => 'Linya ng Address 3 (Opsyonal)',
                'complainant_primary_address_town' => 'Bayan o lungsod',
                'complainant_primary_address_county' => 'Probinsya',
                'complainant_primary_address_postcode' => 'Eircode',
                'complainant_primary_address_country' => 'Bansa',
                'complainant_date_of_birth' => 'Petsa ng kapanganakan (DD/MM/YYYY)',
                'complainant_primary_address_mobile' => 'Numero ng mobile na telepono',
                'complainant_primary_address_phone' => 'Iba pang numero ng telepono (bahay o trabaho) (opsyonal)',
                'complainant_primary_address_email' => 'E-mail address',
                'complainant_primary_address_email_confirm' => 'Kumpirmahin ang e-mail address',

                'representative_legend' => 'Personal na detalye ng kinatawan',
                'representative_required_legend' => 'Kinakailangan niyo ba ng isang kinatawan?',
                'has_consent_form_legend' => 'Pinirmahan na ba ng nagrereklamo ang <a href="#" class="consent-form-link" target="_blank">form ng pahintulot</a>?',
                'third_party_representative_legend' => 'Ano ang relasyon niyo sa nagrereklamo?',
                'third_party_representative_type_guardian' => 'Tagapag-alaga',
                'third_party_representative_type_legal_representative' => 'Legal na Kinatawan',
                'third_party_representative_type_parent' => 'Magulang',
                'third_party_representative_type_social_worker' => 'Manggagawang Panlipunan',
                'third_party_representative_type_other' => 'Iba pa',
                'third_party_representative_description_other' => 'Mangyaring ilarawan ang ‘iba pa’',
                'third_party_representative_name' => 'Pangalan',
                'third_party_representative_forename' => 'Unang pangalan',
                'third_party_representative_surname' => 'Apelyido',
                'third_party_representative_date_of_birth' => 'Petsa ng kapanganakan (DD/MM/YYYY)',
                'third_party_representative_mobile' => 'Numero ng mobile na telepono',
                'third_party_representative_email' => 'E-mail address',
                'third_party_representative_email_confirm' => 'Kumpirmahin ang e-mail address',

                'correspondence_legend' => 'Kung nais niyong sulatan kayo ng Garda Ombudsman sa address na iba sa ibinigay para sa nagrereklamo, mangyaring kudlitan ang nauugnay na kahon.',
                'correspondence_type_1' => 'Pareho nang sa nagrereklamo',
                'correspondence_type_2' => 'Ibang address',

                'complainant_secondary_address_legend' => 'Address',

                'complainant_secondary_address_street' => 'Linya ng Address 1',
                'complainant_secondary_address_town' => 'Bayan o lungsod',
                'complainant_secondary_address_county' => 'Probinsya',
                'complainant_secondary_address_postcode' => 'Eircode',
                'complainant_secondary_address_country' => 'Bansa',
                'complainant_secondary_address_mobile' => 'Numero ng mobile na telepono',
                'complainant_secondary_address_phone' => 'Iba pang numero ng telepono (bahay o trabaho) (opsyonal)',
                'complainant_secondary_address_email' => 'E-mail address',
                'complainant_secondary_address_email_confirm' => 'Kumpirmahin ang e-mail address',

                'complaint_legend' => 'Ang reklamo',

                'complaint_late_submission_legend' => 'Mga detalye ng nahuling pagsusumite ',

                'complaint_date' => 'Petsa ng Insidente (DD/MM/YYYY)',
                'complaint_time_reason' => 'Kung ang insidente ay nangyari nang higit 12 buwan na ang nakalipas, pakisabi sa amin kung bakit may pagkaantala sa pag-uulat nito.',
                'complaint_time' => 'Oras ng Insidente',
                'complaint_location' => 'Lokasyon ng Insidente',
                'complaint_garda_members' => 'Mayroon ba kayong anumang impormasyon na maaaring makatulong na makilala ang Garda na nais niyong ireklamo?',
                'complaint_details' => 'Mangyaring ilarawan ang partikular na maling pag-uugali na inyong ipinaparatang na ginawa ng Garda.',
                'complaint_lead_up' => 'Ano ang nangyari bago ang insidente?',
                'complaint_motive' => 'Bakit sa palagay niyo ay umasal ang garda sa ganoong paraan?',

                'complaint_witnesses_legend' => 'Mayroon bang mga saksi sa insidente?',
                'complaint_witnesses' => 'Kung ‘Oo’: Mangyaring ibigay dito ang mga buong pangalan at detalye ng kontak ng sinumang saksi.',
                'complaint_evidence_legend' => 'Mayroon pa bang ibang ebidensya?',
                'complaint_evidence' => 'Kung ‘Oo’: ano ang ebidensya?',
                'complaint_injuries_legend' => 'Nagkaroon ba kayo ng anumang pinsala o nangailangan ng medikal na paggamot kasunod ng insidente?',
                'complaint_injuries' => 'Kung \'Oo\': Ilarawan ang anumang pinsala o medikal na paggamot na inyong tinanggap pagkatapos ng insidente.',

                'child_protection_legend' => 'Mga obligasyon ng GSOC sa proteksyon at kapakanan ng bata',
                'child_protection' => 'Aking nauunawaan ang Mga Obligasyon ng GSOC sa Proteksyon at Kapakanan ng Bata',

                'declaration_legend' => 'Seksyon 110 Garda Síochána Act, 2005',
                'declaration' => 'Aking nauunawaan ang Seksyon 110 ng Garda Síochána Act, 2005',

                'information_legend' => 'Paggamit ng Impormasyon',
                'information' => 'Aking kinikilala ang batayan sa kung saan ginagamit ng GSOC ang aking impormasyon at kung paano ito maaaring ibahagi',

                'submit' => 'ISUMITE',

            ],

        'additional' =>
            [
                'behalf_note' => 'Kung ginagawa niyo ang reklamong ito sa ngalan ng isa pang tao o isang kumpanya, kakailanganin niyong ibigay ang pahintulot ng tao o ng isang director ng kumpanya na inyong kinakatawan.',
                'date_of_birth' => 'Petsa ng kapanganakan',
                'other_phone' => 'Iba pang numero ng telepono',
                'date_of_incident' => 'Petsa ng Insidente',
                'child_protection_description_simplified' => 'Ang kapakanan ng isang bata ay nakahihigit sa lahat. Kung ang Garda Ombudsman ay may anumang pagkabahala patungkol sa proteksyon o kapakanan ng isang bata, may obligasyon itong ibahagi ang impormasyong ito sa TUSLA, ang Ahensya ng Bata at Pamilya. Hangga’t maaari, amin munang tatalakayin ito sa inyo.',

                'declaration_description_simplified' => 'Isang kriminal na pagkakasala ang sadyang magtustos ng "maling impormasyon o mapanligaw na impormasyon" sa GSOC. Maaari kayong kasuhan kung lumalabas na ginawa niyo ito. Maaari itong magresulta sa isang multa at/o sa isang sentensiya sa bilangguan.
<br /><br />Sa kabuuan, ang impormasyon na inyong ibinibigay ay dapat na makatotohanan at hindi naglalayong iligaw ang GSOC sa anumang reklamo o imbestigasyon. Kung magbibigay kayo ng mali o mapanligaw na impormasyon, maaaring kailanganin niyong magbayad ng multa ng hanggang €2,500 o makulong ng hanggang sa 6 na buwan, o pareho.',

                'information_description_simplified' => 'Ang inyong impormasyon ay maaaring ibunyag sa mga ikatlong partido (third parties) tulad ng Garda Síochána upang magpagana sa amin na imbestigahan nang epektibo ang mga reklamo o kung saan kami ay legal na inoobliga na gawin ito. Ito ay upang matiyak ang isang epektibong imbestigasyon sa inyong reklamo.
<br /><br />Kung saan ay kinakailangan at proporsyonal na gawin ito, maaari kaming magbahagi ng impormasyon sa iba pang mga partido sa anumang nauugnay na kriminal o sibil na paglilitis. Kaugnay ng isang kriminal na pag-uusig, kabilang dito ang pagbabahagi ng impormasyon sa Direktor ng Mga Pampublikong Pag-uusig (Director of Public Prosecutions) na maaaring magbahagi ng impormasyong ito sa iba pang mga partido tulad ng isang saksi o iba pang kinilala sa inyong reklamo.
<br /><br />Sa isang may kaugnayang sibil na paglilitis, kabilang dito ang pagbabahagi ng impormasyon sa iba pang mga partido na maaaring atasan ng batas kabilang ang isang Hukuman ng Batas. ',
            ]


    ];