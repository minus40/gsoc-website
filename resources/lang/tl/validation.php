<?php

return [

    'mismatch' => "Walang tuntunin ng balidasyon para sa patlang na ito",
    'validate_required' => "Ang patlang na ito ay kinakailangan",
    'validate_required_if' => "Ang patlang na ito ay kinakailangan",
    'validate_valid_email' => "Ang patlang na ito ay kailangan na maging isang balidong email address",
    'validate_min_len' => "Ang minimum na bilang ng karakter para sa patlang na ito ay :param",
    'validate_max_len' => "Ang maximum na bilang ng karakter para sa patlang na ito ay :param",
    'validate_exact_len' => "Kinakailangang eksaktong :param (na) character ang :attribute",
    'validate_alpha' => "Mga alpha na character (a-z) lang ang maaaring laman ng :attribute",
    'validate_alpha_space' => "Mga alpha na character (a-z) at espasyo lang ang maaaring laman ng :attribute",
    'validate_alpha_numeric' => "Mga alpha-numeric na character lang ang maaaring laman ng :attribute",
    'validate_alpha_dash' => "Mga alpha na character at dash lang ang maaaring laman ng :attribute",
    'validate_numeric' => "Ang patlang na ito ay maaari lamang maglaman ng mga numerikong karakter",
    'validate_integer' => "Ang patlang na ito ay maaari lamang maglaman ng isang numerikong halaga",
    'validate_boolean' => "Ang patlang na ito ay maaari lamang maglaman ng tama o mali na halaga",
    'validate_valid_url' => "Ang patlang na ito ay kailangan na maging isang balidong URL",
    'validate_url_exists' => "Hindi umiiral ang URL ng :attribute",
    'validate_valid_ip' => "Kinakailangang may valid na IP address ang :attribute",
    'validate_contains' => "Kinakailangang maglaman ng isa sa mga value na ito ang :attribute: :params",
    'validate_contains_list' => "Kinakailangang maglaman ang :attribute ng value mula sa drop down na listahan nito",
    'validate_doesnt_contain_list' => "Naglalaman ang :attribute ng hindi tinatanggap na value",
    'validate_street_address' => "Kinakailangang valid na address ng kalsada ang :attribute",
    'validate_date' => "Ang patlang na ito ay kailangan na maging isang balidong petsa (DD/MM/YYYY)",
    'custom_date' => "Ang patlang na ito ay kailangan na maging isang balidong petsa (DD/MM/YYYY)",
    'validate_min_numeric' => "Kinakailangang numeric na value, o katumbas ng o mas mataas sa :param ang :attribute",
    'validate_max_numeric' => "Kinakailangang numeric na value, o katumbas ng o mas mababa sa :param ang :attribute",
    'validate_min_age' => "Ang patlang na ito ay kailangan na may edad na mas higit sa o katumabas ng :param",
    'validate_phone_number' => "Kinakailangang valid na numero ng telepono ang :attribute",
    'validate_equalsfield' => "Ang mga email address ay hindi nagtutugma",
    'validate_contact' => "Hindi pa kayo nagbibigay sa amin ng numero ng telepono o e-mail address, na magpapahirap sa amin para kontakin kayo, upang maharap nang epektibo ang inyong reklamo. Mangyaring bumalik at maglagay ngayon, kung gusto niyong makontak namin kayo."

];