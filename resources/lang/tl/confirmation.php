<?php

return [
    'title' => 'Kumpirmasyon',
    'paragraph_1' => 'Salamat sa pagsumite ng inyong reklamo sa Garda Ombudsman',
    'paragraph_2' => 'Sa oras na lisanin niyo ang website na ito, hindi niyo na magagawang maiakses muli ang naisumiteng form.',
    'cta_button' => 'I-save ang PDF',
    'subtitle' => 'Mga Susunod na Hakbang',
    'list' => ' <ul>
                    <li>Sa loob ng susunod na linggo, padadalhan namin kayo ng sulat na kumikilala na ang inyong reklamo ay naitala na at isang desisyon ang gagawin sa kung ito ay tatanggapin at haharapin ng GSOC, alinsunod sa kriterya sa seksyon 87 ng Garda Síochána Act 2005.</li>
                    <li>Kapag ang desisyon na ito ay nagawa na, susulat kami para ipaalam sa inyo ang desisyon. Kung maaari naming tanggapin ang inyong reklamo, sasabihin namin sa inyo kung paano ito haharapin. Basahin ang tungkol sa mga posibleng paraan kung paano ito haharapin.</li>
                    <li>Karaniwang inaabot ng 3-9 na buwan para isara ang isang reklamo. Marahil na kayo ay ugnayin sa panahong ito ng opisyal ng GSOC o ng opisyal ng Garda Síochána na humaharap sa inyong kaso, upang humiling ng higit pang impormasyon at bigyan kayo ng mga update sa kanilang progreso.</li>
                    <li>Sa huli, susulatan at sasabihin namin sa inyo ang mga natuklasan at anumang karagdagang aksyon na dapat gawin. Mangyaring ipaalam sa amin, sa pamamagitan ng pag-email sa <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>, kung ang inyong detalye ng kontak ay nagbago.</li>
                </ul>'
];