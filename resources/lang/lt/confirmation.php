<?php

return [
    'title' => 'Patvirtinimas',
    'paragraph_1' => 'Dėkojame už pateiktą skundą policijos ombudsmenui.',
    'paragraph_2' => 'Jei išeisite iš šios svetainės, nebegalėsite vėl pasiekti savo pateiktos formos.',
    'cta_button' => 'Išsaugoti PDF',
    'subtitle' => 'Kiti žingsniai',
    'list' => ' <ul>
                    <li>Per kitą savaitę atsiųsime patvirtinimą, kad jūsų skundas buvo užregistruotas ir, kad turi būti priimtas sprendimas, ar policijos ombudsmeno tarnyba gali nagrinėti skundą pagal 2005 m. policijos įstatymo 87 straipsnyje pateiktus kriterijus.</li>
                    <li>Informuosime apie sprendimą, kai jis bus priimtas. Jei galėsime priimti jūsų skundo, informuosime, kaip jis bus nagrinėjamas. Perskaitykite apie galimus skundo nagrinėjimo būdus.</li>
                    <li>Paprastai skundo nagrinėjimas užtrunka 3–9 mėnesius. Tikriausiai skundo nagrinėjimo metu su jumis susisieks jūsų bylą nagrinėjantis policijos ombudsmeno tarybos pareigūnas arba policijos ombudsmeno pareigūnas, prašydamas pateikti papildomą informaciją ir suteiks jums naujausią informaciją apie skundo nagrinėjimo eigą.</li>
                    <li>Skundo nagrinėjimo pabaigoje informuosime jus apie išvadas bei tolimesnius veiksmus, kurių būtina imtis. Jei pasikeis jūsų kontaktiniai duomenys, informuokite mus el. <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>.</li>
                </ul>'
];
