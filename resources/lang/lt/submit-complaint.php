<?php

return
    [

        'page' =>
            [
                'title' => 'Kaip pateikti skundą',
                'error_msg' => 'Pateikdami formą, patikrinkite žemiau raudonai pažymėtas klaidas.',
                'yes' => 'Taip',
                'no' => 'Ne',
                'max_200_words' => 'Maks. 200 žodžių.'
            ],

        'sections' =>
            [

                'complainant_introduction' =>
                    '
            <div>
                <p>Jūs esate <b>„skundo pateikėjas“</b>, jei teikiate šį skundą.</p>
               
                <p>Jums reikia užpildyti visą informaciją šioje formoje, priešingu atveju Jūsų skundo nagrinėjimas gali užtrukti.</p>

                <p><b>Prieš pradedant</b></p>
                <ul class="vertical-icons">
                    <li><a href="/make-a-complaint/before-you-complain/">Išsiaiškinkite, ar policijos ombudsmeno tarnyba (Garda Siochána Ombudsman Commission) yra tinkama organizacija Jūsų skundo nagrinėjimui</a></li>
                    <li>Susipažinkite su informacija apie <a href="/about-gsoc/data-protection-freedom-of-information/data-protection/">Jūsų duomenų tvarkymą</a> ir <a href="/site-pages/website-terms/privacy-and-cookies/">konfidencialumą</a>.</li>
                </ul>
            </div>
        ',

                'representative_introduction' =>
                    '
            <div class="explainer">
                <h3>A atstovas</h3>

                <p>Jums reikia pateikti asmens arba įmonės, kuriai atstovaujate, direktoriaus sutikimą, jei šį skundą teikiate kito asmens ar įmonės vardu. </p>
                <ul class="vertical-icons">
                    <li>A <a href="#" class="consent-form-link" target="_blank">sutikimo forma</a> pateikta čia.</li>
                </ul>

                <h3>Jaunesnis nei 18 metų/h3>

                <p>Jei esate jaunesnis nei 18 metų, galite pateikti skundą pats arba jį už jus gali pateikti kitas asmuo.</p>

                <p>Jei paprašysite kito asmens, kad jis pateiktų šį skundą už jus, turėsite suteikti jam sutikimą pateikti skundą.</p>

                <ul class="vertical-icons">
                    <li>A <a href="#" class="consent-form-link" target="_blank">sutikimo forma</a> pateikta čia.</li>
                </ul>

                <p><i>Atstovas turi užpildyti visą informaciją šioje formoje. Jei neužpildysite visos mums reikalingos informacijos, jūsų skundo nagrinėjimas gali užtrukti arba policijos ombudsmeno tarnyba nesiims tolesnių veiksmų.</i></p>
            </div>
        ',



                'complaint_garda_members_description' =>
                    '
            <div class="explainer mb-5">
                <p>Pavyzdžiui:</p>
                <ul class="vertical-icons">
                    <li>Vardas, pavardė</li>
                    <li>Pareigūno ženkliuko numeris</li>
                    <li>Skyrius</li>
                    <li>Automobilis</li>
                    <li>Automobilio registracijos numeris</li>
                <ul>
            </div>
        ',
                'complaint_description' => '
            <div class="explainer mb-5">
                <p>Pavyzdžiui, ką jis:</p>
                <ul class="vertical-icons">
                    <li>Pasakė?</li>
                    <li>Padarė?</li>
                    <li>Nepadarė?</li>
                </ul>
                <p>Tai turėtų būti trumpas įvykio aprašymas.</p>
            </div>
        ',
                'evidence_description' =>
                    '
            <div class="explainer mb-5">
                <p>Pavyzdžiui:</p>
                <ul class="vertical-icons">
                    <li>Uždaros grandinės televizijos sistemos (CCTV) ar kita vaizdo filmavimo medžiaga</li>
                    <li>Dokumentai</li>
                </ul>
            </div>
        ',
                'injuries_description' =>
                    '
            <div class="explainer mb-5">
                <p>Nesiųskite jokių medicininių išvadų ar traumų nuotraukų naudojant šią formą. Jei reikės, paprašysime jūsų atskirai.</p>
            </div>
        ',


                'child_protection_description' => '<p>Vaiko gerovė yra visų svarbiausia. Jei policijos ombudsmenui kyla klausimų dėl vaiko apsaugos ir gerovės, jis privalo apie tai informuoti vaiko ir šeimos tarnybą „TUSLA“. Pirmiausia tai aptarsime su Jumis, jei bus įmanoma. Daugiau informacijos apie vaiko teisių pirmumo teisės aktus ir „Tusla“ rasite interneto svetainėje <a href="https://www.tusla.ie" target="_blank">www.TUSLA.ie</a>.</p>',

                'declaration_description' => '<p>Sąmoningas  „melagingos arba klaidinančios informacijos“ teikimas policijos ombudsmeno tarnybai yra baudžiamasis nusikaltimas. Jei paaiškėtų, kad tai padarėte, galite būti patraukti baudžiamojon atsakomybėj. Už tai jums gali būti skirta bauda ir (arba) laisvės atėmimo bausmė.<br /><br />
Apibendrinant, visa informacija, kurią pateikiate, turi būti teisinga ir nesiekianti suklaidinti policijos ombudsmeno tarnybos bet kokiame skunde ar tyrime. Jei pateiksite melagingą ar klaidinančią informaciją, jums gali tekti sumokėti 2500 eurų baudą arba galite būti įkalintas iki 6 mėnesių, arba pritaikytos abi priemonės.<br /><br />
Prašome pažymėti žemiau esantį langelį, taip patvirtinsite,  kad perskaitėte aukščiau esančią informaciją ir ją supratote.
</p>',
                'information_description' => '<p>Jūsų informacija gali būti atskleista trečiosioms šalims tokioms kaip, pvz. policijai, kad galėtume veiksmingai ištirti skundus arba kai esame teisiškai įpareigoti tai padaryti. Tai reikalinga, kad tinkamai ištirti jūsų skundą.
<br /><br />Kai tai būtina, informacija galime pasidalinti ir su kitomis šalimis bet kurioje susijusioje baudžiamojoje arba civilinėje byloje. Jei tai susiję su baudžiamuoju persekiojimu, informacija gali būti atskleista generaliniam prokurorui, kuris šia informacija gali pasidalinti su kitomis šalimis, pvz. liudytoju ar kitais asmenimis, nurodytais jūsų skunde. Daugiau informacijos rasite <a href="https://www.gardaombudsman.ie/about-gsoc/non-party-disclosure/">Non-Party Disclosure</a>.
<br /><br />Susijusioje civilinėje byloje, tai apima dalijimąsi informacija su kitomis šalimis, jei to reikalaujama pagal įstatymą, įskaitant teismo procesą.
<br /><br />Daugiau informacijos rasite šiose nuorodose <a href="https://www.gardaombudsman.ie/about-gsoc/data-protection-freedom-of-information/data-protection/">data protection policy</a> ir <a href="https://www.gardaombudsman.ie/site-pages/website-terms/privacy-and-cookies/">privacy and cookie policy</a>, arba elektroniniu paštu data.protection@gsoc.ie<a href="mailto:data.protection@gsoc.ie">data.protection@gsoc.ie</a>.</p>',

            ],


        'sidebars' =>
            [
                'change_language_title' => 'Pakeisti kalbą',
                'proof_title' => 'Kodėl mums reikalinga ši informacija?',
                'proof_content' => 'Jei negausime sutikimo patvirtinimo ar paaiškinimo, tikriausiai negalėsime nagrinėti skundo.',
                'help_title' => 'Reikia pagalbos?',
                'help_content' => 'Jei reikia pagalbos pildant skundo formą, paskambinkite mums telefonu 0818 600 800. Skambinkite darbo dienomis rytais. Kitais laikais, galite palikti pranešimą, kuriame prašoma perskambinti arba susitarti dėl susitikimo su darbuotoju atsakingu už bylą.',
                'complainant_contact_title' => 'Palaikykime ryšį',
                'complainant_contact_content' => 'Jūsų skundo nagrinėjimo metu mums gali prireikti susisiekti su jumis. Todėl praneškite mums elektroniniu paštu <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>, jei pasikeistų jūsų kontaktiniai duomenys.',
                'representative_contact_title' => 'Kodėl mums reikalinga ši informacija?',
                'representative_contact_content' => 'Jūsų skundo nagrinėjimo metu mums gali prireikti susisiekti su jumis. Todėl praneškite mums elektroniniu paštu <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>, jei pasikeistų jūsų kontaktiniai duomenys.',

            ],


        'fields' =>
            [
                'complainant_legend' => 'Skundo pateikėjo asmens duomenys',
                'complainant_name' => 'Vardas',
                'complainant_forename' => 'Vardas',
                'complainant_surname' => 'SurnamePavardė',
                'complainant_primary_address_street' => 'Adresas (1 eilutė)',
                'complainant_primary_address_street2' => 'Adresas (2 eilutė)',
                'complainant_primary_address_street3' => 'Adresas (3 eilutė) (neprivaloma)',
                'complainant_primary_address_town' => 'Miestas',
                'complainant_primary_address_county' => 'Grafystė/ apskritis',
                'complainant_primary_address_postcode' => 'Pašto kodas',
                'complainant_primary_address_country' => 'Šalis',
                'complainant_date_of_birth' => 'Gimimo data (diena/mėnuo/metai)',
                'complainant_primary_address_mobile' => 'Mobilaus telefono numeris',
                'complainant_primary_address_phone' => 'Kitas telefono numeris (darbo arba namų) (neprivaloma)',
                'complainant_primary_address_email' => 'Elektroninio pašto adresas',
                'complainant_primary_address_email_confirm' => 'Patvirtinkite elektroninio pašto adresą',

                'representative_legend' => 'Atstovo asmens duomenys',
                'representative_required_legend' => 'Ar Jums reikalingas atstovas?',
                'has_consent_form_legend' => 'Ar skundo pateikėjas pasirašė <a href="#" class="consent-form-link" target="_blank">sutikimo formą</a>?',
                'third_party_representative_legend' => 'Koks ryšys Jus sieja su skundo pateikėju?',
                'third_party_representative_type_guardian' => 'Globėjas',
                'third_party_representative_type_legal_representative' => 'Teisėtas atstovas',
                'third_party_representative_type_parent' => 'Tėvas (motina)',
                'third_party_representative_type_social_worker' => 'Socialinis darbuotojas',
                'third_party_representative_type_other' => 'Kita',
                'third_party_representative_description_other' => 'Apibūdinkite „Kita“',
                'third_party_representative_name' => 'Vardas',
                'third_party_representative_forename' => 'Vardas',
                'third_party_representative_surname' => 'Pavardė',
                'third_party_representative_date_of_birth' => 'Gimimo data (diena/mėnuo/metai)',
                'third_party_representative_mobile' => 'Mobilaus telefono numeris',
                'third_party_representative_email' => 'Elektroninio pašto adresas',
                'third_party_representative_email_confirm' => 'Patvirtinkite elektroninio pašto adresą',

                'correspondence_legend' => 'Pažymėkite atitinkamą langelį, jei norite, kad policijos ombudsmenas korespondencijai naudotų kitą adresą negu nurodė skundo pateikėjas.',
                'correspondence_type_1' => 'Toks pat, kokį nurodė skundo pateikėjas',
                'correspondence_type_2' => 'Kitas adresas',

                'complainant_secondary_address_legend' => 'Adresas',

                'complainant_secondary_address_street' => 'Adresas (1 eilutė)',
                'complainant_secondary_address_town' => 'Mestas',
                'complainant_secondary_address_county' => 'Grafystė/ apskritis',
                'complainant_secondary_address_postcode' => 'Pašto kodas',
                'complainant_secondary_address_country' => 'Šalis',
                'complainant_secondary_address_mobile' => 'Mobilaus telefono numeris',
                'complainant_secondary_address_phone' => 'Kitas telefono numeris (darbo arba namų) (neprivaloma)',
                'complainant_secondary_address_email' => 'Elektroninio pašto adresas',
                'complainant_secondary_address_email_confirm' => 'Patvirtinkite elektroninio pašto adresą',

                'complaint_legend' => 'Skundas',

                'complaint_late_submission_legend' => 'Išsami informacija apie pavėluotą pateikimą',

                'complaint_date' => 'Įvykio data (diena/mėnuo/metai)',
                'complaint_time_reason' => 'Jei incidentas įvyko daugiau nei prieš 12 mėnesių, nurodykite pavėluoto pateikimo priežastį.',
                'complaint_time' => 'Įvykio laikas',
                'complaint_location' => 'Įvykio vieta',
                'complaint_garda_members' => 'Ar turite bet kokios informacijos, kuri padėtų identifikuoti policijos pareigūną, dėl kurio norite pateikti skundą?',
                'complaint_details' => 'Prašome apibūdinti konkretų netinkamą veiksmą, kurį Jūsų manymu atliko/ padarė policijos pareigūnas?',
                'complaint_lead_up' => 'Kas atsitiko prieš įvykį?',
                'complaint_motive' => 'Kodėl Jūsų manymu policijos pareigūnas taip pasielgė? ',

                'complaint_witnesses_legend' => 'Ar yra įvykio liudininkų?',
                'complaint_witnesses' => 'Jei „Taip“, nurodykite visų buvusių liudininkų vardus, pavardes ir kontaktinius duomenis.',
                'complaint_evidence_legend' => 'Ar turite kitų įrodymų?',
                'complaint_evidence' => 'Jei „Taip“, pateikite įrodymus.',
                'complaint_injuries_legend' => 'Ar įvykio metu patyrėte sužalojimų ar prireikė medicininės pagalbos po įvykio?',
                'complaint_injuries' => 'Jei atsakymas „Taip“,  apibūdinkite bet kokius sužalojimus ar medicininį gydymą, kuris buvo suteiktas po įvykio.',

                'child_protection_legend' => 'Policijos ombudsmeno tarnybos vaiko teisių apsaugos ir gerovės įsipareigojimai',
                'child_protection' => 'Man suprantami policijos ombudsmeno tarnybos vaiko teisių apsaugos ir gerovės įsipareigojimai.',

                'declaration_legend' => '2005 m. policijos įstatymo 110 skyrius',
                'declaration' => 'Aš suprantu 2005 m. policijos įstatymo 110 skyrių.',

                'information_legend' => 'Informacijos naudojimas',
                'information' => 'Suprantu, kokiais tikslais policijos ombudsmeno tarnyba gali naudoti informaciją apie mane ir kada ja gali pasidalinti.',

                'submit' => 'Pateikti',

            ],

        'additional' =>
            [
                'behalf_note' => 'Jums reikia pateikti asmens arba įmonės, kuriai atstovaujate, direktoriaus sutikimą, jei šį skundą teikiate kito asmens ar įmonės vardu.',
                'date_of_birth' => 'Gimimo data',
                'other_phone' => 'Kitas telefono numeris',
                'date_of_incident' => 'Įvykio data',
                'child_protection_description_simplified' => 'Vaiko gerovė yra visų svarbiausia. Jei policijos ombudsmenui kyla klausimų dėl vaiko apsaugos ir gerovės, jis privalo apie tai informuoti vaiko ir šeimos tarnybą „TUSLA“. Pirmiausia tai aptarsime su Jumis, jei bus įmanoma.',

                'declaration_description_simplified' => 'Sąmoningas  „melagingos arba klaidinančios informacijos“ teikimas policijos ombudsmeno tarnybai yra baudžiamasis nusikaltimas. Jei paaiškėtų, kad tai padarėte, galite būti patraukti baudžiamojon atsakomybėj. Už tai jums gali būti skirta bauda ir (arba) laisvės atėmimo bausmė.<br /><br />
Apibendrinant, visa informacija, kurią pateikiate, turi būti teisinga ir nesiekianti suklaidinti policijos ombudsmeno tarnybos bet kokiame skunde ar tyrime. Jei pateiksite melagingą ar klaidinančią informaciją, jums gali tekti sumokėti 2500 eurų baudą arba galite būti įkalintas iki 6 mėnesių, arba pritaikytos abi priemonės.',
                'information_description_simplified' => 'Jūsų informacija gali būti atskleista trečiosioms šalims tokioms kaip, pvz. policijai, kad galėtume veiksmingai ištirti skundus arba kai esame teisiškai įpareigoti tai padaryti. Tai reikalinga, kad tinkamai ištirti jūsų skundą.
<br /><br />Kai tai būtina, informacija galime pasidalinti ir su kitomis šalimis bet kurioje susijusioje baudžiamojoje arba civilinėje byloje. Jei tai susiję su baudžiamuoju persekiojimu, informacija gali būti atskleista generaliniam prokurorui, kuris šia informacija gali pasidalinti su kitomis šalimis, pvz. liudytoju ar kitais asmenimis, nurodytais jūsų skunde.
<br /><br />Susijusioje civilinėje byloje, tai apima dalijimąsi informacija su kitomis šalimis, jei to reikalaujama pagal įstatymą, įskaitant teismo procesą.',
            ]


    ];