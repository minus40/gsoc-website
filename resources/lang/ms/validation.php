<?php



return [

    'mismatch' => "Tiada peraturan pengesahihan untuk medan ini.",
    'validate_required' => "Medan ini diperlukan",
    'validate_required_if' => "Medan ini diperlukan",
    'validate_valid_email' => "Medan ini haruslah alamat e-mel yang sah",
    'validate_min_len' => "Jumlah minimum aksara medan ini ialah :param",
    'validate_max_len' => "Jumlah maksimum aksara medan ini ialah :param",
    'validate_exact_len' => "[eng]The :attribute field needs to be exactly :param characters in length",
    'validate_alpha' => "[eng]The :attribute field may only contain alpha characters(a-z)",
    'validate_alpha_space' => "[eng]The :attribute field may only contain alpha characters(a-z) and spaces",
    'validate_alpha_numeric' => "[eng]The :attribute field may only contain alpha-numeric characters",
    'validate_alpha_dash' => "[eng]The :attribute field may only contain alpha characters and dashes",
    'validate_numeric' => "Medan ini hanya boleh mengandungi aksara angka",
    'validate_integer' => "Medan ini hanya boleh mengandungi nilai angka",
    'validate_boolean' => "Medan ini hanya boleh mengandungi nilai benar atau salah",
    'validate_valid_url' => "Medan ini memerlukan URL yang sah",
    'validate_url_exists' => "[eng]The :attribute URL does not exist",
    'validate_valid_ip' => "[eng]The :attribute field needs to contain a valid IP address",
    'validate_contains' => "[eng]The :attribute field needs to contain one of these values: :params",
    'validate_contains_list' => "[eng]The :attribute field needs to contain a value from its drop down list",
    'validate_doesnt_contain_list' => "[eng]The :attribute field contains a value that is not accepted",
    'validate_street_address' => "[eng]The :attribute field needs to be a valid street address",
    'validate_date' => "Medan ini memerlukan tarikh yang sah (HH/BB/TTTT)",
    'custom_date' => "Medan ini memerlukan tarikh yang sah (HH/BB/TTTT)",
    'validate_min_numeric' => "[eng]The :attribute field needs to be a numeric value, equal to, or higher than :param",
    'validate_max_numeric' => "[eng]The :attribute field needs to be a numeric value, equal to, or lower than :param",
    'validate_min_age' => "Medan ini perlu mempunyai umur yang lebih besar daripada atau sama dengan :param",
    'validate_phone_number' => "[eng]The :attribute field needs to be a valid phone number",
    'validate_equalsfield' => "Alamat e-mel tidak sepadan",
    'validate_contact' => "Anda belum memberi kami nombor telefon atau alamat e-mel, yang mana ini akan menyukarkan kami untuk menghubungi anda, untuk menangani aduan anda dengan berkesan. Sila kembali dan masukkan satu sekarang, jika anda mahu kami boleh menghubungi anda.",
];