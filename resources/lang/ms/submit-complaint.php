<?php

return
    [

        'page' =>
            [
                'title' => 'Cara membuat aduan',
                'error_msg' => 'Sila periksa ralat yang berwarna merah di bawah, sebelum menghantar borang anda.',
                'yes' => 'Ya',
                'no' => 'Tidak',
                'max_200_words' => 'Maks 200 perkataan.'
            ],

        'sections' =>
            [

                'complainant_introduction' =>
                    '
            <div>
                <p>Jika anda membuat aduan ini, anda adalah <b>\'pengadu\'</b>.</p>
               
                <p>Anda mesti mengisi semua maklumat dalam borang ini atau pemprosesan aduan anda mungkin ditangguhkan.</p>

                <p><b>Sebelum anda bermula</b></p>
                <ul class="vertical-icons">
                    <li><a href="/make-a-complaint/before-you-complain/">Ketahui sama ada GSOC adalah organisasi yang tepat untuk menangani aduan anda</a></li>
                    <li>Anda juga mungkin ingin membaca dasar <a href="/site-pages/website-terms/privacy-and-cookies/">Privasi dan Perlindungan Data kami.<a href="/site-pages/website-terms/privacy-and-cookies/">Privacy</a> and <a href="/about-gsoc/data-protection-freedom-of-information/data-protection/">Data Protection policy</a>.</li>
                </ul>
            </div>
        ',

                'representative_introduction' =>
                    '
            <div class="explainer">
                <h3>Wakil</h3>

                <p>Jika anda membuat aduan ini bagi pihak orang lain atau syarikat, anda perlu memberikan persetujuan pihak orang lain tersebut atau pengarah syarikat yang anda wakili. </p>
                <ul class="vertical-icons">
                    <li><a href="#" class="consent-form-link" target="_blank">Borang Persetujuan</a> disediakan di sini.</li>
                </ul>

                <h3>Di bawah umur 18 Tahun</h3>

                <p>Jika anda berumur di bawah 18 tahun, anda boleh membuat aduan sendiri atau orang lain boleh membuat aduan ini untuk anda.</p>

                <p>Jika anda meminta orang lain untuk membuat aduan ini untuk anda, anda perlu memberi mereka persetujuan untuk membuat aduan anda.</p>

                <ul class="vertical-icons">
                    <li><a href="#" class="consent-form-link" target="_blank">Borang Persetujuan</a> disediakan di sini.</li>
                </ul>

                <p><i>Wakil harus mengisi semua maklumat dalam borang ini. Jika anda tidak mengisi semua maklumat yang kami perlukan, ini mungkin boleh melambatkan pemprosesan aduan anda atau tidak akan dibawa kepada tindakan lanjut oleh GSOC. </i></p>
            </div>
        ',



                'complaint_garda_members_description' =>
                    '
            <div class="explainer mb-5">
                <p>Contohnya:</p>
                <ul class="vertical-icons">
                    <li>Nama</li>
                    <li>Nombor ID</li>
                    <li>Balai</li>
                    <li>Kereta</li>
                    <li>Nombor pendaftaran kereta</li>
                <ul>
            </div>
        ',
                'complaint_description' => '
            <div class="explainer mb-5">
                <p>Contohnya, apa yang mereka:</p>
                <ul class="vertical-icons">
                    <li>Katakan?</li>
                    <li>Lakukan?</li>
                    <li>Tidak lakukan?</li>
                </ul>
                <p>Ini sepatutnya menjadi kenyataan ringkas mengenai kejadian itu.</p>
            </div>
        ',
                'evidence_description' =>
                    '
            <div class="explainer mb-5">
                <p>Contohnya:</p>
                <ul class="vertical-icons">
                    <li>CCTV atau rakaman video lain</li>
                    <li>dokumen</li>
                </ul>
            </div>
        ',
                'injuries_description' =>
                    '
            <div class="explainer mb-5">
                <p>Jangan hantar sebarang laporan atau foto perubatan kecederaan tersebut dengan borang ini. Jika perlu, kami akan memintanya dari anda.</p>
            </div>
        ',


                'child_protection_description' => '<p>Kebajikan kanak-kanak adalah yang paling utama. Jika Garda Ombudsman mempunyai sebarang kebimbangan tentang perlindungan atau kebajikan seseorang kanak-kanak, Garda Ombudsman mempunyai kewajipan untuk berkongsi maklumat ini dengan TUSLA, Agensi Kanak-kanak dan Keluarga. Setakat yang mungkin, kami akan membincangkannya dengan anda terlebih dahulu. Maklumat lanjut tentang Children First Legislation dan TUSLA disediakan di <a href="https://www.tusla.ie" target="_blank">www.TUSLA.ie</a>.</p>',

                'declaration_description' => '<p>Memberi "maklumat palsu atau mengelirukan" kepada GSOC dengan sengaja adalah suatu kesalahan jenayah. Anda boleh didakwa jika nampaknya anda telah melakukan ini. Ini boleh menyebabkan denda dan/atau hukuman penjara dikenakan.<br /><br />
Secara ringkasnya, maklumat yang anda berikan mestilah benar dan tidak bertujuan untuk mengelirukan GSOC dalam sebarang aduan atau siasatan.  Jika anda memberikan maklumat palsu atau mengelirukan, anda mungkin perlu membayar denda sehingga € 2,500 atau dipenjarakan sehingga 6 bulan, atau kedua-duanya. Sila tandakan kotak di bawah untuk menunjukkan bahawa anda telah membaca perkara di atas dan memahaminya.<br /><br />
Sila tandakan kotak di bawah untuk menunjukkan bahawa anda telah membaca perkara di atas dan memahaminya.
</p>',
                'information_description' => '<p>Maklumat anda mungkin didedahkan kepada pihak ketiga seperti Garda Síochána untuk membolehkan kami menyiasat aduan dengan berkesan atau di mana kami diwajibkan oleh undang-undang untuk berbuat demikian. Ini adalah untuk memastikan penyiasatan yang berkesan terhadap aduan anda.
<br /><br />Sekiranya perlu dan berkadar untuk berbuat demikian, kami mungkin berkongsi maklumat dengan pihak lain dalam mana-mana prosiding jenayah atau sivil yang berkaitan. Berhubung dengan pendakwaan jenayah, ini termasuk berkongsi maklumat dengan Pengarah Pendakwaan Awam yang kemudiannya boleh berkongsi maklumat ini dengan pihak lain seperti saksi atau orang lain yang dikenal pasti dalam aduan anda. Untuk maklumat sila layari <a href="https://www.gardaombudsman.ie/about-gsoc/non-party-disclosure/">Pendedahan Bukan Pihak</a>.
<br /><br />Dalam prosiding sivil yang berkaitan, ini termasuk berkongsi maklumat dengan pihak lain yang mungkin diperlukan oleh undang-undang termasuk oleh Mahkamah Perundangan.
<br /><br />Untuk maklumat lanjut sila layari pautan-pautan ke <a href="https://www.gardaombudsman.ie/about-gsoc/data-protection-freedom-of-information/data-protection/">dasar perlindungan data</a> dan <a href="https://www.gardaombudsman.ie/site-pages/website-terms/privacy-and-cookies/">dasar privasi</a> dan kuki kami atau menghantar e-mel kepada <a href="mailto:data.protection@gsoc.ie">data.protection@gsoc.ie</a>.</p>',

            ],


        'sidebars' =>
            [
                'change_language_title' => 'Tukar Bahasa',
                'proof_title' => 'Mengapakah kami memerlukan maklumat ini?',
                'proof_content' => 'Jika kami tidak menerima bukti persetujuan atau penjelasan, kami mungkin tidak akan dapat menangani aduan tersebut.',
                'help_title' => 'Perlukan bantuan?',
                'help_content' => 'Hubungi kami di 0818 600 800 jika anda memerlukan bantuan untuk mengisi borang aduan. Pekerja sosial boleh didapati melalui telefon setiap pagi pada hari bekerja. Pada masa lain, anda boleh meninggalkan mesej yang meminta panggilan semula atau janji temu dengan pekerja sosial.',
                'complainant_contact_title' => 'Marilah kita terus berhubung',
                'complainant_contact_content' => 'Kami perlu menghubungi anda semasa kami berurusan dengan aduan anda. Sila maklumkan kami dengan menghantar e-mel kepada <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>, jika butiran perhubungan anda berubah.',
                'representative_contact_title' => 'Mengapakah kami memerlukan maklumat ini?',
                'representative_contact_content' => 'Kami perlu menghubungi anda semasa kami berurusan dengan aduan anda. Sila maklumkan kami dengan menghantar e-mel kepada <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>, jika butiran perhubungan anda berubah.',

            ],


        'fields' =>
            [
                'complainant_legend' => 'Butiran peribadi pengadu',
                'complainant_name' => 'Nama',
                'complainant_forename' => 'Nama pertama',
                'complainant_surname' => 'Nama keluarga',
                'complainant_primary_address_street' => 'Alamat Barisan 1',
                'complainant_primary_address_street2' => 'Alamat Barisan 2',
                'complainant_primary_address_street3' => 'Alamat Barisan 3 (Pilihan)',
                'complainant_primary_address_town' => 'Pekan atau bandar',
                'complainant_primary_address_county' => 'Daerah',
                'complainant_primary_address_postcode' => 'Poskod',
                'complainant_primary_address_country' => 'Negara',
                'complainant_date_of_birth' => 'Tarikh lahir (HH/BB/TTTT)',
                'complainant_primary_address_mobile' => 'Nombor telefon bimbit',
                'complainant_primary_address_phone' => 'Nombor telefon lain (rumah atau tempat kerja) (Pilihan)',
                'complainant_primary_address_email' => 'Alamat e-mel',
                'complainant_primary_address_email_confirm' => 'Sahkan alamat e-mel',

                'representative_legend' => 'Butiran peribadi wakil',
                'representative_required_legend' => 'Adakah anda memerlukan wakil?',
                'has_consent_form_legend' => 'Adakah pengadu telah menandatangani <a href="#" class="consent-form-link" target="_blank">borang persetujuan</a> tersebut?',
                'third_party_representative_legend' => 'Apakah hubungan anda dengan pengadu?',
                'third_party_representative_type_guardian' => 'Penjaga',
                'third_party_representative_type_legal_representative' => 'Wakil Undang-undang',
                'third_party_representative_type_parent' => 'Ibu Bapa',
                'third_party_representative_type_social_worker' => 'Pekerja Sosial',
                'third_party_representative_type_other' => 'Lain-lain',
                'third_party_representative_description_other' => 'Sila terangkan \'lain-lain\'',
                'third_party_representative_name' => 'Nama',
                'third_party_representative_forename' => 'Nama pertama',
                'third_party_representative_surname' => 'Nama keluarga',
                'third_party_representative_date_of_birth' => 'Tarikh lahir (HH/BB/TTTT)',
                'third_party_representative_mobile' => 'Nombor telefon bimbit',
                'third_party_representative_email' => 'Alamat e-mel',
                'third_party_representative_email_confirm' => 'Sahkan alamat e-mel',

                'correspondence_legend' => 'Jika anda ingin Garda Ombudsman untuk berhubung dengan anda di alamat yang berbeza dengan yang disediakan untuk pengadu, sila tandakan kotak yang berkaitan.',
                'correspondence_type_1' => 'Sama seperti pengadu',
                'correspondence_type_2' => 'Alamat lain',

                'complainant_secondary_address_legend' => 'Alamat',

                'complainant_secondary_address_street' => 'Alamat Barisan 1',
                'complainant_secondary_address_town' => 'Pekan atau bandar',
                'complainant_secondary_address_county' => 'Daerah',
                'complainant_secondary_address_postcode' => 'Poskod',
                'complainant_secondary_address_country' => 'Negara',
                'complainant_secondary_address_mobile' => 'Nombor telefon bimbit',
                'complainant_secondary_address_phone' => 'Nombor telefon lain (rumah atau tempat kerja) (Pilihan)',
                'complainant_secondary_address_email' => 'Alamat e-mel',
                'complainant_secondary_address_email_confirm' => 'Sahkan alamat e-mel',

                'complaint_legend' => 'Aduan',

                'complaint_late_submission_legend' => 'Butiran penghantaran lewat',

                'complaint_date' => 'Tarikh Kejadiant (HH/BB/TTTT)',
                'complaint_time_reason' => 'Jika kejadian tersebut berlaku lebih dari 12 bulan lalu, sila beritahu kami sebab pelaporan itu lewat.',
                'complaint_time' => 'Masa Kejadian',
                'complaint_location' => 'Lokasi Kejadian',
                'complaint_garda_members' => 'Adakah anda mempunyai sebarang maklumat yang boleh membantu mengenal pasti Garda yang ingin anda membuat aduan?',
                'complaint_details' => 'Sila terangkan salah laku khusus yang anda menuduh Garda dilakukan.',
                'complaint_lead_up' => 'Apa yang telah berlaku sebelum kejadian tersebut?',
                'complaint_motive' => 'Mengapa anda rasa garda berkelakuan seperti itu?',

                'complaint_witnesses_legend' => 'Adakah terdapat saksi kejadian tersebut?',
                'complaint_witnesses' => 'Jika \'Ya\': Sila berikan nama penuh dan maklumat perhubungan mana-mana saksi di sini.',
                'complaint_evidence_legend' => 'Adakah terdapat bukti lain?',
                'complaint_evidence' => 'Jika \'Ya\': apakah buktinya?',
                'complaint_injuries_legend' => 'Adakah anda mengalami sebarang kecederaan atau memerlukan rawatan perubatan berikutan kejadian itu?',
                'complaint_injuries' => 'Jika \'Ya\': Terangkan sebarang kecederaan atau rawatan perubatan yang anda terima selepas kejadian tersebut.',

                'child_protection_legend' => 'Perlindungan Kanak-kanak dan tanggungjawab kebajikan GSOC',
                'child_protection' => 'Saya memahami Perlindungan Kanak-kanak dan Tanggungjawab Kebajikan GSOC',

                'declaration_legend' => 'Seksyen 110 Akta Garda Síochána, 2005',
                'declaration' => 'Saya faham Seksyen 110 Akta Garda Síochána, 2005',

                'information_legend' => 'Penggunaan Maklumat',
                'information' => 'Saya mengakui sebab-sebab GSOC menggunakan maklumat saya dan bagaimana maklumat tersebut boleh dikongsi',

                'submit' => 'Hantar',

            ],

        'additional' =>
            [
                'behalf_note' => 'Jika anda membuat aduan ini bagi pihak orang lain atau syarikat, anda perlu memberikan persetujuan pihak orang lain tersebut atau pengarah syarikat yang anda wakili.',
                'date_of_birth' => 'Tarikh lahir',
                'other_phone' => 'Nombor telefon lain',
                'date_of_incident' => 'Tarikh Kejadiant',
                'child_protection_description_simplified' => 'Kebajikan kanak-kanak adalah yang paling utama. Jika Garda Ombudsman mempunyai sebarang kebimbangan tentang perlindungan atau kebajikan seseorang kanak-kanak, Garda Ombudsman mempunyai kewajipan untuk berkongsi maklumat ini dengan TUSLA, Agensi Kanak-kanak dan Keluarga. Setakat yang mungkin, kami akan membincangkannya dengan anda terlebih dahulu.',

                'declaration_description_simplified' => 'Memberi "maklumat palsu atau mengelirukan" kepada GSOC dengan sengaja adalah suatu kesalahan jenayah. Anda boleh didakwa jika nampaknya anda telah melakukan ini. Ini boleh menyebabkan denda dan/atau hukuman penjara dikenakan.<br /><br />
Secara ringkasnya, maklumat yang anda berikan mestilah benar dan tidak bertujuan untuk mengelirukan GSOC dalam sebarang aduan atau siasatan.  Jika anda memberikan maklumat palsu atau mengelirukan, anda mungkin perlu membayar denda sehingga € 2,500 atau dipenjarakan sehingga 6 bulan, atau kedua-duanya. Sila tandakan kotak di bawah untuk menunjukkan bahawa anda telah membaca perkara di atas dan memahaminya.',
                'information_description_simplified' => 'Maklumat anda mungkin didedahkan kepada pihak ketiga seperti Garda Síochána untuk membolehkan kami menyiasat aduan dengan berkesan atau di mana kami diwajibkan oleh undang-undang untuk berbuat demikian. Ini adalah untuk memastikan penyiasatan yang berkesan terhadap aduan anda.
<br /><br />Sekiranya perlu dan berkadar untuk berbuat demikian, kami mungkin berkongsi maklumat dengan pihak lain dalam mana-mana prosiding jenayah atau sivil yang berkaitan. Berhubung dengan pendakwaan jenayah, ini termasuk berkongsi maklumat dengan Pengarah Pendakwaan Awam yang kemudiannya boleh berkongsi maklumat ini dengan pihak lain seperti saksi atau orang lain yang dikenal pasti dalam aduan anda.
<br /><br />Dalam prosiding sivil yang berkaitan, ini termasuk berkongsi maklumat dengan pihak lain yang mungkin diperlukan oleh undang-undang termasuk oleh Mahkamah Perundangan.'
            ]


    ];