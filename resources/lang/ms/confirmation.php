<?php

return [
    'title' => 'Pengesahan',
    'paragraph_1' => 'Terima kasih kerana mengemukakan aduan anda kepada Garda Ombudsman.',
    'paragraph_2' => 'Sebaik sahaja anda meninggalkan laman web ini, anda tidak lagi akan dapat mengakses borang yang dihantar.',
    'cta_button' => 'Simpan PDF',
    'subtitle' => 'Langkah Seterusnya',
    'list' => ' <ul>
                    <li>Dalam minggu depan, kami akan menghantar surat yang mengakui bahawa aduan anda telah dicatatkan dan keputusan akan dibuat sama ada aduan ana boleh diterima dan diuruskan oleh GSOC, mengikut kriteria dalam seksyen 87 Akta Garda Síochána 2005.</li>
                    <li>Apabila keputusan ini dibuat, kami akan maklumkan anda keputusan tersebut. Jika kami boleh menerima aduan anda, kami akan memberitahu anda cara aduan anda akan ditangani. Baca tentang cara yang mungkin aduan anda boleh ditangani.</li>
                    <li>Biasanya tempoh masa yang diambil untuk menutup suatu aduan adalah 3-9 bulan. Pada masa ini, anda kemungkinan besar akan dihubungi oleh pegawai GSOC atau pegawai Garda Síochána yang berurusan dengan kes anda, untuk meminta maklumat lanjut dan memberi anda kemas kini tentang kemajuan mereka.</li>
                    <li>Pada pengakhirnya, kami akan menulis dan memberitahu anda tentang penemuan dan sebarang tindakan lanjut yang perlu diambil. Sila beritahu kami, dengan menghantar e-mel kepada <a href="mailto:complaints@gsoc.ie" title="Send Email">complaints@gsoc.ie</a>, jika butiran hubungan anda telah berubah.</li>
                </ul>'
];