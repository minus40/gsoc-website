/*

track events on this site without using inline javascript
make sure you change the URLs in _externalLinks

*/

let Tracking = (function () {

    let gaID = window.gaID;

    let stagingDomain = 'gsoc.local';
    let realDomain = 'gardaombudsman.ie';

    let _downloads =  'a[data-file-download-name]';
    let _external_links =  'a[href^=http], a[href^=mailto], a[href^=tel]';

    let _outboundTrack = function(url, proceed = true) {

        gtag('event', url, 
        {
          'event_category': 'Outbound Links',
          'event_label': window.location.href,
          'transport_type':'beacon',
        });

        if (proceed)
        {

            setTimeout(function () {
                window.open(url, '_blank').focus();
            },100)

        }
    };

    let _externalLinksAndDownloads = function ()
    {

        $( _external_links ).each(function () {

            if( this.href.indexOf(stagingDomain) == -1 && this.href.indexOf(realDomain) == -1 && this.href.indexOf('mailto:') == -1 && this.href.indexOf('tel:') == -1  ) 
            {
                $( this ).click(function (e) {
                    e.preventDefault();
                    var proceed = true;
                    if ( this.classList.contains("no-process"))
                    {
                        proceed = false;
                    }
                    _outboundTrack(this.href, proceed);
                });
            }
            else
            {

                let filetypes = /\.(zip|exe|dmg|pdf|doc.*|xls.*|ppt.*|mp3|txt|rar|wma|mov|avi|wmv|flv|wav)$/i;
                if (this.href.match(filetypes)) 
                {
                    let extension = (/[.]/.exec(this.href)) ? /[^.]+$/.exec(this.href) : undefined;
                    if (extension)
                    {
                        $( this ).click(function (e) 
                        {

                            gtag('event', this.href, 
                            {
                              'event_category': 'Downloads',
                              'event_label': window.location.href,
                              'transport_type':'beacon',
                            });
  
                        });                     
                    }
                } 

                if (this.href.match(/^mailto\:/i)) 
                {
                    $( this ).click(function (e) {

                        let action =  this.href.replace(/^mailto\:/i, '');

                        gtag('event', action, 
                        {
                          'event_category': 'Email',
                          'event_label': window.location.href,
                          'transport_type':'beacon',
                        });


                    });
                }

                if (this.href.match(/^tel\:/i)) 
                {
                    $( this ).click(function (e) {

                        let action = this.href.replace(/^tel\:/i, '');

                        gtag('event', action, 
                        {
                          'event_category': 'Telephone',
                          'event_label': window.location.href,
                          'transport_type':'beacon',
                        });

                    });
                }

            }
        });
    };

    let _documentDownloads = function ()
    {
        $( _downloads ).each(function () 
        {
            $( this ).click(function (e) 
            {
                //e.preventDefault();
                let filename = $(this).data( 'file-download-name' );
                let filepath = $(this).data( 'file-download-path' );
                //console.log (filepath, filename, "clicked");

                gtag('event', filepath, 
                {
                  'event_category': 'Downloads',
                  'event_label': window.location.href,
                  'transport_type':'beacon',
                });

            });
        });
    };


    let init = function () 
    {

        if (window.gaID && window.gaID !== undefined && window.gaID !== '' )
        {

            _externalLinksAndDownloads();
            _documentDownloads();
        }
    };

    return {
        init: init,
    }

})();

/*$(function()
{
    setTimeout(function()
    {

        Tracking.init();
    }, 100);
});*/

//$( document ).ready( Tracking.init );