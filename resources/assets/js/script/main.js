//jQuery.fn.load = function(callback){ $(window).on("load", callback) };

$(document).ready(function()
{
    $(".search-trigger").click(function(event) 
    {
        event.preventDefault();
        $(".search-active-overlay").addClass("fade-content");
        $("#search-tool").toggleClass("show-search");
        $("html").removeClass("nav-is-open");
        $( ".search-input" ).focus();
        $("body").addClass("search-active");

    });

    $(document).keyup(function(event) 
    {
        if (event.keyCode == 27) { // escape key maps to keycode `27`
            $("#search-tool").removeClass("show-search");
            $(".search-active-overlay").removeClass("fade-content");
            $("body").removeClass("search-active");
            $("html").removeClass("nav-is-open");
        }
    });

    $(".search-close").click(function() {
        $("#search-tool").removeClass("show-search");
        $(".search-active-overlay").removeClass("fade-content");
        $("body").removeClass("search-active");
    });



// Complaint Form -> Complaint Date
    var today = new Date().toISOString().split('T')[0];
    if (document.getElementById('complaint-date') != null) {

        document.getElementsByName("complaint-date")[0].setAttribute('max', today);
    }
    // add placeholders to old browsers
    //$('.search-input').attr('placeholder', 'Start typing....');
    // done

    if ($('.fancybox').length)
    {
        $(".fancybox").fancybox(
        {
            beforeShow : function()
            {
                this.title =  $(this.element).data("caption");
            },
            openEffect: "none",
            closeEffect: "none",

            helpers: 
            { 
                overlay: 
                {
                     css: { 'background': 'rgba(236, 236, 236, 0.8)' }
                } 
            } 
        });
    }

    

});