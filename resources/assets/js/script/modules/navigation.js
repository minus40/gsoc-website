$(document).ready(function()
{

    window.onscroll = function() { scrollDetect() };
    var preheader_offset = 0;


    function scrollDetect() 
    {

        if ($("html").hasClass('nav-is-open'))
        {
            // do nothing, nav is open
        }
        else
        {
            if (window.pageYOffset > preheader_offset) 
            {
                //$("html").removeClass("search-is-open");
                $('html').addClass('sticky-nav'); 
                $("#search-tool").removeClass("show-search");
                $(".search-active-overlay").removeClass("fade-content");
                $("body").removeClass("search-active");

            } 
            else 
            {
                $('html').removeClass('sticky-nav');
            }
        }
    }

    $( "#mobile-nav-toggle" ).click(function()
    {

        $("#search-tool").removeClass("show-search");
        $(".search-active-overlay").removeClass("fade-content");
        $("body").removeClass("search-active");


        if ($("html").hasClass('nav-is-open'))
        {
            $("html").removeClass('nav-is-open');
        }
        else
        {
            $("html").addClass('nav-is-open');
        }
    });


    $("#primary-nav ul li.menu-item-has-children").on('click', function(e)    
    {
        // tnere is 50px margin right on each of these items
        // therefore if the click happens to be within list item width -50px, then it's on the pseudo el
        var is_active = $(this).hasClass("active");
        if (e.offsetX > (this.offsetWidth - 50))
        {
            // close em all
            $("#primary-nav ul li.menu-item-has-children").removeClass('active');
            // only open if it was closed
            if (!is_active)
            {
                $(this).addClass("active");
            }
        }
    });

    // RESET THE MENU
    $(window).on('resize', function()
    {
        $("html").removeClass('nav-is-open');
        $("#primary-nav ul li.menu-item-has-children").removeClass('active');
    });

});