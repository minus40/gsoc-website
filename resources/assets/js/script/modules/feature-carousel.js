$(document).ready(function()
{
    $(".js-slick").each(function(i, div)
    {
        $(div).slick({
            dots: true,
            slidesToShow: 1,
            autoplay: true,
            autoplaySpeed: 7500,
            arrows: false
        });
    });


});