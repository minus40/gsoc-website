$(document).ready(function()
{

    $(".slick-standard").slick({
        dots: false,
        arrows: true,
        autoplay: true,
        autoplaySpeed: 5000,
    });


    $(".slick-cards").slick({
        dots: false,
        arrows: true,
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 3,
        centerMode: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: false,
                    dots: false,
                    arrows: true,
                    centerMode: false,
                }
            },
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false,
                    arrows: true,
                    centerMode: false,

                }
            }
        ]

    });


    var cards = $('.slick-cards .card');
    var maxHeight = 0;

    if (cards)
    {
        // Loop all cards and check height, if bigger than max then save it
        for (var i = 0; i < cards.length; i++) 
        {
            if (maxHeight < $(cards[i]).outerHeight()) 
            {
                maxHeight = $(cards[i]).outerHeight();
            }
        }
        // Set ALL card bodies to this height
        for (var i = 0; i < cards.length; i++) 
        {
            $(cards[i]).height(maxHeight);
        }

    }

});


// slick changes on resize
$(window).on('load resize orientationchange', function() 
{
    $('.slick-on-small').each(function(){
        var $carousel = $(this);

        if ($(window).width() > 640) 
        {
            if ($carousel.hasClass('slick-initialized')) {
                $carousel.slick('unslick');
            }
        }
        else
        {
            if (!$carousel.hasClass('slick-initialized')) 
            {
                $carousel.slick({
                  dots: true,
                  arrows: false,
                  infinite: false,
                  slidesToShow: 1,
                  slidesToScroll: 1,
                  centerMode: false,
                  //autoplay: true,
                  pauseOnHover:true,
                  pauseOnFocus: false,
                  autoplaySpeed: 5000,
                });
            }
        }
    });

});