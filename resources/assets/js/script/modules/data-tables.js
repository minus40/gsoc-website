$(document).ready(function()
{

    if ($('.sortable-table-none').length)
    {
        $('.sortable-table-none').DataTable(
        {
            "searching": false,
            "info": false,
            "paging": false,
            "order": [[ 0, "asc" ]]
        });
    

    } 

    if ($('.map-table').length){
        $('.map-table').DataTable({
            "searching": true,
            "info": false,
            "paging": true,
            "language": {
                "search": "Search Counties/Divisions:"
            }
        });
    }
});

