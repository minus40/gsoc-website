// GSOC/ IIS renders random "l sep" characters for reasons hard ro explain.
// This jquery removes them.
$(document).ready(function () 
{
	if ($('.chartExists').length)
	{
		// this is a hack as LSEP removal kills JS Charts
	}
    else if ($('.dataTable').length)
    {
        // this is a hack as LSEP removal kills Sorting
    }

    else if ($('#complaintForm').length)
    {
        // this is a hack as LSEP removal kills Form
    }
    else if ($('.fancybox').length)
    {
        // this is a hack as LSEP removal kills Form
    }

    else
	{	
        //document.body.innerHTML = document.body.innerHTML.replace(/\u2028/g, ' ');
        if ($('#mainContentStart').length)
        {
            document.getElementById("mainContentStart").innerHTML =  document.getElementById("mainContentStart").innerHTML.replace(/\u2028/g, ' ');
        }

    }

    if ($('#sideNav').length)
    {
    	document.getElementById("sideNav").innerHTML =  document.getElementById("sideNav").innerHTML.replace(/\u2028/g, ' ');
    }


    /*if ($('#theFooter').length)
    {
    	document.getElementById("theFooter").innerHTML =  document.getElementById("theFooter").innerHTML.replace(/\u2028/g, ' ');
    }*/

 })