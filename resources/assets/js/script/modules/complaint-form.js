$(document).ready(function()
{

    if ($('#complaintForm').length)
    {


        $(document).on("click", "#representative_required_yes" , function()
        {
            $( "#representative_consent_question" ).removeClass( "d-none" );
        });

        $(document).on("click", "#representative_required_no" , function()
        {
            $( "#representative_consent_question" ).addClass( "d-none" );
        });

        $(document).on("click", "#behalf_yes" , function()
        {
            $( "#complaint_part_one_section_b" ).removeClass( "d-none" );
        });


        $(document).on("click", "#behalf_no" , function()
        {
            $( "#complaint_part_one_section_b" ).addClass( "d-none" );
        });

        $(document).on("click", "#correspondence_different" , function()
        {
            $( "#complainant_secondary_address" ).removeClass( "d-none" );
        });


        $(document).on("click", "#correspondence_same" , function()
        {
            $( "#complainant_secondary_address" ).addClass( "d-none" );
        });


        $(document).on("click", "#has_witnesses_true" , function()
        {
            $( "#witnesses_area" ).removeClass( "d-none" );
        });

        $(document).on("click", "#has_witnesses_false" , function()
        {
            $( "#witnesses_area" ).addClass( "d-none" );
        });


        $(document).on("click", "#has_evidence_true" , function()
        {
            $( "#evidence_area" ).removeClass( "d-none" );
        });

        $(document).on("click", "#has_evidence_false" , function()
        {
            $( "#evidence_area" ).addClass( "d-none" );
        });

        $(document).on("click", "#has_injuries_true" , function()
        {
            $( "#injuries_area" ).removeClass( "d-none" );
        });

        $(document).on("click", "#has_injuries_false" , function()
        {
            $( "#injuries_area" ).addClass( "d-none" );
        });

        $(document).on("click", "#has_injuries_false" , function()
        {
            $( "#injuries_area" ).addClass( "d-none" );
        });

        let $complaint_form = $('form#complaintForm');
        $complaint_form.submit(function () {
            let $submit_btn = $complaint_form.find('input[type="submit"]');
            $submit_btn.attr('disabled', '1');
            return true;
        });

        $('input[type="radio"][name="third_party_representative_type"]').change(function () 
        {

            if (this.value === 'third_party_representative_type_other') // needs translations too
            {
                $( "#third_party_representative_description_other_holder" ).removeClass( "d-none" );
            }
            else{
                $( "#third_party_representative_description_other_holder" ).addClass( "d-none" );
            }
        });


        setTimeout(function() {

            $("#complainant_date_of_birth").flatpickr({
                allowInput:true,
                //altInput: true,
                dateFormat: 'd/m/Y',
                minDate: '01/01/1920',
                disableMobile: "true",
                /*onChange: function(dateObj, dateStr, instance) 
                {
                    isOver18(dateObj);
                }*/

            });


            $("#third_party_representative_date_of_birth").flatpickr({
                allowInput:true,
                //altInput: true,
                dateFormat: 'd/m/Y',
                minDate: '01/01/1920',
                disableMobile: "true"
            });

            let $complaint_date = $("#complaint_date");
            $complaint_date.flatpickr({
                allowInput:true,
                //altInput: true,
                dateFormat: 'd/m/Y',
                disableMobile: "true",
                onChange: function(dateObj, dateStr, instance) 
                {
                    isOver1(dateObj);
                }

            });
            isOver1(null, $complaint_date.val());

        }, 100);

        const consent_form_path = $("[data-consent-form-path]").data("consent-form-path");
        $("a.consent-form-link").attr("href", consent_form_path);



    }

    function isOver18(manual)
    {

        let manualSubmittedDate = new Date(manual);

        let verdict = false;
        let today = new Date();
        let eighteen_years_ago = subtractYears(today, 18);

        if (manualSubmittedDate.valueOf() < eighteen_years_ago.valueOf())
        {
             verdict =  false; // 'Over 18';
             $( "#complaint_part_one_section_b" ).addClass( "d-none" );
        }
        else
        {
            $( "#complaint_part_one_section_b" ).removeClass( "d-none" );
            verdict = true;
        }
    }


    function isOver1(manual, date_string)
    {
        let manualSubmittedDate = manual ? new Date(manual) : null;
        if(!manualSubmittedDate && date_string){
            let parts = date_string.split("/");
            if(parts.length === 3){
                manualSubmittedDate = new Date(parts[2], parts[1] - 1, parts[0]);
            }

        }
        let verdict = false;
        let today = new Date();
        let one_year_ago = subtractYears(today, 1);

        if (manualSubmittedDate && manualSubmittedDate.valueOf() < one_year_ago.valueOf())
        {
             verdict =  false; // 'Over 1';
             $( "#late_submission_content" ).removeClass( "d-none" );
             $( "#late_submission_instructions" ).removeClass( "d-none" );
        }
        else
        {
             $( "#late_submission_content" ).addClass( "d-none" );
             $( "#late_submission_instructions" ).addClass( "d-none" );
            verdict = true;
        }
    }


    function subtractYears(start_date, years) 
    {
        start_date.setFullYear(start_date.getFullYear() - years);
        return start_date;
    }

});
