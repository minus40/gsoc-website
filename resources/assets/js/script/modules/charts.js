//$(document).ready(function()
//{

    function displayHeatMapChart(jsonfile) 
    {
        //$(window).on('load', function()
        //{
            //if ( $( ".geographic-distribution-chart" ).length ) {
            var data; // loaded asynchronously

            //Albers projection values based on playing with ireland.json using D3's Albers Example
            var proj = d3.geo.albers()
                .origin([-7.9, 53.3])
                .scale(7000)
                .translate([$("#chart").innerWidth() / 2, 250]);

            var tip = d3.tip()
                .direction('e')
                .attr('class', 'd3-tip')
                .offset([-20, 0])
                .html(function (d) {
                    if (d.properties.id === 'Dublin') {
                        return "<div class='chart-modal'><table><thead><tr><th colspan='2' class='text-center'>No. of Allegations made to GSOC by<br/>Members of the Public over year</th></tr></thead><tbody><tr><td>Dublin East<br>Dublin North<br>Dublin North Central<br>Dublin South<br>Dublin South Central<br>Dublin West<br>Dublin Traffic<br>Dublin Castle<br>Garda Headquarters Phoenix Park<br>Garda National Immigration Bureau<br>Harcourt Square</td><td>" + data['Dublin East'] + "<br>" + data['Dublin North'] + "<br>" + data['Dublin North Central'] + "<br>" + data['Dublin South'] + "<br>" + data['Dublin South Central'] + "<br>" + data['Dublin West'] + "<br>" + data['Dublin Traffic'] + "<br>" + data['Dublin Castle'] + "<br>" + data['Garda Headquarters Phoenix Park'] + "<br>" + data['Garda National Immigration Bureau'] + "<br>" + data['Harcourt Square'] + "</td></tr></tbody></table></div>";
                    } else if (d.properties.id === 'Cork') {
                        return "<div class='chart-modal'><table><thead><tr><th colspan='2' class='text-center'>No. of Allegations made to GSOC by<br/>Members of the Public over year</th></tr></thead><tbody><tr><td>Cork City<br>Cork North<br>Cork West</td><td>" + data['Cork City'] + "<br>" + data['Cork North'] + "<br>" + data['Cork West'] + "</td></tr></tbody></table></div>";
                    } else {
                        return "<div class='chart-modal'><table><thead><tr><th colspan='2' class='text-center'>No. of Allegations made to GSOC by<br/>Members of the Public over year</th></tr></thead><tbody><tr><td>" + d.properties.id + "</td><td>" + data[d.properties.id] + "</td></tr></tbody></table></div>";
                    }
                });

            var path = d3.geo.path().projection(proj);

            var svg = d3.select("#chart")
                .append("svg");

            var vis = svg.append("g")
                .attr("id", "ireland");

            vis.call(tip);

            d3.json("/app/themes/hailstone/data/ireland.json", function (json) {
                vis.selectAll("path")
                    .data(json.features)
                    .enter().append("path")
                    .attr("class", "ireland")
                    .attr("d", path);
            });

            d3.json("/app/themes/hailstone/data/" + jsonfile + ".json", function (json) {
                data = json;

                vis.selectAll("path")
                    .attr("id", id)
                    .attr("class", quantize)
                    .attr("data-total", total)
                    .attr("data-county", county)
                    .on('mouseout', tip.hide)
                    .on('mouseover', tip.show);

                var tableData = "";

                $.each(data, function (key, val) {
                    tableData +=
                        "<tr><td>" + key + "</td><td>" + val + "</td></tr>";
                });

                var table = $(document).find(".map-table").dataTable();
                table.fnDestroy();

                $(".map-table tbody").html(tableData);
                table.dataTable();
            });

            function quantize(d) {
                return "q" + Math.ceil(data[d.properties.id] / 50) * 50 + "-350";
            }

            function total(d) {
                return data[d.properties.id];
            }

            function county(d) {
                return d.properties.id;
            }

            function id(d) {
                return d.properties.id.replace(/\s+/g, '-').toLowerCase();
            }

            //}
        //});
    }

//});