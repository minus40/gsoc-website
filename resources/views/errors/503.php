<!doctype html>
<html>
<head>
    <title>Site Maintenance</title>
    <link rel="stylesheet" href="/app/themes/hailstone/css/app.css">


    <style>
        body {
            background-color: white;
        }
        article {
            max-width: 500px;
            width: 90%;
            margin: 10% auto;
        }
        img.logo {
            margin-bottom: 20px;
            display: block;
            margin-left: -10px;
            max-width: 400px;
        }
    </style>

</head>

<body>
    <article>

        <img src="/app/themes/hailstone/img/layout/logo.svg" class="logo" alt="Garda Ombudsman"/>

        <h2>We&rsquo;ll be back soon!</h2>
        <div>
            <p>We're performing some maintenance to our website. Usually this only takes a few moments, and we should be online again shortly. We apologise for any inconvenience this may cause.</p>
        </div>
    </article>
</body>

</html>