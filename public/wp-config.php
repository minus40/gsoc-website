<?php
@ini_set( 'upload_max_size' , '20M' );
@ini_set( 'post_max_size', '20M');
@ini_set( 'max_execution_time', '300' );
/*
|--------------------------------------------------------------------------
| Register The Composer Autoloader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our application. We just need to utilize it! We'll simply require it
| into the script here so that we don't have to worry about manual
| loading any of our classes later on. It feels nice to relax.
|
*/

require __DIR__.'/../vendor/autoload.php';

/*
|--------------------------------------------------------------------------
| Wordpress Config
|--------------------------------------------------------------------------
|
| We need to load in all of our Wordpress configuration / settings.
| This is done through our custom bootstrapper.
|
*/

$app = require_once __DIR__.'/../bootstrap/app.php';

/*
|--------------------------------------------------------------------------
| Set the Database Prefix
|--------------------------------------------------------------------------
| This needs to be set like this for Wordpress to pick up on it.
|
 */

$table_prefix = env('DB_PREFIX', 'zsh_');

/*
|--------------------------------------------------------------------------
| Load Wordpress
|--------------------------------------------------------------------------
|
|
*/
require_once(ABSPATH . 'wp-settings.php');