<?php
global $paged;

$context = Timber\Timber::get_context();
$context['title'] = 'found for "'. get_search_query() . '"';
$context['total'] = $wp_query->found_posts;


$per_page  = $wp_query->query_vars['posts_per_page'];
$cur_page  = $wp_query->query_vars['paged'];
if ($cur_page == 0) $cur_page = 1;
$context['start_count'] = (($cur_page - 1) * $per_page) + 1;
$context['end_count'] = ($cur_page * $per_page);
if ($context['start_count'] < 1) $context['start_count'] = 1;
if ($context['total'] < $context['end_count']) $context['end_count'] = $context['total'];

$context['pagination'] = Timber::get_pagination();


Timber::render('search.twig', $context);