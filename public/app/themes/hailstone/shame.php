<?php

add_action( 'admin_menu', function(){
    add_menu_page(
        'Downloads Monitor',
        'Downloads Monitor',
        'manage_options',
        '/downloads-monitor.php',
        'downloadsPageContent',
        'dashicons-chart-bar'
    );
});

/**
 * Surely there's a way to include this in the class?
 */
function downloadsPageContent()
{
    ?>
    <div class="wrap">
        <?php if(!isset($_GET['time'])): ?>
            <h2>Popular Downloads</h2>
        <?php else: ?>

            <?php if(is_numeric($_GET['time']) && $_GET['time'] > 1): ?>
                <h2>Popular Downloads of last <?php echo $_GET['time']; ?> days</h2>
            <?php endif; ?>

            <?php if(is_numeric($_GET['time']) && $_GET['time'] == 1): ?>
                <h2>Popular Downloads of today</h2>
            <?php endif; ?>

        <?php endif; ?>

        <h3>Filter Results</h3>

        <form method="post" style="clear: both;">


            <lable for="category" style="display: inline-block; min-width: 60px;">Category</lable>
            <?php
            $categories = get_terms( 'media_category', 'hide_empty=1' );

            $list = '<select name="category" id="category">';

            $list .= '<option value="all">All</option>';

            foreach($categories as $category)
            {
                $selected = '';
                if ( isset($_POST['category']) && $_POST['category'] == $category->term_id) $selected = 'selected="selected" ';

                $list .= '<option value="' . $category->term_id . '"' . $selected . '>' . $category->name . '</option>';
            }

            $list .= '</select>';

            echo $list;
            ?>

            <br>

            <lable for="time" style="display: inline-block; min-width: 60px;">Period</lable>
            <select name="time" id="time">
                <option <?php if (isset($_POST['time']) && $_POST['time'] == "all") echo 'selected="selected" '; ?> value="all">All Time</option>
                <option <?php if (isset($_POST['time']) && $_POST['time'] == "90") echo 'selected="selected" '; ?> value="90">Last 90 Days</option>
                <option <?php if (isset($_POST['time']) && $_POST['time'] == "30") echo 'selected="selected" '; ?> value="30">Last 30 Days</option>
                <option <?php if (isset($_POST['time']) && $_POST['time'] == "7") echo 'selected="selected" '; ?> value="7">Last 7 Days</option>
                <option <?php if (isset($_POST['time']) && $_POST['time'] == "1") echo 'selected="selected" '; ?> value="1">Today</option>
            </select>

            <?php submit_button('Filter Table', 'button-primary send-email'); ?>
        </form>

        <?php
        new \Hailstone\Core\Services\DownloadsTable();
        ?>

    </div>
    <?php
}

function prevent_default_theme_deletion($allcaps, $caps, $args) {
    $post_id = 134;
    if ( isset( $args[0] ) && isset( $args[2] ) && $args[2] == $post_id && $args[0] == 'delete_post' ) {
        $allcaps[ $caps[0] ] = false;
    }
    return $allcaps;
}
add_filter ('user_has_cap', 'prevent_default_theme_deletion', 10, 3);

// Submit Complaint Form Anonymous Survey
add_action( 'gform_after_submission_5', 'process_anonymous_form', 10, 2 );

function process_anonymous_form( $entry )
{
    $data = [
        'sex' => filter_var(rgar($entry, '2'),FILTER_SANITIZE_STRING),
        'age' => filter_var(rgar($entry, '3'),FILTER_SANITIZE_STRING),
        'nationality' => filter_var(rgar($entry, '4'),FILTER_SANITIZE_STRING),
        'nationality_other' => filter_var(rgar($entry, '13'),FILTER_SANITIZE_STRING),
        'countryofbirth' => filter_var(rgar($entry, '14'),FILTER_SANITIZE_STRING),
        'other_countryofbirth' => filter_var(rgar($entry, '15'),FILTER_SANITIZE_STRING),
        'ethnicbackground' => filter_var(rgar($entry, '5'),FILTER_SANITIZE_STRING),
        'other_ethnicbackground' => filter_var(rgar($entry, '16'),FILTER_SANITIZE_STRING),
        'primarylanguage' => filter_var(rgar($entry, '6'),FILTER_SANITIZE_STRING),
        'other_primarylanguage' => filter_var(rgar($entry, '17'),FILTER_SANITIZE_STRING),
        'disability' => filter_var(rgar($entry, '7'),FILTER_SANITIZE_STRING),
        'other_disability' => filter_var(rgar($entry, '18'),FILTER_SANITIZE_STRING),
        'religion' => filter_var(rgar($entry, '8'),FILTER_SANITIZE_STRING),
        'other_religion' => filter_var(rgar($entry, '19'),FILTER_SANITIZE_STRING),
        'sexualorientation' => filter_var(rgar($entry, '9'),FILTER_SANITIZE_STRING),
        'other_sexualorientation' => filter_var(rgar($entry, '20'),FILTER_SANITIZE_STRING),
        'housingstatus' => filter_var(rgar($entry, '10'),FILTER_SANITIZE_STRING),
        'other_housingstatus' => filter_var(rgar($entry, '21'),FILTER_SANITIZE_STRING),
        'education' => filter_var(rgar($entry, '11'),FILTER_SANITIZE_STRING),
        'other_education' => filter_var(rgar($entry, '22'),FILTER_SANITIZE_STRING),
        'employmentstatus' => filter_var(rgar($entry, '12'),FILTER_SANITIZE_STRING),
        'other_employmentstatus' => filter_var(rgar($entry, '23'),FILTER_SANITIZE_STRING),
    ];

    $datetime = current_time('mysql');
    $deleted = 0;

   // return true;

    try {
        $conn = new PDO('sqlsrv:server=' . env("SURVEY_DB_HOST") . ";Database=" . env('SURVEY_DB_NAME'), env('SURVEY_DB_USER'), env('SURVEY_DB_PASSWORD'));
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $stmt = $conn->prepare("INSERT INTO survey (sex, age, nationality, nationality_other, countryofbirth, other_countryofbirth, ethnicbackground, other_ethnicbackground, primarylanguage, other_primarylanguage, disability, other_disability, religion, other_religion, sexualorientation, other_sexualorientation, housingstatus, other_housingstatus, education, other_education, employmentstatus, other_employmentstatus, datetimestamp, deleted) 
                              VALUES (:sex, :age, :nationality, :nationality_other, :countryofbirth, :other_countryofbirth, :ethnicbackground, :other_ethnicbackground, :primarylanguage, :other_primarylanguage, :disability, :other_disability, :religion, :other_religion, :sexualorientation, :other_sexualorientation, :housingstatus, :other_housingstatus, :education, :other_education, :employmentstatus, :other_employmentstatus, :datetimestamp, :deleted)");

        $stmt->bindParam(':sex', $data['sex']);
        $stmt->bindParam(':age', $data['age']);
        $stmt->bindParam(':nationality', $data['nationality']);
        $stmt->bindParam(':nationality_other', $data['nationality_other']);
        $stmt->bindParam(':countryofbirth', $data['countryofbirth']);
        $stmt->bindParam(':other_countryofbirth', $data['other_countryofbirth']);
        $stmt->bindParam(':ethnicbackground', $data['ethnicbackground']);
        $stmt->bindParam(':other_ethnicbackground', $data['other_ethnicbackground']);
        $stmt->bindParam(':primarylanguage', $data['primarylanguage']);
        $stmt->bindParam(':other_primarylanguage', $data['other_primarylanguage']);
        $stmt->bindParam(':disability', $data['disability']);
        $stmt->bindParam(':other_disability', $data['other_disability']);
        $stmt->bindParam(':religion', $data['religion']);
        $stmt->bindParam(':other_religion', $data['other_religion']);
        $stmt->bindParam(':sexualorientation', $data['sexualorientation']);
        $stmt->bindParam(':other_sexualorientation', $data['other_sexualorientation']);
        $stmt->bindParam(':housingstatus', $data['housingstatus']);
        $stmt->bindParam(':other_housingstatus', $data['other_housingstatus']);
        $stmt->bindParam(':education', $data['education']);
        $stmt->bindParam(':other_education', $data['other_education']);
        $stmt->bindParam(':employmentstatus', $data['employmentstatus']);
        $stmt->bindParam(':other_employmentstatus', $data['other_employmentstatus']);
        $stmt->bindParam(':datetimestamp', $datetime);
        $stmt->bindParam(':deleted', $deleted);
        $stmt->execute();

    }

    catch(PDOException $e) {
        dd($e->getMessage());
    }

    $conn = null;
};

// Submit Contact Us Form
if(env("CONTACT_DB_NAME") and env("CONTACT_DB_USER") and env("CONTACT_DB_PASSWORD") and env("CONTACT_DB_HOST") and env("CONTACT_DB_TABLE")) {
    add_action( 'gform_after_submission_1', 'process_contact_form', 10, 2 );

    function process_contact_form( $entry )
    {
        $data = [
            'name_first' => filter_var(rgar($entry, '1.3'),FILTER_SANITIZE_STRING),
            'name_last' => filter_var(rgar($entry, '1.6'),FILTER_SANITIZE_STRING),
            'email' => filter_var(rgar($entry, '2'),FILTER_SANITIZE_STRING),
            'phone' => filter_var(rgar($entry, '5'),FILTER_SANITIZE_STRING),
            'comments' => filter_var(rgar($entry, '4'),FILTER_SANITIZE_STRING),
        ];

        $datetime = current_time('mysql');
        $deleted = 0;

        try {
            $conn = new \PDO('sqlsrv:server=' . env("CONTACT_DB_HOST") . ";Database=" . env('CONTACT_DB_NAME'), env('CONTACT_DB_USER'), env('CONTACT_DB_PASSWORD'));
            $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            $stmt = $conn->prepare("INSERT INTO " . env('CONTACT_DB_TABLE') . " (first_name, last_name, email, phone, comments, time_stamp, deleted)
                              VALUES (:name_first, :name_last, :email, :phone, :comments, :datetimestamp, :deleted)");

            $stmt->bindParam(':name_first', $data['name_first']);
            $stmt->bindParam(':name_last', $data['name_last']);
            $stmt->bindParam(':email', $data['email']);
            $stmt->bindParam(':phone', $data['phone']);
            $stmt->bindParam(':comments', $data['comments']);
            $stmt->bindParam(':datetimestamp', $datetime);
            $stmt->bindParam(':deleted', $deleted);
            $stmt->execute();

        }

        catch(PDOException $e) {
            dd($e->getMessage());
        }

        $conn = null;
    };
}

// Submit FOI Form
if(env("FOI_DB_NAME") and env("FOI_DB_USER") and env("FOI_DB_PASSWORD") and env("FOI_DB_HOST") and env("FOI_DB_TABLE")) {
    add_action( 'gform_after_submission_3', 'process_foi_form', 10, 2 );

    function process_foi_form( $entry )
    {
        $data = [
            'name_first' => filter_var(rgar($entry, '1.3'),FILTER_SANITIZE_STRING),
            'name_last' => filter_var(rgar($entry, '1.6'),FILTER_SANITIZE_STRING),
            'address_line_1' => filter_var(rgar($entry, '5.1'),FILTER_SANITIZE_STRING),
            'address_line_2' => filter_var(rgar($entry, '5.2'),FILTER_SANITIZE_STRING),
            'address_line_3' => null,
            'address_line_4' => null,
            'city' => filter_var(rgar($entry, '5.3'),FILTER_SANITIZE_STRING),
            'county' => filter_var(rgar($entry, '5.4'),FILTER_SANITIZE_STRING),
            'postal_code' => filter_var(rgar($entry, '5.5'),FILTER_SANITIZE_STRING),
            'country' => filter_var(rgar($entry, '5.6'),FILTER_SANITIZE_STRING),
            'email' => filter_var(rgar($entry, '2'),FILTER_SANITIZE_STRING),
            'phone' => filter_var(rgar($entry, '3'),FILTER_SANITIZE_STRING),
            'foi_2014' => filter_var(rgar($entry, '6'),FILTER_SANITIZE_STRING),
            'specific_requests' => filter_var(rgar($entry, '7'),FILTER_SANITIZE_STRING),
            'method_of_contact' => filter_var(rgar($entry, '10'),FILTER_SANITIZE_STRING),
        ];

        $datetime = current_time('mysql');
        $deleted = 0;

        try {
            $conn = new \PDO('sqlsrv:server=' . env("FOI_DB_HOST") . ";Database=" . env('FOI_DB_NAME'), env('FOI_DB_USER'), env('FOI_DB_PASSWORD'));
            $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            $stmt = $conn->prepare("INSERT INTO " . env('FOI_DB_TABLE') . " (first_name, last_name, address1, address2, address3, address4, city, county, postal_code, country, email, phone, FOI_act_2014, FOI_details, methodofcontact, time_stamp, deleted) 
                              VALUES (:name_first, :name_last, :address_line_1, :address_line_2, :address_line_3, :address_line_4, :city, :county, :postal_code, :country, :email, :phone, :foi_2014, :specific_requests, :method_of_contact, :datetimestamp, :deleted)");

            $stmt->bindParam(':name_first', $data['name_first']);
            $stmt->bindParam(':name_last', $data['name_last']);
            $stmt->bindParam(':address_line_1', $data['address_line_1']);
            $stmt->bindParam(':address_line_2', $data['address_line_2']);
            $stmt->bindParam(':address_line_3', $data['address_line_3']);
            $stmt->bindParam(':address_line_4', $data['address_line_4']);
            $stmt->bindParam(':city', $data['city']);
            $stmt->bindParam(':county', $data['county']);
            $stmt->bindParam(':postal_code', $data['postal_code']);
            $stmt->bindParam(':country', $data['country']);
            $stmt->bindParam(':email', $data['email']);
            $stmt->bindParam(':phone', $data['phone']);
            $stmt->bindParam(':foi_2014', $data['foi_2014']);
            $stmt->bindParam(':specific_requests', $data['specific_requests']);
            $stmt->bindParam(':method_of_contact', $data['method_of_contact']);
            $stmt->bindParam(':datetimestamp', $datetime);
            $stmt->bindParam(':deleted', $deleted);
            $stmt->execute();

        }

        catch(PDOException $e) {
            dd($e->getMessage());
        }

        $conn = null;
    };
}

// Submit PD Form
if(env("PD_DB_NAME") and env("PD_DB_USER") and env("PD_DB_PASSWORD") and env("PD_DB_HOST") and env("PD_DB_TABLE")) {
    add_action( 'gform_after_submission_4', 'process_pd_form', 10, 2 );

    function process_pd_form( $entry )
    {
        $data = [
            'name_first' => filter_var(rgar($entry, '1.3'),FILTER_SANITIZE_STRING),
            'name_last' => filter_var(rgar($entry, '1.6'),FILTER_SANITIZE_STRING),
            'address_line_1' => filter_var(rgar($entry, '2.1'),FILTER_SANITIZE_STRING),
            'address_line_2' => filter_var(rgar($entry, '2.2'),FILTER_SANITIZE_STRING),
            'address_line_3' => null,
            'address_line_4' => null,
            'city' => filter_var(rgar($entry, '2.3'),FILTER_SANITIZE_STRING),
            'county' => filter_var(rgar($entry, '2.4'),FILTER_SANITIZE_STRING),
            'postal_code' => filter_var(rgar($entry, '2.5'),FILTER_SANITIZE_STRING),
            'country' => filter_var(rgar($entry, '2.6'),FILTER_SANITIZE_STRING),
            'email' => filter_var(rgar($entry, '3'),FILTER_SANITIZE_STRING),
            'phone' => filter_var(rgar($entry, '4'),FILTER_SANITIZE_STRING),
            'specific_requests' => filter_var(rgar($entry, '5'),FILTER_SANITIZE_STRING),
            'gsoc_ref_number' => filter_var(rgar($entry, '6'),FILTER_SANITIZE_STRING),
            'garda_reg_number' => filter_var(rgar($entry, '7'),FILTER_SANITIZE_STRING),
            'list_of_stations' => filter_var(rgar($entry, '8'),FILTER_SANITIZE_STRING),
            'proof_of_id' => filter_var(rgar($entry, '9'),FILTER_SANITIZE_STRING),
            'proof_of_address' => filter_var(rgar($entry, '10'),FILTER_SANITIZE_STRING),
        ];

        $datetime = current_time('mysql');
        $deleted = 0;

        try {
            $conn = new \PDO('sqlsrv:server=' . env("PD_DB_HOST") . ";Database=" . env('PD_DB_NAME'), env('PD_DB_USER'), env('PD_DB_PASSWORD'));
            $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            $stmt = $conn->prepare("INSERT INTO " . env('PD_DB_TABLE') . " (first_name, last_name, address1, address2, address3, address4, city, county, postal_code, country, email, phone, PD_details, gsoc_ref_number, garda_reg_number, list_of_stations, id_proof, address_proof, time_stamp, deleted) 
                              VALUES (:name_first, :name_last, :address_line_1, :address_line_2, :address_line_3, :address_line_4, :city, :county, :postal_code, :country, :email, :phone, :specific_requests, :gsoc_ref_number, :garda_reg_number, :list_of_stations, :proof_of_id, :proof_of_address, :datetimestamp, :deleted)");

            $stmt->bindParam(':name_first', $data['name_first']);
            $stmt->bindParam(':name_last', $data['name_last']);
            $stmt->bindParam(':address_line_1', $data['address_line_1']);
            $stmt->bindParam(':address_line_2', $data['address_line_2']);
            $stmt->bindParam(':address_line_3', $data['address_line_3']);
            $stmt->bindParam(':address_line_4', $data['address_line_4']);
            $stmt->bindParam(':city', $data['city']);
            $stmt->bindParam(':county', $data['county']);
            $stmt->bindParam(':postal_code', $data['postal_code']);
            $stmt->bindParam(':country', $data['country']);
            $stmt->bindParam(':email', $data['email']);
            $stmt->bindParam(':phone', $data['phone']);
            $stmt->bindParam(':specific_requests', $data['specific_requests']);
            $stmt->bindParam(':gsoc_ref_number', $data['gsoc_ref_number']);
            $stmt->bindParam(':garda_reg_number', $data['garda_reg_number']);
            $stmt->bindParam(':list_of_stations', $data['list_of_stations']);
            $stmt->bindParam(':proof_of_id', $data['proof_of_id']);
            $stmt->bindParam(':proof_of_address', $data['proof_of_address']);
            $stmt->bindParam(':datetimestamp', $datetime);
            $stmt->bindParam(':deleted', $deleted);
            $stmt->execute();

        }

        catch(PDOException $e) {
            dd($e->getMessage());
        }

        $conn = null;
    };
}

/*
    Add a button on the admin menu for page to replace DB staging URLs with live URLs
*/
add_action( 'admin_menu', 'makeDbUrlSyncMenuButton' );
function makeDbUrlSyncMenuButton()
{
    add_menu_page(
        'DB URL Sync',
        'DB URL Sync',
        'edit_others_posts',
        '/db-sync-url.php',
        'dbUrlSync',
        'dashicons-share-alt'
    );
}

function dbUrlSync()
{

    global $wpdb;

    // this array needs updated on a project by project basis.
    // you should include the live URL in here too
    // no trailing slashes
    $allUrlsInUse = array
    (
        'http://gsocstaging.gsoc.ie',
        'http://gsoc-staging.m40.xyz',
        'http://gardaombudsman.dev',
        'http://gsoc.ie',
        'http://preprod.gsoc.ie',
        'https://preprod.gsoc.ie',
        'https://gsocstaging.gsoc.ie',
        'https://prod1.ie',
        'https://prod2.ie',
        'https://gsoc-staging.m40.xyz',
        'https://gardaombudsman.ie',
        'http://gardaombudsman.ie'
    );
   
    $useThisUrl = $path = rtrim(env('APP_URL'), '/');

    echo "<br /><br /><br />";
    echo "<h2>Database Sync Report</h2>";
    echo "<div>";


    foreach($allUrlsInUse as $replaceThisUrl):
        if($replaceThisUrl != $useThisUrl):

            $queries = array();
            $queries['option'] = "UPDATE ".$wpdb->prefix."options SET option_value = replace(option_value, '".$replaceThisUrl."', '".$useThisUrl."') WHERE option_name = 'home' OR option_name = 'siteurl'"; 
            $queries['guid'] = "UPDATE ".$wpdb->prefix."posts SET guid = replace(guid, '".$replaceThisUrl."', '".$useThisUrl."')"; 
            $queries['post_content'] = "UPDATE ".$wpdb->prefix."posts SET post_content = replace(post_content, '".$replaceThisUrl."', '".$useThisUrl."')"; 
            $queries['postmeta'] = "UPDATE ".$wpdb->prefix."postmeta SET meta_value = replace(meta_value, '".$replaceThisUrl."', '".$useThisUrl."')"; 
          
            echo "<br /><h4>Converting ". $replaceThisUrl . " to " . $useThisUrl ."</h4>";

            foreach($queries as $key=>$val):
                $result = $wpdb->query($val);
                if ($result === false):
                    echo "<p><b>".$key.": error</b>N/A.</p>";
                else:
                    echo "<p><b>".$key.": </b> success (".$result." row(s) changed).</p>";                    
                endif;
            endforeach;

        endif;
    endforeach;

    echo "</div>";

}

function trimFinalCharIfSlash($string)
{
    return rtrim($string, "/");
}


// set the max results per search page to 10
function changeWPSearchQueryResults($queryVars) {
    if ( isset($_REQUEST['s']) ) 
        $queryVars['posts_per_page'] = 12; 
    return $queryVars;
}
add_filter('request', 'changeWPSearchQueryResults'); 



if (! function_exists('trans')) {

    function trans($key, $fallback_to_english = false)
    {
        $app = app();
        
        $language = (app('session'))->get('language');
        
        if(is_null($language)) 
        {
            $language = 'en';
        }

        list($filename, $key) = explode(".", $key);

        $file = $app->basePath() . "/resources/lang/{$language}/{$filename}.php";

        if(!is_file($file)) 
        {
            return("[ERROR] A translation file does not exist for language: $language.");
        }

        $messages = include $file;

        if(!is_array($messages) || !key_exists($key, $messages))
        {

            if ($fallback_to_english && $language != 'en' )
            {
                $file_en = $app->basePath() . "/resources/lang/en/{$filename}.php";

                if(!is_file($file_en)) 
                {
                    return("[ERROR] Both <b>$language</b> <u>and</u> <b>EN</b> are missing the key <b>$key</b>.");
                }

                $messages_en = include $file_en;
                if (key_exists($key, $messages_en))
                {
                    return "<b>[ERROR] [EN]</b>:" . $messages_en[$key];
                }
            }

            return("[ERROR] A translation item does not exist for key <b>$key</b> in language <b>$language</b>.");

        }

        return $messages[$key];
    }


}


if(!function_exists('formatPhoneNumber'))
{
    // take a phone number with special chars, and clean it up for mobile usage

    function formatPhoneNumber($number)
    {
        $number = str_replace(' ', '', $number); // remove spaces
        $number = str_replace('+', '00', $number); // exchange + for 0
        $number = str_replace('(1)', '', $number); // remove optional (0)
        return ($number);
    }
}

// get_page_by_title is deprecated wp 6.2
function m40_get_page_by_title($title, $post_type = 'page')
{

    $posts = get_posts(
        array(
            'post_type'              => $post_type,
            'title'                  => $title,
            'post_status'            => 'publish',
            'numberposts'            => 1,
            'update_post_term_cache' => false,
            'update_post_meta_cache' => false,           
            'orderby'                => 'post_date ID',
            'order'                  => 'ASC',
        )
    );
     
    if ( ! empty( $posts ) ) {
        return $posts[0];
    }
    return null;
}