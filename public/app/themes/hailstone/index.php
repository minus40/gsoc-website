<?php

use Hailstone\Core\Page;

$context = Timber\Timber::get_context();

$context['page'] = new Page();

//global $wp_query;
//var_dump($wp_query->query_vars);

Timber::render('index.twig', $context);