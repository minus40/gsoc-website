<?php

$context = Timber\Timber::get_context();

$context['post'] = Article::current();

$current_post_id = [
    Article::current()->id
];

$extras = $context['post']->embedExtraScripts();



Timber::render('single-article.twig', $context);