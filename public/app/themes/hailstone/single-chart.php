<?php

$context = Timber\Timber::get_context();

$context['post'] = Chart::current();

$extras = $context['post']->embedExtraScripts('chart');

Timber::render('single-chart.twig', $context);