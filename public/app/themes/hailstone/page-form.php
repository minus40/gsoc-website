<?php
/* Template Name: Page : Form */

use Hailstone\Core\Page;

$context = Timber\Timber::get_context();

$context['page'] = new Page();

$extras = $context['page']->embedExtraScripts();

$context['form_url'] = (empty($_SERVER['HTTPS']) ? 'http' : 'https') . "://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

Timber::render('page-form.twig', $context);