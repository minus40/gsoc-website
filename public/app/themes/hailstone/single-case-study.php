<?php

$context = Timber\Timber::get_context();

$context['post'] = CaseStudy::current();

$extras = $context['post']->embedExtraScripts();

Timber::render('single-case-study.twig', $context);