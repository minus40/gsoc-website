<?php

$context = Timber\Timber::get_context();

$context['post'] = Career::current();

$extras = $context['post']->embedExtraScripts();

Timber::render('single-career.twig', $context);