<?php if (!defined( 'WP_HOME' )) exit;

$context = Timber\Timber::get_context();

$context['post'] = Attachment::current();

$context['is_an_image'] = wp_attachment_is_image( $context['post']->id );

if ($context['is_an_image']):

    $context['image'] = new TimberImage($context['post']->id);

endif;

Timber::render('single-attachment.twig', $context);