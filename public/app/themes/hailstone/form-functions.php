<?php

function translateFormLabels()
{
    return null;
}

function translateComplaintForm($key, $section = 'page', $fallback_to_english = false, $defined_language = null)
{
    $outcome  = translateFormLabel($key, $section, $filename = 'submit-complaint', $fallback_to_english, $defined_language);
    return $outcome;
}

function getLanguageCode()
{

    $language_code = (app('session'))->get('language');

    if(!is_null($language_code))
    {
        return $language_code;
    }
    return 'en';
}

function getLanguageConsentFormPath($language_code = null)
{
    $language_code = $language_code? : getLanguageCode();

    return "/app/themes/hailstone/lang/".strtolower($language_code)."/consent-form.pdf";
}

function setRTLClassIfNeeded()
{

    $language_code = getLanguageCode();

    if (strtoupper($language_code) == 'AR')
    {
        return "rtl-form-page";
    }
    return null;
}


function translateFormLabel($key, $section, $filename, $fallback_to_english = false, $defined_language = null)
{
    $app = app();
    
    $language = $defined_language ? : (app('session'))->get('language');
    
    if(is_null($language)) 
    {
        $language = 'en';
    }

    $file = $app->basePath() . "/resources/lang/{$language}/{$filename}.php";

    if(!is_file($file)) 
    {
        return("[ERROR] A translation file does not exist for language: $language.");
    }

    $messages = include $file;

    if(!is_array($messages) || !array_key_exists($section, $messages) || !is_array($messages[$section]) || !array_key_exists($key, $messages[$section]))
    {

        if ($fallback_to_english && $language != 'en' )
        {
            $file_en = $app->basePath() . "/resources/lang/en/{$filename}.php";

            if(!is_file($file_en)) 
            {
                return("[ERROR] Both <b>$language</b> <u>and</u> <b>EN</b> are missing the key <b>$key</b>.");
            }

            $messages_en = include $file_en;
            if (array_key_exists($section, $messages_en) && array_key_exists($key, $messages_en[$section]))
            {
                return "<b>[ERROR] [EN]</b>:" . ($messages_en[$key] ?? "");
            }
        }

        return("[ERROR] A translation item does not exist for key <b>$key</b> in language <b>$language</b>.");

    }

    return $messages[$section][$key];
}
