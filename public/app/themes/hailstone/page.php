<?php

use Hailstone\Core\Page;

$context = Timber\Timber::get_context();

$context['page'] = new Page();

$extras = $context['page']->embedExtraScripts();


Timber::render('page.twig', $context);