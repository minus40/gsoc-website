<?php
/* Template Name: Page : Press Releases Listing */

$context = Timber\Timber::get_context();

$context['page'] = Page::current();
$context['articles'] = Article::orderBy('date', 'DESC')
                                    ->whereCategoriesIn('press-releases')
                                    ->paginate(8);

Timber::render('page-news.twig', $context);