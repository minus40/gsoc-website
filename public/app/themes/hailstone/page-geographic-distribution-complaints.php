<?php

use Hailstone\Core\Page;

$context = Timber\Timber::get_context();

$context['page'] = new Page();

//$allegations_data = file_get_contents("". get_template_directory_uri() . "/data/allegations.json");
//$allegations = json_decode($allegations_data, true);
//$counties_data = file_get_contents("". get_template_directory_uri() . "/data/ireland.json");
//$counties = json_decode($counties_data, true);
//
//$context['allegations'] = $allegations;
//$context['counties'] = $counties;

$extras = $context['page']->embedExtraScripts('chart');  

Timber::render('page.twig', $context);