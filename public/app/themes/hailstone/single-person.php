<?php

$context = Timber\Timber::get_context();

$context['post'] = People::current();
$extras = $context['post']->embedExtraScripts();

Timber::render('single-person.twig', $context);