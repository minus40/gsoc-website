<?php

use Hailstone\Core\Page;

$context = Timber\Timber::get_context();

$context['page'] = new Page();

$language = (app('session'))->get('language');
if(!$language) {
    (app('session'))->set('language', 'en');
}
$language = (app('session'))->get('language');

if ($language == 'en') 
{
    $context['publish_anonymous_form'] = true;
}


Timber::render('page-confirmation.twig', $context);