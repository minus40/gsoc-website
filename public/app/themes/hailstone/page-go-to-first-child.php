<?php
/*
 Template Name: Page: Go To First Child
 */

use Hailstone\Core\Page;

$context = Timber\Timber::get_context();

$context['page'] = new Page();

$childPages = get_pages("child_of=".$post->ID."&sort_column=menu_order");
if ($childPages)
{
    $firstChild = $childPages[0];
    wp_redirect(get_permalink($firstChild->ID));
}

Timber::render('page.twig', $context);