<?php
/* Template Name: Page : Listings (with Archives and Pagination) */

$context = Timber\Timber::get_context();

$context['page'] = Page::current();
$extras = $context['page']->embedExtraScripts();
$context['extra_partials'] = $extras;

$context['start_count'] = $context['page']->counts_start_count;
$context['end_count'] = $context['page']->counts_end_count;
$context['max_results'] = $context['page']->counts_max_results;

$context['title'] = $context['page']->title;

if (isset($_GET['selected_year']) && is_numeric($_GET['selected_year']) && $_GET['selected_year'] > 2000 && $_GET['selected_year'] < 2300)
{
	$context['is_archive'] = true;
	$context['title'] = "Archive: " . $context['title'] . " (". $_GET['selected_year'] .")";
}



Timber::render('page-listings.twig', $context);