<?php

use Hailstone\Core\Page;

$context = Timber\Timber::get_context();

$context['page'] = new Page();
$context['news'] = Article::orderBy('date', 'DESC')->whereTaxonomyTermsIn('article_categories','news')->limit(2)->get();
$context['appeals'] = Article::orderBy('date', 'DESC')->whereTaxonomyTermsIn('article_categories','witness-appeals')->limit(2)->get();

Timber::render('page-home-alt.twig', $context);