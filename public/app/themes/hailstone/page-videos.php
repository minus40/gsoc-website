<?php

$context = Timber\Timber::get_context();

$context['page'] = Page::current();
$context['videos'] = Video::orderBy('post_title', 'ASC')->paginate(20);

Timber::render('page-videos.twig', $context);