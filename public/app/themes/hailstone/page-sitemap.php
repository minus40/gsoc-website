<?php

$context = Timber\Timber::get_context();

$context['page'] = Page::current();
$context['pages'] = Page::limit('-1')->whereHasNoParent()->orderBy('order', 'ASC')->get();
$context['articles'] = Article::orderBy('date', 'DESC')->get();
$context['careers'] = Career::orderBy('date', 'DESC')->get();
$context['people'] = People::orderBy('post_title', 'ASC')->get();
$context['videos'] = Video::orderBy('date', 'DESC')->get();
$context['faqs'] = FAQ::orderBy('post_title', 'ASC')->get();
$context['locations'] = Location::orderBy('post_title', 'ASC')->get();

Timber::render('page-sitemap.twig', $context);