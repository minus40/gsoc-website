<?php
/* Template Name: Page : Front Page */

use Hailstone\Core\Page;
$context = Timber\Timber::get_context();
$context['page'] = new Page();

$col2 = get_field('column2_choice');
$chart = get_field('highlighted_section_chart');

$context['featured_posts'] = [];

if ($col2 == "manual"):

    $context['temp_posts'] = get_field('column2_posts');

    foreach($context['temp_posts'] as $key => $post) 
    {
        $classname = $context['page']->classNameForTimberFromString($post->post_type);
        $context['featured_posts'][$key] = \Timber::get_post($post->ID, $classname);
    }

endif;

$max_posts = 4;
$posts_remaining = 0;
$used_appeals = [];

if (count($context['featured_posts']) < $max_posts):

    $posts_remaining = $max_posts - count($context['featured_posts']);

    $max_appeals = ($posts_remaining < 2) ? $posts_remaining  : 2;

    $appeals = \Article::orderBy('date', 'DESC')
        ->whereTaxonomyTermsIn('article_categories',['witness-appeals'])
        ->limit(2)
        ->get();

    if ($appeals):
        foreach($appeals as $key => $post) 
        {
            $used_appeals[] = $post->ID;
            $posts_remaining --;
        }  
    endif;

    if ($posts_remaining > 0):
        $news = \Article::orderBy('date', 'DESC')
            ->whereTaxonomyTermsIn('article_categories',['news', 'witness-appeals', 'press-releases'])
            ->limit($posts_remaining)
            ->wherePostNotIn($used_appeals)
            ->get();

        if ($news):
            foreach($news as $key => $post) 
            {
                $context['featured_posts'][] = $post;
            } 
        endif;

    endif;

    // appeals always last
    if ($appeals):
        foreach($appeals as $key => $post) 
        {
            $context['featured_posts'][] = $post;
        } 
    endif;

endif;


if ($chart && is_array($chart) && count($chart) > 0)
{
    $classname = $context['page']->classNameForTimberFromString('chart');
    $context['active_chart'] = \Timber::get_post($chart[0], $classname);
    $extras = $context['page']->embedExtraScripts('chart');
}
wp_reset_query();

Timber::render('front-page.twig', $context);