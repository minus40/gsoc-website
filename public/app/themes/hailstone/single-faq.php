<?php

$context = Timber\Timber::get_context();

$context['post'] = FAQ::current();

$current_post_id = [
    FAQ::current()->id
];

$context['categories'] = Timber::get_terms('faq_category');

$context['related_faqs'] = FAQ::orderBy('rand', 'ASC')
    ->wherePostNotIn($current_post_id)
    ->paginate(6);

$extras = $context['post']->embedExtraScripts();

Timber::render('single-faq.twig', $context);