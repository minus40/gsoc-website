<?php
use Hailstone\Core\Page;

$context = Timber\Timber::get_context();

$context['title'] = 'Archive';
if ( is_day() ) {
    $context['title'] = 'Archive: '.get_the_date( 'D M Y' );
} else if ( is_month() ) {
    $context['title'] = 'Archive: '.get_the_date( 'M Y' );
} else if ( is_year() ) {
    $context['title'] = 'Archive: '.get_the_date( 'Y' );
} else if ( is_tag() ) {
    $context['title'] = single_tag_title( '', false );
} else if ( is_category() ) {
    $context['title'] = single_cat_title( '', false );
    array_unshift( $templates, 'archive-' . get_query_var( 'cat' ) . '.twig' );
} else if ( is_post_type_archive() ) {
    $context['title'] = post_type_archive_title( '', false );
    array_unshift( $templates, 'archive-' . get_post_type() . '.twig' );
}

$context['page'] = new Page();
$context['posts'] = Timber::get_posts();

$paged = (isset($_GET['page']) && is_numeric($_GET['page'])) ? $_GET['page'] : null;

//$pagination = $context['page']->getArchivePagination();


$args = [
    'type'            => 'yearly',
    'limit'           => '10',
    'show_post_count' => false,
    'echo'            => 1,
    'order'           => 'DESC',
    'post_type'     => 'article'
];

$context['archives'] = new TimberArchives($args);

Timber::render( 'archive.twig', $context );