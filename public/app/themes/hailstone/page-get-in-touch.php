<?php
/* Template Name: Page : Get In Touch */

use Hailstone\Core\Page;

$context = Timber\Timber::get_context();

$context['page'] = new Page();
$extras = $context['page']->embedExtraScripts('map');  

Timber::render('page-get-in-touch.twig', $context);