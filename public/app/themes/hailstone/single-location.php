<?php

$context = Timber\Timber::get_context();

$context['post'] = Location::current();
$extras = $context['post']->embedExtraScripts('map');

Timber::render('single-location.twig', $context);