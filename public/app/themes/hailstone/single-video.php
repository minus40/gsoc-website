<?php

$context = Timber\Timber::get_context();

$context['post'] = Video::current();

$extras = $context['post']->embedExtraScripts();

Timber::render('single-video.twig', $context);