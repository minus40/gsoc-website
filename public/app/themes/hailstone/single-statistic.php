<?php

$context = Timber\Timber::get_context();

$context['post'] = Statistic::current();
$extras = $context['post']->embedExtraScripts('chart');

Timber::render('single-statistic.twig', $context);