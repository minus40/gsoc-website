<?php

$context = Timber\Timber::get_context();

$post = new \Timber\Post();
$className = ucwords($post->post_type);
$context['post'] = $className::current();

$extras = $context['post']->embedExtraScripts();


Timber::render('single.twig', $context);