<?php

namespace Hailstone\Core;

class Option
{
    public function __construct()
    {
        $this->options = get_fields('option');
        $this->setup();
    }

    public function all()
    {
        return $this->options;
    }

    private function setup()
    {
        foreach ($this->options['location'] as $key=>$location) {
            $this->options['location'][$key] = new Location($location->ID);
        }
    }

    /**
     * @param $key
     *
     * @return mixed|void
     */
    public function get($key)
    {
        return get_field($key, 'option');
    }
}