<?php

namespace Hailstone\Core;

class Chart extends Post
{
    public $postType = 'chart';
    public $usefulData =
    [
        'post_type' => 'chart',
        'class_name' => 'Chart',
        'posts_page' => 'Statistics',
        'main_taxonomy' => 'chart_categories',
        'orderby' => 'date',
        'order' => 'DESC',
    ];

}