<?php

namespace Hailstone\Core;

use Hailstone\Core\Post;
use Hailstone\Core\Traits\HasCarousel;
use Hailstone\Core\Traits\HasContentBlocks;

class Statistic extends Post
{
    public $postType = 'statistic';
    public $usefulData =
    [
        'post_type' => 'statistic',
        'class_name' => 'Statistic',
        'posts_page' => 'Statistics',
        'main_taxonomy' => 'statistic_categories',
        'orderby' => 'date',
        'order' => 'DESC',
    ];
    var $_categories;


    /**
     * @return array
     */
    public function categories()
    {
        if(!$this->_categories)
        {
            $categories = $this->terms('statistic_categories');
            if (is_array($categories) && count($categories)) {
                $this->_categories = $categories;
            }
        }
        return $this->_categories;
    }


    /**
     * @param $key
     * @param $terms
     * @param bool $includeChildren
     *
     * @return $this
     */
    public function whereCategoriesIn($terms, $includeChildren = true)
    {
        $this->args['tax_query'][] = [
            'taxonomy' => 'statistic_categories',
            'field' => 'slug',
            'terms' => explode(",", $terms),
            'operator' => 'IN',
            'include_children' => $includeChildren
        ];

        return $this;
    }

}