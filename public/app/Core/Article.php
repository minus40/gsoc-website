<?php

namespace Hailstone\Core;

use Hailstone\Core\Post;

class Article extends Post
{
    public $postType = 'article';
    public $usefulData =
    [
        'post_type' => 'article',
        'class_name' => 'Article',
        'posts_page' => 'Newsroom',
        'main_taxonomy' => 'article_categories',
        'orderby' => 'date',
        'order' => 'DESC',
        'max_days' => 2365
    ];
    var $_categories;
    var $_primary_category;
    var $_related_posts;

    /**
     * @return array
     */
    public function categories()
    {
        if(!$this->_categories)
        {
            $categories = $this->terms('article_categories');
            if (is_array($categories) && count($categories)) {
                $this->_categories = $categories;
            }
        }
        return $this->_categories;
    }


    /**
     * @param $key
     * @param $terms
     * @param bool $includeChildren
     *
     * @return $this
     */
    public function whereCategoriesIn($terms, $includeChildren = true)
    {
        $this->args['tax_query'][] = [
            'taxonomy' => 'article_categories',
            'field' => 'slug',
            'terms' => explode(",", $terms),
            'operator' => 'IN',
            'include_children' => $includeChildren
        ];

        return $this;
    }




}