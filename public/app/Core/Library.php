<?php

namespace Hailstone\Core;

use Hailstone\Core\Attachment;
use Timber\Term;
use Timber\Timber;

class Library extends Term
{
    var $_documents;

    /**
     * @return array|bool|null
     */
    public function documents()
    {
        if(!$this->_documents)
        {
            $this->_documents = Timber::get_posts([
                'post_type' => 'attachment',
                'post_status' => 'inherit',
                'tax_query' => [
                    [
                        'taxonomy' => 'media_category',
                        'field' => 'id',
                        'include_children' => true,
                        'terms' => $this->id
                    ]
                ]
            ], Attachment::class);
        }

        return $this->_documents;
    }
}