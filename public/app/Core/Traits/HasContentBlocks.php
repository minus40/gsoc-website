<?php

namespace Hailstone\Core\Traits;

use Hailstone\Core\Attachment;
use Hailstone\Core\FAQ;
use Hailstone\Core\Library;
use Hailstone\Core\Location;
use Hailstone\Core\People;
use Hailstone\Core\Video;
use Hailstone\Core\Image;
use Timber\Timber;

trait HasContentBlocks
{
    var $_blocks;
    var $_supporting_blocks;
    var $_document_libraries;
    var $_external_script_loading;

    /**
     * @return mixed
     */
    public function blocks()
    {

        if(!$this->_blocks)
        {
            $blocks = $this->get_field('content_layouts');

            if($blocks)
            {
                foreach($blocks as $key => $block)
                {
                    if(isset($block['images'])) $blocks[$key]['images'] = $this->convertImages($block);
                    if(isset($block['image'])) $blocks[$key]['image'] = $this->convertImage($block);
                    if(isset($block['videos'])) $blocks[$key]['videos'] = $this->convertVideos($block);
                    if(isset($block['video'])) $blocks[$key]['video'] = $this->convertVideo($block);
                    if(isset($block['galleries'])) $blocks[$key]['galleries'] = $this->convertGalleries($block);
                    if(isset($block['gallery'])) $blocks[$key]['gallery'] = $this->convertGallery($block);
                    if(isset($block['files'])) $blocks[$key]['files'] = $this->convertFiles($block);
                    if(isset($block['libraries'])) $blocks[$key]['libraries'] = $this->convertLibraries($block);
                    if(isset($block['faqs'])) $blocks[$key]['faqs'] = $this->convertFAQs($block);
                    if(isset($block['locations'])) $blocks[$key]['locations'] = $this->convertLocations($block);
                    if(isset($block['post_choice'])) $blocks[$key]['posts'] = $this->convertPosts($block);
                }

                $this->_blocks = $blocks;
            }
        }

        return $this->_blocks;
    }

    /**
     * @return mixed
     */
    public function supporting_blocks()
    {

        if(!$this->_supporting_blocks)
        {
            $blocks = $this->get_field('supporting_content_layouts');

            if($blocks)
            {
                foreach($blocks as $key => $block)
                {
                    if(isset($block['images'])) $blocks[$key]['images'] = $this->convertImages($block);
                    if(isset($block['image'])) $blocks[$key]['image'] = $this->convertImage($block);
                    if(isset($block['videos'])) $blocks[$key]['videos'] = $this->convertVideos($block);
                    if(isset($block['video'])) $blocks[$key]['video'] = $this->convertVideo($block);
                    if(isset($block['galleries'])) $blocks[$key]['galleries'] = $this->convertGalleries($block);
                    if(isset($block['files'])) $blocks[$key]['files'] = $this->convertFiles($block);
                    if(isset($block['libraries'])) $blocks[$key]['libraries'] = $this->convertLibraries($block);
                    if(isset($block['testimonials'])) $blocks[$key]['testimonials'] = $this->convertTestimonials($block);
                    if(isset($block['faqs'])) $blocks[$key]['faqs'] = $this->convertFAQs($block);
                }

                $this->_supporting_blocks = $blocks;
            }
        }

        return $this->_supporting_blocks;
    }


    public function hasSupportingBlocks()
    {
        return !is_null($this->supporting_blocks());
    }

    /**
     * @return mixed
     */
    public function document_libraries()
    {
        //if(!$this->_document_libraries)
        //{
            $libraries = $this->get_field('document_libraries');
            if(!is_null($libraries))
            {
                $libraries = $this->convertDocumentLibraries($libraries);
            }
            $this->_document_libraries = $libraries;
        //}

        return $this->_document_libraries;
    }

    /**
     * @param $block
     *
     * @return mixed
     */
    private function convertImages($block)
    {
        foreach($block['images'] as $key => $image)
        {
            $block['images'][$key] = new Image($image);
        }

        return $block['images'];
    }

    /**
     * @param $block
     *
     * @return mixed
     */
    private function convertImage($block)
    {
        $block['image'] = new Image($block['image']);

        return $block['image'];
    }

    /**
     * @param $block
     *
     * @return mixed
     */
    private function convertVideos($block)
    {
        foreach($block['videos'] as $key => $video)
        {
            $block['videos'][$key] = new Video($video);
        }

        return $block['videos'];
    }

    /**
     * @param $block
     *
     * @return mixed
     */
    private function convertVideo($block)
    {
        return new Video($block['video']);
    }

    /**
     * @param $block
     *
     * @return mixed
     */
    private function convertGalleries($block)
    {

        foreach($block['galleries'] as $key => $mediaCategory)
        {
            $block['galleries'][$key]['images'] = Timber::get_posts([
                'post_type' => 'attachment',
                'post_status' => 'inherit',
                'post_mime_type' => 'image',
                'tax_query' => [
                    [
                        'taxonomy' => 'media_category',
                        'field' => 'id',
                        'include_children' => true,
                        'terms' => $mediaCategory['gallery']->term_id
                    ]
                ]
            ], Image::class);
        }

        return $block;
    }


    /**
     * @param $block
     *
     * @return mixed
     */
    private function convertGallery($block)
    {
        foreach($block['gallery'] as $key => $image)
        {
            $block['gallery'][$key] = new Image($image['ID']);
        }

        return $block['gallery'];
    }


    /**
     * @param $block
     *
     * @return mixed
     */
    private function convertFiles($block)
    {
        foreach($block['files'] as $key => $file)
        {
            if (is_array($file) && array_key_exists('file', $file) && is_array($file['file']))
            {
                $attachment = new Attachment($file['file']['ID']);
                $attachment->setAttachment($file['file']);
                $block['files'][$key] = $attachment;
            }
            else
            {
                unset($block['files'][$key]);
            }
        }

        /*if (count($block['files']) == 0 )
        {
            return null;
        }*/

        return $block['files'];
    }

    /**
     * @param $block
     *
     * @return mixed
     */
    private function convertLibraries($block)
    {
        foreach($block['libraries'] as $key => $library)
        {
            $lib = new Library($library['library']->term_id);
            $block['libraries'][$key] = $lib;
        }

        return $block['libraries'];
    }

    /**
     * @param $block
     *
     * @return mixed
     */
    private function convertFAQs($block)
    {
        foreach($block['faqs'] as $key => $faq)
        {
            $block['faqs'][$key] = new FAQ($faq->ID);
        }

        return $block['faqs'];
    }

    /**
     * @param $block
     *
     * @return mixed
     */
    private function convertPosts($block)
    {

        if (array_key_exists('post_choice', $block) && $block['post_choice'] == "use_post_select"  && $block['posts'] && is_array($block['posts']) && count($block['posts']) > 0 ):
            foreach($block['posts'] as $key => $post)
            {
                if($post->post_type == 'article'){
                    $block['posts'][$key] = new Article($post->ID);
                }
                if($post->post_type == 'career'){
                    $block['posts'][$key] = new Career($post->ID);
                }
                if($post->post_type == 'faq'){
                    $block['posts'][$key] = new FAQ($post->ID);
                }
                if($post->post_type == 'location'){
                    $block['posts'][$key] = new Location($post->ID);
                }
                if($post->post_type == 'person'){
                    $block['posts'][$key] = new People($post->ID);
                }
                if($post->post_type == 'study'){
                    $block['posts'][$key] = new Study($post->ID);
                }
                if($post->post_type == 'tender'){
                    $block['posts'][$key] = new Tender($post->ID);
                }
                if($post->post_type == 'video'){
                    $block['posts'][$key] = new Video($post->ID);
                }
                if($post->post_type == 'company'){
                    $block['posts'][$key] = new Company($post->ID);
                }

            }
        else:
            $block['posts'] = array();
        endif;


        if (array_key_exists('post_choice', $block) && $block['post_choice'] == "use_post_category" ):

            if ($block['post_choice_category'] && !empty($block['post_choice_category']) && is_array($block['post_choice_category']) ):

                $tag_ids = array();

                foreach($block['post_choice_category'] as $p):
                    $tag_ids[] = $p->ID;
                    $cat_tax = $p->taxonomy;
                endforeach;
                $block['posts'] = \Timber::get_posts( array('post_type' => 'article', 'post_status' => 'publish', 'orderby' => 'date', 'order' => 'DESC', 'tax_query' => array( array('taxonomy' => $cat_tax, 'include_children' => true, 'field' => 'term_id','terms' => $tag_ids)) ));

            endif;

        endif;

        if (array_key_exists('post_choice', $block) && $block['post_choice'] == "use_company_category" ):

            if ($block['company_choice_category'] && !empty($block['company_choice_category']) && is_array($block['company_choice_category']) ):

                $tag_ids = array();

                foreach($block['company_choice_category'] as $p):
                    $tag_ids[] = $p->ID;
                    $cat_tax = $p->taxonomy;
                endforeach;
                $block['posts'] = \Timber::get_posts( array('post_type' => 'company', 'post_status' => 'publish', 'orderby' => 'title', 'order' => 'ASC', 'tax_query' => array( array('taxonomy' => $cat_tax, 'include_children' => true, 'field' => 'term_id','terms' => $tag_ids)) ));

            endif;

        endif;

        if (array_key_exists('post_choice', $block) && $block['post_choice'] == "use_faq_category" ):

            if ($block['faq_choice_category'] && is_array($block['faq_choice_category']) ):

                $tag_ids = array();

                foreach($block['faq_choice_category'] as $p):
                    $tag_ids[] = $p->ID;
                    $cat_tax = $p->taxonomy;
                endforeach;

                $block['posts'] = \Timber::get_posts( array('post_type' => 'faq', 'post_status' => 'publish', 'orderby' => 'menu_order', 'order' => 'ASC', 'tax_query' => array( array('taxonomy' => 'faq_category', 'include_children' => true, 'field' => 'term_id','terms' => $tag_ids)) ));

            endif;

        endif;

        return $block['posts'];
    }

    /**
     * @param $block
     *
     * @return mixed
     */
    private function convertLocations($block)
    {
        foreach($block['locations'] as $key => $location)
        {
            $block['locations'][$key] = new Location($location->ID);
        }

        return $block['locations'];
    }

    /**
     * @param $libraries
     *
     * @return mixed
     */
    private function convertDocumentLibraries($libraries)
    {
        $convertedLibraries = [];
        foreach($libraries as $key => $library)
        {
            $lib = new Library($library['library']);
            $convertedLibraries[$key] = $lib;
        }

        return $convertedLibraries;
    }


    public function external_script_loading()
    {

        if(!$this->_external_script_loading)
        {
            $blocks = $this->blocks();
            $blocks2 = $this->supporting_blocks();
            $scripts = 
            [
                'map' => null,
                'chart' => null,
                'table' => null,
                'gallery' => null,
                'form' => null,
                'protected' => null,
            ];


            if($blocks)
            {
                foreach($blocks as $key => $block)
                {
                    if( ((isset($block['files']) && $block['files']) || isset($block['libraries'])) && isset($block['layout']) && $block['layout'] == 'table' ) 
                    {
                        $scripts['table'] = true;
                    }
                    if(isset($block['locations']))
                    {
                        $scripts['map'] = true;
                    }
                    if(isset($block['form']))
                    {
                        $scripts['form'] = true;
                    }
                    if(isset($block['style']) && ($block['style'] == 'image-gallery' || $block['style'] == 'thumbnail-gallery' || $block['style'] == 'image_gallery' || $block['style'] == 'thumbnail_gallery'))
                    {
                        $scripts['gallery'] = true;
                    }
                    if(isset($block['image']))
                    {
                        $scripts['gallery'] = true;
                    }
                    if(isset($block['chart']) || isset($block['layout']) && $block['layout'] == 'chart')
                    {
                        $scripts['chart'] = true;
                        $scripts['table'] = true;
                    }

                    if(isset($block['access']) && $block['access'])
                    {
                        $scripts['protected'] = $this->getAccessFormID();
                    }
                    if(isset($block['register_to_view']) && $block['register_to_view'])
                    {
                        $scripts['protected'] = $this->getAccessFormID();
                    }

                }
            }

            if($blocks2)
            {
                foreach($blocks2 as $key => $block)
                {
                    if( isset($block['files']) || isset($block['libraries']) )
                    {
                        $scripts['table'] = true;
                    }
                    if( isset($block['galleries']) || isset($block['image']) || isset($block['images']) )
                    {
                        $scripts['gallery'] = true;
                    }


                }
            }

            $this->_external_script_loading = $scripts;

            // if nothing set, set the array to null instead
            $has = false;
            foreach ($scripts as $v) 
            {
                if ($v)
                {
                    $has = true;
                }
            }
            if (!$has)
            {
                $this->_external_script_loading = null;
            }

        }

        return $this->_external_script_loading;
    }
    
}