<?php

namespace Hailstone\Core\Traits;

use Hailstone\Core\Carousel;
use Hailstone\Core\Image;
use Hailstone\Core\Video;

trait HasCarousel
{
    var $_carousel;

    public function carousel()
    {
        if(!$this->_carousel)
        {
            $carousel = $this->get_field('feature-carousel');
            if (is_array($carousel) && count($carousel)) {
                $this->_carousel = new Carousel($carousel[0]);
                //dd($this->_carousel);
            }
        }
        return $this->_carousel;
    }
}