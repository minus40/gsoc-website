<?php

namespace Hailstone\Core;

class Attachment extends Post
{
    public $postType = 'attachment';
    var $_url;
    var $_extension;
    var $_mime;
    var $_icon;
    var $_size;
    var $_author;
    var $_title;

    /**
     * @return string
     */
    public function url()
    {
        if(!$this->_url) {
            $this->_url = wp_get_attachment_url($this->ID);
        }

        return $this->_url;
    }

    /**
     * @return mixed
     */
    public function extension()
    {
        if(!$this->_extension)
        {
            $file = wp_check_filetype($this->url());
            $this->_extension = $file['ext'];
        }

        return $this->_extension;
    }

    /**
     * @return mixed
     */
    public function mime()
    {
        if(!$this->_mime)
        {
            $file = wp_check_filetype($this->url());
            $this->_mime = $file['type'];
        }

        return $this->_mime;
    }

    /**
     * @return string
     */
    public function filename()
    {
        return basename(get_attached_file( $this->ID ));
    }

    public function relativePath()
    {
        return get_attached_file($this->id);
    }

    /**
     *
     */
    public function author()
    {
        if(!$this->_author) {
            $this->_author = get_post($this->ID)->post_author;
        }

        return $this->_author;
    }

    public function title()
    {
        if(!$this->_title) {
            $this->_title = get_post($this->ID)->post_title;
        }

        return $this->_title;
    }

    /**
     *
     */
    public function caption()
    {
        if(!$this->_caption) {
            $this->_caption = get_post($this->ID)->post_excerpt;
        }

        return $this->_caption;
    }

    /**
     *
     */
    public function description()
    {
        if(!$this->_description) {
            $this->_description = get_post($this->ID)->post_content;
        }

        return $this->_description;
    }

    /**
     * @return string
     */
    public function size()
    {
        if(!$this->_size)
        {
            if(file_exists(get_attached_file( $this->ID ))) {
                $this->_size = filesize(get_attached_file( $this->ID ));
            }
        }
        return $this->_size;
    }

    /**
     * @return string
     */
    public function friendly_size()
    {
        if(!$this->_friendlysize)
        {
            $this->_friendlysize = size_format($this->_size, 2);
        }
        return $this->_friendlysize;
    }

    /**
     * @return string
     */
    public function updated_timestamp()
    {
        if(!$this->_updatedtimestamp)
        {
            if(file_exists(get_attached_file( $this->ID ))) {
                $this->_updatedtimestamp = get_post_modified_time( 'U', false, $this->ID);
            }
        }
        return $this->_updatedtimestamp;
    }

    /**
     * @return string
     */
    public function updated_date()
    {
        if(!$this->_updateddate)
        {
            if(file_exists(get_attached_file( $this->ID ))) {
                $this->_updateddate = get_post_modified_time( 'd M Y', false, $this->ID);
            }
        }
        return $this->_updateddate;
    }

    /**
     * @return string
     */
    public function created_timestamp()
    {
        if(!$this->_createdtimestamp)
        {
            if(file_exists(get_attached_file( $this->ID ))) {
                $this->_createdtimestamp = get_post_time( 'U', false, $this->ID);
            }
        }
        return $this->_createdtimestamp;
    }

    /**
     * @return string
     */
    public function created_date()
    {
        if(!$this->_createddate)
        {
            if(file_exists(get_attached_file( $this->ID ))) {
                $this->_createddate = get_post_time( 'd M Y', false, $this->ID);
            }
        }
        return $this->_createddate;
    }

    /**
     *
     */
    public function icon()
    {
        if(!$this->_icon)
        {
            switch($this->extension()){
                case "xls":
                    $this->_icon = "excel";
                    break;
                case "xlsx":
                    $this->_icon = "excel";
                    break;
                case "ppt":
                    $this->_icon = "powerpoint";
                    break;
                case "pptx":
                    $this->_icon = "powerpoint";
                    break;
                case "doc":
                    $this->_icon = "word";
                    break;
                case "docx":
                    $this->_icon = "word";
                    break;
                case "rtf":
                    $this->_icon = "text";
                    break;
                case "png":
                    $this->_icon = "image";
                    break;
                case "gif":
                    $this->_icon = "image";
                    break;
                case "jpg":
                    $this->_icon = "image";
                    break;
                case "jpeg":
                    $this->_icon = "image";
                    break;
                case "html":
                    return "code";
                default:
                    $this->_icon = $this->extension();
            }
        }

        return $this->_icon;
    }
}