<?php

namespace Hailstone\Core;

class Video extends Post
{
    public $postType = 'video';

    public $usefulData =
    [
        'post_type' => 'video',
        'class_name' => 'Video',
        'posts_page' => 'Videos',
        'main_taxonomy' => 'category',
        'orderby' => 'title',
        'order' => 'ASC',
    ];

    var $_url;
    var $_thumbnail;
    var $_embed;

    /**
     * @return null|string
     */
    public function url()
    {
        if(!$this->_url)
        {
            switch($this->source)
            {
                case "youtube":
                    $this->_url = "https://www.youtube-nocookie.com/watch?v=" . $this->video_id;
                    break;
                case "vimeo":
                    $this->_url = "https://vimeo.com/" . $this->video_id;
                    break;
            }
        }

        return $this->_url;
    }

    public function embed()
    {
        if(!$this->_embed)
        {
            switch($this->source)
            {

                case "youtube":
                    $this->_embed = '<iframe id="ytplayer_'.$this->video_id.'" data-engaged="video" data-video-title="'.$this->title.'" data-video-id="'.$this->ID.'" type="text/html" width="720" height="405" src="https://www.youtube-nocookie.com/embed/'. $this->video_id . '?autoplay=0&rel=0&modestbranding=1&autohide=1&showinfo=0&controls=0" frameborder="0" allowfullscreen="allowfullscreen"
        mozallowfullscreen="mozallowfullscreen" 
        msallowfullscreen="msallowfullscreen" 
        oallowfullscreen="oallowfullscreen" 
        webkitallowfullscreen="webkitallowfullscreen"></iframe><div class="embed-fallback-message active"><p>We have provided a video as part of this page’s content. But as it is hosted on YouTube, a third party service which release its own cookies, this video is unavailable unless you agree to accept all cookies for our website.</p></div>';
                    break;
                case "vimeo":
                    $this->_embed =  '<iframe id="vplayer_'.$this->video_id.'" src="https://player.vimeo.com/video/' . $this->video_id . '?color=c9ff23&title=0&byline=0&portrait=0" width="720" height="405" frameborder="0" data-engaged="video" data-video-id="'.$this->ID.'" data-video-title="'.$this->title.'" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><div class="embed-fallback-message active"><p>We have provided a video as part of this page’s content. But as it is hosted on Vimeo, a third party service which releases its own cookies, this video is unavailable unless you agree to accept all cookies for our website.</p></div>';
                    break;

            }
        }

        return $this->_embed;
    }

    public function thumbnail()
    {
        if(!$this->_thumbnail)
        {
            switch($this->source)
            {
                case "youtube":
                    $this->_thumbnail = 'http://img.youtube-nocookie.com/vi/' . $this->video_id . '/maxresdefault.jpg';
                    break;
                case "vimeo":
                    $vimeoData = json_decode(file_get_contents("http://vimeo.com/api/v2/video/" . $this->video_id . ".json"));
                    $this->_thumbnail = $vimeoData[0]->thumbnail_large;
                    break;
            }
        }

        return $this->_thumbnail;
    }


    
}