<?php

namespace Hailstone\Core;

use Hailstone\Core\Traits\HasCarousel;
use Hailstone\Core\Traits\HasContentBlocks;
use Timber\Timber;

abstract class Post extends \Timber\Post
{
    use HasContentBlocks, HasCarousel;

    protected $args;

    public $relatedPostsCount = 5;

    var $_should_hide_navigation = false;
    var $_should_hide_featured_image = false;

    var $_primary_category;
    var $_current_category;
    var $_related_posts;

    var $_crumbs;
    var $_fallback_parent_page;
    var $_emergency_contents;


    public function __construct($pid = null)
    {
        if(config('posts.' . $this->postType) && !config('posts.' . $this->postType . '.enabled'))
        {
            throw new \Exception("Referenced post - " . static::class . " - is not enabled in the site configuration");
        }

        parent::__construct($pid);
    }


    public function emergency_contents()
    {
        if (!$this->_emergency_contents)
        {

            $this->_emergency_contents = null;

            $has_emergency = get_field('emergency_sitewide', 'option');
           
            if ($has_emergency):

                $this->_emergency_contents = get_field('emergency_sitewide_content', 'option');

            endif;

        }

        return $this->_emergency_contents;
    }

    /**
     * @return array|bool|null
     */
    public function all()
    {
        $posts = Timber::get_posts([ 'post_type' => $this->postType ], static::class);

        return $posts;
    }

    /**
     * @return array|bool|null
     */
    public function current()
    {
        return Timber::get_post(false, static::class);
    }

    /**
     * @param $id
     *
     * @return array|bool|null
     */
    public function find($id)
    {
        $post = Timber::get_post($id, static::class);

        if($post->post_type !== $this->postType) return null;

        return $post;
    }

    /**
     * @param $limit
     *
     * @return $this
     */
    public function limit($limit)
    {
        $this->args['posts_per_page'] = $limit;

        return $this;
    }

    /**
     * @param $page
     *
     * @return $this
     */
    public function paged($page)
    {
        $this->args['paged'] = $page;

        return $this;
    }

    public function whereHasNoParent()
    {
        $this->args['post_parent'] = 0;

        return $this;
    }

    /**
     * @param $key
     * @param array $terms
     * @param bool $includeChildren
     *
     * @return $this
     */
    public function whereTaxonomyTermsIn($key, $terms = [], $includeChildren = true)
    {
        if($terms)
        {
            $this->args['tax_query'][] = [
                'taxonomy' => $key,
                'field' => 'slug',
                'terms' => is_array($terms) ? $terms : explode(",", $terms),
                'operator' => 'IN',
                'include_children' => $includeChildren
            ];
        }

        return $this;
    }

    /**
     * @param array $postIds
     *
     * @return $this
     */
    public function wherePostNotIn($postIds = [])
    {
        $this->args['post__not_in'] = $postIds;

        return $this;
    }

    /**
     * @param $status
     *
     * @return $this
     */
    public function whereStatus($status)
    {
        $this->args['post_status'] = $status;

        return $this;
    }

    /**
     * @param $field
     * @param string $order
     *
     * @return $this
     */
    public function orderBy($field, $order = 'ASC')
    {
        $this->args['orderby'] = $field;
        $this->args['order'] = $order;

        return $this;
    }

    /**
     * @param $startDateValue
     * @param $endDateValue
     * @param string $startDateField
     * @param string $endDateField
     * @param string $relation
     *
     * @return $this
     */
    public function whereDatesBetween($startDateValue, $endDateValue, $startDateField = 'start_date', $endDateField = 'end_date', $relation = "AND")
    {
        $this->args['meta_query'][] = [
            'relation' => $relation,
            [
                'key' => $startDateField,
                'value' => $startDateValue,
                'compare' => '>=',
                'type'  => 'DATE'
            ],
            [
                'key' => $endDateField,
                'value' => $endDateValue,
                'compare' => '<=',
                'type'  => 'DATE'
            ]
        ];

        return $this;
    }

    /**
     * @param $field
     * @param $operator
     * @param $value
     *
     * @return $this
     */
    public function whereDate($field, $operator, $value)
    {
        $this->args['meta_query'][] = [
            'key' => $field,
            'value' => $value,
            'compare' => $operator,
            'type'  => 'DATE'
        ];

        return $this;
    }

    /**
     * @return WP_Query
     */
    public function get()
    {
        $this->args['post_type'] = $this->postType;

        $query = Timber::get_posts( $this->args, static::class );

        return $query;
    }

    public function paginate($postsPerPage)
    {
        global $paged, $context;

        $this->args = array_merge($this->args,[
            'post_type' => $this->postType,
            'posts_per_page' => $postsPerPage,
            'paged' => !isset($paged) || !$paged ? 1 : $paged
        ]);

        query_posts($this->args);

        $context['pagination'] = Timber::get_pagination();

        return Timber::get_posts($this->args, static::class);
    }

    public function first()
    {
        $this->args['post_type'] = $this->post_type;
        $posts = Timber::get_posts( $this->args, static::class );

        return $posts[count($posts) - 1];
    }

    public function last()
    {
        $this->args['post_type'] = $this->post_type;
        $posts = Timber::get_posts( $this->args, static::class );

        return $posts[0];
    }

    /**
     * @param bool $in_same_term
     *
     * @return Article
     */
    public function next($in_same_term = false)
    {
        if(!parent::next()) return $this->first();

        return new static(parent::next($in_same_term));
    }

    public function prev($in_same_term = false)
    {
        if(!parent::prev()) return $this->last();

        return new static(parent::prev($in_same_term));
    }

    public function should_hide_navigation()
    {
        if(!$this->_should_hide_navigation)
        {
            $this->_should_hide_navigation = $this->get_field('hide_navigation');
        }

        return $this->_should_hide_navigation;
    }

    public function should_hide_featured_image()
    {
        if(!$this->_should_hide_featured_image)
        {
            $this->_should_hide_featured_image = $this->get_field('hide_image');
        }

        return $this->_should_hide_featured_image;
    }

    public function should_hide_social_sharing()
    {
        if(!$this->_should_hide_social_sharing)
        {
            $this->_should_hide_social_sharing = $this->get_field('hide_social');
        }

        return $this->_should_hide_social_sharing;
    }

    public function children( $post_type = null, $childPostClass = false ) 
    {

        $post_type = $this->post_type;

        return parent::children($post_type, $childPostClass);
    }

    public function embedExtraScripts($declared_in_page = null)
    {
        $scripts = $this->external_script_loading(); // from hasContentBlocks
        if (! is_admin() && ( ($scripts && $scripts['table']) || $declared_in_page == 'table' || $declared_in_page == 'chart') ) 
        {
            \add_action( 'wp_footer', function()
            {
                wp_deregister_style('datatables');
                wp_deregister_style('datatables-css');
                wp_deregister_style('datatables-css2');
                wp_register_style('datatables-css2', '//cdn.datatables.net/2.1.7/css/dataTables.dataTables.min.css');
                wp_enqueue_style('datatables-css2');

                wp_deregister_script('datatables');
                wp_deregister_script('datatables-js');
                wp_deregister_script('datatables-js2');
                wp_register_script( 'datatables-js2', '//cdn.datatables.net/2.1.7/js/dataTables.min.js', array( 'jquery' ), null, true);
                wp_enqueue_script( 'datatables-js2' );

            });
        }
        if (! is_admin() && env('GOOGLE_API_KEY') && ( ($scripts && $scripts['map']) || $declared_in_page == 'map') )
        {
            \add_action( 'wp_footer', function()
            {
                wp_deregister_script('google-maps-js');
                wp_register_script( 'google-maps-js', '//maps.googleapis.com/maps/api/js?key=' . env('GOOGLE_API_KEY') , array( 'jquery' ), null, true);
                wp_enqueue_script( 'google-maps-js' );


                $file = get_template_directory() . '/js/map-helpers.js';
                $pub = get_template_directory_uri() . '/js/map-helpers.js';
                wp_register_script( 'map-helpers', $pub, array( 'jquery'), filemtime( $file ),  true );
                wp_enqueue_script( 'map-helpers' );

            });
        }
        if (! is_admin() && $scripts && $scripts['chart'] || $declared_in_page == 'chart')
        {
            \add_action( 'wp_footer', function()
            {
                wp_deregister_script('canvas-to-blob');
                wp_register_script( 'canvas-to-blob-js', '//cdnjs.cloudflare.com/ajax/libs/javascript-canvas-to-blob/3.28.0/js/canvas-to-blob.js', array( 'jquery' ), null, true);
                wp_enqueue_script( 'canvas-to-blob-js' );

                wp_deregister_script('save-as');
                wp_register_script( 'save-as-js', '//cdnjs.cloudflare.com/ajax/libs/FileSaver.js/2.0.0/FileSaver.min.js', array( 'jquery' ), null, true);
                wp_enqueue_script( 'save-as-js' );

                wp_deregister_script('chart-js');
                wp_register_script( 'chart-js', '//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js', array( 'jquery' ), null, true);
                wp_enqueue_script( 'chart-js' );
           
                wp_deregister_style('chart-css');
                wp_register_style('chart-css', '//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.css');
                wp_enqueue_style('chart-css');

                // Custom JavaScript
                $file = get_template_directory() . '/library/d3/d3js.js';
                $pub = get_template_directory_uri() . '/library/d3/d3js.js';
                wp_register_script( 'd3-js', $pub, array( 'jquery' ), filemtime( $file ), true );
                wp_enqueue_script( 'd3-js' );


                wp_deregister_script('labratrevenge-js');
                wp_register_script( 'labratrevenge-js', '//labratrevenge.com/d3-tip/javascripts/d3.tip.v0.6.3.js', array( 'jquery' ), null, true);
                wp_enqueue_script( 'labratrevenge-js' );

            });
        }

        if (! is_admin() && $scripts && $scripts['gallery'])
        {
            \add_action( 'wp_footer', function()
            {
                wp_deregister_style('jquery-fancybox-css');
                wp_register_style('jquery-fancybox-css', get_template_directory_uri().'/library/fancybox/css/jquery.fancybox.min.css');
                wp_enqueue_style('jquery-fancybox-css');

                wp_deregister_script('jquery-fancybox-js');
                wp_register_script( 'jquery-fancybox-js',  get_template_directory_uri().'/library/fancybox/js/jquery.fancybox.min.js' , array( 'jquery' ), null, true);
                wp_enqueue_script( 'jquery-fancybox-js' );


            });
        }
 

        /*if (! is_admin() && $this->carousel_has_video())
        {
            if ($this->carousel_has_video() == "youtube")
            {

                \add_action( 'wp_footer', function()
                {
                    wp_register_style('ytbg-css', get_template_directory_uri().'/css/jquery.mb.YTPlayer.min.css');
                    wp_enqueue_style('ytbg-css');

                    wp_deregister_script('ytbg-js');
                    wp_register_script( 'ytbg-js',  get_template_directory_uri().'/js/jquery.mb.YTPlayer.min.js' , array( 'jquery' ), null, true);
                    wp_enqueue_script( 'ytbg-js' );

                });
            }
            if ($this->carousel_has_video() == "vimeo")
            {

                \add_action( 'wp_footer', function()
                {
                    wp_register_style('vmbg-css', get_template_directory_uri().'/css/jquery.mb.vimeo_player.min.css');
                    wp_enqueue_style('vmbg-css');

                    wp_deregister_script('vmbg-js');
                    wp_register_script( 'vmbg-js',  get_template_directory_uri().'/js/jquery.mb.vimeo_player.min.js' , array( 'jquery' ), null, true);
                    wp_enqueue_script( 'vmbg-js' );

                });
            }
        }*/


        return $scripts;
    }


    public function current_category()
    {
        if (!$this->_current_category)
        {
            if ($this->primary_category())
            {
                $this->_current_category = $this->primary_category();
            }
        }
        return $this->_current_category;
    }

    public function use_this_taxonomy()
    {
        if (!$this->_use_this_taxonomy):
            $this->_use_this_taxonomy = (isset($this->usefulData)) ? $this->usefulData['main_taxonomy'] : null;
        endif;
        return $this->_use_this_taxonomy;
    }

    public function primary_category( $taxonomy = null )
    {
        if (!$this->_primary_category)
        {
            $this->_primary_category = null;

            if ($this->post_type == "page" || $this->postType == "page"):
                $current = $this->query_all_acf("assign_key_category");

                if ($current): // if manually assigned
                    $current = get_term($current);
                    if ($current && !is_wp_error($current)):
                        $this->_primary_category = $current;
                    endif;
                endif;

            elseif (is_archive() && get_queried_object()): // if an archive
                $current = get_queried_object();
                if ($current && !is_wp_error($current) && get_class($current) ==  "WP_Term"):
                    $this->_primary_category = $current;
                endif;

            else:
                $taxonomy = $taxonomy ? $taxonomy : $this->use_this_taxonomy();
                $terms = get_the_terms( $this->ID, $taxonomy);

                if ($terms && !is_wp_error($terms)): // if post
                    if ( class_exists('WPSEO_Primary_Term') ):
                        $wpseo_primary_term_class = new \WPSEO_Primary_Term( $taxonomy, $this->ID  );
                        $wpseo_primary_term = $wpseo_primary_term_class->get_primary_term();
                        $term = get_term( $wpseo_primary_term );
                        if (is_wp_error($term))
                        {
                            $this->_primary_category = $terms[0];
                        }
                        else {
                            $this->_primary_category = $term;
                        }
                    else:
                        $this->_primary_category = $terms[0];
                    endif;
                endif;
            endif;
        }
        return $this->_primary_category;
    }

    public function all_acf()
    {
        if (!$this->_all_acf)
        {
            if (!is_archive())
            {
                $this->_all_acf = get_fields($this->ID);
            }
        }
        return $this->_all_acf;
    }

    public function query_all_acf($fieldval, $field = null)
    {
        if ( $this->all_acf() && is_array( $this->all_acf() ) ):
            $all_acf = $this->all_acf();

            if ($field):
                if (array_key_exists($fieldval, $all_acf ) && array_key_exists($field, $all_acf[$fieldval] ) ):
                    return $all_acf[$fieldval][$field];
                endif;

            else:
                if (array_key_exists($fieldval, $all_acf ) ):
                    $m = $all_acf[$fieldval];
                    return $all_acf[$fieldval];
                endif;
            endif;
        endif;
        return null;
    }

    public function incrementPageURL($link, $page_num)
    {

        $final = $link . "page/".$page_num."/";

        if (isset($_GET['selected_year']) && is_numeric($_GET['selected_year']) && $_GET['selected_year'] > 2000 && $_GET['selected_year'] < 2300)
        {
            $final .= "?selected_year=".$_GET['selected_year'];
        }

        return $final;
    }

    public function related_posts()
    {

        if (!$this->_related_posts)
        {
            if ($this->usefulData):

                $category = $this->primary_category();
                $related_posts = [];
                $not_in = [$this->ID];
                $limit = $this->relatedPostsCount;
       
                if ($category):

                    $args = [

                        'post_type' => $this->postType,
                        'post_status' => ['publish'],
                        'tax_query' => 
                        [
                            'relation' => 'OR',
                            [
                                'taxonomy' => $category->taxonomy,
                                'field' => 'slug',
                                'include_children' => false,
                                'terms' => array($category->slug),
                            ]
                        ],
                        'orderby'          => $this->usefulData['orderby'],
                        'order'        => $this->usefulData['order'],
                        'posts_per_page' => $limit,
                        'post__not_in' => $not_in,
                    ];
                    if (array_key_exists('meta_key', $this->usefulData))
                    {
                        $args['meta_key'] = $this->usefulData['meta_key'];
                    }

                    $tmp = \Timber::get_posts($args, static::class);

                    if ($tmp && is_array($tmp) && count($tmp) > 0)
                    {
                        foreach ($tmp as $t):
                            $related_posts[] = $t;
                            $not_in[] = $t->ID;
                            $limit--;
                        endforeach;
                    }
                endif;


                if ( $limit > 0 ):

                    $args = array
                    (
                        'post_type'      => $this->postType,
                        'post_status'    => ['publish'],
                        'posts_per_page' => $limit,
                        'order'          => $this->usefulData['order'],
                        'orderby'        => $this->usefulData['orderby'],
                        'post__not_in' => $not_in
                    );

      
                    if (array_key_exists('meta_key', $this->usefulData))
                    {
                        $args['meta_key'] = $this->usefulData['meta_key'];
                    }

                    $tmp = \Timber::get_posts($args,static::class);  

                    if ($tmp && is_array($tmp) && count($tmp) > 0)
                    {
                        foreach ($tmp as $t):
                            $related_posts[] = $t;
                        endforeach;
                    }

                endif;

                if (count($related_posts) > 0)
                {
                    $this->_related_posts = $related_posts;   
                }
            endif;

        }
        return $this->_related_posts;      
    }

    public function doAncestors($current_page_id)
    {
        if (get_post_type($current_page_id) == "page")
        {
            $ancestors = get_post_ancestors($current_page_id);
            if (is_array($ancestors))
            {
                return ($ancestors);
            }
        }
        return false;
    }

    public function isTermAssignedToPage($term_id)
    {
        if(is_numeric($term_id)):
            $args = array
            (
                'numberposts'   => 1,
                'post_type'     => 'page',
                'meta_query'    => array
                (
                    'relation' => "AND",
                    array
                    (
                        "key"   => "assign_key_category",
                        "value" => $term_id,
                        "compare" => '='
                    ),
                    array
                    (
                        "key"   => "replace_archive_page",
                        "compare" => '=',
                        "value" => '1'
                    )

                )
            );

            $matches = get_posts($args);
            if ($matches):
                return $matches[0];
            endif;

        endif;
        return null;
    }


    public function fallback_parent_page()
    {

        if(!$this->_fallback_parent_page)
        {
            if (is_archive()) // pull out the first post if an archive page, and use its data
            {
                if ($this->alt_posts())
                {
                    $use = $this->_alt_posts()[0];
                    if (is_object($use) && property_exists($use, 'usefulData') && is_array($use->usefulData))
                    {
                        $this->_fallback_parent_page = $use->usefulData['posts_page'];
                    }
                }
            }
            else
            {
                $this->_fallback_parent_page = (isset($this->usefulData)) ? $this->usefulData['posts_page'] : null;
            }
        }
        return $this->_fallback_parent_page;        
    }
    
    public function crumbs()
    {

        if(!$this->_crumbs)
        {
            $this->_crumbs = null;
            $rtn = array();

            // pages are easy
            if ( $this->postType == "Page" && !is_archive() )
            {
                $ancestors = $this->doAncestors($this->ID);

                if ($ancestors && count($ancestors) > 0):
                    $ancestors = array_reverse($ancestors);
                    foreach($ancestors as $ancestor):
                        $post = get_post($ancestor);
                        if ($post->post_name != "site-pages"):
                            $rtn[] = 
                            [
                                'slug' => $post->post_name,
                                'post_name' => $post->post_name,                       
                                'name' => $post->post_title,
                                'title' => $post->post_title,
                                'link' => get_permalink($post->ID),
                                'step_count' => null
                            ];
                        endif;
                    endforeach;
                endif;
            }
            else // posts and archive pages take a bit of effort
            {
                $term_main = $this->primary_category; //( is_archive() ) ? (new \Timber\Term( get_queried_object())) : ($this->current_category() );
                $used_pages = [];
                $used_parent = null;

                if ($term_main && !is_wp_error($term_main) )
                {
                    $all_parents = get_ancestors( $term_main->term_id, $term_main->taxonomy, 'taxonomy');

                    if (is_array($all_parents)):
                        $reverse = array_reverse($all_parents, true); // we need it the other way around

                        if (!is_archive())
                        {
                            $reverse[] = $term_main->term_id;
                        }

                        $step_count = count($reverse);

                        foreach($reverse as $id):
                            $term = get_term($id);
                            if ($term && !is_wp_error($term)):
                                $page = $this->isTermAssignedToPage($term->term_id);

                                if ($page):
                                    $used_pages[] = $page->ID; // make sure we don't use it twice
                                    $rtn[] = array(
                                        'slug' => $page->post_name,
                                        'post_name' => $page->post_name,
                                        'name' => $page->post_title,
                                        'title' => $page->post_title,
                                        'link' => get_permalink($page->ID),
                                        'step_count' => $step_count,
                                        'term_id' => $term->term_id,
                                    );
                                else:
                                    $rtn[] = array(
                                        'slug' => $term->slug,
                                        'post_name' => $term->slug, // we hack this in for Twig's sake
                                        'name' => $term->name,
                                        'title' => $term->name,
                                        'link' => get_term_link($term->term_id),
                                        'step_count' => $step_count,
                                        'term_id' => $term->term_id
                                    );
                                endif;
                            endif;

                        endforeach;
                    endif;

                }

                //  if the highest term has a parent page assigned in CMS, use it.
                if (count($rtn) > 0):
                    $last = $rtn[0];
                    $parent =  get_term_meta( $last['term_id'], 'taxonomy_parent_page', true );
                    if ($parent):
                        $parent = get_post( $parent );
                        if ($parent && !in_array( $parent->ID, $used_pages)):
                            array_unshift($rtn,  array(
                                'slug' => $parent->post_name,
                                'post_name' => $parent->post_name,
                                'name' => $parent->post_title,
                                'title' => $parent->post_title,
                                'link' => get_permalink($parent->ID),
                                'step_count' => $step_count
                            ));
                        $used_parent = true;
                        endif;
                    endif;
                endif;
                // and if not, then see if one has been assigned in the post class
                if (!$used_parent):
                    $fallback = $this->fallback_parent_page();
                    if ($fallback):
                        $new_page =   m40_get_page_by_title( $fallback );
                        if ($new_page && is_object($new_page))
                        {
                            $par = array
                            (
                                'slug' => $new_page->post_name,
                                'post_name' => $new_page->post_name,
                                'name' => $new_page->post_title,
                                'title' => $new_page->post_title,
                                'link' => get_permalink($new_page->ID),
                                'step_count' => 0
                            );
                            array_unshift($rtn, $par);
                        }

                    endif;
                endif;


            }

            if (count($rtn) > 0):    
                $this->_crumbs = $rtn;
            endif;

        }


        return $this->_crumbs;
    }

    public function classNameForTimberFromString($classname)
    {
        // this is a bit twisty I know: requires absolute consistency in post type names
        // e.g. Hailstone\Core\Posts\Article\Article

        $classname = ucfirst($classname);

        if ($classname == "Post")
        {
            return null;
        }
        if ($classname == "Page")
        {
            return  "\Hailstone\\Core\\Page";
        }
        if ($classname == "Attachment")
        {
            return  "\Hailstone\\Core\\Attachment";
        }
        if ($classname == "Case-study")
        {
            return  "\Hailstone\\Core\\CaseStudy";
        }
        return  "\Hailstone\\Core\\" . ucfirst($classname);  

    }
}