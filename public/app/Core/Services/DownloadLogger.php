<?php

namespace Hailstone\Core\Services;

use Hailstone\Core\Attachment;
use Jaybizzle\CrawlerDetect\CrawlerDetect;

class DownloadLogger
{
    const DB_VERSION = 1.0;

    private $tableName;

    public function __construct()
    {
        global $wpdb;

        $this->wpdb = $wpdb;
        $this->tableName = $this->wpdb->prefix . 'doclog';

        
    }

    /**
     * @param Attachment $attachment
     * @param $referringPage
     */
    public function recordDownload(Attachment $attachment, $referringPage)
    {
        if(preg_match("/bot/", $_SERVER['HTTP_USER_AGENT'])) return;

        $CrawlerDetect = new CrawlerDetect;
        if($CrawlerDetect->isCrawler()) return;

        $this->createTableIfRequired();

        if (is_object($attachment) && is_numeric($referringPage))
        {
            $this->wpdb->insert(
                $this->tableName,
                [
                    'time' => current_time( 'mysql' ),
                    'download_title' => $attachment->title(),
                    'download_page_id' => $referringPage,
                    'file_id' => $attachment->ID,
                    'user_ip' => $this->get_current_user_ip(),
                    'user_id' =>  \get_current_user_id(),
                    'user_agent' => $_SERVER['HTTP_USER_AGENT']
                ]
            );
        }
    }

    public function get_current_user_ip() {
        $ip_keys = array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR');
        foreach ($ip_keys as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    // trim for safety measures
                    $ip = trim($ip);
                    // attempt to validate IP
                    if ($this->validateIPAddress($ip) && $ip != env('LOADBALANCER')) {
                        return $ip;
                    }
                }
            }
        }
        return isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : false;
    }
    function validateIPAddress($ip)
    {
        if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) === false) {
            return false;
        }
        return true;
    }

    /**
     * @param int $numberOfDays
     * @param int $limit
     *
     * @return mixed
     */
    public function getStatisticsLog($numberOfDays = 0, $limit = 100)
    {
        $now = time();
        $timePeriod = "";
        if (is_numeric ($numberOfDays) && $numberOfDays > 0){
            $start = ($now - (60*60*24*$numberOfDays)); // 24 hours ago
            $timePeriod =  " WHERE time > '".date('Y-m-d H:i:s', $start)."'";

        }

        $result = $this->wpdb->get_results ("
            SELECT count(file_id) as count, file_id, download_title
            FROM $this->tableName
                $timePeriod
                GROUP BY file_id
                ORDER BY count(file_id) DESC
                LIMIT $limit;           
        ");

        return $result;
    }

    // get a count of downloads,  accepts a numeric parameter for days to go back
    function getStatisticsCount ($days = 0){

        $now = time();
        $period = "";
        if (is_numeric ($days) && $days > 0){
            $start = ($now - (60*60*24*$days));
            $period =  " WHERE time > '".date('Y-m-d H:i:s', $start)."'";
        }

        $result = $this->wpdb->get_results ( "
        SELECT count(file_id) as count
            FROM $this->tableName
                $period;         
        ");

        if ($result && count($result) == 1) {
            return $result[0]->count;
        }

        return 0;
    }

    /**
     *
     */
    public function createTableIfRequired()
    {

        if($this->wpdb->get_var("SHOW TABLES LIKE '".$this->tableName."'") != $this->tableName) 
        {

            $charset = $this->wpdb->get_charset_collate();

            $sql = "CREATE TABLE $this->tableName (
                        id mediumint(9) NOT NULL AUTO_INCREMENT,
                        time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
                        download_title text NOT NULL,
                        download_page_id mediumint(9) NOT NULL,
                        file_id mediumint(9) NOT NULL,
                        user_ip text NOT NULL,
                        user_id mediumint(9) NOT NULL,
                        user_agent text NOT NULL,
                        UNIQUE KEY id (id)
                    ) $charset;";

            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $sql );

            add_option( 'doclog_db_version', static::DB_VERSION );

        }
    }
}