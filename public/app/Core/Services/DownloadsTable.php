<?php

namespace Hailstone\Core\Services;

class DownloadsTable extends \WP_List_Table
{
    private $data_count;

    public function __construct($args = [])
    {
        parent::__construct($args);

        $this->prepare_items();
        $this->display();
    }

    /**
     * Prepare the items for the table to process
     *
     * @return Void
     */
    public function prepare_items()
    {
        $columns = $this->get_columns();
        $hidden = $this->get_hidden_columns();
        $sortable = $this->get_sortable_columns();

        $data = $this->table_data();
        usort( $data, [&$this, 'sort_data']);

        $perPage = 50;
        $currentPage = $this->get_pagenum();
        $totalItems = count($data);

        $this->set_pagination_args([
            'total_items' => $totalItems,
            'per_page'    => $perPage
        ]);

        $data_count = 0;
        foreach($data as $now) {
            $data_count += $now['downloads'];
        }

        $data = array_slice($data,(($currentPage-1)*$perPage),$perPage);
        $this->data_count = $data_count;

        echo "<p><b>Total downloads during period: </b>".$data_count."</p>";
        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->items = $data;
    }


    /**
     * Override the parent columns method. Defines the columns to use in your listing table
     *
     * @return Array
     */
    public function get_columns()
    {
        $columns = array(
            'name'      => 'Name',
            'downloads' => 'Downloads',
        );

        return $columns;
    }


    /**
     * Define which columns are hidden
     *
     * @return Array
     */
    public function get_hidden_columns()
    {
        return array();
    }


    /**
     * Define the sortable columns
     *
     * @return Array
     */
    public function get_sortable_columns()
    {
        return array(
            'name'  => array('name', false),
            'downloads'  => array('downloads', false)
        );
    }


    /**
     * Get the table data
     *
     * @return Array
     */
    private function table_data()
    {
        $days2 = ""; // variable for the end date, only used with periods

        // if we are using periods (quarters), the boundaries are defined in this array
        if(isset($_POST['quarter']) && $_POST['quarter'] != '')
        {
            $periods = array (
                'Quarter-1' => array (
                    'start_day' => 1,
                    'start_month' => 1,
                    'end_day' => 31,
                    'end_month' => 3
                ),
                'Quarter-2' => array (
                    'start_day' => 1,
                    'start_month' => 4,
                    'end_day' => 30,
                    'end_month' => 6
                ),
                'Quarter-3' => array (
                    'start_day' => 1,
                    'start_month' => 7,
                    'end_day' =>30,
                    'end_month' => 9
                ),
                'Quarter-4' => array (
                    'start_day' => 1,
                    'start_month' => 10,
                    'end_day' => 31,
                    'end_month' => 12
                )
            );
            // check if we have a valid name for the quarter
            $quarter_full_name = $_POST['quarter'];
            $quarter_arr = explode(" ", $quarter_full_name);
            if (count($quarter_arr) == 2 && array_key_exists($quarter_arr[0], $periods) )
            {
                $period_name = $quarter_arr[0];
                $period = $periods[$period_name];
                $year = $quarter_arr[1];
                // start and end timestamps for the period
                $first_second_of_period = mktime(0, 0, 0, $period['start_month'], $period['start_day'], $year);
                $last_second_of_period = mktime(23, 59, 59, $period['end_month'], $period['end_day'], $year);

                // start time
                $s_str = date("Y-m-d", $first_second_of_period);
                $s_date = date_create($s_str);

                // end time
                $e_str = date("Y-m-d", $last_second_of_period);
                $e_date = date_create($e_str);

                // time right about now
                $n_str = date("Y-m-d", time());
                $n_date = date_create($n_str);

                // find out the number of days since
                $s_interval = date_diff($n_date, $s_date);
                $days = $s_interval->days;

                // only set the end boundary if it is in past
                if ($n_date > $e_date) {
                    $e_interval = date_diff($n_date, $e_date);
                    $days2 = $e_interval->days;
                }
            }
            else {
                $days = 9999;
            }
        }
        else
        {
            $days = 9999;
            if(isset($_POST['time']) && is_numeric($_POST['time']))
            {
                $days = $_POST['time'];
            }

        }

        $categoryID = 'all';
        if(isset($_POST['category']))
        {
            $categoryID = $_POST['category'];
        }

        $rawData = (new DownloadLogger())->getStatisticsLog($days, 9999);

        $data = array();

        if($rawData) {

            foreach ($rawData as $item) {

                $data[] = array(
                    'name'      => $item->download_title,
                    'downloads' => $item->count,
                );

            }
        }
        return $data;
    }


    /**
     * Define what data to show on each column of the table
     *
     * @param  Array $item        Data
     * @param  String $column_name - Current column name
     *
     * @return Mixed
     */
    public function column_default( $item, $column_name )
    {
        switch( $column_name ) {

            case 'position':
                return $item[ $column_name ];
                break;

            case 'downloads':
                return $item[ $column_name ];
                break;

            case 'name':
                return $item[ $column_name ];
                break;

            default:
                return print_r( $item, true ) ;
        }
    }


    /**
     * Allows you to sort the data by the variables set in the $_GET
     *
     * @return Mixed
     */
    private function sort_data( $a, $b )
    {
        // Set defaults
        $orderby = 'downloads';
        $order = 'desc';

        // If orderby is set, use this as the sort column
        if(!empty($_GET['orderby']))
        {
            $orderby = $_GET['orderby'];
        }

        // If order is set use this as the order
        if(!empty($_GET['order']))
        {
            $order = $_GET['order'];
        }


        $result = strnatcmp( $a[$orderby], $b[$orderby] );

        if($order === 'asc')
        {
            return $result;
        }

        return -$result;
    }
}