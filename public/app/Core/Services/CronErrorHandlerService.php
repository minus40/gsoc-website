<?php


namespace Hailstone\Core\Services;


use Exception;

class CronErrorHandlerService
{
    private static ?CronErrorHandlerService $instance = null;

    public static function getInstance(): CronErrorHandlerService
    {
        if ( self::$instance !== null )
        {
            return self::$instance;
        }

        self::$instance = new CronErrorHandlerService();

        return self::$instance;
    }

    public function handleCronError(array $custom_ignore_files = []): void
    {

        set_error_handler(
            function ( $error_type, $error_message, $error_file, $error_line ) use ( $custom_ignore_files ) {

                if ( in_array( $error_type, [ E_DEPRECATED, E_USER_DEPRECATED ]) )
                {
                    return false;
                }
                if ( in_array( $error_type, [ E_WARNING, E_NOTICE, E_CORE_WARNING, E_COMPILE_WARNING, E_USER_WARNING, E_USER_NOTICE ]) )
                {
                    $ignore_files = [
                        "class-wp-site-health.php",
                        "Cache_File_Cleaner_Generic.php",
                        "ninjafirewall/lib/utils.php",
                        "ninjafirewall/lib/security_rules_update.php",
                        "wp/wp-includes/functions.php",
                        "gravity-forms-2018/form_display.php"
                    ];
                    $ignore_files = array_merge($ignore_files, $custom_ignore_files);
                    foreach ($ignore_files as $ignore_file) {
                        if (
                            str_ends_with( $error_file, $ignore_file )
                        )
                        {
                            return false;
                        }
                    }
                }

                $values = [
                    "Type" => $error_type,
                    "Message" => $error_message,
                    "File" => $error_file,
                    "Line" => $error_line
                ];

                $headers = [
                    'Content-Type: text/html; charset=UTF-8',
                    'Reply-To: error@' . env( 'APP_URL' ) . ' <error@' . env( 'APP_URL' ) . '>'
                ];

                $body = "<table>";
                $exception_message = "";
                foreach ( $values as $key => $value )
                {
                    $body .= "<tr>" .
                        "<td><b>" . $key . ":&nbsp;</b></td>" .
                        "<td>" . $value . "</td>" .
                        "</tr>";
                    if ( $exception_message )
                    {
                        $exception_message .= " ";
                    }
                    $exception_message .= $key . ": " . $value . ".";
                }
                $body .= "</table>";

                wp_mail( 'support@minus40.co', ( 'Fatal Error during cron script for: ' . get_bloginfo( 'name' ) ), $body, $headers );

                throw new Exception( $exception_message );
            } );
    }
}