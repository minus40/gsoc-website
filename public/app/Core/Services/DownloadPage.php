<div class="wrap">
    <?php if(!isset($_GET['time'])): ?>
        <h2>Popular Downloads</h2>
    <?php else: ?>

        <?php if(is_numeric($_GET['time']) && $_GET['time'] > 1): ?>
            <h2>Popular Downloads of last <?php echo $_GET['time']; ?> days</h2>
        <?php endif; ?>

        <?php if(is_numeric($_GET['time']) && $_GET['time'] == 1): ?>
            <h2>Popular Downloads of today</h2>
        <?php endif; ?>

    <?php endif; ?>

    <h3>Filter Results</h3>

    <form method="post" style="clear: both;">


        <lable for="category" style="display: inline-block; min-width: 60px;">Category</lable>
        <?php
        $categories = get_terms( 'media_category', 'hide_empty=1' );

        $list = '<select name="category" id="category">';

        $list .= '<option value="all">All</option>';

        foreach($categories as $category)
        {
            $selected = '';
            if ( isset($_POST['category']) && $_POST['category'] == $category->term_id) $selected = 'selected="selected" ';

            $list .= '<option value="' . $category->term_id . '"' . $selected . '>' . $category->name . '</option>';
        }

        $list .= '</select>';

        echo $list;
        ?>

        <br>

        <lable for="time" style="display: inline-block; min-width: 60px;">Period</lable>
        <select name="time" id="time">
            <option <?php if (isset($_POST['time']) && $_POST['time'] == "all") echo 'selected="selected" '; ?> value="all">All Time</option>
            <option <?php if (isset($_POST['time']) && $_POST['time'] == "90") echo 'selected="selected" '; ?> value="90">Last 90 Days</option>
            <option <?php if (isset($_POST['time']) && $_POST['time'] == "30") echo 'selected="selected" '; ?> value="30">Last 30 Days</option>
            <option <?php if (isset($_POST['time']) && $_POST['time'] == "7") echo 'selected="selected" '; ?> value="7">Last 7 Days</option>
            <option <?php if (isset($_POST['time']) && $_POST['time'] == "1") echo 'selected="selected" '; ?> value="1">Today</option>
        </select>

        <?php submit_button('Filter Table', 'button-primary send-email'); ?>
    </form>

    <?php
        new \Hailstone\Core\Services\DownloadsTable();
    ?>

</div>