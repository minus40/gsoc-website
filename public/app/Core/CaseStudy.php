<?php

namespace Hailstone\Core;

use Hailstone\Core\Post;
use Hailstone\Core\Traits\HasCarousel;
use Hailstone\Core\Traits\HasContentBlocks;

class CaseStudy extends Post
{
    public $postType = 'case-study';
    public $usefulData =
    [
        'post_type' => 'case-study',
        'class_name' => 'CaseStudy',
        'posts_page' => 'Case Summaries',
        'main_taxonomy' => 'case_study_categories',
        'orderby' => 'menu_order',
        'order' => 'ASC',
        'posts_per_page' => -1
    ];
    var $_categories;
    var $_primary_category;
    var $_related_posts;

    /**
     * @return array
     */
    public function categories()
    {
        if(!$this->_categories)
        {
            $categories = $this->terms('case_study_categories');
            if (is_array($categories) && count($categories)) {
                $this->_categories = $categories;
            }
        }
        return $this->_categories;
    }


    /**
     * @param $key
     * @param $terms
     * @param bool $includeChildren
     *
     * @return $this
     */
    public function whereCategoriesIn($terms, $includeChildren = true)
    {
        $this->args['tax_query'][] = [
            'taxonomy' => 'case_study_categories',
            'field' => 'slug',
            'terms' => explode(",", $terms),
            'operator' => 'IN',
            'include_children' => $includeChildren
        ];

        return $this;
    }


}