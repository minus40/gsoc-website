<?php

namespace Hailstone\Core;

use Hailstone\Core\Post;
use Hailstone\Core\Traits\HasCarousel;

class Location extends Post
{
    public $postType = 'location';
    public $usefulData =
    [
        'post_type' => 'location',
        'class_name' => 'Location',
        'posts_page' => 'Get in Touch',
        'main_taxonomy' => 'category',
        'orderby' => 'menu_order',
        'order' => 'ASC',
    ];

    var $_categories;
    var $_latitude;
    var $_longitude;

    /**
     * @return array
     */
    public function categories()
    {
        if(!$this->_categories)
        {
            $categories = $this->terms('groups');
            if (is_array($categories) && count($categories)) {
                $this->_categories = $categories;
            }
        }
        return $this->_categories;
    }

    /**
     * @return mixed
     */
    public function latitude()
    {
        if(!$this->_latitude)
        {
            $this->_latitude = $this->map['lat'];
        }

        return $this->_latitude;
    }

    /**
     * @return mixed
     */
    public function longitude()
    {
        if(!$this->_longitude)
        {
            $this->_longitude = $this->map['lng'];
        }

        return $this->_longitude;
    }


    // all we are doing here is taking a bunch of fields and making them easier to use as a list
    public function address_details()
    {

        $items = [];
        $fields = ['address', 'address_2', 'town', 'county', 'postcode']; //'address_country'
        foreach($fields as $field):
            $now = $this->get_field($field);
            if( $now && !empty( $now )):
                $items[str_replace("address_", "", $field)] = $now;
            endif;
        endforeach;

        if (count($items) == 0):
            $items = null;
        endif;

        $this->_address_details = $items;
        return $items;
    }

    // all we are doing here is taking a bunch of fields and making them easier to use as a list
    public function address_details_gmap()
    {

        $str = "";
        $details = $this->address_details();
        if ($details)
        {
            $str .= $this->title();
            foreach($details as $detail)
            {
                $str .= ($str == "") ? "" : ", ";
                $str .= $detail;
            }
            $str = urlencode($str);
        }
        return $str;
    }
    

    /**
     * @return mixed
     */
    public function formatted_telephone_number()
    {
        return formatPhoneNumber($this->get_field('telephone_number'));
    }

    /**
     * @return mixed
     */
    public function formatted_secondary_number()
    {
        return formatPhoneNumber($this->get_field('secondary_number'));
    }

    /**
     * @return mixed
     */
    public function formatted_mobile_number()
    {
        return formatPhoneNumber($this->get_field('mobile_number'));
    }
}