<?php

namespace Hailstone\Core;

use Timber\Timber;

class User extends \Timber\User
{
    public $postType = 'user';

    public function all()
    {
        $args = [
            'meta_key'     => '',
            'orderby'      => 'display_name',
            'order'        => 'ASC',
            'count_total'  => false,
        ];

        $users = get_users($args);

        $allUsers = [];
        foreach($users as $user) {
            $allUsers[] = new static($user->ID);
        }

        return $allUsers;
    }
}