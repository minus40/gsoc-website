<?php

namespace Hailstone\Core\Support;

use Hailstone\Core\Foundation\Application;

abstract class ServiceProvider
{
    /**
     * @var Application
     */
    protected $app;

    /**
     * ServiceProvider constructor.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    abstract public function register();
}