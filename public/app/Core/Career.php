<?php

namespace Hailstone\Core;

use Hailstone\Core\Post;

class Career extends Post
{
    public $postType = 'career';
    public $usefulData =
    [
        'post_type' => 'career',
        'class_name' => 'Career',
        'posts_page' => 'Careers',
        'main_taxonomy' => 'career_categories',
        'orderby' => 'meta_value',
        'order' => 'ASC',
        'meta_key' => 'ends_at'
    ];

    var $_categories;

    /**
     * @return array
     */
    public function categories()
    {
        if(!$this->_categories)
        {
            $categories = $this->terms('career_categories');
            if (is_array($categories) && count($categories)) {
                $this->_categories = $categories;
            }
        }
        return $this->_categories;
    }


}