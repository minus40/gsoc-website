<?php

namespace Hailstone\Core;

use ArrayAccess;
use Illuminate\Support\Collection;

class Carousel extends Collection
{
    var $_layout;
    var $_video;
    var $_slides = [];
    var $_image;
    var $_heading;
    var $_subheading;
    var $_actions;
    var $_slides_have_content;

    public function __construct(array $data = [])
    {
        $this->build($data);
    }

    /**
     * @param $data
     * @return $this
     */
    private function build($data)
    {
        $this->_layout = $data['acf_fc_layout'];
        $this->_image = isset($data['image']) && $data['image'] != '' ? new Image($data['image']['ID']) : null;
        $this->_video = isset($data['video']) && $data['video'] != '' ? new Video($data['video']->ID) : null;
        $this->_heading = isset($data['heading']) ? $data['heading'] : null;
        $this->_subheading = isset($data['subheading']) ? $data['subheading'] : null;
        $this->_actions = isset($data['actions']) ? $data['actions'] : null;

        if ($data['slides'] != '') {
            foreach($data['slides'] as $slide)
            {
                $this->_slides[] = new Slide($slide);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function layout()
    {
        return $this->_layout;
    }

    /**
     * @return mixed
     */
    public function video()
    {
        return $this->_video;
    }

    /**
     * @return mixed
     */
    public function image()
    {
        return $this->_image;
    }

    /**
     * @return mixed
     */
    public function slides()
    {
        return $this->_slides;
    }

    public function heading()
    {
        return $this->_heading;
    }

    public function subheading()
    {
        return $this->_subheading;
    }

    public function slides_have_content()
    {
        if(!$this->_slides_have_content)
        {
            foreach($this->_slides as $slide) {
                if(strlen($slide->heading()) > 0 || strlen($slide->subheading()) > 0) return true;
            }
        }
    }

    public function actions()
    {
        return $this->_actions;
    }
}