<?php

namespace Hailstone\Core;

use Hailstone\Core\Post;
use Hailstone\Core\Video;

class FAQ extends Post
{
    public $postType = 'faq';
    public $usefulData =
    [
        'post_type' => 'faq',
        'class_name' => 'Faq',
        'posts_page' => 'Frequently asked questions',
        'main_taxonomy' => 'faq_category',
        'orderby' => 'title',
        'order' => 'ASC',
        'posts_per_page' => -1
    ];
    var $_categories;


    /**
     * @return array
     */
    public function categories()
    {
        if(!$this->_categories)
        {
            $categories = $this->terms('faq_category');
            if (is_array($categories) && count($categories)) {
                $this->_categories = $categories;
            }
        }
        return $this->_categories;
    }

    /**
     * @return Video
     */
    public function video()
    {
        if(!is_object($this->_video)) {
            $video = $this->get_field('video');

            if($video) {
                $this->_video = new Video($video->ID);
            }
        }

        return $this->_video;
    }



}