<?php
Routes::map('news-room/archive/page/:pg', function($params){
    //$query = 'post_type=article&paged='.$params['pg'];
    //Routes::load('page-news-room.php', false, $query);
});

Routes::map('make-a-complaint/before-you-complain/how-we-deal-with-complaints/case-summaries/page/:pg', function($params){
    //$query = 'post_type=case-study&paged='.$params['pg'];
    //Routes::load('page-case-studies-investigation.php', false, $query);
});

Routes::map('forms/complaint', function($params) {
    (new \Hailstone\Core\Forms\ComplaintForm())->handle();
    exit;
});

Routes::map('forms/pdf', function($params) {
    (new \Hailstone\Core\Forms\ComplaintForm())->generatePDF();
    exit;
});

Routes::map('language/switch/:lang', function($params)
{
    if(isset($params['lang'])) {
        (app('session'))->set('language', $params['lang']);
    }

    if(wp_get_referer()) {
        wp_safe_redirect( wp_get_referer() );
    }

    wp_safe_redirect( '/make-a-complaint/submit-a-complaint/' );
    exit;
});