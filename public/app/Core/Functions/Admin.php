<?php

namespace Hailstone\Core\Functions;

use Hailstone\Core\Services\DownloadsTable;
use Hailstone\Core\Services\DownloadLogger;

class Admin
{
    const DB_VERSION = 1.0;
    private $tableName;

    public function __construct()
    {
        global $wpdb;
        $this->wpdb = $wpdb;
        $this->tableName = $this->wpdb->prefix . 'doclog';

        $this->wpdb = $GLOBALS['wpdb'];

        $this->setupAdminCSS();
        $this->customiseLoginScreen();
        $this->addChosenToAdmin();
        $this->enableDuplicatingPostsAsDraft();
        $this->createDownloadsTable();
        $this->displayDownloadsWidget();
        $this->addAdminCapabilities();
        $this->addAuthorCapabilities();
        $this->applyACFStylingToAdmin();
        $this->customPasswordResetMessage();

        $this->removePHPNag();
        $this->customLoginValidations();
        $this->fixLocalizeIncorrectly();

        $this->removeLanguageSwitcher();
        $this->preventEnumerationAttempts();
        $this->addACFSelectOptions();
        $this->addACFJS();

        $this->AddPagesToRequest1();
        $this->AddPagesToRequest2();

        $this->setLastLogin();

        //$this->enableMaintenanceMode();

    }


    private function enableMaintenanceMode() 
    {
        add_action('template_redirect', function()
        {

            $admin_ips = array
            (
                "194.46.129.12",
                //"213.205.241.95"
            );

            $ip = $this->getUserIP();

            if (!$ip || !in_array($ip, $admin_ips)):

                if (!current_user_can('edit_themes') || !is_user_logged_in()) 
                {
                    wp_die('<h1>Under Maintenance</h1><br />The website is currently undergoing planned maintenance. It will be available again shortly');
                }
            endif;
        });
    }
   

    private function preventEnumerationAttempts()
    {

        # Prevent user Enumeration Query Parameter
        add_action( 'template_redirect', function() 
        {
            $is_author_set = get_query_var( 'author', '' );
            if ( $is_author_set != '' && !is_admin()) {
                wp_redirect( home_url(), 301 );
                exit;
            }
        });

        # Prevent user Enumeration JSON REST Endpoints
        add_filter( 'rest_endpoints', function($endpoints)
        {
            if ( isset( $endpoints['/wp/v2/users'] ) ) {
                unset( $endpoints['/wp/v2/users'] );
            }
            if ( isset( $endpoints['/wp/v2/users/(?P<id>[\d]+)'] ) ) {
                unset( $endpoints['/wp/v2/users/(?P<id>[\d]+)'] );
            }
            if ( isset( $endpoints['/wp-json/wp/v2/users'] ) ) {
                unset( $endpoints['/wp-json/wp/v2/users'] );
            }
            if ( isset( $endpoints['/wp-json/wp/v2/users/(?P<id>[\d]+)'] ) ) {
                unset( $endpoints['/wp-json/wp/v2/users/(?P<id>[\d]+)'] );
            }

            return $endpoints;
        });

    }


    private function removeLanguageSwitcher()
    {
        add_filter( 'login_display_language_dropdown' , '__return_false' );
    }

    // fixes a WP 5.7 warning
    private function fixLocalizeIncorrectly()
    {
        add_filter('doing_it_wrong_trigger_error', function() 
        {
            return false;
        }, 10, 0);

        add_filter( 'big_image_size_threshold', '__return_false' );
    }

    private function removePHPNag()
    {
        add_action( 'wp_dashboard_setup', function()
        {
            remove_meta_box( 'dashboard_php_nag', 'dashboard', 'normal' );
        });
    }


    private function customLoginValidations()
    {

        //drop old filter
        remove_filter('authenticate', 'custom_authenticate_username_password', 20, 3);

        //add new filter
        add_filter('authenticate', function($user, $username, $password) 
        {
            if ( \is_a($user, 'WP_User') ) { return $user; }

            if ( empty($username) || empty($password) ) {
                $error = new \WP_Error();

                if ( empty($username) )
                    $error->add('empty_username', __('<strong>ERROR</strong>: The USERNAME/EMAIL field is empty.'));

                if ( empty($password) )
                    $error->add('empty_password', __('<strong>ERROR</strong>: The PASSWORD field is empty.'));

                return $error;
            }

            $userdata = \get_user_by('login', $username);

            if ( !$userdata )
            {
                //invalid username
                return new \WP_Error('invalid_username', sprintf(__('<strong>ERROR</strong>: Invalid username or password. <a href="%s" title="Password Lost and Found">Lost your password</a>?'), wp_lostpassword_url()));
            }

            $userdata = apply_filters('wp_authenticate_user', $userdata, $password);
            if ( is_wp_error($userdata) )
            {
                return $userdata;
            }

            if ( !wp_check_password($password, $userdata->user_pass, $userdata->ID) )
            {
                // invalid password
                return new \WP_Error( 'incorrect_password', sprintf( __( '<strong>ERROR</strong>: Either your username or password is incorrect. <a href="%2$s" title="Password Lost and Found">Lost your password</a>?' ),
                $username, wp_lostpassword_url() ) );
            }

            $user =  new \WP_User($userdata->ID);
            return $user;
        }, 30, 3);

    }

    /**
    * Registers Admin CSS.
    */
    private function setupAdminCSS()
    {
        // Register CSS file.
        add_action( 'admin_head', function(){
            $file = get_template_directory() . '/css/dashboard.css';
            $pub = get_template_directory_uri() . '/css/dashboard.css';
            
            wp_register_style( 'm40-dashboard-css', $pub, '', filemtime( $file ), '' );
            wp_enqueue_style( 'm40-dashboard-css' );
        });
    }

    /**
     * Registers custom CSS and a logo for the Admin Login Screen.
     */
    private function customiseLoginScreen()
    {
        // Register CSS file.
        add_action( 'login_enqueue_scripts', function(){

            $file = get_template_directory() . '/library/chosen-js/chosen.min.css';
            $pub = get_template_directory_uri() . '/library/chosen-js/chosen.min.css';

            wp_register_style( 'chosen-css', $pub, '', filemtime( $file ), '' );
            wp_enqueue_style( 'chosen-css' );

            $file = get_template_directory() . '/css/login.css';
            $pub = get_template_directory_uri() . '/css/login.css';

            wp_register_style( 'm40-login-css', $pub, '', filemtime( $file ), '' );
            wp_enqueue_style( 'm40-login-css' );

        });

        add_filter( 'login_headertext', function(){ return config('admin.login.site_name'); });
        add_filter( 'login_headerurl', function(){ return config('admin.login.site_url'); });
    }

    private function addChosenToAdmin()
    {
        add_action( 'admin_enqueue_scripts', function() {
            wp_register_script('chosen', get_template_directory_uri() . '/js/admin.js', ['jquery']);
            wp_enqueue_script('chosen');
        });
    }

    /**
     *
     */
    private function createDownloadsTable()
    {
        $logger = new DownloadLogger;

        if(is_admin()) 
        {
           $make = $logger->createTableIfRequired();
        }
    }

    /**
     *
     */
    private function displayDownloadsWidget()
    {
        add_action( 'wp_dashboard_setup', function(){
            if ( current_user_can('editor') || current_user_can('administrator') ) {
                wp_add_dashboard_widget( 'doclog_dashboard_downloads', 'Downloads Monitor', function() {
                    ?>

                    <div id="ddownload-count">
                        <ul>
                            <li id="ddownload-count-1">
                                <span class="count"><?php echo $this->doclog_stats_count(1);?></span>
                                <span class="label">Last 24 Hours</span>
                            </li>
                            <li id="ddownload-count-7">
                                <span class="count"><?php echo $this->doclog_stats_count(7);?></span>
                                <span class="label">Last 7 Days</span>
                            </li>
                            <li id="ddownload-count-30">
                                <span class="count"><?php echo $this->doclog_stats_count(30);?></span>
                                <span class="label">Last 30 Days</span>
                            </li>
                            <li id="ddownload-count-0">
                                <span class="count"><?php echo $this->doclog_stats_count(0);?></span>
                                <span class="label">All Time</span>
                            </li>
                        </ul>
                    </div>
                    <div id="ddownload-popular">
                        <h4>Popular Downloads</h4>

                        <?php
                        echo $this->buildDownloadsList(0, "block", 10);
                        echo $this->buildDownloadsList(1, "none", 10);
                        echo $this->buildDownloadsList(7, "none", 10);
                        echo $this->buildDownloadsList(30, "none", 10);
                        ?>

                        <div class="sub">
                            <select id="popular-downloads-dropdown" onchange="changeDownloadList(this)">
                                <option value="1">Last 24 Hours</option>
                                <option value="7">Last 7 Days</option>
                                <option value="30">Last 30 Days</option>
                                <option value="0" selected="selected">All Time</option>
                            </select>
                            <span class="spinner"></span>
                            <p class="error" style="display: none"></p>
                        </div>
                    </div>
                    <script>
                        // this would be nicer in jQuery, but will do for now.
                        function changeDownloadList(sel) {
                            var days = "popular-downloads-" + sel.value;
                            document.getElementById("popular-downloads-0").style.display = "none";
                            document.getElementById("popular-downloads-1").style.display = "none";
                            document.getElementById("popular-downloads-7").style.display = "none";
                            document.getElementById("popular-downloads-30").style.display = "none";
                            document.getElementById(days).style.display = "block";
                        }
                    </script>
                    <?php
                }
                );
            }
        });
    }

    private function doclog_stats_log ($days = 0, $limit = 100){
        global $wpdb;
        $now = time();
        $period_clause = "";
        if (is_numeric ($days) && $days > 0){
            $start = ($now - (60*60*24*$days));
            $period_clause =  " WHERE time > '".date('Y-m-d H:i:s', $start)."'";

        }
        $table_name = $wpdb->prefix . 'doclog';

        $result = $wpdb->get_results ( "
        SELECT count(file_id) as count, file_id, download_title
            FROM $table_name
                $period_clause
                GROUP BY file_id
                ORDER BY count(file_id) DESC
                LIMIT $limit;           
    " );
        return ($result);

    }

    // get a count of downloads,  accepts a numeric parameter for days to go back
    public function doclog_stats_count ($days = 0){
        global $wpdb;
        $now = time();
        $period_clause = "";
        if (is_numeric ($days) && $days > 0){
            $start = ($now - (60*60*24*$days));
            $period_clause =  " WHERE time > '".date('Y-m-d H:i:s', $start)."'";

        }
        $table_name = $wpdb->prefix . 'doclog';

        $result = $wpdb->get_results ( "
        SELECT count(file_id) as count
            FROM $table_name
                $period_clause;         
        ");

        if ($result && count($result) == 1) {
            return $result[0]->count;
        }

        return 0;
    }

    private function buildDownloadsList($days, $css_switch = "block", $rows = 10){

        $rtn = "";
        if (is_numeric($days)) {

            $popular_downloads = $this->doclog_stats_log($days, $rows);
            $key_count = 0;
            if ( is_array($popular_downloads) && count($popular_downloads) > 0 ) {
                $rtn .= '<ol id="popular-downloads-'.$days.'" style="display:'.$css_switch.'">';

                foreach ( $popular_downloads as $download) {
                    $rtn .= '<li>';
                    $rtn .= '<a href="/?downloads=file&file=' .  $download->file_id . '"><span class="position">' . ( $key_count + 1 ) . '.</span>' . $download->download_title . ' <span class="count">' . $download->count . '</span></a>';
                    $rtn .= '</li>';
                    $key_count++;
                }

                $rtn .= '</ol>';
            }
            else {
                $rtn .= '<p id="popular-downloads-'.$days.'" style="display:'.$css_switch.'">There are no recorded downloads yet.</p>';
            }
        }
        return ($rtn);
    }

    // User Capabilities
    private function addAdminCapabilities()
    {
        add_action( 'admin_init', function(){
            $roles = array('editor', 'administrator');
            foreach ($roles as $the_role) {
                $role = get_role($the_role);
                $role->add_cap( 'read' );

                $role->add_cap( 'read_article');
                $role->add_cap( 'read_private_articles' );
                $role->add_cap( 'edit_article' );
                $role->add_cap( 'edit_articles' );
                $role->add_cap( 'edit_others_articles' );
                $role->add_cap( 'edit_published_articles' );
                $role->add_cap( 'publish_articles' );
                $role->add_cap( 'delete_others_articles' );
                $role->add_cap( 'delete_private_articles' );
                $role->add_cap( 'delete_published_articles' );

                $role->add_cap( 'read_audio');
                $role->add_cap( 'read_private_audios' );
                $role->add_cap( 'edit_audio' );
                $role->add_cap( 'edit_audios' );
                $role->add_cap( 'edit_others_audios' );
                $role->add_cap( 'edit_published_audios' );
                $role->add_cap( 'publish_audios' );
                $role->add_cap( 'delete_others_audios' );
                $role->add_cap( 'delete_private_audios' );
                $role->add_cap( 'delete_published_audios' );

                $role->add_cap( 'read_career');
                $role->add_cap( 'read_private_careers' );
                $role->add_cap( 'edit_career' );
                $role->add_cap( 'edit_careers' );
                $role->add_cap( 'edit_others_careers' );
                $role->add_cap( 'edit_published_careers' );
                $role->add_cap( 'publish_careers' );
                $role->add_cap( 'delete_others_careers' );
                $role->add_cap( 'delete_private_careers' );
                $role->add_cap( 'delete_published_careers' );

                $role->add_cap( 'read_casestudy');
                $role->add_cap( 'read_private_casestudies' );
                $role->add_cap( 'edit_casestudy' );
                $role->add_cap( 'edit_casestudies' );
                $role->add_cap( 'edit_others_casestudies' );
                $role->add_cap( 'edit_published_casestudies' );
                $role->add_cap( 'publish_casestudies' );
                $role->add_cap( 'delete_others_casestudies' );
                $role->add_cap( 'delete_private_casestudies' );
                $role->add_cap( 'delete_published_casestudies' );

                $role->add_cap( 'read_chart');
                $role->add_cap( 'read_private_charts' );
                $role->add_cap( 'edit_chart' );
                $role->add_cap( 'edit_charts' );
                $role->add_cap( 'edit_others_charts' );
                $role->add_cap( 'edit_published_charts' );
                $role->add_cap( 'publish_charts' );
                $role->add_cap( 'delete_others_charts' );
                $role->add_cap( 'delete_private_charts' );
                $role->add_cap( 'delete_published_charts' );

                $role->add_cap( 'read_event');
                $role->add_cap( 'read_private_events' );
                $role->add_cap( 'edit_event' );
                $role->add_cap( 'edit_events' );
                $role->add_cap( 'edit_others_events' );
                $role->add_cap( 'edit_published_events' );
                $role->add_cap( 'publish_events' );
                $role->add_cap( 'delete_others_events' );
                $role->add_cap( 'delete_private_events' );
                $role->add_cap( 'delete_published_events' );

                $role->add_cap( 'read_faq');
                $role->add_cap( 'read_private_faqs' );
                $role->add_cap( 'edit_faq' );
                $role->add_cap( 'edit_faqs' );
                $role->add_cap( 'edit_others_faqs' );
                $role->add_cap( 'edit_published_faqs' );
                $role->add_cap( 'publish_faqs' );
                $role->add_cap( 'delete_others_faqs' );
                $role->add_cap( 'delete_private_faqs' );
                $role->add_cap( 'delete_published_faqs' );

                $role->add_cap( 'read_location');
                $role->add_cap( 'read_private_locations' );
                $role->add_cap( 'edit_location' );
                $role->add_cap( 'edit_locations' );
                $role->add_cap( 'edit_others_locations' );
                $role->add_cap( 'edit_published_locations' );
                $role->add_cap( 'publish_locations' );
                $role->add_cap( 'delete_others_locations' );
                $role->add_cap( 'delete_private_locations' );
                $role->add_cap( 'delete_published_locations' );

                $role->add_cap( 'read_person');
                $role->add_cap( 'read_private_people' );
                $role->add_cap( 'edit_person' );
                $role->add_cap( 'edit_people' );
                $role->add_cap( 'edit_others_people' );
                $role->add_cap( 'edit_published_people' );
                $role->add_cap( 'publish_people' );
                $role->add_cap( 'delete_others_people' );
                $role->add_cap( 'delete_private_people' );
                $role->add_cap( 'delete_published_people' );

                $role->add_cap( 'read_product');
                $role->add_cap( 'read_private_products' );
                $role->add_cap( 'edit_product' );
                $role->add_cap( 'edit_products' );
                $role->add_cap( 'edit_others_products' );
                $role->add_cap( 'edit_published_products' );
                $role->add_cap( 'publish_products' );
                $role->add_cap( 'delete_others_products' );
                $role->add_cap( 'delete_private_products' );
                $role->add_cap( 'delete_published_products' );

                $role->add_cap( 'read_report');
                $role->add_cap( 'read_private_reports' );
                $role->add_cap( 'edit_report' );
                $role->add_cap( 'edit_reports' );
                $role->add_cap( 'edit_others_reports' );
                $role->add_cap( 'edit_published_reports' );
                $role->add_cap( 'publish_reports' );
                $role->add_cap( 'delete_others_reports' );
                $role->add_cap( 'delete_private_reports' );
                $role->add_cap( 'delete_published_reports' );

                $role->add_cap( 'read_service');
                $role->add_cap( 'read_private_services' );
                $role->add_cap( 'edit_service' );
                $role->add_cap( 'edit_services' );
                $role->add_cap( 'edit_others_services' );
                $role->add_cap( 'edit_published_services' );
                $role->add_cap( 'publish_services' );
                $role->add_cap( 'delete_others_services' );
                $role->add_cap( 'delete_private_services' );
                $role->add_cap( 'delete_published_services' );

                $role->add_cap( 'read_statistic');
                $role->add_cap( 'read_private_statistics' );
                $role->add_cap( 'edit_statistic' );
                $role->add_cap( 'edit_statistics' );
                $role->add_cap( 'edit_others_statistics' );
                $role->add_cap( 'edit_published_statistics' );
                $role->add_cap( 'publish_statistics' );
                $role->add_cap( 'delete_others_statistics' );
                $role->add_cap( 'delete_private_statistics' );
                $role->add_cap( 'delete_published_statistics' );


                $role->add_cap( 'read_video');
                $role->add_cap( 'read_private_videos' );
                $role->add_cap( 'edit_video' );
                $role->add_cap( 'edit_videos' );
                $role->add_cap( 'edit_others_videos' );
                $role->add_cap( 'edit_published_videos' );
                $role->add_cap( 'publish_videos' );
                $role->add_cap( 'delete_others_videos' );
                $role->add_cap( 'delete_private_videos' );
                $role->add_cap( 'delete_published_videos' );

            }
        });
    }

    private function addAuthorCapabilities()
    {
        add_action( 'admin_init', function(){
            $roles = array('author');
            foreach ($roles as $the_role) {
                $role = get_role($the_role);
                $role->add_cap( 'read' );
                $role->remove_cap('edit_posts');
                $role->remove_cap('edit_published_posts');
                $role->remove_cap('delete_posts');
                $role->remove_cap('delete_published_posts');
                $role->remove_cap('publish_posts');

                $role->add_cap( 'read_article');
                $role->remove_cap('read_private_articles');
                $role->add_cap( 'read_articles');
                $role->add_cap( 'edit_article' );
                $role->add_cap( 'edit_articles' );
                $role->add_cap( 'delete_article' );
                $role->add_cap( 'delete_articles' );
                $role->remove_cap('edit_others_articles');
                $role->remove_cap('edit_published_articles');
                $role->remove_cap('publish_articles');
                $role->remove_cap('delete_others_articles');
                $role->remove_cap('delete_private_articles');
                $role->remove_cap('delete_published_articles');

                $role->add_cap( 'read_audio');
                $role->remove_cap('read_private_audios');
                $role->add_cap( 'read_audios');
                $role->add_cap( 'edit_audio' );
                $role->add_cap( 'edit_audios' );
                $role->add_cap( 'delete_audio' );
                $role->add_cap( 'delete_audios' );
                $role->remove_cap('edit_others_audios');
                $role->remove_cap('edit_published_audios');
                $role->remove_cap('publish_audios');
                $role->remove_cap('delete_others_audios');
                $role->remove_cap('delete_private_audios');
                $role->remove_cap('delete_published_audios');

                $role->add_cap( 'read_career');
                $role->remove_cap('read_private_careers');
                $role->add_cap( 'read_careers');
                $role->add_cap( 'edit_career' );
                $role->add_cap( 'edit_careers' );
                $role->add_cap( 'delete_career' );
                $role->add_cap( 'delete_careers' );
                $role->remove_cap('edit_others_careers');
                $role->remove_cap('edit_published_careers');
                $role->remove_cap('publish_careers');
                $role->remove_cap('delete_others_careers');
                $role->remove_cap('delete_private_careers');
                $role->remove_cap('delete_published_careers');

                $role->add_cap( 'read_casestudy');
                $role->remove_cap('read_private_casestudies');
                $role->add_cap( 'read_casestudies');
                $role->add_cap( 'edit_casestudy' );
                $role->add_cap( 'edit_casestudies' );
                $role->add_cap( 'delete_casestudy' );
                $role->add_cap( 'delete_casestudies' );
                $role->remove_cap('edit_others_casestudies');
                $role->remove_cap('edit_published_casestudies');
                $role->remove_cap('publish_casestudies');
                $role->remove_cap('delete_others_casestudies');
                $role->remove_cap('delete_private_casestudies');
                $role->remove_cap('delete_published_casestudies');

                $role->add_cap( 'read_chart');
                $role->remove_cap('read_private_charts');
                $role->add_cap( 'read_charts');
                $role->add_cap( 'edit_chart' );
                $role->add_cap( 'edit_charts' );
                $role->add_cap( 'delete_chart' );
                $role->add_cap( 'delete_charts' );
                $role->remove_cap('edit_others_charts');
                $role->remove_cap('edit_published_charts');
                $role->remove_cap('publish_charts');
                $role->remove_cap('delete_others_charts');
                $role->remove_cap('delete_private_charts');
                $role->remove_cap('delete_published_charts');

                $role->add_cap( 'read_event');
                $role->remove_cap('read_private_events');
                $role->add_cap( 'read_events');
                $role->add_cap( 'edit_event' );
                $role->add_cap( 'edit_events' );
                $role->add_cap( 'delete_event' );
                $role->add_cap( 'delete_events' );
                $role->remove_cap('edit_others_events');
                $role->remove_cap('edit_published_events');
                $role->remove_cap('publish_events');
                $role->remove_cap('delete_others_events');
                $role->remove_cap('delete_private_events');
                $role->remove_cap('delete_published_events');

                $role->add_cap( 'read_faq');
                $role->remove_cap('read_private_faqs');
                $role->add_cap( 'read_faqs');
                $role->add_cap( 'edit_faq' );
                $role->add_cap( 'edit_faqs' );
                $role->add_cap( 'delete_faq' );
                $role->add_cap( 'delete_faqs' );
                $role->remove_cap('edit_others_faqs');
                $role->remove_cap('edit_published_faqs');
                $role->remove_cap('publish_faqs');
                $role->remove_cap('delete_others_faqs');
                $role->remove_cap('delete_private_faqs');
                $role->remove_cap('delete_published_faqs');

                $role->add_cap( 'read_location');
                $role->remove_cap('read_private_locations');
                $role->add_cap( 'read_locations');
                $role->add_cap( 'edit_location' );
                $role->add_cap( 'edit_locations' );
                $role->add_cap( 'delete_location' );
                $role->add_cap( 'delete_locations' );
                $role->remove_cap('edit_others_locations');
                $role->remove_cap('edit_published_locations');
                $role->remove_cap('publish_locations');
                $role->remove_cap('delete_others_locations');
                $role->remove_cap('delete_private_locations');
                $role->remove_cap('delete_published_locations');

                $role->add_cap( 'read_person');
                $role->remove_cap('read_private_people');
                $role->add_cap( 'read_people');
                $role->add_cap( 'edit_person' );
                $role->add_cap( 'edit_people' );
                $role->add_cap( 'delete_person' );
                $role->add_cap( 'delete_people' );
                $role->remove_cap('edit_others_people');
                $role->remove_cap('edit_published_people');
                $role->remove_cap('publish_people');
                $role->remove_cap('delete_others_people');
                $role->remove_cap('delete_private_people');
                $role->remove_cap('delete_published_people');

                $role->add_cap( 'read_product');
                $role->remove_cap('read_private_products');
                $role->add_cap( 'read_products');
                $role->add_cap( 'edit_product' );
                $role->add_cap( 'edit_products' );
                $role->add_cap( 'delete_product' );
                $role->add_cap( 'delete_products' );
                $role->remove_cap('edit_others_products');
                $role->remove_cap('edit_published_products');
                $role->remove_cap('publish_products');
                $role->remove_cap('delete_others_products');
                $role->remove_cap('delete_private_products');
                $role->remove_cap('delete_published_products');

                $role->add_cap( 'read_report');
                $role->remove_cap('read_private_reports');
                $role->add_cap( 'read_reports');
                $role->add_cap( 'edit_report' );
                $role->add_cap( 'edit_reports' );
                $role->add_cap( 'delete_report' );
                $role->add_cap( 'delete_reports' );
                $role->remove_cap('edit_others_reports');
                $role->remove_cap('edit_published_reports');
                $role->remove_cap('publish_reports');
                $role->remove_cap('delete_others_reports');
                $role->remove_cap('delete_private_reports');
                $role->remove_cap('delete_published_reports');

                $role->add_cap( 'read_service');
                $role->remove_cap('read_private_services');
                $role->add_cap( 'read_services');
                $role->add_cap( 'edit_service' );
                $role->add_cap( 'edit_services' );
                $role->add_cap( 'delete_service' );
                $role->add_cap( 'delete_services' );
                $role->remove_cap('edit_others_services');
                $role->remove_cap('edit_published_services');
                $role->remove_cap('publish_services');
                $role->remove_cap('delete_others_services');
                $role->remove_cap('delete_private_services');
                $role->remove_cap('delete_published_services');

                $role->add_cap( 'read_statistic');
                $role->remove_cap('read_private_statistics');
                $role->add_cap( 'read_statistics');
                $role->add_cap( 'edit_statistic' );
                $role->add_cap( 'edit_statistics' );
                $role->add_cap( 'delete_statistic' );
                $role->add_cap( 'delete_statistics' );
                $role->remove_cap('edit_others_statistics');
                $role->remove_cap('edit_published_statistics');
                $role->remove_cap('publish_statistics');
                $role->remove_cap('delete_others_statistics');
                $role->remove_cap('delete_private_statistics');
                $role->remove_cap('delete_published_statistics');


                $role->add_cap( 'read_video');
                $role->remove_cap('read_private_videos');
                $role->add_cap( 'read_videos');
                $role->add_cap( 'edit_video' );
                $role->add_cap( 'edit_videos' );
                $role->add_cap( 'delete_video' );
                $role->add_cap( 'delete_videos' );
                $role->remove_cap('edit_others_videos');
                $role->remove_cap('edit_published_videos');
                $role->remove_cap('publish_videos');
                $role->remove_cap('delete_others_videos');
                $role->remove_cap('delete_private_videos');
                $role->remove_cap('delete_published_videos');
            }
        });
    }

    private function applyACFStylingToAdmin()
    {
        \add_action('acf/input/admin_head', function()
        {
            ?>
            <style type="text/css">
                .acf-flexible-content .layout .acf-fc-layout-handle {
                    /*background-color: #00B8E4;*/
                    background-color: #00A0D6;
                    color: #fff;
                    border-bottom: 1px solid #00A0D6;
                }
                .acf-flexible-content .layout
                {
                    border: 1px solid #00A0D6;
                    border-radius:5px;
                }
                .acf-repeater.-row > table > tbody > tr > td,
                .acf-repeater.-block > table > tbody > tr > td {
                    border-top: 1px solid #00A0D6;
                }
                .acf-repeater .acf-row-handle {
                    vertical-align: top !important;
                    padding-top: 16px;
                }
                .acf-repeater .acf-row-handle span {
                    font-size: 20px;
                    font-weight: bold;
                    color: #202428;
                }
                .imageUpload img {
                    width: 75px;
                }
                .acf-repeater .acf-row-handle .acf-icon.-minus {
                    top: 30px;
                }
                .acf-field-group .acf-fields.-border
                {
                    border: 1px solid #ADD8E6;
                }
            </style>
            <?php

        });
    }

    
    /**
     * Enables and handles the ability to duplicate posts as a draft.
     */
    private function enableDuplicatingPostsAsDraft()
    {
        add_action( 'admin_action_rd_duplicate_post_as_draft', function()
        {
            $this->checkOriginalPostWasProvided();

            $originalPost = $this->getOriginalPost();
            $newPostAuthor = wp_get_current_user()->ID;
            // $newPostAuthor = $originalPost->post_author;

            $draftPostId = $this->duplicateAndInsertDraftPost($originalPost, $newPostAuthor);

            $this->updateTermsForDraftPost($originalPost, $draftPostId);
            $this->updatePostMetaForDraftPost($originalPost, $draftPostId);

            return wp_redirect(admin_url( 'post.php?action=edit&post=' . $draftPostId ));
        });

        // Add the "Duplicate" link in the admin panel.
        add_filter( 'post_row_actions', function($actions, $post)
        {
            if( current_user_can('editor') || current_user_can('administrator') ) {
                $actions['duplicate'] = '<a href="admin.php?action=rd_duplicate_post_as_draft&amp;post=' . $post->ID . '" title="Duplicate this item" rel="permalink">Duplicate</a>';
            }
            return $actions;
        }, 10, 2);

        // Add the "Duplicate" link in the admin panel.
        add_filter( 'page_row_actions', function($actions, $post)
        {
            if( current_user_can('editor') || current_user_can('administrator') ) {
                $actions['duplicate'] = '<a href="admin.php?action=rd_duplicate_post_as_draft&amp;post=' . $post->ID . '" title="Duplicate this item" rel="permalink">Duplicate</a>';
            }
            return $actions;
        }, 10, 2);


    }

    /**
     * Checks if an original Post ID was provided in the request.
     */
    private function checkOriginalPostWasProvided()
    {
        if(! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'rd_duplicate_post_as_draft' == $_REQUEST['action'] ) ) )
        {
            wp_die('No post to duplicate has been supplied!');
        }
    }

    /**
     * @return array|null|\WP_Post
     */
    private function getOriginalPost()
    {
        // Get the original post ID, and attempt to get the original post.
        $originalPostId = isset($_GET['post']) ? $_GET['post'] : $_POST['post'];
        $originalPost = get_post(isset($_GET['post']) ? $_GET['post'] : $_POST['post']);

        if (is_null($originalPost)) wp_die('Post creation failed, could not find original post: ' . $originalPostId);

        return $originalPost;
    }

    /**
     * Maps the original post data to the new draft post and saves it.
     *
     * @param $originalPost
     * @param $newPostAuthor
     *
     * @return int|\WP_Error
     */
    private function duplicateAndInsertDraftPost($originalPost, $newPostAuthor)
    {
        /*
         * new post data array
         */
        $args = array(
            'comment_status' => $originalPost->comment_status,
            'ping_status' => $originalPost->ping_status,
            'post_author' => $newPostAuthor,
            'post_content' => $originalPost->post_content,
            'post_excerpt' => $originalPost->post_excerpt,
            'post_name' => $originalPost->post_name,
            'post_parent' => $originalPost->post_parent,
            'post_password' => $originalPost->post_password,
            'post_status' => 'draft',
            'post_title' => $originalPost->post_title,
            'post_type' => $originalPost->post_type,
            'to_ping' => $originalPost->to_ping,
            'menu_order' => $originalPost->menu_order
        );

        return wp_insert_post( $args );
    }

    /**
     * @param $originalPost
     * @param $draftPostId
     */
    private function updateTermsForDraftPost($originalPost, $draftPostId)
    {
        $taxonomies = get_object_taxonomies($originalPost->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
        foreach ($taxonomies as $taxonomy)
        {
            $terms = wp_get_object_terms($originalPost->ID, $taxonomy, array('fields' => 'slugs'));
            wp_set_object_terms($draftPostId, $terms, $taxonomy, false);
        }
    }

    /**
     * Copies the PostMeta data from the original post to the new post.
     *
     * @param $originalPost
     * @param $draftPostId
     */
    private function updatePostMetaForDraftPost($originalPost, $draftPostId)
    {
        $postMeta = $this->wpdb->get_results("SELECT meta_key, meta_value FROM {$this->wpdb->postmeta} WHERE post_id={$originalPost->ID}");

        if ( count($postMeta) > 0)
        {
            $querySegments = [];
            $query = "INSERT INTO {$this->wpdb->postmeta} (post_id, meta_key, meta_value)";

            foreach ($postMeta as $item) {
                $metaKey = $item->meta_key;
                $metaValue = addslashes($item->meta_value);
                $querySegments[]= "SELECT $draftPostId, '$metaKey', '$metaValue'";
            }

            $query .= implode(" UNION ALL ", $querySegments);
            $this->wpdb->query($query);
        }
    }


    private function customPasswordResetMessage()
    {

        \add_filter("retrieve_password_message", function ($message, $key, $user_login, $user_data )
        {

            $message = "Someone has requested a password reset for the following account: \n\r
                " . sprintf(__('%s'), $user_data->user_email) . " \n\r
                If this was a mistake, just ignore this email and nothing will happen.\n\r
                To reset your password, visit the following address: \n\r
                " . network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user_login), 'login') . "\r\n"
            ;

            return $message;

        }, 99, 4);
    }

    private function addACFSelectOptions()
    {

        add_filter('acf/load_field/key=acf-template-variations-listings-post-type', function($field)
        {
            $pts = get_post_types($args = [ 'public' => true, 'publicly_queryable' => true ], 'objects' );
            $field['choices'] = [];
            
            if ($pts && count($pts) > 0)
            {
                foreach ($pts as $key=>$pt)
                {
                    if ($key != "post" && $key != "attachment")
                    {
                        $field['choices'][$key] = $pt->label;
                    }
                }
            }
            
            return $field;
        });
        add_filter('acf/load_field/key=acf-template-variations-listings-category-type', function($field)
        {
            $field['choices'] = [];
            $loop = $this->loopForAllTaxonomies();

            if ($loop && count($loop) > 0)
            {
                $count = 1;
                foreach($loop as $key => $group)
                {
                    $join = 'is-disabled-' . $count;
                    $field['choices'][$join] = $key;
                    foreach($group as $unit)
                    {
                        $field['choices'][$unit->term_id] = $unit->name;   
                    }
                    $count++;
                }
            }
            
            return $field;
        });

    }
   // this works in tandem w/ addACFSelectOptions()
    private function addACFJS()
    {
        add_action( 'admin_print_footer_scripts', function()
        {
        ?>
                <script>
                    jQuery(document).ready(function($)
                    {

                        $("select option").each(function() 
                        {
                            var tmp = $(this).val();
                            if (tmp.includes("is-disabled-"))
                            {
                                $(this).prop( "disabled", true );
                                $(this).attr('style', 'color:#ccc; text-transform:uppercase; font-weight:bold; font-size:0.7rem; cursor:not-allowed; padding-top: 10px');
                            }

                        });
                    });

                </script>
        <?php

        });

    }

    public function loopForAllTaxonomies()
    {
        $items = [];

        $taxonomiesToRemoveFromSelect = 
        [
            'post_tag',
            //'category',
            'nav_menu',
            'link_category',
            'media_categories',
            'media_category',
            'post_format'
        ];
        $all = get_taxonomies();

        if ($all && is_array($all) && count($all) > 0)
        {
            asort($all);
            foreach($all as $tax)
            {
                if (!in_array($tax, $taxonomiesToRemoveFromSelect))
                {
                    $terms = get_terms($tax);
                    if ($terms && is_array($terms) && count($terms) > 0)
                    {
                        $labels = get_taxonomy($tax);
                        $items[$labels->label] = $terms;
                    }
                }   
            }
        }
        return (count($items) > 0) ? $items : null;
    }

    /* these two functions ensure that /1/ is read on archive pages */
    private function AddPagesToRequest1()
    {
        add_filter('request', function($query_string)
        {
            if( isset( $query_string['page'] ) ) {
                if( ''!=$query_string['page'] ) {
                    if( isset( $query_string['name'] ) ) {
                        unset( $query_string['name'] );
                    }
                }

            }
            return $query_string;
        });
        
    }

    private function AddPagesToRequest2() 
    { 
        add_action('pre_get_posts', function($query)
        {
            if( $query->is_main_query() && !$query->is_feed() && !$query->is_search() && !is_admin() ) { 
                $query->set( 'paged', str_replace( '/', '', get_query_var( 'page' ) ) ); 
            } 
        });
    }

    private function getUserIP() 
    {
        $ip_keys = array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR');
        foreach ($ip_keys as $key) {
            if (array_key_exists($key, $_SERVER) === true) 
            {
                foreach (explode(',', $_SERVER[$key]) as $ip) 
                {
                    // trim for safety measures
                    $ip = trim($ip);

                    // attempt to validate IP
                    if ($this->checkIPValid($ip))
                    {
                        return $ip;
                    }
                }
            }
        }
        return isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : false;
    }

    private function checkIPValid($ip)
    {
        if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) === false) {
            return false;
        }
        return true;
    }


    private function setLastLogin()
    {

        // set the last login date for a user
        add_action('wp_login', function($login, $user)
        {  
            $user = get_user_by('login',$login);
            $time = date('Y-m-d H:i:s');
            $last_login = get_user_meta( $user->ID, '_last_login', 'true' );
         
            if(!$last_login)
            {
                update_user_meta( $user->ID, '_last_login', $time );
            }
            else
            {
                update_user_meta( $user->ID, '_last_login_prev', $last_login );
                update_user_meta( $user->ID, '_last_login', $time );
            }
         
        }, 0, 2);
    }

}