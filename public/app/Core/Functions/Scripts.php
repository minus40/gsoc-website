<?php

namespace Hailstone\Core\Functions;

class Scripts
{

    /**
     * Scripts constructor.
     */
    public function __construct()
    {
        $this->enqueueScripts();
        $this->enqueueFooterCSS();
        $this->enqueueScriptAttributes();
        $this->removeJqueryMigrate();
        $this->removeGravityCSS();
    }

    /**
     *
     */
    private function enqueueScripts()
    {
        \add_action( 'wp_enqueue_scripts', function()
        {

            wp_deregister_script('jquery');
            wp_register_script( 'jquery', '//code.jquery.com/jquery-3.6.0.min.js', array(), '', false );
            wp_enqueue_script('jquery');

            wp_deregister_script('popper');
            wp_register_script( 'popper-js', '//cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js', array('jquery'), '', true );
            wp_enqueue_script('popper-js');

            wp_deregister_script('bootstrap-js');
            wp_register_script( 'bootstrap-js', '//cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js', array('jquery'), '', true );
            wp_enqueue_script('bootstrap-js');

             wp_deregister_script('modernizr');

            // Custom JavaScript
            $file = get_template_directory() . '/library/chosen-js/chosen.jquery.min.js';
            $pub = get_template_directory_uri() . '/library/chosen-js/chosen.jquery.min.js';
            wp_register_script( 'chosen-js', $pub, array( 'jquery' ), filemtime( $file ), true );
            wp_enqueue_script( 'chosen-js' );

            // Custom JavaScript
            $file = get_template_directory() . '/library/flatpickr/flatpickr.min.js';
            $pub = get_template_directory_uri() . '/library/flatpickr/flatpickr.min.js';
            wp_register_script( 'flatpickr-js', $pub, array( 'jquery' ), filemtime( $file ), true );
            wp_enqueue_script( 'flatpickr-js' );

            // Custom JavaScript
            $file = get_template_directory() . '/library/slick-carousel/slick.min.js';
            $pub = get_template_directory_uri() . '/library/slick-carousel/slick.min.js';
            wp_register_script( 'slick-js', $pub, array( 'jquery' ), filemtime( $file ), true );
            wp_enqueue_script( 'slick-js' );

            // Custom JavaScript
            $file = get_template_directory() . '/library/lazysizes/lazysizes.min.js';
            $pub = get_template_directory_uri() . '/library/lazysizes/lazysizes.min.js';
            wp_register_script( 'lazysizes-js', $pub, array( 'jquery' ), filemtime( $file ), true );
            wp_enqueue_script( 'lazysizes-js' );

            $file = get_template_directory() . '/library/vanilla-cookieconsent/cookieconsent-edit.js';
            wp_register_script( 'cookieconsent-js', get_template_directory_uri() . '/library/vanilla-cookieconsent/cookieconsent-edit.js', [], filemtime( $file ), true );
            wp_enqueue_script( 'cookieconsent-js' );

            // Custom JavaScript
            $file = get_template_directory() . '/js/script.js';
            $pub = get_template_directory_uri() . '/js/script.js';
            wp_register_script( 'script-js', $pub, array( 'jquery' ), filemtime( $file ), true );
            wp_enqueue_script( 'script-js' );


            $file = get_template_directory() . '/css/bootstrap-customised.css';
            $pub = get_template_directory_uri() . '/css/bootstrap-customised.css';
            wp_register_style( 'bootstrap-css', $pub, '', filemtime( $file ), '' );
            wp_enqueue_style( 'bootstrap-css' );


            $file = get_template_directory() . '/library/chosen-js/chosen.min.css';
            $pub = get_template_directory_uri() . '/library/chosen-js/chosen.min.css';
            wp_register_style( 'chosen-css', $pub, '', filemtime( $file ), '' );
            wp_enqueue_style( 'chosen-css' );


            $file = get_template_directory() . '/library/flatpickr/flatpickr.min.css';
            $pub = get_template_directory_uri() . '/library/flatpickr/flatpickr.min.css';
            wp_register_style( 'flatpickr-css', $pub, '', filemtime( $file ), '' );
            wp_enqueue_style( 'flatpickr-css' );


            $file = get_template_directory() . '/library/slick-carousel/slick.min.css';
            $pub = get_template_directory_uri() . '/library/slick-carousel/slick.min.css';
            wp_register_style( 'slick-css', $pub, '', filemtime( $file ), '' );
            wp_enqueue_style( 'slick-css' );


            $file = get_template_directory() . '/library/slick-carousel/slick-theme.min.css';
            $pub = get_template_directory_uri() . '/library/slick-carousel/slick-theme.min.css';
            wp_register_style( 'slick-theme-css', $pub, '', filemtime( $file ), '' );
            wp_enqueue_style( 'slick-theme-css' );


            $file = get_template_directory() . '/css/app.css';
            $pub = get_template_directory_uri() . '/css/app.css';
            wp_register_style( 'main-css', $pub, '', filemtime( $file ), '' );
            wp_enqueue_style( 'main-css' );

            wp_deregister_script('typekit');
            wp_register_script('typekit-js', '//use.typekit.net/koo2ezn.js', [],'', false);
            wp_enqueue_script('typekit-js');


            wp_deregister_style('font-awesome');
            wp_deregister_script('font-awesome');
            wp_register_script('font-awesome-5-js', '//kit.fontawesome.com/6ec1955cf1.js', [], '', true);
            wp_enqueue_script('font-awesome-5-js');


            wp_deregister_script( 'wp-embed' ); // we embed this ourselves


        }, 10);


    }


    private function enqueueFooterCSS()
    {

        add_action( 'wp_footer', function()
        {

            $file = get_template_directory() . '/css/print.css';
            $pub = get_template_directory_uri() . '/css/print.css';
            wp_register_style( 'print-css', $pub, '', filemtime( $file ), 'print' );
            wp_enqueue_style( 'print-css' );

            // DataTables
            /*wp_deregister_style('datatables');
            wp_register_style('datatables', '//cdn.datatables.net/v/zf/dt-1.10.16/datatables.min.css');
            wp_enqueue_style('datatables');

            wp_deregister_style('font-awesome');
            wp_register_style('font-awesome-css', '//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css');
            wp_enqueue_style('font-awesome-css');

            wp_deregister_style('ionicons');
            wp_register_style('ionicons-css', '//cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css');
            wp_enqueue_style('ionicons-css');*/



        });


    }


    // function allows us to replace WP's Script tags with ones that include attributes
    // if you don't need attributes added to a script, then don't add anything to the attributes array
    // otherwise make sure you choose beyween script or style as the type
    private function enqueueScriptAttributes()
    {

        if ( ! is_admin() ) {

            add_filter('script_loader_tag', function($tag, $handle, $src)
            {
                $attributes = 
                [
                    'jquery' =>
                    [
                        'integrity' => 'sha384-vtXRMe3mGCbOeY7l30aIg8H9p3GdeSe4IFlP6G8JMa7o7lXvnz3GFKzPxzJdPfGK',
                        //'defer' => 'defer',
                        'crossorigin' => 'anonymous',
                    ],
                    'popper' =>
                    [
                        //'integrity' => 'sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q',
                        'integrity' => 'sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p',
                        'defer' => 'defer',
                        'crossorigin' => 'anonymous',
                    ],
                    'bootstrap-js' =>
                    [
                        //'integrity' => 'sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl',
                        'integrity' => 'sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF',
                        'defer' => 'defer',
                        'crossorigin' => 'anonymous',
                    ],
                    'd3js' =>
                    [
                        'integrity' => 'sha384-TMMFW4AlpsWActNB8UrToQR4yThkcAjclFMofAQ7Lms5hGJUZALYpi1wBJipgJ4F',
                        'defer' => 'defer',
                        'crossorigin' => 'anonymous',
                    ],
                    'modernizr' =>
                    [
                        'integrity' => 'sha384-bPV3mA2eo3edoq56VzcPBmG1N1QVUfjYMxVIJPPzyFJyFZ8GFfN7Npt06Zr23qts',
                        'defer' => 'defer',
                        'crossorigin' => 'anonymous',
                    ],

                    'datatables-js2' =>
                    [
                        
                        'defer' => 'defer',
                        'crossorigin' => 'anonymous',
                    ],
                    'datatables-bs-js' =>
                    [

                        'defer' => 'defer',
                        'crossorigin' => 'anonymous',
                    ],

                    'chart-js' =>
                    [
                        //'integrity' => 'sha384-W8cTqoHl7yDp2Uz0Vpv3DLdW28EVXFcPdY+t64oQypRLFRfCruurQFkVnK4a/82K',
                        'defer' => 'defer',
                        'crossorigin' => 'anonymous',
                    ],
                    'typekit-js' =>
                    [
                        //'integrity' => 'sha384-g5SYuZHpLcoS8u+zL/dzXgNdjYMvCCON5/a8Bo7WIUW532eW2XwSnU9C63VWStE6',
                        'defer' => 'defer',
                        'crossorigin' => 'anonymous',
                    ],
                    'labratrevenge' =>
                    [
                        //'integrity' => 'sha384-0CPX6nYTwyfkDh/6t9fQfkLjyoL/CgSUOu/7w17vrdkPq1EJM/G6Uin92OsgirF+',
                        'defer' => 'defer',
                        'crossorigin' => 'anonymous',
                    ],
                    'flatpickr-js' =>
                    [
                        //'integrity' => 'sha384-UiZzAA+k9B6Xr4SykueV26lljeUk/hTx/fFNONRwB3Dt3dLDOXg8lQlbf/j8rB7B',
                        'defer' => 'defer',
                        'crossorigin' => 'anonymous',
                    ],

                    'script-js' =>
                    [
                        'defer' => 'defer',
                    ],
                ];


                $key = array_key_exists($handle, $attributes);

                if ( FALSE !== $key)
                {

                    $tag = "\t\t".'<script type="text/javascript" id="'.$handle.'" src="'.esc_url($src).'"';
                    foreach ($attributes[$handle] as $k => $v) 
                    {
                        $tag .= " ".$k."=".'"'.$v.'"';
                    }
                    $tag .= "></script>\n";

                }
                else // just defer by default
                {
                    $tag = "\t\t".'<script type="text/javascript" id="'.$handle.'" src="'.esc_url($src).'"';
                    $tag .= ' defer="defer"';
                    $tag .= "></script>\n";                   
                }
                return $tag;


            }, 10, 3); 


            add_filter('style_loader_tag', function($html, $handle, $href, $media)
            {
                $attributes = 
                [
                    'aos-css' =>
                    [
                        'integrity' => 'sha384-HrojG8PWtUTfXT5oycGf3oXzWSNdB/mJCMszDd6O6TeIH3PDZZ8F5yEkJA8NsoG4',
                        'defer' => 'defer',
                        'crossorigin' => 'anonymous',
                    ],
                    'font-awesome-css' =>
                    [
                        'integrity' => 'sha384-MI32KR77SgI9QAPUs+6R7leEOwtop70UsjEtFEezfKnMjXWx15NENsZpfDgq8m8S',
                        'defer' => 'defer',
                        'crossorigin' => 'anonymous',
                    ],
                    'ionicons-css' =>
                    [
                        'integrity' => 'sha384-gOaRlqAhqPUMlR/5HfjaLm+COAJ+Ka0Am9GCueJAWwFluNWKDUZJ8GUGhBJ1r+J/',
                        'defer' => 'defer',
                        'crossorigin' => 'anonymous',
                    ],
                    'flatpickr-css' =>
                    [

                    ],
                    'chosen-css' =>
                    [

                    ],
                    'slick-css' =>
                    [

                    ],
                    'slick-theme' =>
                    [

                    ],
                    'main-css' =>
                    [
                        // so it's not caught in the defer cycle
                    ],
                    'plugins-css-css' =>
                    [
                        // so it's not caught in the defer cycle
                    ],

                    'print-css' =>
                    [
                        'defer' => 'defer',
                    ],
                ];


                $key = array_key_exists($handle, $attributes);

                if ( FALSE !== $key)
                {

                    $html = "\t\t".'<link rel="stylesheet" id="'.$handle.'-css" href="'.esc_url($href).'"';
                    foreach ($attributes[$handle] as $k => $v) 
                    {
                        $html .= " ".$k."=".'"'.$v.'"';
                    }
                    $html .= ' type="text/css" />'."\n";

                }
                else
                {

                    $html = "\t\t".'<link rel="stylesheet" id="'.$handle.'-css" href="'.esc_url($href).'"';
                    $html .= ' defer="defer"';
                    $html .= ' type="text/css" />'."\n";
                }

                return $html;


            }, 10, 4); 
        }

    }

    private function deferGravityLoading()
    {

        // Force Gravity Forms to init scripts in the footer and ensure that the DOM is loaded before scripts are executed.
        add_filter( 'gform_init_scripts_footer', '__return_true' );
    }


    public function do_wrap_gform_cdata() 
    {
        if (
            is_admin()
            || ( defined( 'DOING_AJAX' ) && DOING_AJAX )
            || isset( $_POST['gform_ajax'] )
            || isset( $_GET['gf_page'] ) // Admin page (eg. form preview).
            || doing_action( 'wp_footer' )
            || did_action( 'wp_footer' )
        ) {
            return false;
        }
        return true;
    }

    //Remove JQuery migrate
    private function removeJqueryMigrate( ) 
    {
        if ( ! is_admin() ) 
        {
            add_action( 'wp_default_scripts', function ($scripts)
            {
                if (isset( $scripts->registered['jquery'] )):
                    $script = $scripts->registered['jquery'];
             
                    if ( $script->deps ) { // Check whether the script has any dependencies
                        $script->deps = array_diff( $script->deps, array( 'jquery-migrate' ) );
                    }
                endif;
            });
        }
    }
    private function removeGravityCSS()
    {
        add_filter( 'gform_disable_css', '__return_true' );
    }
}