<?php

namespace Hailstone\Core\Functions;

class Images
{
    /**
     * Images constructor.
     */
    public function __construct()
    {
        $this->limitFileDimensions();
        $this->registerImageSizes();
    }

    /**
     *
     */
    private function registerImageSizes()
    {
        foreach(config('images.sizes') as $key => $size)
        {
            \add_image_size($key, $size['width'], $size['height'], $size['crop']);
        }
    }

    /**
     * Sets the minimum file dimensions of uploaded images.
     */
    private function limitFileDimensions()
    {
        \add_filter('wp_handle_upload_prefilter',function($file)
        {
            $mimes = array( 'image/jpeg', 'image/png', 'image/gif' );

            if( !in_array( $file['type'], $mimes ) ) return $file;

            if($image = getimagesize( $file['tmp_name'] )) {
                $minimumDimensions = ['width' => config('images.min_width'), 'height' => config('images.min_height')];

                if ( $image[0] < $minimumDimensions['width'] ) {
                    $file['error'] = 'Image too small. Minimum width is ' . $minimumDimensions['width'] . 'px. Uploaded image width is ' . $image[0] . 'px';
                } elseif ( $image[1] < $minimumDimensions['height'] ) {
                    $file['error'] = 'Image too small. Minimum height is ' . $minimumDimensions['height'] . 'px. Uploaded image height is ' . $image[1] . 'px';
                }
            }

            return $file;
        });
    }
}