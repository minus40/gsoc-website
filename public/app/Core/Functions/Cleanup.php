<?php

namespace Hailstone\Core\Functions;

class Cleanup
{
    /**
     * Cleanup constructor.
     */
    public function __construct()
    {
        $this->removeEmojiCode();
        $this->removeBlogFeatures();
        $this->removeDefaultWidgets();
        $this->removeAdminDashboardMeta();
        $this->removeAdminBarLinks();
        $this->replaceAdminFooterText();
        $this->moveYoastToBottom();
        $this->removeHyphensFromUploadTitles();
    }

    /**
     *
     */
    private function removeEmojiCode()
    {
        // remove the emoji rubbish that inserts in HEAD of wp 4.2
        \remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
        \remove_action( 'wp_print_styles', 'print_emoji_styles' );
        \remove_action( 'admin_print_styles', 'print_emoji_styles' );
        \remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
        \remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
        \remove_action( 'wp_print_styles', 'print_emoji_styles' );
        \remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
        \remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
        \remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
    }

    /**
     *
     */
    private function removeBlogFeatures()
    {
        // General Cleanup - removes unnecessary WordPress features
        \remove_action( 'wp_head', 'feed_links_extra', 3 );
        \remove_action( 'wp_head', 'feed_links', 2 );
        \remove_action( 'wp_head', 'rsd_link' );
        \remove_action( 'wp_head', 'wlwmanifest_link' );
        \remove_action( 'wp_head', 'index_rel_link' );
        \remove_action( 'wp_head', 'parent_post_rel_link' );
        \remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
        \remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
        \remove_action( 'wp_head', 'wp_generator' );
        \remove_action( 'wp_head', 'wp_shortlink_wp_head');
        // Remove Yoast SEO Dashboard Widget
        
        \add_action('wp_dashboard_setup', function() {
            
            \remove_meta_box( 'wpseo-dashboard-overview', 'dashboard', 'side' );
        
        });

    }

    /**
     *
     */
    private function removeDefaultWidgets()
    {
        \unregister_widget('WP_Widget_Pages');
        \unregister_widget('WP_Widget_Calendar');
        \unregister_widget('WP_Widget_Archives');
        \unregister_widget('WP_Widget_Links');
        \unregister_widget('WP_Widget_Meta');
        \unregister_widget('WP_Widget_Search');
        \unregister_widget('WP_Widget_Text');
        \unregister_widget('WP_Widget_Categories');
        \unregister_widget('WP_Widget_Recent_Posts');
        \unregister_widget('WP_Widget_Recent_Comments');
        \unregister_widget('WP_Widget_RSS');
        \unregister_widget('WP_Widget_Tag_Cloud');
    }

    /**
     *
     */
    private function removeAdminDashboardMeta()
    {
        add_action( 'admin_init', function()
        {
            \remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
            \remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
            \remove_meta_box( 'dashboard_primary', 'dashboard', 'normal' ); // Wordpress News
            \remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
            \remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' ); // Quick Draft
            \remove_meta_box( 'dashboard_activity', 'dashboard', 'normal'); // Activity
            \remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
            \remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
            //\remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' ); // At A Glance
        });

    }

    /**
     *
     */
    private function removeAdminBarLinks()
    {
        \add_action( 'admin_bar_menu', function($wp_admin_bar)
        {
            $wp_admin_bar->remove_node('comments');         // Remove the comments link
            $wp_admin_bar->remove_node('updates');          // Remove the updates link
            $wp_admin_bar->remove_node('wp-logo');          // Remove the WordPress logo
            $wp_admin_bar->remove_node('about');            // Remove the about WordPress link
            $wp_admin_bar->remove_node('wporg');            // Remove the WordPress.org link
            $wp_admin_bar->remove_node('documentation');    // Remove the WordPress documentation link
            $wp_admin_bar->remove_node('support-forums');   // Remove the support forums link
            $wp_admin_bar->remove_node('feedback');         // Remove the feedback link
            $wp_admin_bar->remove_node('new-content');      // Remove the content link
            $wp_admin_bar->remove_node( 'customize' );
            $wp_admin_bar->remove_node( 'customize-background' );
            $wp_admin_bar->remove_node( 'customize-header' );
            $wp_admin_bar->remove_node( 'customize-themes' );
            $wp_admin_bar->remove_node( 'customize-widgets' );
            $wp_admin_bar->remove_node( 'customize-menus' );
        }, 999);


    }

    /**
     *
     */
    private function replaceAdminFooterText()
    {
        \add_filter('admin_footer_text', function(){
            echo 'Website by <a href="http://minus40.co">@minus40co</a>.';
        });
    }

    /**
     *
     */
    private function moveYoastToBottom()
    {
        \add_filter( 'wpseo_metabox_prio', function() { return 'low';});
    }

    /**
     *
     */
    private function removeHyphensFromUploadTitles()
    {
        // Fixes issue in 4.6.1 that adds hyphens to WP Titles
        \add_action( 'add_attachment', function ( $attachment_ID ) {
            $filename   =   $_REQUEST['name']; // or get_post by ID
            $withoutExt =   preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
            $withoutExt =   str_replace(array('-','_'), ' ', $withoutExt);
            $my_post = array(
                'post_title' => $withoutExt,  // title
                'ID'           => $attachment_ID,
            );
            wp_update_post( $my_post );
            // Update alt text for post
            update_post_meta($attachment_ID, '_wp_attachment_image_alt', $withoutExt );
        });
    }



}