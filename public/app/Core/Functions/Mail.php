<?php

namespace Hailstone\Core\Functions;

class Mail
{
    /**
     * PHP Mailer constructor.
     */
    public function __construct()
    {
        $this->defineSMTPSettings();
    }

    /**
     *
     */
    private function defineSMTPSettings()
    {
        add_action( 'phpmailer_init', function($phpmailer)
        {
            $phpmailer->isSMTP();
            $phpmailer->Host = env('MAIL_HOST');
            $phpmailer->SMTPAuth = false;
            if (env('MAIL_USERNAME') != "" and env('MAIL_PASSWORD') != "") {
                $phpmailer->SMTPAuth = true; // Force it to use Username and Password to authenticate
                $phpmailer->Username = env('MAIL_USERNAME');
                $phpmailer->Password = env('MAIL_PASSWORD');
            }
            $phpmailer->Port = env('MAIL_PORT');
            $phpmailer->SMTPSecure = env('MAIL_ENCRYPTION');
            $phpmailer->From = env('MAIL_FROM');
            $phpmailer->FromName = env('MAIL_FROMNAME');
        });
    }

}
