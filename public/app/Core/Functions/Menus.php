<?php

namespace Hailstone\Core\Functions;

class Menus
{
    public function __construct()
    {
        $this->registerMenus();
    }

    /**
     *
     */
    private function registerMenus()
    {
        \add_action( 'after_setup_theme', function ()
        {
            register_nav_menus(config('menus'));
        });
    }
}