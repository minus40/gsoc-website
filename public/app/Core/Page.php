<?php

namespace Hailstone\Core;

use Hailstone\Core\Traits\HasCarousel;
use Hailstone\Core\Traits\HasContentBlocks;

class Page extends Post
{
    public $postType = 'Page';
    var $_top_parent;
    var $_welcome_ctas_converted;
    var $_secondary_ctas_converted;
    var $_half_siblings;


    var $_counts_max_results;
    var $_counts_max_pages;
    var $_counts_found_results;
    var $_counts_posts_per_page;
    var $_counts_current_page;
    var $_counts_start_count;
    var $_counts_end_count;

    var $_paginationSimulation;
    var $_pageNumberFromURL;

    var $_alt_posts;
    var $_listingsPagePostsUsefulData;
    var $_listingsPagePostsArgs;

    var $_archives_list;


    public function listingsPagePostsUsefulData()
    {
        if (!$this->_listingsPagePostsUsefulData)
        {
            $this->_listingsPagePostsUsefulData = null;   
            $post_type = $this->query_all_acf("listings_post_type");

            if ($post_type): // get one from post type to set useful data

                $args = 
                [
                    'post_type' =>
                        [
                            $post_type
                        ],
                    'post_status' => 'publish',
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'posts_per_page' => 1,
                ];

                $classname = $this->classNameForTimberFromString($post_type);


                $posts = \Timber::get_posts($args, $classname);

   
                if ($posts && count($posts) == 1)
                {
                    $this->_listingsPagePostsUsefulData = $posts[0]->usefulData();
                }

            endif;  
        }

        return $this->_listingsPagePostsUsefulData;
    }


    public function listingsPagePostsArgs()
    {

        if (!$this->_listingsPagePostsArgs)
        {
            $basis = $this->listingsPagePostsUsefulData();
            $args = [];
            if ($basis)
            {   
                if(array_key_exists('post_type', $basis)) 
                {  
                    $args['post_type'] = $basis['post_type'];
                }
                if(array_key_exists('orderby', $basis)) 
                {  
                    $args['orderby'] = $basis['orderby'];
                }
                if(array_key_exists('order', $basis)) 
                {  
                    $args['order'] = $basis['order'];
                }
                if(array_key_exists('meta_key', $basis)) 
                {  
                    $args['meta_key'] = $basis['meta_key'];
                }
            }
            $term_current =  $this->primary_category; //(is_archive()) ?  (new \Timber\Term( get_queried_object())) : $this->primary_category();
            if ($term_current)
            {
                $args['tax_query'][] =
                [
                    'taxonomy' => $term_current->taxonomy,
                    'field' => 'slug',
                    'terms' => array($term_current->slug),
                    'include_children' => 1,
                ];
                $args['tax_query']['relation'] = "AND";
            }

            // limit to x days if set
            if($basis && array_key_exists('max_days', $basis) && is_numeric($basis['max_days'])) 
            {
                $after = "-" . $basis['max_days'] . " days";
                $args['date_query'] = array
                (
                     'after' => $after,
                     'column' => 'post_date',
                );
            }
            // overwrite max days if a year is in the parameters
            if (isset($_GET['selected_year']) && is_numeric($_GET['selected_year']) && $_GET['selected_year'] > 2000 && $_GET['selected_year'] < 2300)
            {

                $args['date_query'] = array
                (
                    'relation' => 'OR',
                    array('year' => $_GET['selected_year'])
                );
            }


            $this->_listingsPagePostsArgs = (count($args) > 0) ? $args : null; 

        }

        return $this->_listingsPagePostsArgs;

    }

    /**
     * @return $this
     */
    public function top_parent()
    {
        if(!$this->_top_parent)
        {
            $parents = get_post_ancestors( $this->ID );
            $this->_top_parent = ($parents) ? new static($parents[count($parents)-1]) : $this;
        }

        return $this->_top_parent;
    }


    public function half_siblings()
    {
        if(!$this->_half_siblings)
        {
            $this->_half_siblings = null;
            $parent = wp_get_post_parent_id($this->ID);

            if (!$parent || $parent == 0)
            {
                $parent = $this->ID;
            }

            $rtn = [];
            if (!is_null ($parent) ):

                $args = array
                (
                    'post_status' => 'publish',
                    'post_parent' => $parent,
                    'post_type'     => 'page',
                    'orderby' => 'menu_order',
                    'order' => 'ASC',
                    'meta_query'    => array
                    (
                        'relation' => 'AND',
                        [
                            'relation' => 'OR',
                            [
                                'key' => '_yoast_wpseo_meta-robots-noindex',
                                'compare' => 'NOT EXISTS',
                            ],
                            [
                                'key' => '_yoast_wpseo_meta-robots-noindex',
                                'value' => '1',
                                'compare' => '!=',
                            ]
                        ],
                        [
                            //'relation' => 'OR',
                            [
                                'key' => '_wp_page_template',
                                'value'    => 'page-go-to-first-child.php"',
                                'compare'  => '!=',
                            ],
                            /*[
                                'key' => '_wp_page_template',
                                'compare'  => 'NOT EXISTS'
                            ],*/

                        ],
                        

                    )
                );

                $posts = \Timber::get_posts( $args, static::class );


                if ($posts):
                    $this->_half_siblings = $posts;
                endif;
            endif;
        }

        return $this->_half_siblings;
    }


    public function welcome_ctas_converted()
    {
        if(!$this->_welcome_ctas_converted)
        {
            $this->_welcome_ctas_converted = $this->convertLinkList('welcome_ctas');
        }

        return $this->_welcome_ctas_converted;
    }

    public function secondary_ctas_converted()
    {
        if(!$this->_secondary_ctas_converted)
        {
            $this->_secondary_ctas_converted = $this->convertLinkList('secondary_ctas');
        }

        return $this->_secondary_ctas_converted;
    }

    public function convertLinkList($field)
    {

        $links = get_field($field, $this->ID);

        if ($links && is_array($links)):

            $rtn = [];

            foreach($links as $link)
            {
                $rtn[] = $link;
            }

            return $rtn;
        endif;

        return null;
    }


    public function alt_posts()
    {
        if(!$this->_alt_posts)
        {
            $this->_alt_posts = null;
            $posts_info = $this->listingsPagePostsUsefulData();
            $additional_args = $this->listingsPagePostsArgs();

            if ($posts_info || $additional_args):

                $args = 
                [
                    'post_status' => 'publish',
                    'posts_per_page' => $this->counts_posts_per_page,
                    'paged' => $this->counts_current_page,
                    'post__not_in' => [$this->ID],
                ];

                foreach ($additional_args as $key => $value):
                    $args[$key] = $value;
                endforeach;

                if (!$posts_info)
                {

                    $args['post_type'] = 'any';
                    $this->_alt_posts = \Timber::get_posts($args, static::class);
                }

                else
                {
                    $this->_alt_posts = \Timber::get_posts($args, $this->classNameForTimberFromString($posts_info['class_name']));
                }
               


            endif;
        }

        return $this->_alt_posts;
    }

    public function counts_max_results()
    {
        if(!$this->_counts_max_results):
            $additional_args = $this->listingsPagePostsArgs();
            $posts_info = $this->listingsPagePostsUsefulData();
            if ($additional_args):
                $args = 
                [
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                    'paged' => 1,
                    'post__not_in' => [$this->ID]
                ];

                if (!$posts_info)
                {
                    $args['post_type'] = 'any';
                }
                foreach ($additional_args as $key => $value):
                    $args[$key] = $value;
                endforeach;

                $this->_counts_max_results = count(get_posts($args));
            endif;
        endif;

        return $this->_counts_max_results;
    }

    public function counts_max_pages()
    {
        if(!$this->_counts_max_pages):
            $this->_counts_max_pages = 1;

            if ($this->counts_max_results && $this->counts_posts_per_page):
                $this->_counts_max_pages = ceil($this->counts_max_results / $this->counts_posts_per_page);
            endif;
        endif;
        return $this->_counts_max_pages;
    }

    public function counts_found_results()
    {
        if(!$this->_counts_found_results):
            $this->_counts_found_results = null;

            if ($this->_alt_posts):
                $this->_counts_found_results = count($this->_alt_posts);
            endif;
        endif;

        return $this->_counts_found_results;
    }

    public function counts_posts_per_page()
    {
        if(!$this->_counts_posts_per_page):

            if ($this->query_all_acf("show_all_matching_results"))
            {
                $this->_counts_posts_per_page = -1;
            }
            elseif ($this->listingsPagePostsUsefulData() && is_array($this->listingsPagePostsUsefulData()) && array_key_exists('posts_per_page', $this->listingsPagePostsUsefulData()))
            {
                $this->_counts_posts_per_page = $this->listingsPagePostsUsefulData()['posts_per_page'];
            }   
            else
            {
                 $this->_counts_posts_per_page = 12;
            }
        

        endif;
        return $this->_counts_posts_per_page;
    }

    public function counts_current_page()
    {
        if(!$this->_counts_current_page):
            $this->_counts_current_page = (isset($this->paged)) ? $this->paged : ( ( isset($_GET['paged']) ) ? $_GET['paged'] : $this->pageNumberFromURL  );

            if ($this->_counts_current_page == 0 || is_null($this->_counts_current_page)):
                $this->_counts_current_page = 1;
            endif;

        endif;

        return $this->_counts_current_page;
    }

    public function counts_start_count()
    {
        if(!$this->_counts_start_count):

            $this->_counts_start_count = (($this->counts_current_page - 1) * $this->counts_posts_per_page) + 1;
            if ($this->_counts_start_count < 1):
                $this->_counts_start_count = 1;
            endif;
        endif;

        return $this->_counts_start_count;
    }

    public function counts_end_count()
    {
        if(!$this->_counts_end_count):

            if ($this->counts_posts_per_page == -1)
            {
                $this->_counts_end_count = $this->counts_max_results;
            }
            else
            {
                $this->_counts_end_count = ($this->counts_current_page * $this->counts_posts_per_page);

                if ($this->counts_max_results <  $this->_counts_end_count):
                    $this->_counts_end_count = $this->counts_max_results;
                endif;
            }

        endif;

        return $this->_counts_end_count;
    }

    public function paginationSimulation()
    {
        if(!$this->_paginationSimulation):
            $this->_paginationSimulation = null;
            $alias = $this->listingsPagePostsArgs();


            if ($alias):

                $prev = null;
                if ($this->counts_current_page > 1):
                    if ($this->counts_current_page > 2):
                        $prev =
                            [
                                'link' => ( $this->pageURLBasic . "page/". ($this->counts_current_page - 1) . "/" ),
                                'class' => 'page-numbers prev'
                            ];
                    endif;

                    if ($this->counts_current_page == 2):
                        $prev =
                            [
                                'link' => ( $this->pageURLBasic ),
                                'class' => 'page-numbers prev'
                            ];
                    endif;
                endif;

                $next = null;
                if ($this->counts_max_pages > 1 && $this->counts_current_page != $this->counts_max_pages):
                    $next =
                        [
                            'link' => ( $this->pageURLBasic . "page/". ($this->counts_current_page + 1) . "/" ),
                            'class' => 'page-numbers next'
                        ];
                endif;

                $pages = null;

                if ($this->counts_max_pages > 1):

                    //dd($this->pageURLBasic);
                    $pages = [];

                    if ($this->counts_max_pages > 7)
                    {

                        $start_at = $this->counts_current_page - 2; 
                        $highest = $this->counts_max_pages - 4;
                        $include_dots = null;
                        if ($start_at < 1)
                        {
                            while ($start_at < 1)
                            {
                                $start_at++;
                            }
                            $include_dots = 'end';
                        }
                        if ($start_at > $highest)
                        {
                            while ($start_at > $highest)
                            {
                                $start_at--;
                            }
                            $include_dots = 'start';
                        }
                        $show_to = $start_at + 4;

                        for($i = 1; $i <= $this->counts_max_pages; $i++):
                            $active = ($i == $this->counts_current_page) ? true : false;
                            $included = ($i >= $start_at && $i <= $show_to) ? ' included' : false;
                            $active_class = ($active) ? " current" : "";
                            $active_link = $this->incrementPageURL($this->link, $i); //($i == 1) ? $this->pageURLBasic : ( $this->pageURLBasic . "page/". $i . "/" );

                            if ( ( $i == 1 && $include_dots == 'start' ) || ( $i == $this->counts_max_pages && $include_dots == 'end' ) )
                            {
                                $included = true;
                            }
                            if ( $i == $this->counts_max_pages && $include_dots == 'end' ) 
                            {
                                $included = true;
                                $pages[] =
                                    [
                                        'class' => ('dots'),
                                        'title' => 'dots',
                                        'text' => '...',
                                        'name' => 'dots',
                                        'current' => false,
                                        'link' => false,
                                        'included' => true,
                                    ];
                            }
                            $pages[] =
                                [
                                    'class' => ('page number page numbers' . $included . $active_class),
                                    'title' => $i,
                                    'text' => $i,
                                    'name' => $i,
                                    'current' => $active,
                                    'link' => $active_link,
                                    'included' => $included,
                                ];
                            if ($i == 1 && $include_dots == 'start')
                            {
                                $pages[] =
                                    [
                                        'class' => ('dots'),
                                        'title' => 'dots',
                                        'text' => '...',
                                        'name' => 'dots',
                                        'current' => false,
                                        'link' => false,
                                        'included' => true,
                                    ];
                            }

                        endfor;
                    }

                    else
                    {

                        for($i = 1; $i <= $this->counts_max_pages; $i++):
                            $active = ($i == $this->counts_current_page) ? true : false;
                            $active_class = ($active) ? " current" : "";
                            $active_link = $this->incrementPageURL($this->link, $i); // ($i == 1) ? $this->pageURLBasic : ( $this->pageURLBasic . "page/". $i . "/" );
                            //dd($active_link);
                            $pages[] =
                                [
                                    'class' => ('page number page numbers included' . $active_class),
                                    'title' => $i,
                                    'text' => $i,
                                    'name' => $i,
                                    'current' => $active,
                                    'link' => $active_link,
                                    'included' => true
                                ];
                        endfor;
                    }

                endif;

                $this->_paginationSimulation = 
                [
                    'current' => $this->counts_current_page,
                    'total' => $this->counts_max_pages,
                    'pages' => $pages,
                    'prev' => $prev,
                    'next' => $next
                ];

            endif;


            return $this->_paginationSimulation;
        endif;

    }

    public function archives_list()
    {

        if(!$this->_archives_list)
        {
            $this->_archives_list = null;

            $posts_info = $this->listingsPagePostsUsefulData();
            $additional_args = $this->listingsPagePostsArgs();


            if ($posts_info):

                $args = [
                    'type'            => 'yearly',
                    'limit'           => '10',
                    'show_post_count' => false,
                    'echo'            => 1,
                    'order'           => $posts_info['order'],
                    'orderby'         => $posts_info['orderby'],
                    'post_type'     => $posts_info['post_type']
                ];

                if ($additional_args && is_array($additional_args))
                {
                    foreach($additional_args as $k=>$v)
                    {
                        $args[$k] = $v;
                    }
                }


                $this->_archives_list = new \TimberArchives($args);
            endif;
        }

        return $this->_archives_list;


    }


    // if all else fails, read the URL
    public function pageNumberFromURL()
    {
        if(!$this->_pageNumberFromURL):
            $this->_pageNumberFromURL = 1;

            $name = "page";
            $strURL = $_SERVER['REQUEST_URI'];
            $arrVals = explode("/",$strURL);

            $found = 0;
            foreach ($arrVals as $index => $value):
                if($value == $name):
                    $found = $index;
                endif;
            endforeach;


            $place = $found + 1;
            if (is_numeric($arrVals[$place])):
                $this->_pageNumberFromURL = $arrVals[$place];
            endif;

            return $this->_pageNumberFromURL;

        endif;
    }

    // if all else fails, read the URL
    public function pageURLBasic()
    {
        if(!$this->_pageURLBasic):
            $this->_pageURLBasic = "/";

            $name = "page";

            $strURL = $_SERVER['REQUEST_URI'];
            $arrVals = explode("/",$strURL);

            if (in_array($name, $arrVals)):

                $count = array_search($name, $arrVals);
                $arrVals2 = array_slice($arrVals, 0, $count, true);

                foreach($arrVals2 as $now):
                    if ($now != "")
                        $this->_pageURLBasic .= $now ."/";
                endforeach;

            else:
                foreach($arrVals as $now):
                    if ($now != "")
                        $this->_pageURLBasic .= $now ."/";
                endforeach;
            endif;


            return $this->_pageURLBasic;

        endif;
    }
}