<?php

namespace Hailstone\Core\Facades;

use Blast\Facades\AbstractFacade;
use Hailstone\Core\Image;

class ImageFacade extends AbstractFacade
{
    /**
     * @return mixed
     */
    protected static function accessor()
    {
        return Image::class;
    }
}