<?php

namespace Hailstone\Core\Facades;

use Blast\Facades\AbstractFacade;
use Hailstone\Core\Page;

class PageFacade extends AbstractFacade
{
    /**
     * @return mixed
     */
    protected static function accessor()
    {
        return Page::class;
    }
}