<?php

namespace Hailstone\Core\Facades;

use Blast\Facades\AbstractFacade;
use Hailstone\Core\Career;

class CareerFacade extends AbstractFacade
{
    /**
     * @return mixed
     */
    protected static function accessor()
    {
        return Career::class;
    }
}