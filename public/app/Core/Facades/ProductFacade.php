<?php

namespace Hailstone\Core\Facades;

use Blast\Facades\AbstractFacade;
use Hailstone\Core\Product;

class ProductFacade extends AbstractFacade
{
    /**
     * @return mixed
     */
    protected static function accessor()
    {
        return Product::class;
    }
}