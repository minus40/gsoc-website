<?php

namespace Hailstone\Core\Facades;

use Blast\Facades\AbstractFacade;
use Hailstone\Core\Location;

class LocationFacade extends AbstractFacade
{
    /**
     * @return mixed
     */
    protected static function accessor()
    {
        return Location::class;
    }
}