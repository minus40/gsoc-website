<?php

namespace Hailstone\Core;

use Hailstone\Core\Post;
use Hailstone\Core\Traits\HasCarousel;

class People extends Post
{

    public $postType = 'person';
    public $usefulData =
    [
        'post_type' => 'person',
        'class_name' => 'People',
        'posts_page' => 'Ombudsman Commissioners',
        'main_taxonomy' => 'person_categorie',
        'orderby' => 'menu_order',
        'order' => 'ASC',
    ];
    var $_categories;



    /**
     * @return array
     */
    public function categories()
    {
        if(!$this->_categories)
        {
            $categories = $this->terms('person_categories');
            if (is_array($categories) && count($categories)) {
                $this->_categories = $categories;
            }
        }
        return $this->_categories;
    }


}