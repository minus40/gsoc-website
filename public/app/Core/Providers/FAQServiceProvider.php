<?php

namespace Hailstone\Core\Providers;

use Hailstone\Core\FAQ;
use Hailstone\Core\Support\ServiceProvider;

class FAQServiceProvider extends ServiceProvider
{
    /**
     *
     */
    public function register()
    {
        $this->app->bind(FAQ::class);

        $this->registerPostType();
        $this->registerTaxonomies();
        $this->registerFields();
    }

    /**
     *
     */
    private function registerPostType()
    {
        \add_action( 'init', function()
        {
            \register_post_type( 'faq',
                [
                    'labels' => [
                        'name' => __( 'FAQs' ),
                        'singular_name' => __( 'FAQ' ),
                        'add_new' => "Add FAQ",
                        'add_new_item' => "Add FAQ",
                        'all_items' => 'All FAQs',
                        'view_item' => "View FAQ",
                        'edit_item' => "Edit FAQ",
                        'new_item' => "New FAQ",
                        'featured_image' => 'Featured Image'
                    ],
                    'menu_position' => 93,
                    'menu_icon' => 'dashicons-editor-spellcheck',
                    'supports' => array('title', 'revisions', 'thumbnail', 'author', 'page-attributes'),
                    'hierarchical' => false,
                    'exclude_from_search' => false,
                    'public' => true,
                    'has_archive' => false,
                    'rewrite' => [
                        'slug' => 'about-gsoc/faqs'
                    ],
                    'show_in_menu' => true,
                    'show_ui' => true,
                    'capability_type' => array('faq', 'faqs'),
                    'map_meta_cap' => true,
                ]
            );
        });
    }

    /**
     *
     */
    private function registerTaxonomies()
    {
        \register_taxonomy(
            'faq_category',
            'faq',
            [
                'labels' => [
                    'name' => 'FAQ Categories',
                    'add_new_item' => 'Add New Category',
                    'new_item_name' => "New Category"
                ],
                'show_ui' => true,
                'show_tagcloud' => false,
                'hierarchical' => true,
            ]
        );
    }

    private function registerFields()
    {
        \acf_add_local_field_group([
            'key' => 'acf-faqs',
            'title' => 'FAQ Information',
            'fields' => [

                ### Question
                [
                    'key' => 'acf-faqs-question',
                    'label' => 'Question',
                    'name' => 'question',
                    'type' => 'text',
                    'instructions' => 'As the user would read it...',
                    'maxlength' => 92,
                ],

                ### Short Answer
                [
                    'key' => 'acf-faqs-answer',
                    'label' => 'Short Answer',
                    'name' => 'answer',
                    'type' => 'textarea',
                    'instructions' => 'Limited to 480 characters',
                    'required' => 1,
                    'maxlength' => 480,
                    'rows' => 3,
                ],

                ### Video
                [
                    'key' => 'acf-faqs-videos',
                    'label' => 'Related Video',
                    'name' => 'video',
                    'type' => 'post_object',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'post_type' => ['video'],
                    'return_format' => 'object',
                    'ui' => 1,
                ],

                ### Full Post
                [
                    'key' => 'acf-faqs-show-more',
                    'label' => 'Show More',
                    'name' => 'show_more',
                    'type' => 'radio',
                    'instructions' => 'Select if you require a longer answer. This will take the form of a full post and allow images, video, etc.',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'choices' => array (
                        'Yes' => 'Yes',
                        'No' => 'No',
                    ),
                    'allow_null' => 0,
                    'other_choice' => 0,
                    'save_other_choice' => 0,
                    'default_value' => '',
                    'layout' => 'vertical',
                ]
            ],
            'location' => [
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'faq',
                    ],
                ],
            ],
            'menu_order' => 1,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
        ]);
    }
}