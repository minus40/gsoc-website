<?php

namespace Hailstone\Core\Providers;

use Hailstone\Core\Option;
use Hailstone\Core\Support\ServiceProvider;

class OptionsServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(Option::class);

        $this->addOptionsPage();
        $this->addFields();
    }

    /**
     *
     */
    private function addOptionsPage()
    {
        \acf_add_options_page(array(
            'page_title'    => 'Organisation Details',
            'menu_title'    => 'Organisation Details',
            'menu_slug'     => 'sitewide-organisational-details',
            'capability'    => 'edit_posts',
            'redirect'  => false
        ));
    }

    /**
     *
     */
    private function addFields()
    {
        \acf_add_local_field_group(array (
            'key' => 'acf-organisation-details',
            'title' => 'Organisation Details',
            'fields' => [
                [
                    'key' => 'acf-organisation-details-location',
                    'label' => 'Location',
                    'name' => 'location',
                    'type' => 'relationship',
                    'instructions' => 'Please choose your primary location to display in the site footer.',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'post_type' => [
                        0 => 'location',
                    ],
                    'taxonomy' => [],
                    'filters' => [
                        0 => 'search',
                    ],
                    'elements' => '',
                    'min' => 1,
                    'max' => 1,
                    'return_format' => 'object',
                ],
                array(
                    'key' => 'acf-emergency-details-two-toggle',
                    'label' => 'Enable Sitewide Notification?',
                    'name' => 'emergency_sitewide',
                    'type' => 'true_false',
                    'instructions' => 'If you choose <b>Yes</b>, the associated message will be displayed on most page templates.',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'message' => '',
                    'default_value' => 0,
                    'ui' => 1,
                    'ui_on_text' => 'Yes',
                    'ui_off_text' => 'No',
                ),

                array(
                    'key' => 'acf-emergency-details-two-content',
                    'label' => 'Sitewide Emergency Message',
                    'name' => 'emergency_sitewide_content',
                    'type' => 'wysiwyg',
                    'instructions' => 'This is the content that will display on most pages when Sitewide Notification is enabled.',
                    'required' => 0,
                    'conditional_logic' => array(
                        array(
                            array(
                                'field' => 'acf-emergency-details-two-toggle',
                                'operator' => '==',
                                'value' => '1',
                            ),
                        ),
                    ),
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'tabs' => 'all',
                    'toolbar' => 'full',
                    'media_upload' => 0,
                    'delay' => 0,
                ),

            ],
            'location' => [
                [
                    [
                        'param' => 'options_page',
                        'operator' => '==',
                        'value' => 'sitewide-organisational-details',
                    ],
                ],
            ],
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));
    }
}