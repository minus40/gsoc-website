<?php

namespace Hailstone\Core\Providers;

use Hailstone\Core\Support\ServiceProvider;

class FeatureCarouselServiceProvider extends ServiceProvider
{
    /**
     *
     */
    public function register()
    {
        if (function_exists('acf_add_local_field_group')) {
            $this->registerFeatureCarousel();
        }

    }

    /**
     *
     */
    private function registerFeatureCarousel()
    {
        \acf_add_local_field_group([
            'key' => 'acf-feature-carousel',
            'title' => 'Feature Image, Video or Carousel',
            'fields' => [
                [
                    'key' => 'feature-carousel',
                    'label' => 'Feature Type',
                    'name' => 'feature-carousel',
                    'type' => 'flexible_content',
                    'instructions' => 'Use this tool to add a full-page image, video or slider to the top of this page. You can only have one feature carousel per page.',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'button_label' => 'Add Carousel',
                    'min' => 0,
                    'max' => 1,
                    'layouts' => [
                        // Single Background Video or Image (With optional multiple foreground slides)
                        $this->registerSingleVideoOrImage(),
                        // Multiple Background Videos or Images (With optional multiple foreground slides)
                        $this->registerMultipleBackgroundVideosAndImages(),
                        // Multiple Background Videos or Images (With single foreground slide)
                        $this->registerMultipleBackgroundVideosAndImageWithSingleForeground(),
                    ],
                ],
            ],
            'location' => [
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'page',
                    ],
                    [
                        'param' => 'page_template',
                        'operator' => '!=',
                        'value' => 'front-page.php',
                    ],

                ],
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'article',
                    ],
                ],
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'career',
                    ],
                ],
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'case-study',
                    ],
                ],
//                [
//                    [
//                        'param' => 'post_type',
//                        'operator' => '==',
//                        'value' => 'chart',
//                    ],
//                ],
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'event',
                    ],
                ],
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'faq',
                    ],
                ],
//                [
//                    [
//                        'param' => 'post_type',
//                        'operator' => '==',
//                        'value' => 'location',
//                    ],
//                ],
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'person',
                    ],
                ],
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'product',
                    ],
                ],
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'report',
                    ],
                ],
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'service',
                    ],
                ],
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'statistic',
                    ],
                ],

//                [
//                    [
//                        'param' => 'post_type',
//                        'operator' => '==',
//                        'value' => 'video',
//                    ],
//                ],
            ],
            'menu_order' => 2,
            'position' => 'acf_after_title',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
        ]);
    }

    /**
     * @return array
     */
    private function registerSingleVideoOrImage()
    {
        return [
            'key' => 'single-background',
            'name' => 'single-background',
            'label' => 'Fixed Background Image (text moves)',
            'display' => 'block',
            'sub_fields' => [
                [
                    'key' => 'single-background-image',
                    'label' => 'Background Image',
                    'name' => 'image',
                    'type' => 'image',
                    'instructions' => 'Your background image. It must be at least 800 pixels wide.',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'preview_size' => 'thumbnail',
                    'min_width' => '800',
                    'mime_types' => 'png, jpg, jpeg, gif',
                    'return_format' => 'array',
                    'library' => 'all',
                ],
                [
                    'key' => 'single-background-slides',
                    'label' => 'Text / Multimedia Slides',
                    'name' => 'slides',
                    'type' => 'repeater',
                    'instructions' => 'You can add up to 7 messages, including CTA links with each. These will appear above the video background and will automatically carousel. If you choose both an internal link and an external link, the internal link will display.',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'collapsed' => '',
                    'min' => '',
                    'max' => 7,
                    'button_label' => 'Add Slide',
                    'layout' => 'row',
                    'sub_fields' => [
                        [
                            'key' => 'single-background-slides-slide-heading',
                            'label' => 'Slide Heading',
                            'name' => 'heading',
                            'type' => 'text',
                            'instructions' => 'Limited to 48 characters.',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'maxlength' => 48,
                            'readonly' => 0,
                            'disabled' => 0,
                        ],
                        [
                            'key' => 'single-background-slides-slide-subheading',
                            'label' => 'Slide Subheading',
                            'name' => 'subheading',
                            'type' => 'text',
                            'instructions' => 'Limited to 128 characters.',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'new_lines' => 'wpautop',
                            'readonly' => 0,
                            'disabled' => 0,
                        ],
                        [
                            'key' => 'single-background-slides-slide-ctas',
                            'label' => 'Calls to Action',
                            'name' => 'actions',
                            'type' => 'repeater',
                            'instructions' => 'You can add up to 3 CTA links per slide. If you choose both an internal and external link, the internal one will be used.',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'collapsed' => '',
                            'min' => '',
                            'max' => 3,
                            'button_label' => 'Add CTA',
                            'layout' => 'table',
                            'sub_fields' => [
                                [
                                    'key' => 'single-background-slides-slide-ctas-link-internal',
                                    'label' => 'Link (Internal)',
                                    'name' => 'internal_link',
                                    'type' => 'page_link',
                                    'allow_null' => 0,
                                    'multiple' => 0,
                                ],
                                [
                                    'key' => 'single-background-slides-slide-ctas-link-external',
                                    'label' => 'Link (External)',
                                    'name' => 'external_link',
                                    'type' => 'url',
                                    'default_value' => '',
                                    'placeholder' => '',
                                ],
                                [
                                    'key' => 'single-background-slides-slide-ctas-button-text',
                                    'label' => 'Button Text',
                                    'name' => 'button_text',
                                    'type' => 'text',
                                    'instructions' => '',
                                    'required' => 0,
                                    'conditional_logic' => 0,
                                    'maxlength' => 24,
                                    'readonly' => 0,
                                    'disabled' => 0,
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    private function registerMultipleBackgroundVideosAndImages()
    {
        return [
            'key' => 'multi-slide',
            'name' => 'multi-slide',
            'label' => 'Multi Slide (everything moves)',
            'display' => 'block',
            'sub_fields' => [
                [
                    'key' => 'multi-slide-slides',
                    'label' => 'Slides',
                    'name' => 'slides',
                    'type' => 'repeater',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'collapsed' => '',
                    'min' => '',
                    'max' => 7,
                    'layout' => 'row',
                    'button_label' => 'Add Slide',
                    'sub_fields' => [
                        [
                            'key' => 'multi-slide-bg-image',
                            'label' => 'Background Image',
                            'name' => 'image',
                            'type' => 'image',
                            'instructions' => 'Your background image.  It must be at least 800 pixels wide.',
                            'required' => 1,
                            'conditional_logic' => 0,
                            'preview_size' => 'thumbnail',
                            'min_width' => '800',
                            'mime_types' => 'png, jpg, jpeg, gif',
                            'return_format' => 'array',
                            'library' => 'all',
                        ],

                        [
                            'key' => 'multi-slide-heading',
                            'label' => 'Slide Heading',
                            'name' => 'heading',
                            'type' => 'text',
                            'instructions' => 'Limited to 48 characters.',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'maxlength' => 48,
                            'readonly' => 0,
                            'disabled' => 0,
                        ],
                        [
                            'key' => 'multi-slide-subheading',
                            'label' => 'Slide Subheading',
                            'name' => 'subheading',
                            'type' => 'textarea',
                            'instructions' => 'Limited to 256 characters.',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => [
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ],
                            'default_value' => '',
                            'placeholder' => '',
                            'maxlength' => '256',
                            'rows' => '',
                            'new_lines' => 'wpautop',
                            'readonly' => 0,
                            'disabled' => 0,
                        ],

                        [
                            'key' => 'multi-slide-actions',
                            'label' => 'Calls to Action',
                            'name' => 'actions',
                            'type' => 'repeater',
                            'instructions' => 'You can add up to 3 CTA links per slide. If you choose both an internal and external link, the internal one will be used.',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => [
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ],
                            'collapsed' => '',
                            'min' => '',
                            'max' => 3,
                            'button_label' => 'Add CTA',
                            'layout' => 'table',
                            'sub_fields' => [
                                [
                                    'key' => 'multi-slide-internal-link',
                                    'label' => 'Internal Link',
                                    'name' => 'internal_link',
                                    'type' => 'page_link',
                                    'instructions' => '',
                                    'required' => 0,
                                    'conditional_logic' => 0,
                                    'post_type' => [],
                                    'taxonomy' => [],
                                    'allow_null' => 0,
                                    'multiple' => 0,
                                ],
                                [
                                    'key' => 'multi-slide-external-link',
                                    'label' => 'External Link',
                                    'name' => 'external_link',
                                    'type' => 'url',
                                    'instructions' => '',
                                    'required' => 0,
                                    'conditional_logic' => 0,
                                    'wrapper' => [
                                        'width' => '',
                                        'class' => '',
                                        'id' => '',
                                    ],
                                    'default_value' => '',
                                    'placeholder' => '',
                                ],
                                [
                                    'key' => 'multi-slide-button-text',
                                    'label' => 'Button Copy',
                                    'name' => 'button_text',
                                    'type' => 'text',
                                    'instructions' => '',
                                    'required' => 0,
                                    'conditional_logic' => 0,
                                    'wrapper' => [
                                        'width' => '',
                                        'class' => '',
                                        'id' => '',
                                    ],
                                    'default_value' => '',
                                    'placeholder' => '',
                                    'prepend' => '',
                                    'append' => '',
                                    'maxlength' => 24,
                                    'readonly' => 0,
                                    'disabled' => 0,
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    private function registerMultipleBackgroundVideosAndImageWithSingleForeground()
    {
        return [
            'key' => 'multi-background',
            'name' => 'multi-background',
            'label' => 'Multi Background (fixed text)',
            'display' => 'block',
            'sub_fields' => [
                [
                    'key' => 'multi-background-heading',
                    'label' => 'Slide Heading',
                    'name' => 'heading',
                    'type' => 'text',
                    'instructions' => 'Limited to 48 characters.',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'maxlength' => 48,
                    'readonly' => 0,
                    'disabled' => 0,
                ],
                [
                    'key' => 'multi-background-subheading',
                    'label' => 'Slide Subheading',
                    'name' => 'subheading',
                    'type' => 'text',
                    'instructions' => 'Limited to 128 characters.',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'maxlength' => '128',
                    'rows' => '',
                    'new_lines' => 'wpautop',
                    'readonly' => 0,
                    'disabled' => 0,
                ],

                [
                    'key' => 'multi-background-actions',
                    'label' => 'Calls to Action',
                    'name' => 'actions',
                    'type' => 'repeater',
                    'instructions' => 'You can add up to 3 CTA links to the slide. If you choose both an internal and external link, the internal one will be used.',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'collapsed' => '',
                    'min' => '',
                    'max' => 3,
                    'button_label' => 'Add CTA',
                    'layout' => 'table',
                    'sub_fields' => [
                        [
                            'key' => 'multi-background-internal-link',
                            'label' => 'Internal Link',
                            'name' => 'internal_link',
                            'type' => 'page_link',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'post_type' => [],
                            'taxonomy' => [],
                            'allow_null' => 0,
                            'multiple' => 0,
                        ],
                        [
                            'key' => 'multi-background-external-link',
                            'label' => 'External Link',
                            'name' => 'external_link',
                            'type' => 'url',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'default_value' => '',
                            'placeholder' => '',
                        ],
                        [
                            'key' => 'multi-background-button-text',
                            'label' => 'Button Text',
                            'name' => 'button_text',
                            'type' => 'text',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'maxlength' => 24,
                            'readonly' => 0,
                            'disabled' => 0,
                        ],
                    ],
                ],
                [
                    'key' => 'multi-background-slides',
                    'label' => 'Slides',
                    'name' => 'slides',
                    'type' => 'repeater',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => 0,

                    'min' => 1,
                    'max' => 7,
                    'layout' => 'row',
                    'button_label' => 'Add Slide',
                    'sub_fields' => [
                        [
                            'key' => 'multi-background-slides-image',
                            'label' => 'Background Image',
                            'name' => 'image',
                            'type' => 'image',
                            'instructions' => 'Your background image. It must be at least 800 pixels wide.',
                            'required' => 1,
                            'conditional_logic' => 0,
                            'return_format' => 'array',
                            'preview_size' => 'thumbnail',
                            'library' => 'all',
                            'min_width' => '800',
                            'mime_types' => 'jpg,jpeg,png,gif',
                        ],
                    ],
                ],
            ],
        ];
    }
}