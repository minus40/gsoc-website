<?php

namespace Hailstone\Core\Providers;

use Hailstone\Core\Support\ServiceProvider;

class MainContentServiceProvider extends ServiceProvider
{
    /**
     *
     */
    public function register()
    {
        $this->removeEditorSupport();
        $this->removeCommentSupport();
        $this->addSEODescription();
        $this->registerMainContentBlocks();
        $this->registerSupportingContentBlocks();
        $this->addTemplateVariations();
    }

    /**
     * Registers all of the main content blocks
     *
     * @return void
     */
    private function registerMainContentBlocks()
    {
        acf_add_local_field_group([
            'key' => 'acf-main-content-blocks',
            'title' => 'Main Content Blocks',
            'fields' => [
                [
                    'key' => 'acf-main-content-blocks-message',
                    'label' => 'What Is This?',
                    'name' => 'main-content-blocks-message',
                    'type' => 'message',
                    'message' => "The main content area of the page is made up of a series of Content Blocks (Sections). You can add as many content sections as you need - note that you can re-order them at any time.",
                    'new_lines' => 'wpautop',
                ],

                [
                    'key' => 'acf-main-content-blocks-layouts',
                    'label' => 'Content Layouts',
                    'name' => 'content_layouts',
                    'type' => 'flexible_content',
                    'button_label' => 'Add a Content Layout',
                    'layouts' => [
                        $this->registerWysiwygSection(),
                        $this->registerMultimediaComboSection(),
                        $this->registerFeaturedPostsSection(),
                        $this->registerDownloadsSection(),
                        $this->registerDownloadGroupsSection(),
                        $this->registerContactFormSection(),
                        $this->registerFAQSection(),
                        $this->registerLocationSection(),
                        $this->registerChartSection(),


                        $this->registerFeaturedNewsSection(),
                        $this->registerPageLinkSection(),
                        $this->registerGallerySection(),
                        $this->registerGalleryGroupSection(),
                        $this->registerFullSizeImageSection(),
                        $this->registerVideoSection(),


                    ],
                ],
            ],
            'location' => $this->allPostTypeLocations(),
            'menu_order' => 3,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
        ]);
    }


    /**
     * Registers all of the supporting content blocks
     *
     * @return void
     */
    private function registerSupportingContentBlocks()
    {
        acf_add_local_field_group([
            'key' => 'acf-supporting-content-blocks',
            'title' => 'Supporting Content Sections',
            'fields' => [
                [
                    'key' => 'acf-supporting-content-blocks-message',
                    'label' => 'What Is This?',
                    'name' => 'message',
                    'type' => 'message',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'message' => "Supporting content appears on the right-hand side (desktop) or beneath the main content (mobile), and consists of a series of Content Blocks (Sections). You can add as many supporting sections as you need - note that you can re-order them at any time.",
                    'new_lines' => 'wpautop',
                    'esc_html' => 0,
                ],
                [
                    'key' => 'acf-supporting-content-blocks-layouts',
                    'label' => 'Content Sections',
                    'name' => 'supporting_content_layouts',
                    'type' => 'flexible_content',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => [
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ],
                    'button_label' => 'Add a Supporting Content Section',
                    'min' => '',
                    'max' => '',
                    'layouts' => [
                        $this->registerSupportingGallery(),
                        $this->registerSupportingGalleryGroup(),
                        $this->registerSupportingVideoSection(),
                        $this->registerSupportingDownloadsSection(),
                        $this->registerSupportingDownloadGroupsSection(),
                        $this->registerSupportingLinks(),
                    ],
                ]
            ],
            'location' => $this->allPostTypeLocations(),
            'menu_order' => 4,
            'position' => 'normal',
        ]);
    }


    /**
     * @return array
     */
    private function registerPageLinkSection()
    {

        $lcl = 'page-link';

        return [
            'key' => 'acf-main-content-blocks-layouts-pagelinks',
            'name' => 'pagelinks_layout',
            'label' => 'Page Listings Secton (Deprecated)',
            'display' => 'block',
            'sub_fields' => [

                $this->deprecatedMessage($lcl),

                # TITLE
                [
                    'key' => 'acf-main-content-blocks-layouts-pagelinks-title',
                    'label' => 'Section Title',
                    'name' => 'title',
                    'type' => 'text',
                    'instructions' => 'Maximum of 64 characters',
                    'placeholder' => 'In this section....',
                    'maxlength' => 64,
                ],

                ### INTRODUCTION
                [
                    'key' => 'acf-main-content-blocks-layouts-pagelinks-introduction',
                    'label' => 'Section Introduction',
                    'name' => 'introduction',
                    'type' => 'textarea',
                    'instructions' => 'Will appear directly beneath any title.',
                    'maxlength' => 1000,
                    'rows' => 3,
                    'new_lines' => 'br',
                ],

                # PAGE LINKS
                [
                    'key' => 'acf-main-content-blocks-layouts-pagelinks-articles',
                    'label' => 'Select Pages to List',
                    'name' => 'articles',
                    'type' => 'relationship',
                    'instructions' => 'Choose which pages should be listed and linked from this page.',
                    'required' => 3,
                    'taxonomy' => [],
                    'filters' => ['search'],
                    'post_type' => [
                        0 => 'page',
                        1 => 'article',
                        2 => 'case-study',
                        3 => 'statistic',
                        4 => 'location',
                        5 => 'person',
                    ],
                    'allow_archives' => 0,                   
                    'min' => 1,
                    'max' => 24,
                    'return_format' => 'object',
                ],
             
            ],
        ];
    }


    /**
     * @return array
     */
    private function registerWysiwygSection()
    {
        return [
            'key' => 'acf-main-content-blocks-layouts-wysiwyg',
            'name' => 'wysiwyg_layout',
            'label' => 'Standard Content Section',
            'display' => 'block',
            'sub_fields' => [

                # TITLE
                [
                    'key' => 'acf-main-content-blocks-layouts-wysiwyg-title',
                    'label' => 'Section Title',
                    'name' => 'title',
                    'type' => 'text',
                    'instructions' => 'Maximum of 64 characters',
                    'placeholder' => 'In this section....',
                    'maxlength' => 64,
                ],

                ### INTRODUCTION
                [
                    'key' => 'acf-main-content-blocks-layouts-wysiwyg-introduction',
                    'label' => 'Section Introduction',
                    'name' => 'introduction',
                    'type' => 'textarea',
                    'instructions' => 'Will appear directly beneath any title.',
                    'maxlength' => 1000,
                    'rows' => 3,
                    'new_lines' => 'br',
                ],

                # WYSIWYG TEXTAREA
                [
                    'key' => 'acf-main-content-blocks-layouts-wysiwyg-content',
                    'label' => 'Standard Content Item',
                    'name' => 'content',
                    'type' => 'wysiwyg',
                    'instructions' => 'Use this section to add a standard content area to the page.',
                    'required' => 1,
                    'tabs' => 'all',
                    'toolbar' => 'full',
                    'media_upload' => 0,
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-wysiwyg-anchor',
                    'label' => 'Anchor Reference',
                    'name' => 'anchor',
                    'type' => 'text',
                    'maxlength' => 32,
                    'instructions' => 'Add anchor link to this section if required. Anchors must be unique, lowercase, and consist of alphanumeric characters (a-z, 0-9). Do not include spaces or #(hashtags).'
                ],

            ],
        ];
    }

    private function registerFeaturedPostsSection()
    {
        return [
            'key' => 'acf-main-content-blocks-layouts-posts',
            'name' => 'posts_layout',
            'label' => 'Featured Posts Section',
            'display' => 'row',
            'sub_fields' => [
                [
                    'key' => 'acf-main-content-blocks-layouts-posts-title',
                    'label' => 'Section Title',
                    'name' => 'title',
                    'type' => 'text',
                    'instructions' => 'Max 48 characters',
                    'maxlength' => 48,
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-posts-introduction',
                    'label' => 'Section Introduction',
                    'name' => 'introduction',
                    'type' => 'textarea',
                    'instructions' => 'Will appear directly beneath any title.',
                    'maxlength' => 1000,
                    'rows' => 3,
                    'new_lines' => 'br',
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-posts-layout',
                    'label' => 'Layout',
                    'name' => 'layout',
                    'type' => 'radio',
                    'choices' => [
                        'cards' => 'Cards',
                        'list' => 'List',
                        'accordion' => 'Accordion',
                        'slideshow' => 'Slideshow'
                    ],
                    'layout' => 'horizontal',
                    'default_value' => 'cards',
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-posts-filter',
                    'label' => 'Filter By',
                    'name' => 'post_choice',
                    'type' => 'radio',
                    'instructions' => 'Filter by post type or post category to display relevant posts below.',
                    'choices' => [
                        'use_post_select' => 'Select Posts',
                        'use_post_category' => 'Recent News Posts by Category',
                        'use_faq_category' => 'Ordered FAQs by Category',
                    ],
                    'layout' => 'horizontal',
                    'default_value' => 'use_post_select',
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-posts-selected-category',
                    'label' => 'Select Category for News Posts',
                    'name' => 'post_choice_category',
                    'type' => 'taxonomy',
                    'instructions' => 'The category you choose below will show on the page, along with any children categories',
                    'required' => 1,
                    'conditional_logic' => [
                        [
                            [
                                'field' => 'acf-main-content-blocks-layouts-posts-filter',
                                'operator' => '==',
                                'value' => 'use_post_category',
                            ],
                        ],
                    ],
                    'taxonomy' => 'article_categories',
                    'field_type' => 'checkbox',
                    'add_term' => 0,
                    'save_terms' => 0,
                    'load_terms' => 0,
                    'return_format' => 'object',
                    'multiple' => 0,
                    'allow_null' => 0,
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-posts-faq-category',
                    'label' => 'Select Category for FAQ Posts',
                    'name' => 'faq_choice_category',
                    'type' => 'taxonomy',
                    'instructions' => 'The category of FAQs you choose below will show on the page, along with any children categories',
                    'required' => 1,
                    'conditional_logic' => [
                        [
                            [
                                'field' => 'acf-main-content-blocks-layouts-posts-filter',
                                'operator' => '==',
                                'value' => 'use_faq_category',
                            ],
                        ],
                    ],
                    'taxonomy' => 'faq_category',
                    'field_type' => 'checkbox',
                    'add_term' => 0,
                    'save_terms' => 0,
                    'load_terms' => 0,
                    'return_format' => 'object',
                    'multiple' => 0,
                    'allow_null' => 0,
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-posts-selected-posts',
                    'label' => 'Select Posts',
                    'name' => 'posts',
                    'type' => 'relationship',
                    'instructions' => 'Choose posts to link to.',
                    'required' => 0,
                    'post_type' => [],
                    'taxonomy' => [],
                    'filters' => [
                        0 => 'search',
                        1 => 'post_type',
                        2 => 'taxonomy',
                    ],
                    'elements' => ['featured_image'],
                    'return_format' => 'object',
                    'conditional_logic' => [
                        [
                            [
                                'field' => 'acf-main-content-blocks-layouts-posts-filter',
                                'operator' => '==',
                                'value' => 'use_post_select',
                            ],
                        ],
                    ],
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-posts-images',
                    'label' => 'Hide Thumbnails',
                    'name' => 'hide_thumbs',
                    'type' => 'true_false',
                    'instructions' => 'Prevent thumbnails from displaying?',
                    'required' => 0,
                    'default_value' => null,
                    'conditional_logic' => [
                        [
                            [
                                'field' => 'acf-main-content-blocks-layouts-posts-layout',
                                'operator' => '==',
                                'value' => 'cards',
                            ],
                        ],
                        [
                            [
                                'field' => 'acf-main-content-blocks-layouts-posts-layout',
                                'operator' => '==',
                                'value' => 'list',
                            ],
                        ],
                        [
                            [
                                'field' => 'acf-main-content-blocks-layouts-posts-layout',
                                'operator' => '==',
                                'value' => 'slideshow',
                            ],
                        ],

                    ],

                    'message' => '',
                    'default_value' => 0,
                    'ui' => 1,
                    'ui_on_text' => 'Yes',
                    'ui_off_text' => 'No',
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-posts-count',
                    'label' => 'Maximum number of Posts',
                    'name' => 'numberOf',
                    'type' => 'number',
                    'instructions' => 'Choose the maximum number of category based posts to display.',
                    'required' => 1,
                    'default_value' => 12,
                    'min' => 1,
                    'step' => 1,
                    'readonly' => 0,
                    'disabled' => 0,
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-posts-anchor',
                    'label' => 'Anchor Reference',
                    'name' => 'anchor',
                    'type' => 'text',
                    'maxlength' => 32,
                    'instructions' => 'Add anchor link to this section if required. Anchors must be unique, lowercase, and consist of alphanumeric characters (a-z, 0-9). Do not include spaces or #(hashtags).'
                ],
            ]
        ];
    }

    /**
     * @return array
     */
    private function registerMultimediaComboSection()
    {
        return [
            'key' => 'acf-main-content-blocks-layouts-multimedia-combo',
            'name' => 'multimedia_combo_layout',
            'label' => 'Multimedia Block (Optional Content)',
            'display' => 'row',
            'sub_fields' => [

                ### TITLE
                [
                    'key' => 'acf-main-content-blocks-layouts-multimedia-combo-title',
                    'label' => 'Section Title',
                    'name' => 'title',
                    'type' => 'text',
                    'maxlength' => 48,
                ],

                ### INTRODUCTION
                [
                    'key' => 'acf-main-content-blocks-layouts-multimedia-combo-introduction',
                    'label' => 'Section Introduction',
                    'name' => 'introduction',
                    'type' => 'textarea',
                    'instructions' => 'Will appear directly beneath any title.',
                    'maxlength' => 1000,
                    'rows' => 3,
                    'new_lines' => 'br',
                ],

                ### STYLE
                [
                    'key' => 'acf-main-content-blocks-layouts-multimedia-combo-style',
                    'label' => 'Layout Style',
                    'name' => 'style',
                    'type' => 'radio',
                    'instructions' => 'The layout will change depending on which Style you choose. ',
                    'required' => 1,
                    'choices' => 
                    [
                        'featured_image' => 'Featured Image',
                        'thumbnail_gallery' => 'Thumbnail Gallery',
                        'slideshow' => 'Slideshow',
                        'video_gallery' => 'Video Gallery',
                    ],
                    'allow_null' => 0,
                    'other_choice' => 0,
                    'save_other_choice' => 0,
                    'default_value' => '',
                    'layout' => 'horizontal',
                ],

                ### GALLERY
                [
                    'key' => 'acf-main-content-blocks-layouts-multimedia-combo-gallery',
                    'label' => 'Combo Area Gallery',
                    'name' => 'gallery',
                    'type' => 'gallery',
                    'instructions' => 'You must select at least one image, even if using a "Featured Video" Combo. If you choose "Featured Image" as the Combo Area Style, the first image will be used. ',
                    'min' => 1,
                    'max' => 12,
                    'preview_size' => 'thumbnail',
                    'insert' => 'append',
                    'library' => 'all',
                    'mime_types' => 'png,jpeg,jpg,gif',
                    'conditional_logic' => 
                    [
                        [
                            [
                                'field' => 'acf-main-content-blocks-layouts-multimedia-combo-style',
                                'operator' => '!=',
                                'value' => 'video_gallery',
                            ],
                        ]
                    ], 


                ],

                ### Video
                [
                    'key' => 'acf-main-content-blocks-layouts-multimedia-combo-video',
                    'label' => 'Video Gallery',
                    'name' => 'videos',
                    'type' => 'relationship',
                    'instructions' => 'Choose up to 50 videos to display in the video gallery. On large screens, the size of video displayed will vary depending on the number of selected videos.',
                    'post_type' => [
                        0 => 'video',
                    ],
                    'filters' => [
                        0 => 'search',
                    ],
                    'max' => 50,
                    'return_format' => 'object',
                    'conditional_logic' => 
                    [
                        [
                            [
                                'field' => 'acf-main-content-blocks-layouts-multimedia-combo-style',
                                'operator' => '==',
                                'value' => 'video_gallery',
                            ],
                        ]
                    ], 
                ],


                [
                    'key' => 'acf-main-content-blocks-layouts-multimedia-combo-has-content',
                    'label' => 'Has Content?',
                    'name' => 'has_content',
                    'type' => 'true_false',
                    'instructions' => 'include a content area with this block?',
                    'required' => 0,
                    'ui' => '1',
                    'ui_off_text' => 'No',
                    'ui_on_text' => 'Yes',
                    'default_value' => 1,
                ],


                [
                    'key' => 'acf-main-content-blocks-layouts-multimedia-combo-content',
                    'label' => 'Combo Area Content',
                    'name' => 'content',
                    'type' => 'wysiwyg',
                    'required' => 1,
                    'tabs' => 'all',
                    'toolbar' => 'basic',
                    'media_upload' => 0,
                    'conditional_logic' => 
                    [
                        [
                            [
                                'field' => 'acf-main-content-blocks-layouts-multimedia-combo-has-content',
                                'operator' => '==',
                                'value' => 1,
                            ],
                        ]
                    ], 

                ],

                [
                    'key' => 'acf-main-content-blocks-layouts-multimedia-combo-content-position',
                    'label' => 'Content Position',
                    'name' => 'content_position',
                    'type' => 'radio',
                    'instructions' => "If you choose 'Before', content will appear above the multimedia on mobile, and left of it on desktop",
                    'required' => 1,
                    'choices' => 
                    [
                        'before' => 'Before',
                        'after' => 'After',
                    ],
                    'allow_null' => 0,
                    'other_choice' => 0,
                    'save_other_choice' => 0,
                    'default_value' => 'after',
                    'layout' => 'horizontal',
                    'media_upload' => 0,
                    'conditional_logic' => 
                    [
                        [
                            [
                                'field' => 'acf-main-content-blocks-layouts-multimedia-combo-has-content',
                                'operator' => '==',
                                'value' => 1,
                            ],
                        ]
                    ],

                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-multimedia-combocontent-full-width',
                    'label' => 'Full Width Content on Desktop?',
                    'name' => 'full_width',
                    'type' => 'true_false',
                    'instructions' => 'Choose YES to break the content block into its own row, regardless of setting above',
                    'required' => 0,
                    'ui' => '1',
                    'ui_off_text' => 'No',
                    'ui_on_text' => 'Yes',
                    'default_value' => 0,
                    'conditional_logic' => 
                    [
                        [
                            [
                                'field' => 'acf-main-content-blocks-layouts-multimedia-combo-has-content',
                                'operator' => '==',
                                'value' => 1,
                            ],
                        ]
                    ],

                ],

                ### Internal Page Link
                [
                    'key' => 'acf-main-content-blocks-layouts-multimedia-combo-video-internal-link',
                    'label' => 'CTA Link (Internal)',
                    'name' => 'internal_link',
                    'type' => 'page_link',
                    'instructions' => 'You do not have to provide a link, but if you do, this will create a button beneath the Combo Area Content.',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'post_type' => [],
                    'taxonomy' => [],
                    'allow_null' => 1,
                ],

                ### External Page Link
                [
                    'key' => 'acf-main-content-blocks-layouts-multimedia-combo-video-external-link',
                    'label' => 'CTA Link (External)',
                    'name' => 'external_link',
                    'type' => 'url',
                    'instructions' => 'If you add both an internal and external link, the internal link will be used.',
                ],

                ### Button Copy
                [
                    'key' => 'acf-main-content-blocks-layouts-multimedia-combo-video-button-copy',
                    'label' => 'CTA Button Copy',
                    'name' => 'button_copy',
                    'type' => 'text',
                    'instructions' => 'This will default to "More Info" if a link is applied.',
                    'placeholder' => 'More Info',
                    'maxlength' => 20,
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    private function registerGallerySection()
    {

        $lcl = 'layouts-gallery';

        return 
        [
            'key' => 'acf-main-content-blocks-layouts-gallery',
            'name' => 'gallery_layout',
            'label' => 'Photo Gallery Section - Select Photos (Deprecated)',
            'display' => 'row',
            'sub_fields' => [

                 $this->deprecatedMessage($lcl),

                # GALLERY TITLE
                [
                    'key' => 'acf-main-content-blocks-layouts-gallery-title',
                    'label' => 'Section Title',
                    'name' => 'title',
                    'type' => 'text',
                    'instructions' => 'Maximum of 64 characters',
                    'placeholder' => 'Gallery',
                    'maxlength' => 64,
                ],

                ### INTRODUCTION
                [
                    'key' => 'acf-main-content-blocks-layouts-gallery-introduction',
                    'label' => 'Section Introduction',
                    'name' => 'introduction',
                    'type' => 'textarea',
                    'instructions' => 'Will appear directly beneath any title.',
                    'maxlength' => 1000,
                    'rows' => 3,
                    'new_lines' => 'br',
                ],

                # GALLERY PHOTOS
                [
                    'key' => 'acf-main-content-blocks-layouts-gallery-images',
                    'label' => 'Choose Photos',
                    'name' => 'images',
                    'type' => 'gallery',
                    'instructions' => 'You can choose up to 15 photos for an in-page gallery. The size of photos will resize dynamically according to the number per row. If you choose just one photo, it will be full width.<br/><br />If you have a large bank of photos, you should consider adding a thumbnail gallery in the Support Content section.',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'min' => 1,
                    'max' => 15,
                    'preview_size' => 'thumb-square',
                    'library' => 'all',
                    'mime_types' => 'png,gig,jpg,jpeg,svg',
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    private function registerGalleryGroupSection()
    {
        $lcl = 'layouts-galery-group';
        return [
            'key' => 'acf-main-content-blocks-layouts-gallery-group',
            'name' => 'gallery_group_layout',
            'label' => 'Photo Gallery Section - Select Galleries (Deprecated)',
            'display' => 'row',
            'sub_fields' => 
            [
                 $this->deprecatedMessage($lcl),
                [
                    'key' => 'acf-main-content-blocks-layouts-gallery-group-title',
                    'label' => 'Section Title',
                    'name' => 'title',
                    'type' => 'text',
                    'instructions' => 'Maximum of 64 characters',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'placeholder' => 'Gallery',
                    'maxlength' => 64,
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-gallery-group-introduction',
                    'label' => 'Section Introduction',
                    'name' => 'introduction',
                    'type' => 'textarea',
                    'instructions' => 'Will appear directly beneath any title.',
                    'maxlength' => 1000,
                    'rows' => 3,
                    'new_lines' => 'br',
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-gallery-group-galleries',
                    'label' => 'Choose Gallery(s):',
                    'name' => 'galleries',
                    'type' => 'repeater',
                    'instructions' => 'If you have created pre-defined galleries (media categories), you can assign them as galleries to this page.',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'min' => '1',
                    'max' => '5',
                    'layout' => 'row',
                    'button_label' => 'Add Gallery',
                    'sub_fields' => [
                        [
                            'key' => 'acf-main-content-blocks-layouts-gallery-group-galleries-gallery',
                            'label' => 'Gallery',
                            'name' => 'gallery',
                            'type' => 'taxonomy',
                            'instructions' => '',
                            'required' => 1,
                            'conditional_logic' => 0,
                            'taxonomy' => 'media_category',
                            'field_type' => 'select',
                            'return_format' => 'object',
                        ],
                    ],
                ],
            ]
        ];
    }

    /**
     * @return array
     */
    private function registerVideoSection()
    {
        $lcl = 'layouts-video';
        return 
        [
            'key' => 'acf-main-content-blocks-layouts-video',
            'name' => 'video_layout',
            'label' => 'Video Gallery Section (Deprecated)',
            'display' => 'row',
            'sub_fields' => [
                 $this->deprecatedMessage($lcl),
                [
                    'key' => 'acf-main-content-blocks-layouts-video-title',
                    'label' => 'Section Title',
                    'name' => 'title',
                    'type' => 'text',
                    'instructions' => 'Maximum of 64 characters',
                    'placeholder' => 'Video Gallery',
                    'maxlength' => 64,
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-video-introduction',
                    'label' => 'Section Introduction',
                    'name' => 'introduction',
                    'type' => 'textarea',
                    'instructions' => 'Will appear directly beneath any title.',
                    'maxlength' => 1000,
                    'rows' => 3,
                    'new_lines' => 'br',
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-video-videos',
                    'label' => 'Select Video(s)',
                    'name' => 'videos',
                    'type' => 'relationship',
                    'instructions' => 'You can choose up to 15 videos for an in-page gallery. The size of videos will resize dynamically according to the number per row. If you choose just one video, it will be full width.',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'post_type' => ['video'],
                    'taxonomy' => [],
                    'filters' => ['search'],
                    'elements' => ['featured_image'],
                    'min' => 1,
                    'max' => 15,
                    'return_format' => 'object',
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    private function registerAudioSection()
    {
        return [
            'key' => 'acf-main-content-blocks-layouts-audio',
            'name' => 'audio_layout',
            'label' => 'Audio Gallery Section',
            'display' => 'row',
            'sub_fields' => [
                [
                    'key' => 'acf-main-content-blocks-layouts-audio-title',
                    'label' => 'Section Title',
                    'name' => 'title',
                    'type' => 'text',
                    'instructions' => 'Maximum of 64 characters',
                    'placeholder' => 'Audio Gallery',
                    'maxlength' => 64,
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-audio-introduction',
                    'label' => 'Section Introduction',
                    'name' => 'introduction',
                    'type' => 'textarea',
                    'instructions' => 'Will appear directly beneath any title.',
                    'maxlength' => 1000,
                    'rows' => 3,
                    'new_lines' => 'br',
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-audio-embed',
                    'label' => 'Select Audio',
                    'name' => 'audios',
                    'type' => 'relationship',
                    'instructions' => 'You can choose a single audio file for an in-page section.',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'post_type' => ['audio'],
                    'taxonomy' => [],
                    'filters' => ['search'],
                    'elements' => ['featured_image'],
                    'min' => 1,
                    'max' => 1,
                    'return_format' => 'object',
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    private function registerChartSection()
    {
        return [
            'key' => 'acf-main-content-blocks-layouts-chart',
            'name' => 'chart_layout',
            'label' => 'Chart Section',
            'display' => 'row',
            'sub_fields' => [
                [
                    'key' => 'acf-main-content-blocks-layouts-chart-title',
                    'label' => 'Section Title',
                    'name' => 'title',
                    'type' => 'text',
                    'instructions' => 'Maximum of 64 characters',
                    'placeholder' => '',
                    'maxlength' => 64,
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-chart-introduction',
                    'label' => 'Section Introduction',
                    'name' => 'introduction',
                    'type' => 'textarea',
                    'instructions' => 'Will appear directly beneath any title.',
                    'maxlength' => 1000,
                    'rows' => 3,
                    'new_lines' => 'br',
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-chart-embed',
                    'label' => 'Select Chart',
                    'name' => 'chart',
                    'type' => 'relationship',
                    'instructions' => 'You can choose a single chart file for an in-page section.',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'post_type' => ['chart'],
                    'taxonomy' => [],
                    'filters' => ['search'],
                    'elements' => ['featured_image'],
                    'min' => 1,
                    'max' => 1,
                    'return_format' => 'object',
                ],
            ],
        ];
    }

    private function registerDownloadsSection()
    {
        return [
            'key' => 'acf-main-content-blocks-layouts-downloads',
            'name' => 'downloads_layout',
            'label' => 'Downloads Section - Select Files',
            'display' => 'row',
            'sub_fields' => [
                [
                    'key' => 'acf-main-content-blocks-layouts-downloads-title',
                    'label' => 'Section Title',
                    'name' => 'title',
                    'type' => 'text',
                    'instructions' => 'Maximum of 64 characters',
                    'placeholder' => 'Downloads',
                    'maxlength' => 64,
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-downloads-introduction',
                    'label' => 'Section Introduction',
                    'name' => 'introduction',
                    'type' => 'textarea',
                    'instructions' => 'Will appear directly beneath any title.',
                    'maxlength' => 1000,
                    'rows' => 3,
                    'new_lines' => 'br',
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-downloads-layout',
                    'label' => 'Layout',
                    'name' => 'layout',
                    'type' => 'radio',
                    'instructions' => 'Choose to display downloadable files or libraries as a data table or a series of clickable panels within this section.',
                    'choices' => [
                        'table' => 'Table Layout.',
                        'panel' => 'Panel Layout.',
                    ],
                    'layout' => 'horizontal',
                ],

                [
                    'key' => 'acf-main-content-blocks-layouts-downloads-files',
                    'label' => 'Choose Files to Download',
                    'name' => 'files',
                    'type' => 'repeater',
                    'instructions' => 'If you have one or more downloads you would like to include in this page, you can add them here.',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'min' => '',
                    'max' => 15,
                    'layout' => 'table',
                    'button_label' => 'Add Download',
                    'sub_fields' => [
                        [
                            'key' => 'acf-main-content-blocks-layouts-downloads-files-file',
                            'label' => 'Selected Download',
                            'name' => 'file',
                            'type' => 'file',
                            'return_format' => 'object',
                            'library' => 'all',
                            'mime_types' => 'pdf,doc,docx,rtf,xls,xlsx,ppt,pptx,',
                        ],
                    ],
                    'collapsed' => '',
                ],
            ],
            'min' => '',
            'max' => '',
        ];

    }


    private function registerDownloadGroupsSection()
    {
        return [
            'key' => 'acf-main-content-blocks-layouts-download-groups',
            'name' => 'download_groups_layout',
            'label' => 'Downloads Section - Select Libraries',
            'display' => 'row',
            'sub_fields' => [
                [
                    'key' => 'acf-main-content-blocks-layouts-download-groups-title',
                    'label' => 'Section Title',
                    'name' => 'title',
                    'type' => 'text',
                    'instructions' => 'Maximum of 64 characters',
                    'conditional_logic' => 0,
                    'placeholder' => 'Document Libraries',
                    'maxlength' => 64,
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-download-groups-introduction',
                    'label' => 'Section Introduction',
                    'name' => 'introduction',
                    'type' => 'textarea',
                    'instructions' => 'Will appear directly beneath any title.',
                    'maxlength' => 1000,
                    'rows' => 3,
                    'new_lines' => 'br',
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-download-groups-libraries',
                    'label' => 'Choose Library(s):',
                    'name' => 'libraries',
                    'type' => 'repeater',
                    'instructions' => 'If you have created pre-defined libraries (media categories), you can assign them as libraries to this page.',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'min' => '1',
                    'max' => '5',
                    'layout' => 'row',
                    'button_label' => 'Add Library',
                    'sub_fields' => [
                        [
                            'key' => 'acf-main-content-blocks-layouts-download-groups-libraries-id',
                            'label' => 'Library',
                            'name' => 'library',
                            'type' => 'taxonomy',
                            'instructions' => '',
                            'required' => 1,
                            'conditional_logic' => 0,
                            'taxonomy' => 'media_category',
                            'field_type' => 'select',
                            'return_format' => 'object',
                            'multiple' => 0,
                        ],
                    ],
                ],
            ],
        ];
    }

    private function registerContactFormSection()
    {
        return [
            'key' => 'acf-main-content-blocks-layouts-contact-form',
            'name' => 'contact_form_layout',
            'label' => 'Contact Form Section',
            'display' => 'row',
            'sub_fields' => [
                [
                    'key' => 'acf-main-content-blocks-layouts-contact-form-title',
                    'label' => 'Section Title',
                    'name' => 'title',
                    'type' => 'text',
                    'instructions' => 'Maximum of 64 characters',
                    'placeholder' => 'Get in Touch',
                    'maxlength' => 64,
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-contact-form-introduction',
                    'label' => 'Section Introduction',
                    'name' => 'introduction',
                    'type' => 'textarea',
                    'instructions' => 'Will appear directly beneath any title.',
                    'maxlength' => 1000,
                    'rows' => 3,
                    'new_lines' => 'br',
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-contact-form-introduction',
                    'label' => 'Form Introduction',
                    'type' => 'textarea',
                    'name' => 'introduction',
                    'instructions' => 'A couple of paragraphs at most. It is recommended you use a Standard Content section for HTML copy.',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'maxlength' => 480,
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-contact-form-form',
                    'label' => 'Contact Form',
                    'name' => 'form',
                    'type' => 'gravity_forms_field',
                    'instructions' => 'Select an application form. Leave blank if there is another method of application.',
                    'required' => 1,
                    'taxonomy' => [],
                    'return_format' => 'id',
                    'ui' => 1,
                ],
            ]
        ];
    }


    private function registerLocationSection()
    {
        return [
            'key' => 'acf-main-content-blocks-layouts-location',
            'name' => 'location_layout',
            'label' => 'Location Section',
            'display' => 'row',
            'sub_fields' => [
                [
                    'key' => 'acf-main-content-blocks-layouts-location-title',
                    'label' => 'Section Title',
                    'name' => 'title',
                    'type' => 'text',
                    'instructions' => 'Maximum of 64 characters',
                    'placeholder' => '',
                    'maxlength' => 64,
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-location-introduction',
                    'label' => 'Section Introduction',
                    'name' => 'introduction',
                    'type' => 'textarea',
                    'instructions' => 'Will appear directly beneath any title.',
                    'maxlength' => 1000,
                    'rows' => 3,
                    'new_lines' => 'br',
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-location-location',
                    'label' => 'Choose a Location',
                    'name' => 'locations',
                    'type' => 'relationship',
                    'instructions' => 'Select a location. If it does not already exist, you should create it in the Locations section of the CMS.',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'post_type' => [
                        0 => 'location',
                    ],
                    'taxonomy' => [],
					'allow_null' => 0,
					'multiple' => 0,
					'return_format' => 'object',
					'ui' => 1,
                ],
            ]
        ];
    }

    private function registerFAQSection()
    {
        return [
            'key' => 'acf-main-content-blocks-layouts-faq-selection',
            'name' => 'faq_selection_layout',
            'label' => 'FAQ Section - Selected Entries',
            'display' => 'row',
            'sub_fields' => [
                [
                    'key' => 'acf-main-content-blocks-layouts-faq-selection-title',
                    'label' => 'Section Title',
                    'name' => 'title',
                    'type' => 'text',
                    'instructions' => 'Max 48 characters',
                    'maxlength' => 48,
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-faq-selection-introduction',
                    'label' => 'Section Introduction',
                    'name' => 'introduction',
                    'type' => 'textarea',
                    'instructions' => 'Will appear directly beneath any title.',
                    'maxlength' => 1000,
                    'rows' => 3,
                    'new_lines' => 'br',
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-faq-selection-faqs',
                    'label' => 'Select FAQs',
                    'name' => 'faqs',
                    'type' => 'relationship',
                    'instructions' => 'Choose up to 12 FAQs to include in this page. If you need more FAQs, add a new section.',
                    'required' => 1,
                    'post_type' => ['faq'],
                    'taxonomy' => [],
                    'filters' => ['search'],
                    'elements' => ['featured_image'],
                    'min' => 1,
                    'max' => 12,
                    'return_format' => 'object',
                ],
            ],
        ];
    }


    private function registerFullSizeImageSection()
    {
        $lcl = 'layouts-full-size-image';
        return 
        [
            'key' => 'acf-main-content-blocks-layouts-full-size-image',
            'name' => 'full_size_image_layout',
            'label' => 'Full Size Image Section (Deprecated)',
            'display' => 'row',
            'sub_fields' => 
            [
                 $this->deprecatedMessage($lcl),
                [
                    'key' => 'acf-main-content-blocks-layouts-full-size-image-title',
                    'label' => 'Section Title',
                    'name' => 'title',
                    'type' => 'text',
                    'instructions' => 'Maximum of 64 characters',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'default_value' => '',
                    'placeholder' => 'e.g. infographic title',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => 64,
                    'readonly' => 0,
                    'disabled' => 0,
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-full-size-image-introduction',
                    'label' => 'Section Introduction',
                    'name' => 'introduction',
                    'type' => 'textarea',
                    'instructions' => 'Will appear directly beneath any title.',
                    'maxlength' => 1000,
                    'rows' => 3,
                    'new_lines' => 'br',
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-full-size-image-image',
                    'label' => 'Choose Photo',
                    'name' => 'image',
                    'type' => 'image',
                    'instructions' => 'You can choose up to 1 photo. This will display in its original uploaded resolution.',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'min' => 1,
                    'max' => 1,
                    'preview_size' => 'thumb-square',
                    'library' => 'all',
                    'min_width' => '300',
                    'min_height' => '300',
                    'mime_types' => 'png,gig,jpg,jpeg,svg',
                ],
            ],
        ];
    }

    private function registerFeaturedNewsSection()
    {
        $lcl = "layouts-featured-news";
        return [
            'key' => 'acf-main-content-blocks-layouts-featured-news',
            'name' => 'featured_news_layout',
            'label' => 'Featured News - Selected Entries (Deprecated)',
            'display' => 'row',
            'sub_fields' => [
                 $this->deprecatedMessage($lcl),
                [
                    'key' => 'acf-main-content-blocks-layouts-featured-news-title',
                    'label' => 'Section Title',
                    'name' => 'title',
                    'type' => 'text',
                    'instructions' => 'Max 48 characters',
                    'maxlength' => 48,
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-featured-news-introduction',
                    'label' => 'Section Introduction',
                    'name' => 'introduction',
                    'type' => 'textarea',
                    'instructions' => 'Will appear directly beneath any title.',
                    'maxlength' => 1000,
                    'rows' => 3,
                    'new_lines' => 'br',
                ],
                [
                    'key' => 'acf-main-content-blocks-layouts-featured-news-articles',
                    'label' => 'Select Articles',
                    'name' => 'articles',
                    'type' => 'relationship',
                    'instructions' => 'Choose 3 News articles to feature in this block.',
                    'required' => 3,
                    'post_type' => ['article'],
                    'taxonomy' => [],
                    'filters' => ['search'],
                    'min' => 3,
                    'max' => 3,
                    'return_format' => 'object',
                ],
            ]
        ];
    }

    /**
     * Removes comments from pages
     */
    private function removeCommentSupport()
    {
        \add_action('init', function(){
            \remove_post_type_support( 'page', 'comments' );
        });
    }

    /**
     * Removes editor support from Pages
     */
    private function removeEditorSupport()
    {
        \add_action('init', function()
        {
            \remove_post_type_support('page', 'editor');
        });
    }

    /**
     *
     */
    private function addSEODescription()
    {
        acf_add_local_field_group([
            'key' => 'acf-seo-description',
            'title' => 'SEO Description',
            'fields' => [
                [
                    'key' => 'acf-seo-description-text',
                    'label' => 'Description - Vital for SEO and Search Results',
                    'name' => 'description',
                    'type' => 'text',
                    'instructions' => 'A description containing some keywords of the content of this post. Maximum of 160 characters.',
                    'required' => 1,
                    'maxlength' => 160,
                ],
                /*[
                    'key' => 'acf-extended-description-text-2',
                    'label' => 'Extended Description',
                    'name' => 'description_extended',
                    'type' => 'textarea',
                    'instructions' => 'If you provide copy here, it will be used instead of the SEO description on internal page link descriptions. Otherwise the SEO copy (above) will be used.',
                    'required' => 0,
                    'maxlength' => 480,
                ],*/                
            ],
            'location' => $this->allPostTypeLocations(),
            'menu_order' => 0,
            'position' => 'acf_after_title',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
        ]);
    }

    /**
     * All of the different post types we have registered
     * To be used with the location attribute of certain post types.
     *
     * @return array
     */
    private function allPostTypeLocations()
    {
        return [
            [
                [
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'page',
                ],
            ],
            [
                [
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'article',
                ],
            ],
            [
                [
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'career',
                ],
            ],
            [
                [
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'case-study',
                ],
            ],
//            [
//                [
//                    'param' => 'post_type',
//                    'operator' => '==',
//                    'value' => 'chart',
//                ],
//            ],
            [
                [
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'event',
                ],
            ],
            [
                [
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'faq',
                ],
            ],
//            [
//                [
//                    'param' => 'post_type',
//                    'operator' => '==',
//                    'value' => 'location',
//                ],
//            ],
            [
                [
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'person',
                ],
            ],
            [
                [
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'product',
                ],
            ],
            [
                [
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'report',
                ],
            ],
            [
                [
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'service',
                ],
            ],
            [
                [
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'statistic',
                ],
            ],
//            [
//                [
//                    'param' => 'post_type',
//                    'operator' => '==',
//                    'value' => 'testimonial',
//                ],
//            ],
//            [
//                [
//                    'param' => 'post_type',
//                    'operator' => '==',
//                    'value' => 'video',
//                ],
//            ],
        ];
    }

    /**
     * @return array
     */
    private function registerSupportingGallery()
    {
        return [
            'key' => 'acf-supporting-content-blocks-layouts-gallery',
            'name' => 'supporting_gallery_layout',
            'label' => 'Thumbnail Gallery Section - Select Photos',
            'display' => 'row',
            'sub_fields' => [
                [
                    'key' => 'acf-supporting-content-blocks-layouts-gallery-title',
                    'label' => 'Section Title',
                    'name' => 'title',
                    'type' => 'text',
                    'instructions' => 'Maximum of 32 characters',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'placeholder' => 'Gallery',
                    'maxlength' => 32,
                ],
                [
                    'key' => 'acf-supporting-content-blocks-layouts-gallery-introduction',
                    'label' => 'Section Introduction',
                    'name' => 'introduction',
                    'type' => 'textarea',
                    'instructions' => 'Will appear directly beneath any title.',
                    'maxlength' => 1000,
                    'rows' => 3,
                    'new_lines' => 'br',
                ],
                [
                    'key' => 'acf-supporting-content-blocks-layouts-gallery-images',
                    'label' => 'Choose Images',
                    'name' => 'images',
                    'type' => 'gallery',
                    'instructions' => 'You can add as many images as you choose (up to 100). The photos will resize as clickable thumbnails. If you use more than 15 photos, the later ones will be concealed in a "plus X more" message.',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'min' => 1,
                    'max' => 100,
                    'preview_size' => 'thumb-square',
                    'library' => 'all',
                ],
            ],
            'min' => '',
            'max' => '',
        ];
    }

    private function registerSupportingLinks()
    {
        return [
            'key' => 'acf-supporting-content-blocks-layouts-links',
            'name' => 'related_links_layout',
            'label' => 'Related Links Section',
            'display' => 'row',
            'sub_fields' => [
                [
                    'key' => 'acf-supporting-content-blocks-layouts-links-title',
                    'label' => 'Section Title',
                    'name' => 'title',
                    'type' => 'text',
                    'instructions' => 'Maximum of 32 characters',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'placeholder' => 'Related Links',
                    'maxlength' => 32,
                ],
                [
                    'key' => 'acf-supporting-content-blocks-layouts-links-introduction',
                    'label' => 'Section Introduction',
                    'name' => 'introduction',
                    'type' => 'textarea',
                    'instructions' => 'Will appear directly beneath any title.',
                    'maxlength' => 1000,
                    'rows' => 3,
                    'new_lines' => 'br',
                ],
                [
                    'key' => 'acf-supporting-content-blocks-layouts-links-links',
                    'label' => 'Related Links',
                    'name' => 'links',
                    'type' => 'repeater',
                    'instructions' => 'If you would like to include related links on this page, you can add them here. Every link must have a name.',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'collapsed' => 'm40_acf_supp_content_links_section_links_link_url',
                    'min' => '',
                    'max' => 10,
                    'layout' => 'table',
                    'button_label' => 'Add Related Link',
                    'sub_fields' => [
                        [
                            'key' => 'acf-supporting-content-blocks-layouts-links-name',
                            'label' => 'Link Name',
                            'name' => 'name',
                            'type' => 'text',
                            'required' => 1,
                            'maxlength' => 48,
                            'readonly' => 0,
                            'disabled' => 0,
                        ],
                        [
                            'key' => 'acf-supporting-content-blocks-layouts-links-internal-link',
                            'label' => 'Internal Link',
                            'name' => 'internal_link',
                            'type' => 'post_object',
                            'required' => 0,
                            'conditional_logic' => 0,
                        ],
                        [
                            'key' => 'acf-supporting-content-blocks-layouts-links-url',
                            'label' => 'External Link',
                            'name' => 'external_link',
                            'type' => 'url',
                            'required' => 0,
                            'conditional_logic' => 0,
                        ],
                    ],
                ],
            ],
            'min' => '',
            'max' => '',
        ];
    }

    /**
     * @return array
     */
    private function registerSupportingGalleryGroup()
    {
        return [
            'key' => 'acf-supporting-content-blocks-layouts-gallery-group',
            'name' => 'supporting_gallery_group_layout',
            'label' => 'Thumbnail Gallery Section - Select Galleries',
            'display' => 'row',
            'sub_fields' => [
                [
                    'key' => 'acf-supporting-content-blocks-layouts-gallery-group-title',
                    'label' => 'Section Title',
                    'name' => 'title',
                    'type' => 'text',
                    'instructions' => 'Maximum of 32 characters',
                    'placeholder' => 'Galleries',
                    'maxlength' => 32,
                ],
                ### INTRODUCTION
                [
                    'key' => 'acf-supporting-content-blocks-layouts-gallery-group-introduction',
                    'label' => 'Section Introduction',
                    'name' => 'introduction',
                    'type' => 'textarea',
                    'instructions' => 'Will appear directly beneath any title.',
                    'maxlength' => 1000,
                    'rows' => 3,
                    'new_lines' => 'br',
                ],
                [
                    'key' => 'acf-supporting-content-blocks-layouts-gallery-group-galleries',
                    'label' => 'Choose Galleries:',
                    'name' => 'galleries',
                    'type' => 'repeater',
                    'instructions' => 'If you have created pre-defined galleries (media categories), you can assign them as thumbnail galleries to this page.',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'layout' => 'row',
                    'button_label' => 'Add Gallery',
                    'sub_fields' => [
                        [
                            'key' => 'acf-supporting-content-blocks-layouts-gallery-group-galleries-id',
                            'label' => 'Gallery',
                            'name' => 'gallery',
                            'type' => 'taxonomy',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'taxonomy' => 'media_category',
                            'field_type' => 'select',
                            'return_format' => 'object',
                            'multiple' => 0,
                        ],
                    ],
                ],
            ],
            'min' => '',
            'max' => '',
        ];
    }

    /**
     * @return array
     */
    private function registerSupportingVideoSection()
    {
        return [
            'key' => 'acf-supporting-content-blocks-layouts-video',
            'name' => 'supporting_video_layout',
            'label' => 'Video Gallery Section',
            'display' => 'row',
            'sub_fields' => [
                [
                    'key' => 'acf-supporting-content-blocks-layouts-video-title',
                    'label' => 'Section Title',
                    'name' => 'title',
                    'type' => 'text',
                    'instructions' => 'Maximum of 64 characters',
                    'placeholder' => 'Video Gallery',
                    'maxlength' => 64,
                ],
                [
                    'key' => 'acf-supporting-content-blocks-layouts-video-introduction',
                    'label' => 'Section Introduction',
                    'name' => 'introduction',
                    'type' => 'textarea',
                    'instructions' => 'Will appear directly beneath any title.',
                    'maxlength' => 1000,
                    'rows' => 3,
                    'new_lines' => 'br',
                ],
                [
                    'key' => 'acf-supporting-content-blocks-layouts-video-videos',
                    'label' => 'Select Video(s)',
                    'name' => 'videos',
                    'type' => 'relationship',
                    'instructions' => 'You can choose up to 15 videos for an in-page gallery. The size of videos will resize dynamically according to the number per row. If you choose just one video, it will be full width.',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'post_type' => ['video'],
                    'taxonomy' => [],
                    'filters' => ['search'],
                    'elements' => ['featured_image'],
                    'min' => 1,
                    'max' => 15,
                    'return_format' => 'object',
                ],
            ],
        ];
    }

    private function registerSupportingDownloadsSection()
    {
        return [
            'key' => 'acf-supporting-content-blocks-layouts-downloads',
            'name' => 'supporting_downloads_layout',
            'label' => 'Downloads Section - Select Files',
            'display' => 'row',
            'sub_fields' => [
                [
                    'key' => 'acf-supporting-content-blocks-layouts-downloads-title',
                    'label' => 'Section Title',
                    'name' => 'title',
                    'type' => 'text',
                    'instructions' => 'Maximum of 64 characters',
                    'placeholder' => 'Downloads',
                    'maxlength' => 64,
                ],
                [
                    'key' => 'acf-supporting-content-blocks-layouts-downloads-introduction',
                    'label' => 'Section Introduction',
                    'name' => 'introduction',
                    'type' => 'textarea',
                    'instructions' => 'Will appear directly beneath any title.',
                    'maxlength' => 1000,
                    'rows' => 3,
                    'new_lines' => 'br',
                ],
                [
                    'key' => 'acf-supporting-content-blocks-layouts-downloads-files',
                    'label' => 'Choose Files to Download',
                    'name' => 'files',
                    'type' => 'repeater',
                    'instructions' => 'If you have one or more downloads you would like to include in this page, you can add them here.',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'min' => '',
                    'max' => 15,
                    'layout' => 'table',
                    'button_label' => 'Add Download',
                    'sub_fields' => [
                        [
                            'key' => 'acf-supporting-content-blocks-layouts-downloads-files-file',
                            'label' => 'Selected Download',
                            'name' => 'file',
                            'type' => 'file',
                            'return_format' => 'object',
                            'library' => 'all',
                            'mime_types' => 'pdf,doc,docx,rtf,xls,xlsx,ppt,pptx,',
                        ],
                    ],
                    'collapsed' => '',
                ],
            ],
            'min' => '',
            'max' => '',
        ];

    }


    private function registerSupportingDownloadGroupsSection()
    {
        return [
            'key' => 'acf-supporting-content-blocks-layouts-download-groups',
            'name' => 'supporting_download_groups_layout',
            'label' => 'Downloads Section - Select Libraries',
            'display' => 'row',
            'sub_fields' => [
                [
                    'key' => 'acf-supporting-content-blocks-layouts-download-groups-title',
                    'label' => 'Section Title',
                    'name' => 'title',
                    'type' => 'text',
                    'instructions' => 'Maximum of 64 characters',
                    'conditional_logic' => 0,
                    'placeholder' => 'Document Libraries',
                    'maxlength' => 64,
                ],
                [
                    'key' => 'acf-supporting-content-blocks-layouts-download-groups-introduction',
                    'label' => 'Section Introduction',
                    'name' => 'introduction',
                    'type' => 'textarea',
                    'instructions' => 'Will appear directly beneath any title.',
                    'maxlength' => 1000,
                    'rows' => 3,
                    'new_lines' => 'br',
                ],
                [
                    'key' => 'acf-supporting-content-blocks-layouts-download-groups-libraries',
                    'label' => 'Choose Library(s):',
                    'name' => 'libraries',
                    'type' => 'repeater',
                    'instructions' => 'If you have created pre-defined libraries (media categories), you can assign them as libraries to this page.',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'min' => '1',
                    'max' => '5',
                    'layout' => 'row',
                    'button_label' => 'Add Library',
                    'sub_fields' => [
                        [
                            'key' => 'acf-supporting-content-blocks-layouts-download-groups-libraries-id',
                            'label' => 'Library',
                            'name' => 'library',
                            'type' => 'taxonomy',
                            'instructions' => '',
                            'required' => 1,
                            'conditional_logic' => 0,
                            'taxonomy' => 'media_category',
                            'field_type' => 'select',
                            'return_format' => 'object',
                            'multiple' => 0,
                        ],
                    ],
                ],
            ],
        ];
    }

    private function deprecatedMessage($label)
    {
        return
        [
            'key' => 'acf-' . $label . 'deprecated-msg',
            'name' => 'deprecated_message',
            'label' => 'Warning',
            'type' => 'message',
            'message' => 'This content block type has been deprecated. Though you can see its content here for reference, that content will not appear for the end user. You should migrate the content to a different block, then remove this one.',
            'new_lines' => 'wpautop',
            'esc_html' => 1,
        ];
    }

    private function addTemplateVariations()
    {
        \acf_add_local_field_group(
        [
            'key' => 'acf-template-variations-listings',
            'title' => 'Listings Page Settings',
            'fields' => [
                [
                    'key' => 'acf-template-variations-listings-post-type',
                    'label' => 'Post Type',
                    'name' => 'listings_post_type',
                    'type' => 'select',
                    'allow_null' => 1,
                    'multiple' => 0,
                    'required' => 0,
                    'return_format' => 'value',
                    'placeholder' => 'Choose a Post Type to list:',
                    'instructions' => 'When you assign a post type to a listings page, only its type will appear. e.g. News.'
                ],
                [
                    'key' => 'acf-template-variations-listings-category-type',
                    'label' => 'Category Type',
                    'name' => 'assign_key_category',
                    'type' => 'select',
                    'allow_null' => 1,
                    'multiple' => 0,
                    'required' => 0,
                    'return_format' => 'value',
                    'placeholder' => 'Choose a Category to list:',
                    'instructions' => 'When you assign a category to a listings page, only its type and its children will appear. e.g. Featured News.'
                ],
                [
                    'key' => 'acf-listing-title',
                    'label' => 'Listings Section Title',
                    'name' => 'listings_title',
                    'type' => 'text',
                    'instructions' => '(Optional)',
                    'placeholder' => '',
                    'maxlength' => 64,
                ],

                [
                    'key' => 'acf-listings-empty-copy',
                    'label' => 'Copy to Display if Empty',
                    'name' => 'empty_listings_copy',
                    'type' => 'wysiwyg',
                    'instructions' => 'A helpful message for the user.',
                    'tabs' => 'all',
                    'toolbar' => 'basic',
                    'media_upload' => 0,
                    'conditional_logic' => 
                    [
                        [
                            [
                                'field' => 'acf-listings-if-empty',
                                'operator' => '==',
                                'value' => 1,
                            ],
                        ],
                    ],

                ],
                [
                    'key' => 'acf-listings-show-archive',
                    'label' => 'Show Archive Links (if available)?',
                    'name' => 'show_archive_links',
                    'type' => 'true_false',
                    'instructions' => 'Only really suitable for News content.',
                    'required' => 0,
                    'ui' => '1',
                    'ui_off_text' => 'No',
                    'ui_on_text' => 'Yes',
                    'default_value' => 0,
                ],
            ],
            'location' => 
            [
                [
                    [
                        'param' => 'page_template',
                        'operator' => '==',
                        'value' => 'page-listings.php',
                    ],
                ],

            ],
            'menu_order' => 1,
            'position' => 'acf_after_title',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
        ]);
    
        /*\acf_add_local_field_group(
        [
            'key' => 'acf-template-variations-complaint-form',
            'title' => 'Complaint Form: Downloads Area',
            'fields' => 
            [


                [
                    'key' => 'acf-template-variations-complaint-form-title',
                    'label' => 'Section Title',
                    'name' => 'title',
                    'type' => 'text',
                    'instructions' => 'Maximum of 64 characters',
                    'placeholder' => 'Downloads',
                    'maxlength' => 64,
                ],
                [
                    'key' => 'acf-template-variations-complaint-forms-introduction',
                    'label' => 'Section Introduction',
                    'name' => 'introduction',
                    'type' => 'textarea',
                    'instructions' => 'Will appear directly beneath any title.',
                    'maxlength' => 1000,
                    'rows' => 3,
                    'new_lines' => 'br',
                ],
                [
                    'key' => 'acf-template-variations-complaint-form-layout',
                    'label' => 'Layout',
                    'name' => 'layout',
                    'type' => 'radio',
                    'instructions' => 'Choose to display downloadable files or libraries as a data table or a series of clickable panels within this section.',
                    'choices' => [
                        'table' => 'Table Layout',
                        'panel' => 'Panel Layout',
                    ],
                    'layout' => 'horizontal',
                ],

                [
                    'key' => 'acf-template-variations-complaint-form-files',
                    'label' => 'Choose Files to Download',
                    'name' => 'files',
                    'type' => 'repeater',
                    'instructions' => 'If you have one or more downloads you would like to include in this page, you can add them here.',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'min' => '',
                    'max' => 15,
                    'layout' => 'table',
                    'button_label' => 'Add Download',
                    'sub_fields' => [
                        [
                            'key' => 'acf-template-variations-complaint-form-files-file',
                            'label' => 'Selected Download',
                            'name' => 'file',
                            'type' => 'file',
                            'return_format' => 'object',
                            'library' => 'all',
                            'mime_types' => 'pdf,doc,docx,rtf,xls,xlsx,ppt,pptx,zip',
                        ],
                    ],
                    'collapsed' => '',
                ],

            ],
            'location' => 
            [
                [
                    [
                        'param' => 'page_template',
                        'operator' => '==',
                        'value' => 'page-form.php',
                    ],
                ],

            ],
            'menu_order' => 1,
            'position' => 'acf_after_title',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
        ]);*/
    }
}