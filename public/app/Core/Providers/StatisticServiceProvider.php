<?php

namespace Hailstone\Core\Providers;

use Hailstone\Core\Statistic;
use Hailstone\Core\Support\ServiceProvider;

class StatisticServiceProvider extends ServiceProvider
{
    /**
     *
     */
    public function register()
    {
        $this->app->bind(Statistic::class);

        $this->registerPostType();
        $this->registerTaxonomies();
    }

    /**
     *
     */
    private function registerPostType()
    {
        \add_action( 'init', function()
        {
            \register_post_type( 'statistic',
                [
                    'labels' => [
                        'name' => __( 'Statistics' ),
                        'singular_name' => __( 'Statistic' ),
                        'add_new' => "Add Statistics",
                        'add_new_item' => "Add Statistics",
                        'all_items' => 'All Statistics',
                        'view_item' => "View Statistics",
                        'edit_item' => "Edit Statistics",
                        'new_item' => "New Statistics",
                        'featured_image' => 'Featured Image'
                    ],
                    'menu_position' => 93,
                    'menu_icon' => 'dashicons-analytics',
                    'supports' => array('title', 'revisions', 'thumbnail', 'author', 'page-attributes'),
                    'hierarchical' => false,
                    'exclude_from_search' => false,
                    'public' => true,
                    'has_archive' => false,
                    'rewrite' => [
                        'slug' => 'publications/statistics/archive'
                    ],
                    'show_in_menu' => true,
                    'show_ui' => true,
                    'capability_type' => array('statistic', 'statistics'),
                    'map_meta_cap' => true,
                ]
            );
        });
    }

    /**
     *
     */
    private function registerTaxonomies()
    {
        \register_taxonomy(
            'statistic_categories',
            'statistic',
            [
                'labels' => [
                    'name' => 'Statistics Categories',
                    'add_new_item' => 'Add New Category',
                    'new_item_name' => "New Category"
                ],
                'show_ui' => true,
                'show_tagcloud' => false,
                'hierarchical' => true,
            ]
        );
    }
}