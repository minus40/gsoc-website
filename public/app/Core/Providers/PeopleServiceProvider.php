<?php

namespace Hailstone\Core\Providers;

use Hailstone\Core\People;
use Hailstone\Core\Support\ServiceProvider;

class PeopleServiceProvider extends ServiceProvider
{
    /**
     *
     */
    public function register()
    {
        $this->app->bind(People::class);

        $this->registerPostType();
        $this->registerTaxonomies();
        $this->registerCustomFields();
    }

    private function registerPostType()
    {
        \add_action( 'init', function()
        {
            \register_post_type( 'person',
                array(
                    'labels' => [
                        'name' => __( 'People' ),
                        'singular_name' => __( 'Person' ),
                        'add_new' => "Add Person",
                        'add_new_item' => "Add Person",
                        'all_items' => 'All People',
                        'view_item' => "View Person",
                        'edit_item' => "Edit Person",
                        'new_item' => "New Person",
                        'featured_image' => 'Featured Image'
                    ],
                    'menu_position' => 90,
                    'menu_icon' => 'dashicons-groups',
                    'supports' => array('title', 'revisions', 'thumbnail', 'author', 'page-attributes'),
                    'hierarchical' => false,
                    'exclude_from_search' => false,
                    'public' => true,
                    'has_archive' => false,
                    'rewrite' => [
                        'slug' => 'about-gsoc/ombudsman-commissioners'
                    ],
                    'show_in_menu' => true,
                    'show_ui' => true,
                    'capability_type' => array('person', 'people'),
                    'map_meta_cap' => true,
                )
            );
        });
    }

    /**
     *
     */
    private function registerTaxonomies()
    {
        \register_taxonomy(
            'person_categories',
            'person',
            array(
                'labels' => array(
                    'name' => 'People Categories',
                    'add_new_item' => 'Add New Category',
                    'new_item_name' => "New Category"
                ),
                'show_ui' => true,
                'show_tagcloud' => false,
                'hierarchical' => true
            )
        );
    }

    /**
     *
     */
    private function registerCustomFields()
    {
        acf_add_local_field_group([
            'key' => 'acf-person-details',
            'title' => 'Person Details',
            'display' => 'table',
            'fields' => [
                [
                    'key' => 'acf-person-details-name',
                    'label' => 'Staff Member Name',
                    'name' => 'name',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ],
                [
                    'key' => 'acf-person-details-role',
                    'label' => 'Staff Member Role',
                    'name' => 'role',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ],
                [
                    'key' => 'acf-person-details-details',
                    'label' => 'Staff Member Details',
                    'name' => 'details',
                    'type' => 'wysiwyg',
                    'instructions' => '',
                    'required' => 0,
                    'tabs' => 'all',
                    'toolbar' => 'full',
                    'media_upload' => 0,
                    'new_lines' => 'wpautop',
                ],
                [
                    'key' => 'acf-person-details-twitter',
                    'label' => 'Social Media Twitter',
                    'name' => 'twitter',
                    'type' => 'text',
                    'instructions' => 'Twitter Username',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '@JohnSmith',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ],
                [
                    'key' => 'acf-person-details-linkedin',
                    'label' => 'Social Media LinkedIn',
                    'name' => 'linkedin',
                    'type' => 'url',
                    'instructions' => 'Insert LinkedIn URL to profile',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                ],
                [
                    'key' => 'acf-person-details-email',
                    'label' => 'Staff Member Email Address',
                    'name' => 'email',
                    'type' => 'email',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                ]
            ],

            'location' => [
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'person',
                    ],
                ],
            ],

            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ]);
    }

}