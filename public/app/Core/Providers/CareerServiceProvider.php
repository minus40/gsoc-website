<?php

namespace Hailstone\Core\Providers;

use Hailstone\Core\Career;
use Hailstone\Core\Support\ServiceProvider;

class CareerServiceProvider extends ServiceProvider
{
    /**
     *
     */
    public function register()
    {
        $this->app->bind(Career::class);

        $this->registerPostType();
        $this->registerTaxonomies();
        $this->registerCustomFields();
    }

    private function registerPostType()
    {
        \add_action( 'init', function()
        {
            \register_post_type( 'career',
                array(
                    'labels' => [
                        'name' => __( 'Careers' ),
                        'singular_name' => __( 'Career' ),
                        'add_new' => "Add Career",
                        'add_new_item' => "Add Career",
                        'all_items' => 'All Careers',
                        'view_item' => "View Career",
                        'edit_item' => "Edit Career",
                        'new_item' => "New Career",
                        'featured_image' => 'Featured Image'
                    ],
                    'menu_position' => 90,
                    'menu_icon' => 'dashicons-hammer',
                    'supports' => array('title', 'revisions', 'thumbnail', 'author', 'page-attributes'),
                    'hierarchical' => false,
                    'exclude_from_search' => false,
                    'public' => true,
                    'has_archive' => false,
                    'rewrite' => [
                        'slug' => 'gardai/careers'
                    ],
                    'show_in_menu' => true,
                    'show_ui' => true,
                    'capability_type' => array('career', 'careers'),
                    'map_meta_cap' => true,
                )
            );
        });
    }

    /**
     *
     */
    private function registerTaxonomies()
    {
        \register_taxonomy(
            'career_categories',
            'career',
            array(
                'labels' => array(
                    'name' => 'Career Categories',
                    'add_new_item' => 'Add New Category',
                    'new_item_name' => "New Category"
                ),
                'show_ui' => true,
                'show_tagcloud' => false,
                'hierarchical' => true
            )
        );
    }

    /**
     *
     */
    private function registerCustomFields()
    {
        acf_add_local_field_group([
            'key' => 'acf-career-details',
            'title' => 'Career Details',
            'display' => 'table',
            'fields' => [
                [
                    'key' => 'acf-career-details-ends-at',
                    'label' => 'Closing Date and Time',
                    'name' => 'ends_at',
                    'type' => 'date_time_picker',
                    'instructions' => 'Careers listings will be dynamically removed from the website after their end date.',
                    'required' => 1,
                    'display_format' => 'd/m/Y g:i a',
                    'return_format' => 'Y-m-d H:i:s',
                    'first_day' => 1,
                ],
                [
                    'key' => 'acf-career-details-location',
                    'label' => 'Job Location',
                    'name' => 'location',
                    'type' => 'text',
                    'instructions' => 'Provide a name for the job location that users can understand.',
                    'required' => 0,
                    'maxlength' => 64,
                    'readonly' => 0,
                    'disabled' => 0,
                ],
                [
                    'key' => 'acf-career-details-salary',
                    'label' => 'Job Salary',
                    'name' => 'salary',
                    'type' => 'text',
                    'instructions' => 'Provide the expected salary for the job. You may enter text as well as a currency value.',
                    'required' => 0,
                    'maxlength' => 64,
                    'readonly' => 0,
                    'disabled' => 0,
                ],
            ],
            [
                'key' => 'acf-career-details-application',
                'label' => 'Job Application Form',
                'name' => 'application',
                'type' => 'gravity_forms_field',
                'instructions' => 'Select an application form. Leave blank if there is another method of application.',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'taxonomy' => array (
                ),
                'allow_null' => 0,
                'multiple' => 0,
                'return_format' => 'id',
                'ui' => 1,
            ],

            'location' => [
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'career',
                    ],
                ],
            ],

            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ]);
    }

}

