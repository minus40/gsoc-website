<?php

namespace Hailstone\Core\Providers;

use Hailstone\Core\Article;
use Hailstone\Core\Carousel;
use Hailstone\Core\Support\ServiceProvider;

class CarouselServiceProvider extends ServiceProvider
{
    /**
     *
     */
    public function register()
    {
        $this->app->bind(Carousel::class);
    }
}