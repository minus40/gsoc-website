<?php

namespace Hailstone\Core\Providers;

use Hailstone\Core\CaseStudy;
use Hailstone\Core\Support\ServiceProvider;

class CaseStudyServiceProvider extends ServiceProvider
{
    /**
     *
     */
    public function register()
    {
        $this->app->bind(CaseStudy::class);

        $this->registerPostType();
        $this->registerTaxonomies();
    }

    /**
     *
     */
    private function registerPostType()
    {
        \add_action( 'init', function()
        {
            \register_post_type( 'case-study',
                [
                    'labels' => [
                        'name' => __( 'Case Studies' ),
                        'singular_name' => __( 'Case Study' ),
                        'add_new' => "Add Case Study",
                        'add_new_item' => "Add Case Study",
                        'all_items' => 'All Case Studies',
                        'view_item' => "View Case Study",
                        'edit_item' => "Edit Study",
                        'new_item' => "New Case Study",
                        'featured_image' => 'Featured Image'
                    ],
                    'menu_position' => 93,
                    'menu_icon' => 'dashicons-list-view',
                    'supports' => array('title', 'revisions', 'thumbnail', 'author', 'page-attributes'),
                    'hierarchical' => false,
                    'exclude_from_search' => false,
                    'public' => true,
                    'has_archive' => false,
                    'rewrite' => [
                        'slug' => 'make-a-complaint/before-you-complain/how-we-deal-with-complaints/case-summaries'
                    ],
                    'show_in_menu' => true,
                    'show_ui' => true,
                    'capability_type' => array('casestudy', 'casestudies'),
                    'map_meta_cap' => true,
                ]
            );
        });
    }

    /**
     *
     */
    private function registerTaxonomies()
    {
        \register_taxonomy(
            'case_study_categories',
            'case-study',
            [
                'labels' => [
                    'name' => 'Case Study Categories',
                    'add_new_item' => 'Add New Category',
                    'new_item_name' => "New Category"
                ],
                'show_ui' => true,
                'show_tagcloud' => false,
                'hierarchical' => true,
            ]
        );
    }
}