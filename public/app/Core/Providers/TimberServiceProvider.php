<?php

namespace Hailstone\Core\Providers;

use AdamWathan\Form\FormBuilder;
use Hailstone\Core\Forms\ErrorStore;
use Hailstone\Core\Forms\OldInput;
use Hailstone\Core\Menu;
use Hailstone\Core\Option;
use Hailstone\Core\Support\ServiceProvider;
use Timber\Timber;

class TimberServiceProvider extends ServiceProvider
{
    public function __construct()
    {
        if(!class_exists(Timber::class)) throw new \Exception("Timber is missing.");
    }

    public function register()
    {
        $timber = new Timber();

        /** This is information shared to every view */
        add_filter('timber/context', function($context)
        {
            global $app;

            $context['primary_menu'] = new Menu('primary');
            $context['footer_menu'] = new Menu('footer');
            $context['options'] = (new Option())->all();
            $context['config'] = config();

            $context['session'] = $app->make('session');
            $context['language'] = ($app->make('session'))->get('language') ? ($app->make('session'))->get('language') : 'en';
            $context['builder'] = new FormBuilder();
            $context['builder']->setOldInputProvider(new OldInput($app));
            $context['errors'] = $app->make('session')->getFlash('errors');
            $context['builder']->setErrorStore(new ErrorStore((array) $context['errors']));

            return $context;
        });

        return $timber;
    }
}