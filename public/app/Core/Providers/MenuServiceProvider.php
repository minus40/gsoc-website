<?php

namespace Hailstone\Core\Providers;

//use Hailstone\Core\Option;
use Hailstone\Core\Support\ServiceProvider;

class MenuServiceProvider extends ServiceProvider
{
    /**
     *
     */
    public function register()
    {
        //$this->app->bind(Option::class);
        $this->registerMenus();
    }

    /**
     *
     */
    private function registerMenus()
    {
        foreach(config('menus') as $key => $name) {
            \register_nav_menu( $key, $name );
        }
    }
}