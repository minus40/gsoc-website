<?php

namespace Hailstone\Core\Providers;

use Hailstone\Core\Services\DownloadLogger;
use Hailstone\Core\Support\ServiceProvider;

class DocumentLibraryServiceProvider extends ServiceProvider
{
    public function register()
    {
        if (function_exists('acf_add_local_field_group')) {
            $this->registerDocumentLibrary();
        }

        $this->forceDownloads();
        $this->addAttachmentDate();
        $this->addAttachmentAuthor();
        $this->saveCustomFieldData();
    }

    private function registerDocumentLibrary()
    {
        acf_add_local_field_group([
            'key' => 'acf-document-library',
            'title' => 'Assign Document Libraries to this Page',
            'fields' => [

                [
                    'key' => 'acf-document-library-libraries',
                    'label' => 'Related Libraries',
                    'name' => 'document_libraries',
                    'type' => 'repeater',
                    'instructions' => 'Select one or more libraries to include as download list groups.',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'min' => '',
                    'max' => 10,
                    'layout' => 'row',
                    'button_label' => 'Add Library',
                    'sub_fields' => [
                        [
                            'key' => 'acf-document-library-libraries-library',
                            'label' => 'Related Library',
                            'name' => 'library',
                            'type' => 'taxonomy',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'taxonomy' => 'media_category',
                            'field_type' => 'select',
                            'allow_null' => 1,
                            'add_term' => 0,
                            'load_save_terms' => 0,
                            'return_format' => 'id',
                            'multiple' => 0,
                            'load_terms' => 0,
                            'save_terms' => 0,
                        ],
                        [
                            'key' => 'acf-document-library-libraries-library-children',
                            'label' => 'Include children?',
                            'name' => 'include_children',
                            'type' => 'radio',
                            'instructions' => 'Would you like children libraries of this library to be displayed?',
                            'required' => 1,
                            'conditional_logic' => 0,
                            'wrapper' => [
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ],
                            'choices' => [
                                'no' => 'No',
                                'yes' => 'Yes',
                            ],
                            'other_choice' => 0,
                            'save_other_choice' => 0,
                            'default_value' => 'no',
                            'layout' => 'horizontal',
                            'allow_null' => 0,
                        ],
                    ],
                    'collapsed' => '',
                ],

            ],
            'location' => [
                [
                    [
                        'param' => 'page_template',
                        'operator' => '==',
                        'value' => 'page-document-library.php',
                    ],
                ],
            ],
            'menu_order' => 2,
            'position' => 'acf_after_title',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
        ]);
    }

    /**
     * replaced Oct 24 for viewing inline
     */
    private function forceDownloads()
    {
        add_action('template_redirect', function() {
            if(isset($_GET['download']) && $_GET['download'] == 'file' && isset($_GET['file']) && is_numeric($_GET['file'])) {

                $attachment = \Attachment::find($_GET['file']);


                if ($attachment):

                    header("Pragma: public");
                    header("Expires: 0");
                    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                    header("Content-type: ".$attachment->mime."");
                    header("Content-disposition: inline; filename=\"".$attachment->filename."\"");
                    header("Content-Length: ".$attachment->size);

                    $content = @file_get_contents($attachment->relativePath());

                    if($content === FALSE)
                    {
                        wp_redirect("/404");
                        exit;                     
                    }


                    echo $content;

                    $referrerId = get_the_ID();
                    if ( wp_get_referer() ) $referrerId = url_to_postid( wp_get_referer() );

                    $logger = new DownloadLogger();
                    $logger->recordDownload($attachment, $referrerId);

                    exit;

                else:
                    wp_redirect("/404");
                    exit;
                endif;


            }
        });
    }

    /*private function forceDownloads()
    {
        add_action('template_redirect', function() {
            if(!is_admin() && isset($_GET['download']) && $_GET['download'] == 'file' && isset($_GET['file']) && is_numeric($_GET['file'])) 
            {
                $attachment = \Attachment::find($_GET['file']);

                wp_redirect( $attachment->url() );

                $referrerId = get_the_ID();
                if ( wp_get_referer() ) $referrerId = url_to_postid( wp_get_referer() );

                if (!isset($_GET['notrack']))
                {
                    $logger = new DownloadLogger();
                    $logger->recordDownload($attachment, $referrerId);
                }
            }
        });
    }*/

    /**
     *
     */
    private function addAttachmentDate()
    {
        add_filter('attachment_fields_to_edit', function($form_fields, $post) {
            $form_fields["post_date"]["tr"] = "
                <th scope='row' class='label'><label for='attachments[" . $post->ID . "][post_date]' name='attachments[" . $post->ID . "][post_date]'><span class='alignleft'>Set File Date:</span><br class='clear'></label></th>
                <tr id='date-picker'>
                    <td>
                        <input type='date' id='attachments[" . $post->ID . "][post_date]' name='attachments[" . $post->ID . "][post_date]' value='" . mysql2date('Y-m-d', $post->post_date) . "'/>
                    </td>
                </tr>";
            return $form_fields;
        }, 10, 2);
    }

    /**
     *
     */
    private function addAttachmentAuthor()
    {
        add_filter('attachment_fields_to_edit', function($form_fields, $post) {
            $authorID = $post->post_author;

            $users = \User::all();

            $options = '';

            foreach($users as $user) {
                $selected = '';

                if ($authorID == $user->ID) {
                    $selected = ' selected ';
                }

                $options .= "<option " . $selected . " value='" . $user->ID . "'>" . $user->name . "</option>";
            }

            $form_fields["post_author"]["tr"] = "
                <th scope='row' class='label'><label for='attachments[" . $post->ID . "][post_author]' name='attachments[" . $post->ID . "][post_author]'><span class='alignleft'>Document Author</span><br class='clear'></label></th>
                <tr id='author-picker'>
                    <td>
                        <select name='attachments[{$post->ID}][post_author]' id='attachments[{$post->ID}][post_author]'>" .
                        $options .
                        "</select>
                    </td>
                </tr>";
            return $form_fields;
        }, 10,2);
    }

    /**
     *
     */
    private function saveCustomFieldData()
    {
        add_filter( 'attachment_fields_to_save', function($post, $attachments) {
            $post['post_date'] = $attachments['post_date'];
            $post['post_author'] = $attachments['post_author'];
            return $post;
        }, 10, 2);
    }
}