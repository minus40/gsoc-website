<?php

namespace Hailstone\Core\Providers;

use Hailstone\Core\Video;
use Hailstone\Core\Support\ServiceProvider;

class VideoServiceProvider extends ServiceProvider
{
    /**
     *
     */
    public function register()
    {
        $this->app->bind(Video::class);

        $this->registerPostType();
        $this->registerVideoEditor();
    }

    private function registerPostType()
    {
        \add_action( 'init', function()
        {
            register_post_type( 'video',
                [
                    'labels' => [
                        'name' => __( 'Videos' ),
                        'singular_name' => __( 'Video' ),
                        'add_new' => "Add Video",
                        'add_new_item' => "Add Video",
                        'all_items' => 'All Videos',
                        'view_item' => "View Video",
                        'edit_item' => "Edit Video",
                        'new_item' => "New Video",
                    ],
                    'menu_position' => 99,
                    'menu_icon' => 'dashicons-video-alt3',
                    'supports' => array('title', 'revisions', 'thumbnail', 'author', 'page-attributes'),
                    'hierarchical' => false,
                    'exclude_from_search' => false,
                    'public' => true,
                    'has_archive' => false,
                    'rewrite' => [
                        'slug' => 'publications/videos'
                    ],
                    'show_in_menu' => true,
                    'show_ui' => true,
                    'capability_type' => array('video', 'videos'),
                    'map_meta_cap' => true,
                ]
            );
        });
    }

    private function registerVideoEditor()
    {
        acf_add_local_field_group(array (
            'key' => 'acf-video',
            'title' => 'Video Content',
            'fields' => [
                [
                    'key' => 'acf-video-introduction',
                    'label' => 'Introduction',
                    'name' => 'introduction',
                    'type' => 'textarea',
                    'instructions' => 'A short description of this video to appear on the site.',
                    'maxlength' => 1000,
                    'rows' => 3,
                    'new_lines' => 'br',
                ],
                [
                    'key' => 'acf-video-id',
                    'label' => 'Video ID',
                    'name' => 'video_id',
                    'type' => 'text',
                    'instructions' => 'You should get the unique ID for this media clip. For example in http://youtube.com/watch?v=ngElkyQ6Rhs, it is everything after the "v=" i.e. ngElkyQ6Rhs',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'maxlength' => 20,
                    'readonly' => 0,
                    'disabled' => 0,
                ],
                [
                    'key' => 'source',
                    'label' => 'Source Website',
                    'name' => 'source',
                    'type' => 'select',
                    'instructions' => 'You must select from these approved host sites.',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'choices' => [
                        'youtube' => 'YouTube',
                        'vimeo' => 'Vimeo'
                    ],
                ],
            ],
            'location' => array (
                array (
                    array (
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'video',
                    ),
                ),
            ),
            'menu_order' => 1,
            'position' => 'acf_after_title',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
        ));
    }
}