<?php

namespace Hailstone\Core\Providers;

use Hailstone\Core\Support\ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    public function register()
    {
        require_once(__DIR__ . '/../Http/Routes/web.php');
    }
}