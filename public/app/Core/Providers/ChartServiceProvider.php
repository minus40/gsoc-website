<?php

namespace Hailstone\Core\Providers;

use Hailstone\Core\Chart;
use Hailstone\Core\Support\ServiceProvider;

class ChartServiceProvider extends ServiceProvider
{
    /**
     *
     */
    public function register()
    {
        $this->app->bind(Chart::class);

        $this->registerPostType();
        $this->registerTaxonomies();
        $this->registerFields();
    }

    /**
     *
     */
    private function registerPostType()
    {
        \add_action( 'init', function()
        {
            \register_post_type( 'chart',
                [
                    'labels' => [
                        'name' => __( 'Charts' ),
                        'singular_name' => __( 'Chart' ),
                        'add_new' => "Add Chart",
                        'add_new_item' => "Add Chart",
                        'all_items' => 'All Charts',
                        'view_item' => "View Chart",
                        'edit_item' => "Edit Chart",
                        'new_item' => "New Chart",
                        'featured_image' => 'Featured Image'
                    ],
                    'menu_position' => 93,
                    'menu_icon' => 'dashicons-chart-pie',
                    'supports' => array('title', 'revisions', 'thumbnail', 'author', 'page-attributes'),
                    'hierarchical' => false,
                    'exclude_from_search' => false,
                    'public' => true,
                    'has_archive' => false,
                    'show_in_menu' => true,
                    'show_ui' => true,
                    'capability_type' => array('chart', 'charts'),
                    'map_meta_cap' => true,
                ]
            );
        });
    }

    /**
     *
     */
    private function registerTaxonomies()
    {
        \register_taxonomy(
            'chart_categories',
            'chart',
            [
                'labels' => [
                    'name' => 'Chart Categories',
                    'add_new_item' => 'Add New Category',
                    'new_item_name' => "New Category"
                ],
                'show_ui' => true,
                'show_tagcloud' => false,
                'hierarchical' => true,
            ]
        );
    }

    private function registerFields()
    {

        \acf_add_local_field_group([
            'key' => 'acf-chart',
            'title' => 'Chart',
            'fields' => [
                [
                    'key' => 'acf-chart-type',
                    'label' => 'Chart Type',
                    'name' => 'type',
                    'type' => 'select',
                    'instructions' => 'Please select the type of chart you would like to create. This will determine the data required.',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => [
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ],
                    'choices' => [
                        'bar' => 'Bar',
                        'doughnut' => 'Doughnut',
                        'heatmap' => 'Heat Map',
                        'horizontalBar' => 'Horizontal Bar',
                        'line' => 'Line',
                        'pie' => 'Pie',
                        'polarArea' => 'Polar',
                        'table' => 'Table',
                    ],
                    'default_value' => [],
                    'allow_null' => 0,
                    'multiple' => 0,
                    'ui' => 0,
                    'ajax' => 0,
                    'placeholder' => '',
                    'disabled' => 0,
                    'readonly' => 0,
                ],
                [
                    'key' => 'acf-chart-label',
                    'label' => 'Chart Label',
                    'name' => 'label',
                    'type' => 'text',
                    'instructions' => 'Please enter a meaningful label for the data on your chart.',
                    'placeholder' => '',
                    'disabled' => 0,
                    'readonly' => 0,
                ],
                [
                    'key' => 'acf-chart-heatmap-data',
                    'label' => 'Heat Map',
                    'name' => 'heatmap-data',
                    'type' => 'select',
                    'instructions' => 'Please select the data you would like to display on your heatmap.',
                    'required' => 0,
                    'conditional_logic' => [
                        [
                            [
                                'field' => 'acf-chart-type',
                                'operator' => '==',
                                'value' => 'heatmap',
                            ],
                        ],
                    ],
                    'choices' => [
                        'geographical-distribution-2020' => 'Geographical Distribution 2020',
                        'geographical-distribution-2018' => 'Geographical Distribution 2018',
                        'geographical-distribution-2017' => 'Geographical Distribution 2017',
                        'geographical-distribution-2016' => 'Geographical Distribution 2016',
                    ],
                    'default_value' => [],
                    'allow_null' => 0,
                    'multiple' => 0,
                    'ui' => 0,
                    'ajax' => 0,
                    'placeholder' => '',
                    'disabled' => 0,
                    'readonly' => 0,
                ],

                [
                    'key' => 'acf-chart-line-dataset',
                    'label' => 'Line Chart Data',
                    'name' => 'line_dataset',
                    'type' => 'repeater',
                    'instructions' => 'Add a dataset. Think of a dataset as an individual line on the chart.',
                    'required' => 0,
                    'conditional_logic' => [
                        [
                            [
                                'field' => 'acf-chart-type',
                                'operator' => '==',
                                'value' => 'line',
                            ],
                        ],
                    ],
                    'max' => 12,
                    'layout' => 'table',
                    'button_label' => 'Add Dataset',
                    'sub_fields' => [
                        [
                            'key' => 'acf-chart-line-dataset-data',
                            'label' => 'Dataset',
                            'name' => 'data',
                            'type' => 'repeater',
                            'instructions' => 'Begin adding your data. Labels will appear along the X axis. The numerical data determines the line plotted on your chart.',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'max' => 12,
                            'layout' => 'table',
                            'button_label' => 'Add Data',
                            'sub_fields' => [
                                [
                                    'key' => 'acf-chart-line-dataset-data-label',
                                    'label' => 'Label',
                                    'name' => 'label',
                                    'type' => 'text',
                                    'instructions' => '',
                                    'required' => 0,
                                    'conditional_logic' => 0,
                                    'maxlength' => '16',
                                    'readonly' => 0,
                                    'disabled' => 0,
                                ],
                                [
                                    'key' => 'acf-chart-line-dataset-data-data',
                                    'label' => 'Data',
                                    'name' => 'data',
                                    'type' => 'number',
                                    'instructions' => '',
                                    'required' => 0,
                                    'conditional_logic' => 0,
                                    'disabled' => 0,
                                ],
                            ],
                        ],
                    ],
                ],
                [
                    'key' => 'act-chart-data',
                    'label' => 'Chart Data',
                    'name' => 'chart_data',
                    'type' => 'repeater',
                    'instructions' => 'Begin adding your data. Labels must be paired with a numerical value. You can preview your chart before publishing to the site.',
                    'required' => 0,
                    'conditional_logic' => [
                        [
                            [
                                'field' => 'acf-chart-type',
                                'operator' => '==',
                                'value' => 'bar',
                            ],
                        ],
                        [
                            [
                                'field' => 'acf-chart-type',
                                'operator' => '==',
                                'value' => 'horizontalBar',
                            ],
                        ],
                        [
                            [
                                'field' => 'acf-chart-type',
                                'operator' => '==',
                                'value' => 'doughnut',
                            ],
                        ],
                        [
                            [
                                'field' => 'acf-chart-type',
                                'operator' => '==',
                                'value' => 'pie',
                            ],
                        ],
                        [
                            [
                                'field' => 'acf-chart-type',
                                'operator' => '==',
                                'value' => 'polarArea',
                            ],
                        ],
                    ],
                    'max' => 12,
                    'layout' => 'table',
                    'button_label' => 'Add Data',
                    'sub_fields' => [
                        [
                            'key' => 'acf-chart-data-label',
                            'label' => 'Label',
                            'name' => 'label',
                            'type' => 'text',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'readonly' => 0,
                            'disabled' => 0,
                        ],
                        [
                            'key' => 'acf-chart-data-data',
                            'label' => 'Data',
                            'name' => 'data',
                            'type' => 'number',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'disabled' => 0,
                        ],
                    ],
                ],
                [
                    'key' => 'acf-chart-notes',
                    'label' => 'Chart Notes',
                    'name' => 'notes',
                    'type' => 'text',
                    'instructions' => 'Please enter any notes to feature in the footer of the chart.',
                    'placeholder' => '',
                    'disabled' => 0,
                    'readonly' => 0,
                ],
                [
                    'key' => 'acf-chart-table',
                    'label' => 'Table',
                    'name' => 'table',
                    'type' => 'table',
                    'instructions' => 'You may also display data in an HTML table on the site.',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'use_header' => 0,
                ],
            ],
            'location' => [
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'chart',
                    ],
                ],
            ],
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ]);
    }

}