<?php

namespace Hailstone\Core\Providers;

use Hailstone\Core\Attachment;
use Hailstone\Core\Functions\Admin;
use Hailstone\Core\Functions\Cleanup;
use Hailstone\Core\Functions\Images;
use Hailstone\Core\Functions\Mail;
use Hailstone\Core\Functions\Menus;
use Hailstone\Core\Functions\Scripts;
use Hailstone\Core\Image;
use Hailstone\Core\Page;
use Hailstone\Core\Support\ServiceProvider;
use Hailstone\Core\User;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->includeFunctions();
        $this->app->bind(Image::class);

        $this->app->bind(Page::class);

        $this->app->bind(Attachment::class);
        $this->app->bind(User::class);

        $this->registerCustomFields();
    }

    /**
     *
     */
    private function includeFunctions()
    {
        new Admin();
        new Cleanup();
        new Mail();
        new Menus();
        new Images();
        new Scripts();

    }

    /**
     *
     */
    private function registerCustomFields()
    {
        acf_add_local_field_group([
            'key' => 'acf-hide-options',
            'name' => 'hide_options',
            'title' => 'Content Visibility Options',
            'fields' => [
                [
                    'key' => 'acf-hide-options-navigation',
                    'label' => 'Hide Supporting Nav Bar',
                    'name' => 'hide_navigation',
                    'type' => 'true_false',
                    'instructions' => 'Check this box if you would prefer not to display a (right-hand) sub navigation bar on this page.',
                    'required' => 0,
                    'default_value' => 0,
                ],
                [
                    'key' => 'acf-hide-options-image',
                    'label' => 'Hide Featured Image on Page',
                    'name' => 'hide_image',
                    'type' => 'true_false',
                    'instructions' => 'Check this box if you would prefer not to use the featured image as this page\'s introduction (i.e. it is for thumbnail purposes only).',
                    'required' => 0,
                    'default_value' => 0,
                ],
//                [
//                    'key' => 'acf-hide-options-social',
//                    'label' => 'Hide Social Sharing on Page',
//                    'name' => 'hide_social',
//                    'type' => 'true_false',
//                    'instructions' => 'Check this box if you would prefer not to show options for sharing this page or post on social media.',
//                    'required' => 0,
//                    'default_value' => 0,
//                ],
            ],
            'location' => [
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'page',
                    ],
                ],
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'article',
                    ],
                ],
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'career',
                    ],
                ],
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'chart',
                    ],
                ],
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'event',
                    ],
                ],
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'faq',
                    ],
                ],
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'location',
                    ],
                ],
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'person',
                    ],
                ],
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'product',
                    ],
                ],
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'report',
                    ],
                ],
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'service',
                    ],
                ],
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'statistic',
                    ],
                ],
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'video',
                    ],
                ],
            ],
            'menu_order' => 99,
            'position' => 'side',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
        ]);
    }
}