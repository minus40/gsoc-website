<?php

namespace Hailstone\Core\Providers;

use Hailstone\Core\Support\ServiceProvider;

class GoogleMapsServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->configureACF();
    }

    private function configureACF()
    {
        add_action('acf/init', function ()
        {
            acf_update_setting('google_api_key', env('GOOGLE_API_KEY'));
        });
    }
}