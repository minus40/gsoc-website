<?php

namespace Hailstone\Core\Providers;

use Hailstone\Core\Article;
use Hailstone\Core\Support\ServiceProvider;

class ArticleServiceProvider extends ServiceProvider
{
    /**
     *
     */
    public function register()
    {
        $this->app->bind(Article::class);

        $this->registerPostType();
        $this->registerTaxonomies();
        //$this->archiveRewrites();
    }

    private function registerPostType()
    {
        \add_action( 'init', function()
        {
            \register_post_type( 'article',
                array(
                    'labels' => [
                        'name' => __( 'News' ),
                        'singular_name' => __( 'News Article' ),
                        'add_new' => "Add News Article",
                        'add_new_item' => "Add News Article",
                        'all_items' => 'All News',
                        'view_item' => "View News Article",
                        'edit_item' => "Edit News Article",
                        'new_item' => "New News Article",
                        'featured_image' => 'Featured Image'
                    ],
                    'menu_position' => 90,
                    'menu_icon' => 'dashicons-welcome-write-blog',
                    'supports' => array('title', 'revisions', 'thumbnail', 'author'),
                    'exclude_from_search' => false,
                    'public' => true,
                    'has_archive' => false,
                    'rewrite' => [
                        'slug' => 'news-room/archive'
                    ],
                    'taxonomy' => 'article_categories',
                    'show_in_menu' => true,
                    'show_ui' => true,
                    'capability_type' => array('article', 'articles'),
                    'map_meta_cap' => true,
                )
            );
        });
    }

    private function archiveRewrites()
    {
        // Add day archive (and pagination)
        add_rewrite_rule("news-room/archive/([0-9]{4})/([0-9]{2})/([0-9]{2})/page/?([0-9]{1,})/?",'index.php?post_type=book&year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]','top');
        add_rewrite_rule("news-room/archive/([0-9]{4})/([0-9]{2})/([0-9]{2})/?",'index.php?post_type=book&year=$matches[1]&monthnum=$matches[2]&day=$matches[3]','top');

        // Add month archive (and pagination)
        add_rewrite_rule("news-room/archive/([0-9]{4})/([0-9]{2})/page/?([0-9]{1,})/?",'index.php?post_type=book&year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]','top');
        add_rewrite_rule("news-room/archive/([0-9]{4})/([0-9]{2})/?",'index.php?post_type=book&year=$matches[1]&monthnum=$matches[2]','top');

        // Add year archive (and pagination)
        add_rewrite_rule("news-room/archive/([0-9]{4})/page/?([0-9]{1,})/?",'index.php?post_type=book&year=$matches[1]&paged=$matches[2]','top');
        add_rewrite_rule("news-room/archive/([0-9]{4})/?",'index.php?post_type=book&year=$matches[1]','top');
    }

    /**
     *
     */
    private function registerTaxonomies()
    {
        \register_taxonomy(
            'article_categories',
            'article',
            array(
                'labels' => array(
                    'name' => 'News Categories',
                    'add_new_item' => 'Add New Category',
                    'new_item_name' => "New Category"
                ),
                'show_ui' => true,
                'show_tagcloud' => false,
                'hierarchical' => true,
                'rewrite' => [
                    'slug' => 'news/category'
                ],
            )
        );
    }
}