<?php

namespace Hailstone\Core\Forms;

use AdamWathan\Form\OldInput\OldInputInterface;
use Illuminate\Support\Collection;

class OldInput implements OldInputInterface
{
    private $session;
    private $input;

    public function __construct($app)
    {
        $this->session = $app->make('session');
        $this->input = new Collection($this->session->getFlash('oldInput'));
    }

    /**
     * @return bool
     */
    public function hasOldInput()
    {
        return $this->input->count();
    }

    /**
     * @param $key
     *
     * @return mixed
     */
    public function getOldInput($key)
    {
        return $this->input->has($key) ? $this->input[$key] : false;
    }
}