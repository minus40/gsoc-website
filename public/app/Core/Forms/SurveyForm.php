<?php

namespace Hailstone\Core\Forms;

use Hailstone\Core\Forms\Validator;
use Illuminate\Container\Container;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Translation\Translator;
use PDOException;

class SurveyForm
{

    public function process_form()
    {
        add_action('gform_after_submission_5', function ($entry) 
        {
            $data = [
                'sex' => rgar($entry, '2'),
                'age' => rgar($entry, '3'),
                'nationality' => rgar($entry, '4'),
                'nationality_other' => rgar($entry, '13'),
                'countryofbirth' => rgar($entry, '14'),
                'other_countryofbirth' => rgar($entry, '15'),
                'ethnicbackground' => rgar($entry, '5'),
                'other_ethnicbackground' => rgar($entry, '16'),
                'primarylanguage' => rgar($entry, '6'),
                'other_primarylanguage' => rgar($entry, '17'),
                'disability' => rgar($entry, '7'),
                'other_disability' => rgar($entry, '18'),
                'religion' => rgar($entry, '8'),
                'other_religion' => rgar($entry, '19'),
                'sexualorientation' => rgar($entry, '9'),
                'other_sexualorientation' => rgar($entry, '20'),
                'housingstatus' => rgar($entry, '10'),
                'other_housingstatus' => rgar($entry, '21'),
                'education' => rgar($entry, '11'),
                'other_education' => rgar($entry, '22'),
                'employmentstatus' => (boolean) rgar($entry, '12'),
                'other_employmentstatus' => rgar($entry, '23'),
            ];

            // var_dump($entry); echo json_encode($data); exit;

            try {

                $conn = new \PDO('sqlsrv:Server=' . env("SURVEY_DB_HOST") . ";Database=" . env('SURVEY_DB_NAME'), env('SURVEY_DB_USER'), env('SURVEY_DB_PASSWORD'));
                $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

                $stmt = $conn->prepare("INSERT INTO survey (id, sex, age, nationality, nationality_other, countryofbirth, other_countryofbirth, ethnicbackground, other_ethnicbackground, primarylanguage, other_primarylanguage, disability, other_disability, religion, other_religion, sexualorientation, other_sexualorientation, housingstatus, other_housingstatus, education, other_education, employmentstatus, other_employmentstatus, datetimestamp, deleted) 
                              VALUES (:id, :sex, :age, :nationality, :nationality_other, :countryofbirth, :other_countryofbirth, :ethnicbackground, :other_ethnicbackground, :primarylanguage, :other_primarylanguage, :disability, :other_disability, :religion, :other_religion, :sexualorientation, :other_sexualorientation, :housingstatus, :other_housingstatus, :education, :other_education, :employmentstatus, :other_employmentstatus, :datetimestamp, :deleted)");

                $current_time = current_time('mysql');
                $deleted = 0;
                $stmt->bindParam(':sex', $this->$data['sex']);
                $stmt->bindParam(':age', $this->$data['age']);
                $stmt->bindParam(':nationality', $this->$data['nationality']);
                $stmt->bindParam(':nationality_other', $this->$data['nationality_other']);
                $stmt->bindParam(':countryofbirth', $this->$data['countryofbirth']);
                $stmt->bindParam(':other_countryofbirth', $this->$data['other_countryofbirth']);
                $stmt->bindParam(':ethnicbackground', $this->$data['ethnicbackground']);
                $stmt->bindParam(':other_ethnicbackground', $this->$data['other_ethnicbackground']);
                $stmt->bindParam(':primarylanguage', $this->$data['primarylanguage']);
                $stmt->bindParam(':other_primarylanguage', $this->$data['other_primarylanguage']);
                $stmt->bindParam(':disability', $this->$data['disability']);
                $stmt->bindParam(':other_disability', $this->$data['other_disability']);
                $stmt->bindParam(':religion', $this->$data['religion']);
                $stmt->bindParam(':other_religion', $this->$data['other_religion']);
                $stmt->bindParam(':sexualorientation', $this->$data['sexualorientation']);
                $stmt->bindParam(':other_sexualorientation', $this->$data['other_sexualorientation']);
                $stmt->bindParam(':housingstatus', $this->$data['housingstatus']);
                $stmt->bindParam(':other_housingstatus', $this->$data['other_housingstatus']);
                $stmt->bindParam(':education', $this->$data['education']);
                $stmt->bindParam(':other_education', $this->$data['other_education']);
                $stmt->bindParam(':employmentstatus', $this->$data['employmentstatus']);
                $stmt->bindParam(':other_employmentstatus', $this->$data['other_employmentstatus']);
                $stmt->bindParam(':datetimestamp', $current_time);
                $stmt->bindParam(':deleted', $deleted);
                $stmt->execute();

            }

            catch(\Exception $e) {
                dd($e->getMessage());
            }

            $conn = null;

        });

        return true;

    }

}