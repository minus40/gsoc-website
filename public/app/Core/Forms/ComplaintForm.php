<?php

namespace Hailstone\Core\Forms;

use Illuminate\Container\Container;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Translation\Translator;
use \duncan3dc\Sessions\Session;
use QM_DB;
use wpdb;

class ComplaintForm
{
    private array $attributes = [];

    const LOCAL_COMPLAINT_DB_VERSION = 1.1;

    private QM_DB|wpdb $wpdb;
    private string $local_complaint_table_name;

    private $lang;

    public function __construct()
    {
        global $wpdb;
        $this->wpdb = $wpdb;
        $this->local_complaint_table_name = $this->wpdb->prefix . 'local_complaint';

        $language = (app('session'))->get('language');

        if ( ! $language) {
            (app('session'))->set('language', 'en');
        }
        $this->lang = (app('session'))->get('language');

    }

    public function handle(): bool
    {
        global $app;

        $this->setupPost();

        $validator = new Validator($app, $this->lang);
        //$old = new OldInput($app);
        //$this->old = ($old);


        $http_referrer = isset($_SERVER['HTTP_REFERER']) ? rtrim($_SERVER['HTTP_REFERER'],"/") : "";
        $http_origin = isset($_SERVER['HTTP_ORIGIN']) ? rtrim($_SERVER['HTTP_ORIGIN'],"/") : "";
        if($http_referrer === $http_origin){
            $form_url = isset($_POST['form_url']) ? rtrim($_POST['form_url'],"/") : "";
            if($form_url){
                $http_referrer = $form_url;
            }
        }


        if ( ! $validator->validate($this->attributes, $this->rules())) {
            return wp_redirect($http_referrer  . "/#error-warning");
        }

        $this->sendEmailNotification();
        if(!env('USE_DUMMY_COMPLAINT_DB')){
            $this->insertData();
        } else {
            $this->insertDummyData();
        }
        $this->buildPDF();

        

        return wp_redirect($http_referrer. "/confirmation");
    }

    private function sendEmailNotification(): void
    {
        add_filter('wp_mail_content_type', function () {
            return 'text/html';
        });
        //$to = 'complaints@gsoc.ie';  
        $to = env('COMPLAINT_RECIPIENT');
        $yes = "Yes";
        $no = "No";

        $submitted_language = $this->lang ? : "en";

        $message = "<p style='color:rgb(100, 102, 106); font-family:din-2014, sans-serif; font-size:14.4px; font-style:normal; font-weight:300; margin-bottom:20px;'>You have a new ONLINE COMPLAINT FORM record.<br />Please visit <a href='" . env('ADMIN_URL') . "'>Administration Area</a> to get the information</p><br><br><br><br><img src='" . get_template_directory_uri() . "/img/layout/logo.svg' alt='Garda Ombudsman' style='max-width:190px; margin-bottom:40px;'/>";
        $message .= "<h3 style='color:rgb(138, 34, 51); font-family:din-2014, sans-serif; font-size:32px; font-style:normal; font-weight:300; margin-bottom:8px;margin-top:3.2px;'>Complainant Details</h3>";
        $message .= "<hr style='border-bottom:none; border-image-outset:0px; border-image-repeat:stretch; border-image-slice:100%; border-image-source:none; border-image-width:1; border-left:none; border-right:none; border-top-color:rgb(100, 102, 106); border-top-style:dotted; border-top-width:1px; color:rgb(34, 34, 34); height:0px; line-height:24px; margin-bottom:19px; margin-top:20px;'>";
        $message .= "<p style='color:rgb(100, 102, 106); font-family:din-2014, sans-serif; font-size:14.4px; font-style:normal; font-weight:300; margin-bottom:20px;'><strong>Language:</strong><br/>" . $submitted_language . "</p>";
        $message .= "<hr style='border-bottom:none; border-image-outset:0px; border-image-repeat:stretch; border-image-slice:100%; border-image-source:none; border-image-width:1; border-left:none; border-right:none; border-top-color:rgb(100, 102, 106); border-top-style:dotted; border-top-width:1px; color:rgb(34, 34, 34); height:0px; line-height:24px; margin-bottom:19px; margin-top:20px;'>";
        $message .= "<p style='color:rgb(100, 102, 106); font-family:din-2014, sans-serif; font-size:14.4px; font-style:normal; font-weight:300; margin-bottom:20px;'><strong>Name:</strong><br/>" . $this->attributes['complainant_forename'] . ' ' . $this->attributes['complainant_surname'] . "</p>";
        $message .= "<p style='color:rgb(100, 102, 106); font-family:din-2014, sans-serif; font-size:14.4px; font-style:normal; font-weight:300; margin-bottom:20px;'><strong>Address:</strong><br>" . $this->attributes['complainant_primary_address_street_combined'] . "<br/>" . $this->attributes['complainant_primary_address_town'] . "<br/>" . $this->attributes['complainant_primary_address_county'] . "<br/>" . $this->attributes['complainant_primary_address_postcode'] . "<br/>" . $this->attributes['complainant_primary_address_country'] . "</p>";
        $message .= "<p style='color:rgb(100, 102, 106); font-family:din-2014, sans-serif; font-size:14.4px; font-style:normal; font-weight:300; margin-bottom:20px;'><strong>Date of Birth:</strong><br/>" . $this->attributes['complainant_date_of_birth'] . "</p>";
        $message .= "<p style='color:rgb(100, 102, 106); font-family:din-2014, sans-serif; font-size:14.4px; font-style:normal; font-weight:300; margin-bottom:20px;'><strong>Contact Details</strong><br/>Mobile Number:" . $this->attributes['complainant_primary_address_mobile'] . "<br/>Other Contact Number:" . $this->attributes['complainant_primary_address_phone'] . "<br/>Email Address:" . $this->attributes['complainant_primary_address_email'] . "</p>";
        $representative_required_check = $this->attributes['representative_required_check'] == "1";
        $message .= "<p style='color:rgb(100, 102, 106); font-family:din-2014, sans-serif; font-size:14.4px; font-style:normal; font-weight:300; margin-bottom:20px;'><strong>Is this complaint being made on behalf of a third party?</strong><br/>" . ($representative_required_check ? $yes : $no) . "</p>";
        $message .= "<hr style='border-bottom:none; border-image-outset:0px; border-image-repeat:stretch; border-image-slice:100%; border-image-source:none; border-image-width:1; border-left:none; border-right:none; border-top-color:rgb(100, 102, 106); border-top-style:dotted; border-top-width:1px; color:rgb(34, 34, 34); height:0px; line-height:24px; margin-bottom:19px; margin-top:20px;'>";
        if($representative_required_check){
            $message .= "<h3 style='color:rgb(138, 34, 51); font-family:din-2014, sans-serif; font-size:32px; font-style:normal; font-weight:300; margin-bottom:8px;margin-top:3.2px;'>Third Party Representative Details</h3>";
            $message .= "<p style='color:rgb(100, 102, 106); font-family:din-2014, sans-serif; font-size:14.4px; font-style:normal; font-weight:300; margin-bottom:20px;'><strong>Representative Type:</strong><br/>" . $this->attributes['third_party_representative_type_submit'] . "</p>";
            $message .= "<hr style='border-bottom:none; border-image-outset:0px; border-image-repeat:stretch; border-image-slice:100%; border-image-source:none; border-image-width:1; border-left:none; border-right:none; border-top-color:rgb(100, 102, 106); border-top-style:dotted; border-top-width:1px; color:rgb(34, 34, 34); height:0px; line-height:24px; margin-bottom:19px; margin-top:20px;'>";
            $message .= "<p style='color:rgb(100, 102, 106); font-family:din-2014, sans-serif; font-size:14.4px; font-style:normal; font-weight:300; margin-bottom:20px;'><strong>Name:</strong><br/>" . $this->attributes['third_party_representative_forename'] . $this->attributes['third_party_representative_surname'] . "</p>";
            $message .= "<p style='color:rgb(100, 102, 106); font-family:din-2014, sans-serif; font-size:14.4px; font-style:normal; font-weight:300; margin-bottom:20px;'><strong>Date of Birth:</strong><br/>" . $this->attributes['third_party_representative_date_of_birth'] . "</p>";
            $message .= "<p style='color:rgb(100, 102, 106); font-family:din-2014, sans-serif; font-size:14.4px; font-style:normal; font-weight:300; margin-bottom:20px;'><strong>Contact Details:</strong><br/>Mobile Number:" . $this->attributes['third_party_representative_mobile'] . "<br/>Email Address:" . $this->attributes['third_party_representative_email'] . "</p>";
            $message .= "<hr style='border-bottom:none; border-image-outset:0px; border-image-repeat:stretch; border-image-slice:100%; border-image-source:none; border-image-width:1; border-left:none; border-right:none; border-top-color:rgb(100, 102, 106); border-top-style:dotted; border-top-width:1px; color:rgb(34, 34, 34); height:0px; line-height:24px; margin-bottom:19px; margin-top:20px;'>";
            $correspondence_check = $this->attributes['correspondence_check'] == "1";
            $message .= "<h3 style='color:rgb(138, 34, 51); font-family:din-2014, sans-serif; font-size:32px; font-style:normal; font-weight:300; margin-bottom:8px;margin-top:3.2px;'>Correspondence Address</h3>";
            $message .= "<p style='color:rgb(100, 102, 106); font-family:din-2014, sans-serif; font-size:14.4px; font-style:normal; font-weight:300; margin-bottom:20px;'><strong>Was a different Correspondence Address requested?</strong><br/>" . ($correspondence_check ? $yes : $no) . "</p>";
            if($correspondence_check){
                $message .= "<p style='color:rgb(100, 102, 106); font-family:din-2014, sans-serif; font-size:14.4px; font-style:normal; font-weight:300; margin-bottom:20px;'><strong>Address:</strong><br/>" . $this->attributes['complainant_secondary_address_street'] . "<br/>" . $this->attributes['complainant_secondary_address_town'] . "<br/>" . $this->attributes['complainant_secondary_address_county'] . "<br/>" . $this->attributes['complainant_secondary_address_postcode'] . "<br/>" . $this->attributes['complainant_secondary_address_country'] . "</p>";
                $message .= "<p style='color:rgb(100, 102, 106); font-family:din-2014, sans-serif; font-size:14.4px; font-style:normal; font-weight:300; margin-bottom:20px;'><strong>Contact Details:</strong><br/>Mobile Number:" . $this->attributes['complainant_secondary_address_mobile'] . "<br/>Other Contact Number:" . $this->attributes['complainant_secondary_address_phone'] . "<br/>Email Address:" . $this->attributes['complainant_secondary_address_email'] . "</p>";
            }
            $message .= "<hr style='border-bottom:none; border-image-outset:0px; border-image-repeat:stretch; border-image-slice:100%; border-image-source:none; border-image-width:1; border-left:none; border-right:none; border-top-color:rgb(100, 102, 106); border-top-style:dotted; border-top-width:1px; color:rgb(34, 34, 34); height:0px; line-height:24px; margin-bottom:19px; margin-top:20px;'>";
        }
        $message .= "<h3 style='color:rgb(138, 34, 51); font-family:din-2014, sans-serif; font-size:32px; font-style:normal; font-weight:300; margin-bottom:8px;margin-top:3.2px;'>Incident Details</h3>";
        $message .= "<p style='color:rgb(100, 102, 106); font-family:din-2014, sans-serif; font-size:14.4px; font-style:normal; font-weight:300; margin-bottom:20px;'><strong>Date of Incident:</strong><br/>" . $this->attributes['complaint_date'] . "</p>";
        $complaint_date_exceeds_one_year = $this->attributes['complaint_date_exceeds_one_year'];
        if($complaint_date_exceeds_one_year){
            $message .= "<p style='color:rgb(100, 102, 106); font-family:din-2014, sans-serif; font-size:14.4px; font-style:normal; font-weight:300; margin-bottom:20px;'><strong>Reason for Late Complaint:</strong><br/>" . $this->attributes['complaint_time_reason'] . "</p>";
        }
        $message .= "<p style='color:rgb(100, 102, 106); font-family:din-2014, sans-serif; font-size:14.4px; font-style:normal; font-weight:300; margin-bottom:20px;'><strong>Time of Incident:</strong><br/>" . $this->attributes['complaint_time'] . "</p>";
        $message .= "<p style='color:rgb(100, 102, 106); font-family:din-2014, sans-serif; font-size:14.4px; font-style:normal; font-weight:300; margin-bottom:20px;'><strong>Location of Incident:</strong><br/>" . $this->attributes['complaint_location'] . "</p>";
        $message .= "<p style='color:rgb(100, 102, 106); font-family:din-2014, sans-serif; font-size:14.4px; font-style:normal; font-weight:300; margin-bottom:20px;'><strong>Details of Garda Member(s):</strong><br/>" . $this->attributes['complaint_garda_members'] . "</p>";
        $message .= "<p style='color:rgb(100, 102, 106); font-family:din-2014, sans-serif; font-size:14.4px; font-style:normal; font-weight:300; margin-bottom:20px;'><strong>What happened:</strong><br/>" . $this->attributes['complaint_details'] . "</p>";
        $message .= "<p style='color:rgb(100, 102, 106); font-family:din-2014, sans-serif; font-size:14.4px; font-style:normal; font-weight:300; margin-bottom:20px;'><strong>What led up to the incident:</strong><br/>" . $this->attributes['complaint_lead_up'] . "</p>";
        $message .= "<p style='color:rgb(100, 102, 106); font-family:din-2014, sans-serif; font-size:14.4px; font-style:normal; font-weight:300; margin-bottom:20px;'><strong>Believed motive for alleged misbehaviour:</strong><br/>" . $this->attributes['complaint_motive'] . "</p>";

        $has_witnesses_check = $this->attributes['has_witnesses_check'] == "1";
        if(!$has_witnesses_check){
            $message .= "<p style='color:rgb(100, 102, 106); font-family:din-2014, sans-serif; font-size:14.4px; font-style:normal; font-weight:300; margin-bottom:20px;'><strong>Are there any witnesses to the incident?:</strong><br/>" . $no . "</p>";
        } else {
            $message .= "<p style='color:rgb(100, 102, 106); font-family:din-2014, sans-serif; font-size:14.4px; font-style:normal; font-weight:300; margin-bottom:20px;'><strong>Details of any Witnesses:</strong><br/>" . $this->attributes['complaint_witnesses'] . "</p>";
        }
        $has_evidence_check = $this->attributes['has_evidence_check'] == "1";
        if(!$has_evidence_check){
            $message .= "<p style='color:rgb(100, 102, 106); font-family:din-2014, sans-serif; font-size:14.4px; font-style:normal; font-weight:300; margin-bottom:20px;'><strong>Is there any other evidence?:</strong><br/>" . $no . "</p>";
        } else {
            $message .= "<p style='color:rgb(100, 102, 106); font-family:din-2014, sans-serif; font-size:14.4px; font-style:normal; font-weight:300; margin-bottom:20px;'><strong>Details of documentation or evidence:</strong><br/>" . $this->attributes['complaint_evidence'] . "</p>";
        }
        $has_injuries_check = $this->attributes['has_injuries_check'] == "1";
        if(!$has_injuries_check){
            $message .= "<p style='color:rgb(100, 102, 106); font-family:din-2014, sans-serif; font-size:14.4px; font-style:normal; font-weight:300; margin-bottom:20px;'><strong>Did you have any injuries or need medical treatment following the incident?:</strong><br/>" . $no . "</p>";
        } else {
            $message .= "<p style='color:rgb(100, 102, 106); font-family:din-2014, sans-serif; font-size:14.4px; font-style:normal; font-weight:300; margin-bottom:20px;'><strong>Details of injuries or medical treatment received:</strong><br/>" . $this->attributes['complaint_injuries'] . "</p>";
        }
        $message .= "<hr style='border-bottom:none; border-image-outset:0px; border-image-repeat:stretch; border-image-slice:100%; border-image-source:none; border-image-width:1; border-left:none; border-right:none; border-top-color:rgb(100, 102, 106); border-top-style:dotted; border-top-width:1px; color:rgb(34, 34, 34); height:0px; line-height:24px; margin-bottom:19px; margin-top:20px;'>";
        $message .= "<p style='color:rgb(100, 102, 106); font-family:din-2014, sans-serif; font-size:14.4px; font-style:normal; font-weight:300; margin-bottom:20px;'><small>In accordance with Section 110 of the Garda Síochána Act, 2005, a person who provides information to the Ombudsman Commission that the person knows to be false or misleading is guilty of an offence and is liable on summary conviction to a fine not exceeding €2,500 or imprisonment for a term not exceeding six months or both.</small></p>";
        $headers[] = 'From: ' . $this->attributes['complainant_forename'] . ' ' . $this->attributes['complainant_surname'] . ' <support@minus40.co>';

        wp_mail($to, 'Submitted Complaint', $message, $headers = '');
        remove_filter('wp_mail_content_type', function () {
            return 'text/html';
        });
    }

    private function insertData(): void
    {
        try {

            $conn = new \PDO('sqlsrv:Server=' . env("COMPLAINTS_DB_HOST") . ";Database=" . env('COMPLAINTS_DB_NAME'), env('COMPLAINTS_DB_USER'), env('COMPLAINTS_DB_PASSWORD'));
            $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            $stmt = $conn->prepare("INSERT INTO ComplaintsV3 (
                          time_stamp, 
                          submitted_language, 
                          complainant_firstname, 
                          complainant_lastname, 
                          complainant_address1, 
                          complainant_address2, 
                          complainant_county, 
                          complainant_postcode, 
                          complainant_country, 
                          complainant_dateofbirth, 
                          complainant_mobilephonenumber, 
                          complainant_otherphonenumber, 
                          complainant_emailaddress, 
                          is_represented, 
                          rep_type, 
                          rep_firstname, 
                          rep_lastname, 
                          rep_dateofbirth, 
                          rep_mobilephonenumber, 
                          rep_emailaddress,
                          is_correspondenceaddress, 
                          correspondence_address1, 
                          correspondence_address2, 
                          correspondence_county, 
                          correspondence_postcode, 
                          rep_country, 
                          correspondence_mobilephonenumber, 
                          correspondence_otherphonenumber, 
                          correspondence_emailaddress, 
                          inc_incidentdate, 
                          inc_desc_overyearold, 
                          inc_incidenttime, 
                          inc_incidentlocation, 
                          inc_gardadescription, 
                          inc_reasonforcomplain, 
                          inc_circumstances, 
                          inc_motive, 
                          inc_witness, 
                          inc_evidence, 
                          inc_medicaltreament, 
                          ack_useofinformation, 
                          ack_consequencemisleadinginfo, 
                          complainant_gender, 
                          rep_gender, 
                          rep_address1, 
                          rep_address2, 
                          rep_county, 
                          rep_postcode, 
                          rep_otherphonenumber, 
                          previnc_gardastations, 
                          prevInc_contactdate, 
                          previnc_gardadetails, 
                          previnc_details
                          ) 
                              VALUES (
                                      :datetime, 
                                      :submitted_language, 
                                      :complainant_forename, 
                                      :complainant_surname, 
                                      :complainant_primary_address_street_combined, 
                                      :complainant_primary_address_town, 
                                      :complainant_primary_address_county, 
                                      :complainant_primary_address_postcode, 
                                      :complainant_primary_address_country,
                                      :complainant_date_of_birth, 
                                      :complainant_primary_address_mobile, 
                                      :complainant_primary_address_phone, 
                                      :complainant_primary_address_email, 
                                      :representative_required_check, 
                                      :third_party_representative_type, 
                                      :third_party_representative_forename, 
                                      :third_party_representative_surname, 
                                      :third_party_representative_date_of_birth, 
                                      :third_party_representative_mobile, 
                                      :third_party_representative_email, 
                                      :correspondence_check, 
                                      :complainant_secondary_address_street, 
                                      :complainant_secondary_address_town, 
                                      :complainant_secondary_address_county, 
                                      :complainant_secondary_address_postcode, 
                                      :complainant_secondary_address_country, 
                                      :complainant_secondary_address_mobile, 
                                      :complainant_secondary_address_phone, 
                                      :complainant_secondary_address_email, 
                                      :complaint_date, 
                                      :complaint_time_reason, 
                                      :complaint_time, 
                                      :complaint_location, 
                                      :complaint_garda_members, 
                                      :complaint_details, 
                                      :complaint_lead_up, 
                                      :complaint_motive, 
                                      :complaint_witnesses, 
                                      :complaint_evidence, 
                                      :complaint_injuries, 
                                      :declaration, 
                                      :information, 
                                      :complainant_gender,
                                      :third_party_representative_gender,
                                      :third_party_representative_street,
                                      :third_party_representative_town,
                                      :third_party_representative_county,
                                      :third_party_representative_postcode,
                                      :third_party_representative_phone,
                                      :previous_complaint_station,
                                      :previous_complaint_date,
                                      :previous_complaint_garda,
                                      :previous_complaint_outcome
                                      )");

            $current_time = current_time('mysql');
            $submitted_language = $this->lang ? : "en";
            $stmt->bindParam(':datetime', $current_time);
            $stmt->bindParam(':submitted_language', $submitted_language);

            $stmt->bindParam(':complainant_forename', $this->attributes['complainant_forename']);
            $stmt->bindParam(':complainant_surname', $this->attributes['complainant_surname']);
            $stmt->bindParam(':complainant_primary_address_street_combined', $this->attributes['complainant_primary_address_street_combined']);
            $stmt->bindParam(':complainant_primary_address_town', $this->attributes['complainant_primary_address_town']);
            $stmt->bindParam(':complainant_primary_address_county', $this->attributes['complainant_primary_address_county']);
            $stmt->bindParam(':complainant_primary_address_postcode', $this->attributes['complainant_primary_address_postcode']);
            $stmt->bindParam(':complainant_primary_address_country', $this->attributes['complainant_primary_address_country']);
            $stmt->bindParam(':complainant_date_of_birth', $this->attributes['complainant_date_of_birth']);
            $stmt->bindParam(':complainant_primary_address_mobile', $this->attributes['complainant_primary_address_mobile']);
            $stmt->bindParam(':complainant_primary_address_phone', $this->attributes['complainant_primary_address_phone']);
            $stmt->bindParam(':complainant_primary_address_email', $this->attributes['complainant_primary_address_email']);

            $stmt->bindParam(':representative_required_check', $this->attributes['representative_required_check']);
            $stmt->bindParam(':third_party_representative_type', $this->attributes['third_party_representative_type_submit']);
            $stmt->bindParam(':third_party_representative_forename', $this->attributes['third_party_representative_forename']);
            $stmt->bindParam(':third_party_representative_surname', $this->attributes['third_party_representative_surname']);
            $stmt->bindParam(':third_party_representative_date_of_birth', $this->attributes['third_party_representative_date_of_birth']);
            $stmt->bindParam(':third_party_representative_mobile', $this->attributes['third_party_representative_mobile']);
            $stmt->bindParam(':third_party_representative_email', $this->attributes['third_party_representative_email']);

            $stmt->bindParam(':correspondence_check', $this->attributes['correspondence_check']);
            $stmt->bindParam(':complainant_secondary_address_street', $this->attributes['complainant_secondary_address_street']);
            $stmt->bindParam(':complainant_secondary_address_town', $this->attributes['complainant_secondary_address_town']);
            $stmt->bindParam(':complainant_secondary_address_county', $this->attributes['complainant_secondary_address_county']);
            $stmt->bindParam(':complainant_secondary_address_postcode', $this->attributes['complainant_secondary_address_postcode']);
            $stmt->bindParam(':complainant_secondary_address_country', $this->attributes['complainant_secondary_address_country']);
            $stmt->bindParam(':complainant_secondary_address_mobile', $this->attributes['complainant_secondary_address_mobile']);
            $stmt->bindParam(':complainant_secondary_address_phone', $this->attributes['complainant_secondary_address_phone']);
            $stmt->bindParam(':complainant_secondary_address_email', $this->attributes['complainant_secondary_address_email']);

            $stmt->bindParam(':complaint_date', $this->attributes['complaint_date']);
            $stmt->bindParam(':complaint_time_reason', $this->attributes['complaint_time_reason']);
            $stmt->bindParam(':complaint_time', $this->attributes['complaint_time']);
            $stmt->bindParam(':complaint_location', $this->attributes['complaint_location']);
            $stmt->bindParam(':complaint_garda_members', $this->attributes['complaint_garda_members']);
            $stmt->bindParam(':complaint_details', $this->attributes['complaint_details']);
            $stmt->bindParam(':complaint_lead_up', $this->attributes['complaint_lead_up']);
            $stmt->bindParam(':complaint_motive', $this->attributes['complaint_motive']);
            $stmt->bindParam(':complaint_witnesses', $this->attributes['complaint_witnesses']);
            $stmt->bindParam(':complaint_evidence', $this->attributes['complaint_evidence']);
            $stmt->bindParam(':complaint_injuries', $this->attributes['complaint_injuries']);

            $stmt->bindParam(':declaration', $this->attributes['declaration']);
            $stmt->bindParam(':information', $this->attributes['information']);


            $complainant_gender = "N/A";
            $third_party_representative_gender = "";
            $third_party_representative_street = "";
            $third_party_representative_town = "";
            $third_party_representative_county = "";
            $third_party_representative_postcode = "";
            $third_party_representative_phone = "";
            $previous_complaint_station = "";
            $previous_complaint_date = "";
            $previous_complaint_garda = "";
            $previous_complaint_outcome = "";

            $stmt->bindParam(':complainant_gender', $complainant_gender);
            $stmt->bindParam(':third_party_representative_gender', $third_party_representative_gender);
            $stmt->bindParam(':third_party_representative_street', $third_party_representative_street);
            $stmt->bindParam(':third_party_representative_town', $third_party_representative_town);
            $stmt->bindParam(':third_party_representative_county', $third_party_representative_county);
            $stmt->bindParam(':third_party_representative_postcode', $third_party_representative_postcode);
            $stmt->bindParam(':third_party_representative_phone', $third_party_representative_phone);
            $stmt->bindParam(':previous_complaint_station', $previous_complaint_station);
            $stmt->bindParam(':previous_complaint_date', $previous_complaint_date);
            $stmt->bindParam(':previous_complaint_garda', $previous_complaint_garda);
            $stmt->bindParam(':previous_complaint_outcome', $previous_complaint_outcome);

            $stmt->execute();

        }

        catch(PDOException $e) {
            dd($e->getMessage());
        }

        $conn = null;
    }

    public function insertDummyData(): int
    {
        $this->create_local_complaint_table_if_required();


        $current_time = current_time('mysql');
        $submitted_language = $this->lang ? : "en";
        $complainant_gender = "N/A";
        $third_party_representative_gender = "";
        $third_party_representative_street = "";
        $third_party_representative_town = "";
        $third_party_representative_county = "";
        $third_party_representative_postcode = "";
        $third_party_representative_phone = "";
        $previous_complaint_station = "";
        $previous_complaint_date = "";
        $previous_complaint_garda = "";
        $previous_complaint_outcome = "";

        $response = $this->wpdb->insert(
            $this->local_complaint_table_name,
            [
                'time_stamp' => $current_time,
                'submitted_language' => $submitted_language,
                'complainant_firstname' => $this->attributes['complainant_forename'],
                'complainant_lastname' => $this->attributes['complainant_surname'],
                'complainant_address1' => $this->attributes['complainant_primary_address_street_combined'],
                'complainant_address2' => $this->attributes['complainant_primary_address_town'],
                'complainant_county' => $this->attributes['complainant_primary_address_county'],
                'complainant_postcode' => $this->attributes['complainant_primary_address_postcode'],
                'complainant_country' => $this->attributes['complainant_primary_address_country'],
                'complainant_dateofbirth' => $this->attributes['complainant_date_of_birth'],
                'complainant_mobilephonenumber' => $this->attributes['complainant_primary_address_mobile'],
                'complainant_otherphonenumber' => $this->attributes['complainant_primary_address_phone'],
                'complainant_emailaddress' => $this->attributes['complainant_primary_address_email'],

                'is_represented' => $this->attributes['representative_required_check'],
                'rep_type' => $this->attributes['third_party_representative_type_submit'],
                'rep_firstname' => $this->attributes['third_party_representative_forename'],
                'rep_lastname' => $this->attributes['third_party_representative_surname'],
                'rep_dateofbirth' => $this->attributes['third_party_representative_date_of_birth'],
                'rep_mobilephonenumber' => $this->attributes['third_party_representative_mobile'],
                'rep_emailaddress' => $this->attributes['third_party_representative_email'],

                'is_correspondenceaddress' => $this->attributes['correspondence_check'],
                'correspondence_address1' => $this->attributes['complainant_secondary_address_street'],
                'correspondence_address2' => $this->attributes['complainant_secondary_address_town'],
                'correspondence_county' => $this->attributes['complainant_secondary_address_county'],
                'correspondence_postcode' => $this->attributes['complainant_secondary_address_postcode'],
                'rep_country' => $this->attributes['complainant_secondary_address_country'],
                'correspondence_mobilephonenumber' => $this->attributes['complainant_secondary_address_mobile'],
                'correspondence_otherphonenumber' => $this->attributes['complainant_secondary_address_phone'],
                'correspondence_emailaddress' => $this->attributes['complainant_secondary_address_email'],

                'inc_incidentdate' => $this->attributes['complaint_date'],
                'inc_desc_overyearold' => $this->attributes['complaint_time_reason'],
                'inc_incidenttime' => $this->attributes['complaint_time'],
                'inc_incidentlocation' => $this->attributes['complaint_location'],
                'inc_gardadescription' => $this->attributes['complaint_garda_members'],
                'inc_reasonforcomplain' => $this->attributes['complaint_details'],
                'inc_circumstances' => $this->attributes['complaint_lead_up'],
                'inc_motive' => $this->attributes['complaint_motive'],
                'inc_witness' => $this->attributes['complaint_witnesses'],
                'inc_evidence' => $this->attributes['complaint_evidence'],
                'inc_medicaltreament' => $this->attributes['complaint_injuries'],

                'ack_useofinformation' => $this->attributes['declaration'],
                'ack_consequencemisleadinginfo' => $this->attributes['information'],

                'complainant_gender' => $complainant_gender,
                'rep_gender' => $third_party_representative_gender,
                'rep_address1' => $third_party_representative_street,
                'rep_address2' => $third_party_representative_town,
                'rep_county' => $third_party_representative_county,
                'rep_postcode' => $third_party_representative_postcode,
                'rep_otherphonenumber' => $third_party_representative_phone,
                'previnc_gardastations' => $previous_complaint_station,
                'prevInc_contactdate' => $previous_complaint_date,
                'previnc_gardadetails' => $previous_complaint_garda,
                'previnc_details' => $previous_complaint_outcome
            ]
        );

        return $response ? $this->wpdb->insert_id : 0;
    }

    public function create_local_complaint_table_if_required(): void
    {

        $charset = $this->wpdb->get_charset_collate();

        $sql = "CREATE TABLE $this->local_complaint_table_name (
                    id mediumint(9) NOT NULL AUTO_INCREMENT,
                    time_stamp timestamp,
                    submitted_language varchar(4),
                    complainant_firstname varchar(100),
                    complainant_lastname varchar(100),
                    complainant_address1 varchar(4000),
                    complainant_address2 varchar(4000),
                    complainant_county varchar(100),
                    complainant_postcode varchar(100),
                    complainant_dateofbirth varchar(100),
                    complainant_mobilephonenumber varchar(100),
                    complainant_otherphonenumber varchar(100),
                    complainant_emailaddress varchar(100),
                    is_represented varchar(100),
                    rep_type varchar(100),
                    rep_firstname varchar(100),
                    rep_lastname varchar(100),
                    rep_dateofbirth varchar(100),
                    rep_mobilephonenumber varchar(100),
                    rep_emailaddress varchar(100),
                    is_correspondenceaddress varchar(100),
                    correspondence_address1 text,
                    correspondence_address2 text,
                    correspondence_county varchar(100),
                    correspondence_postcode varchar(100),
                    correspondence_mobilephonenumber varchar(100),
                    correspondence_otherphonenumber varchar(100),
                    correspondence_emailaddress varchar(100),
                    inc_incidentdate varchar(100),
                    inc_desc_overyearold text,
                    inc_incidenttime text,
                    inc_incidentlocation varchar(100),
                    inc_gardadescription text,
                    inc_reasonforcomplain text,
                    inc_circumstances text,
                    inc_motive text,
                    inc_witness text,
                    inc_evidence text,
                    inc_medicaltreament text,
                    ack_useofinformation varchar(100),
                    ack_consequencemisleadinginfo varchar(100),
                    complainant_gender varchar(100),
                    complainant_country varchar(100), 
                    rep_gender varchar(100), 
                    rep_address1 varchar(100), 
                    rep_address2 varchar(100), 
                    rep_county varchar(100), 
                    rep_postcode varchar(100), 
                    rep_country varchar(100), 
                    rep_otherphonenumber varchar(100), 
                    previnc_gardastations varchar(100), 
                    prevInc_contactdate varchar(100), 
                    previnc_gardadetails varchar(100), 
                    previnc_details varchar(100),
                    UNIQUE KEY id (id)
                ) $charset;";


        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );

        add_option( 'local_complaint_db_version', static::LOCAL_COMPLAINT_DB_VERSION );
    }


    public function makePDFTag($copy, $pre = '', $tag = "p"): ?string
    {
        $rtn = null;
        if ( ($pre && strlen($pre) > 0) || ($copy && strlen($copy) > 0))
        {
            if ( $pre && strlen($pre) > 0 )
            {
                $copy = '<strong>'. $pre . ': </strong> ' . $copy;
            }

            switch($tag)
            {
                case "h1":
                    $rtn = '<h1 style="color:rgb(138, 34, 51); font-family:din-2014, sans-serif;">' . $copy . '</h1>';
                    break;
                case "h2":
                    $rtn = '<h2 style="color:rgb(138, 34, 51); font-family:din-2014, sans-serif;">' . $copy . '</h2>';
                    break;
                case "h3":
                    $rtn = '<h3 style="color:rgb(138, 34, 51); font-family:din-2014, sans-serif;">' . $copy . '</h3>';
                    break;
                default: //paragraph
                    $rtn = '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif">' . $copy . '</p>'; 
            }
        }

        return $rtn;

    }

    private function buildPDF(): void
    {
        $yes = translateComplaintForm('yes');
        $no = translateComplaintForm('no');

        $complainants_personal_details = translateComplaintForm('complainant_legend', 'fields');
        $name = translateComplaintForm('complainant_name', 'fields');
        $surname = translateComplaintForm('complainant_surname', 'fields');
        $address = translateComplaintForm('complainant_secondary_address_legend', 'fields');
        $mobile = translateComplaintForm('complainant_primary_address_mobile', 'fields');

        $email_address = translateComplaintForm('complainant_primary_address_email', 'fields');
        $representatives_personal_details = translateComplaintForm('representative_legend', 'fields');
        $do_you_require_a_representative = translateComplaintForm('representative_required_legend', 'fields');
        $relationship_to_complainant = translateComplaintForm('third_party_representative_legend', 'fields');
        $select_different_address = translateComplaintForm('correspondence_legend', 'fields');

        $different_address = translateComplaintForm('correspondence_type_2', 'fields');
        $the_complaint = translateComplaintForm('complaint_legend', 'fields');
        $if_the_incident_occurred_more_than_12_months_ago = translateComplaintForm('complaint_time_reason', 'fields');
        $time_of_incident = translateComplaintForm('complaint_time', 'fields');
        $location_of_incident = translateComplaintForm('complaint_location', 'fields');

        $identify_garda = translateComplaintForm('complaint_garda_members', 'fields');
        $specific_misbehaviour = translateComplaintForm('complaint_details', 'fields');
        $what_happened_before = translateComplaintForm('complaint_lead_up', 'fields');
        $why_garda_behaved = translateComplaintForm('complaint_motive', 'fields');
        $any_witnesses = translateComplaintForm('complaint_witnesses_legend', 'fields');

        $witnesses_details = translateComplaintForm('complaint_witnesses', 'fields');
        $any_evidence= translateComplaintForm('complaint_evidence_legend', 'fields');
        $what_evidence = translateComplaintForm('complaint_evidence', 'fields');
        $any_injuries = translateComplaintForm('complaint_injuries_legend', 'fields');
        $describe_injuries = translateComplaintForm('complaint_injuries', 'fields');

        $title = 'GSOC';

        $date_of_birth = translateComplaintForm('date_of_birth', 'additional');
        $other_phone = translateComplaintForm('other_phone', 'additional');
        $behalf_note = translateComplaintForm('behalf_note', 'additional');
        $date_of_incident = translateComplaintForm('date_of_incident', 'additional');


        $child_protection_legend = translateComplaintForm('child_protection_legend', 'fields');
        $declaration_legend = translateComplaintForm( 'declaration_legend', 'fields');
        $information_legend = translateComplaintForm( 'information_legend', 'fields');
        $child_protection_description = translateComplaintForm('child_protection_description_simplified', 'additional');
        $declaration_description= translateComplaintForm( 'declaration_description_simplified', 'additional');
        $information_description = translateComplaintForm( 'information_description_simplified', 'additional');


        $representative_required_check = $this->attributes['representative_required_check'] == "1";
        $correspondence_check = $this->attributes['correspondence_check'] == "1";
        $complaint_date_exceeds_one_year = $this->attributes['complaint_date_exceeds_one_year'];
        $has_witnesses_check = $this->attributes['has_witnesses_check'] == "1";
        $has_evidence_check = $this->attributes['has_evidence_check'] == "1";
        $has_injuries_check = $this->attributes['has_injuries_check'] == "1";

        $pdf_content = '<h1 style="color:rgb(138, 34, 51); font-family:din-2014, sans-serif;">' . $title . '</h1>';
        $pdf_content .= '<hr>';
        $pdf_content .= '<h3 style="color:rgb(138, 34, 51); font-family:din-2014, sans-serif;">' . $complainants_personal_details. '</h3>';
        $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $name. ': </strong>' . $this->attributes['complainant_forename'] . '</p>';
        $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $surname. ': </strong>' . $this->attributes['complainant_surname'] . '</p>';
        $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $address. ': </strong><br>' . $this->attributes['complainant_primary_address_street_combined'] . '<br>' . $this->attributes['complainant_primary_address_town'] . '<br>' . $this->attributes['complainant_primary_address_county'] . '<br>' .$this->attributes['complainant_primary_address_postcode'] . '<br>' .$this->attributes['complainant_primary_address_country'] . '</p>';
        $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $date_of_birth. ': </strong>' . $this->attributes['complainant_date_of_birth'] . '</p>';
        $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $mobile. ': </strong>' . $this->attributes['complainant_primary_address_mobile'] . '</p>';
        $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $other_phone. ': </strong>' . $this->attributes['complainant_primary_address_phone'] . '</p>';
        $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $email_address. ': </strong>' . $this->attributes['complainant_primary_address_email'] . '</p>';
        $pdf_content .= '<hr>';
        $pdf_content .= '<h3 style="color:rgb(138, 34, 51); font-family:din-2014, sans-serif;">' . $representatives_personal_details. '</h3>';
        $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $do_you_require_a_representative .'</strong></p>';
        $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;">' . ($representative_required_check ? $yes : $no) . '</p>';
        $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><small>' . $behalf_note. '</small></p>';
        if($representative_required_check){
            $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $relationship_to_complainant. ': </strong>' . $this->attributes['third_party_representative_type_submit'] . '</p>';
            $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $name. ': </strong>' . $this->attributes['third_party_representative_forename'] . '</p>';
            $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $surname. ': </strong>' . $this->attributes['third_party_representative_surname'] . '</p>';
            $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $date_of_birth. ': </strong>' . $this->attributes['third_party_representative_date_of_birth'] . '</p>';
            $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $mobile. ': </strong>' . $this->attributes['third_party_representative_mobile'] . '</p>';
            $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $email_address. ': </strong>' . $this->attributes['third_party_representative_email'] . '</p>';
            $pdf_content .= '<hr>';
            if(!$correspondence_check){
                $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $select_different_address .'</strong></p>';
                $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;">' . $no . '</p>';
            } else {
                $pdf_content .= '<h3 style="color:rgb(138, 34, 51); font-family:din-2014, sans-serif;">' . $different_address. '</h3>';
                $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $address. ': </strong><br>' . $this->attributes['complainant_secondary_address_street'] . '<br>' . $this->attributes['complainant_secondary_address_town'] . '<br>' . $this->attributes['complainant_secondary_address_county'] . '<br>' . $this->attributes['complainant_secondary_address_postcode'] . '<br>' . $this->attributes['complainant_secondary_address_country'] . '</p>';
                $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $mobile. ': </strong>' . $this->attributes['complainant_secondary_address_mobile'] . '</p>';
                $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $other_phone. ': </strong>' . $this->attributes['complainant_secondary_address_phone'] . '</p>';
                $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $email_address. ': </strong>' . $this->attributes['complainant_secondary_address_email'] . '</p>';
            }
        }
        $pdf_content .= '<hr>';
        $pdf_content .= '<h2 style="color:rgb(138, 34, 51); font-family:din-2014, sans-serif;">' . $the_complaint. '</h2>';
        $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $date_of_incident. ': </strong>' . $this->attributes['complaint_date'] . '</p>';
        if($complaint_date_exceeds_one_year){
            $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $if_the_incident_occurred_more_than_12_months_ago. '</strong></p>';
            $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;">' . $this->attributes['complaint_time_reason'] . '</p>';
        }
        $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $time_of_incident. ': </strong>' . $this->attributes['complaint_time'] . '</p>';
        $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $location_of_incident. ': </strong>' . $this->attributes['complaint_location'] . '</p>';
        $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $identify_garda. '</strong></p>';
        $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;">' . $this->attributes['complaint_garda_members'] . '</p>';
        $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $specific_misbehaviour. '</strong></p>';
        $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;">' . $this->attributes['complaint_details'] . '</p>';
        $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $what_happened_before. '</strong>';
        $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;">' . $this->attributes['complaint_lead_up'] . '</p>';
        $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $why_garda_behaved. '</strong></p>';
        $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;">' .  $this->attributes['complaint_motive'] . '</p>';
        if(!$has_witnesses_check){
            $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $any_witnesses .'</strong></p>';
            $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;">' . $no . '</p>';
        } else {
            $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $witnesses_details. '</strong></p>';
            $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;">' . $this->attributes['complaint_witnesses'] . '</p>';
        }
        if(!$has_evidence_check){
            $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $any_evidence .'</strong></p>';
            $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;">' . $no . '</p>';
        } else {
            $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $what_evidence. '</strong></p>';
            $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;">' . $this->attributes['complaint_evidence'] . '</p>';
        }
        if(!$has_injuries_check){
            $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $any_injuries .'</strong></p>';
            $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;">' . $no . '</p>';
        } else {
            $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><strong>' . $describe_injuries. '</strong>';
            $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;">' . $this->attributes['complaint_injuries'] . '</p>';
        }
        $pdf_content .= '<hr>';
        $pdf_content .= '<h3 style="color:rgb(138, 34, 51); font-family:din-2014, sans-serif;">' . $child_protection_legend. '</h3>';
        $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><small>' . $child_protection_description. '</small></p>';
        $pdf_content .= '<hr>';
        $pdf_content .= '<h3 style="color:rgb(138, 34, 51); font-family:din-2014, sans-serif;">' . $declaration_legend. '</h3>';
        $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><small>' . $declaration_description. '</small></p>';
        $pdf_content .= '<hr>';
        $pdf_content .= '<h3 style="color:rgb(138, 34, 51); font-family:din-2014, sans-serif;">' . $information_legend. '</h3>';
        $pdf_content .= '<p style="color:rgb(100, 102, 106); font-family:din-2014, sans-serif;"><small>' . $information_description. '</small></p>';



        Session::name("buildPDF");
       // '<img src="/app/uploads/logo.jpg" style="width:190px; height:45px; margin:30px 0 30px 0;" />' .
        Session::set('pdfSCJK', in_array($this->lang, ['zhs', 'zht']));
        Session::set('pdfContent', $pdf_content);
    }

    public function generatePDF(): void
    {
        Session::name("buildPDF");
        //$mpdf = new Mpdf();

        $config = !Session::get('pdfSCJK') ? [] :  [
            'mode' => '+aCJK',
            "autoScriptToLang" => true,
            "autoLangToFont" => true
        ];

        $config['tempDir'] = __DIR__ . '/../../uploads/tmppdf/';

        $mpdf = new \Mpdf\Mpdf($config);
        $mpdf->showImageErrors = true;
        $mpdf->charset_in='utf-8';
        $mpdf->SetHeader('GSOC Online Complaint Form: PDF Version ||{PAGENO}');
        $mpdf->SetFooter('www.gardaombudsman.ie');
        $mpdf->WriteHTML(stripslashes(Session::get('pdfContent')));
        $mpdf->Output('submission.pdf', 'I');
        // works??
    }

    /**
     * @return array
     */
    private function rules(): array
    {

        $base = [];
        $base['complainant_forename'] = 'required|min_len,3|max_len,100';
        $base['complainant_surname'] = 'required|min_len,3|max_len,100';
        $base['complainant_primary_address_street'] = 'required|max_len,4000';
        $base['complainant_primary_address_street2'] = 'max_len,4000';
        $base['complainant_primary_address_street3'] = 'max_len,4000';
        $base['complainant_primary_address_town'] = 'required|max_len,4000';
        $base['complainant_primary_address_county'] = 'max_len,100';
        $base['complainant_primary_address_postcode'] = 'max_len,100';
        $base['complainant_primary_address_country'] = 'max_len,100';
        $base['complainant_date_of_birth'] = 'required|custom_date|max_len,100';
        $base['complainant_primary_address_mobile'] = 'required|max_len,100';
        $base['complainant_primary_address_phone'] = 'max_len,100';
        $base['complainant_primary_address_email'] = 'valid_email|max_len,100';
        if($this->attributes['complainant_primary_address_email']){
            $base['complainant_primary_address_email_confirm'] = 'required|valid_email|max_len,100|equalsfield,complainant_primary_address_email';
        }

        //representative_required_check - no validate
        $representative_required_check = ($this->attributes['representative_required_check'] ?? null) == "1";
        if ($representative_required_check)
        {
            $base['has_consent_form_check'] = 'required';
            //third_party_representative_type - no validate
            if(($this->attributes['third_party_representative_type'] ?? null) === 'third_party_representative_type_other'){
                $base['third_party_representative_description_other'] = 'required|max_len,100';
            }

            $base['third_party_representative_forename'] = 'required|min_len,3|max_len,100';
            $base['third_party_representative_surname'] = 'required|min_len,3|max_len,100';
            $base['third_party_representative_date_of_birth'] = 'custom_date|max_len,100';
            $base['third_party_representative_mobile'] = 'max_len,100';
            $base['third_party_representative_email'] = 'valid_email|max_len,100';
            if($this->attributes['third_party_representative_email']){
                $base['third_party_representative_email_confirm'] = 'required|valid_email|max_len,100|equalsfield,third_party_representative_email';
            }

            //correspondence_check - no validate
            $correspondence_check = ($this->attributes['correspondence_check'] ?? null) == "1";
            if($correspondence_check){
                $base['complainant_secondary_address_street'] = 'max_len,4000';
                $base['complainant_secondary_address_town'] = 'max_len,4000';
                $base['complainant_secondary_address_county'] = 'max_len,100';
                $base['complainant_secondary_address_postcode'] = 'max_len,100';
                $base['complainant_secondary_address_country'] = 'max_len,100';
                $base['complainant_secondary_address_mobile'] = 'max_len,100';
                $base['complainant_secondary_address_phone'] = 'max_len,100';
                $base['complainant_secondary_address_email'] = 'valid_email|max_len,100';
                if($this->attributes['complainant_secondary_address_email']){
                    $base['complainant_secondary_address_email_confirm'] = 'required|valid_email|max_len,100|equalsfield,complainant_secondary_address_email';
                }
            }
        }


        $base['complaint_date'] = 'required|custom_date|max_len,100';
        $complaint_date_exceeds_one_year = $this->attributes['complaint_date_exceeds_one_year'] ?? false;
        if($complaint_date_exceeds_one_year){
            $base['complaint_time_reason'] = 'required|max_len,4000';
        }

        $base['complaint_time'] = 'max_len,100';
        $base['complaint_location'] = 'max_len,4000';
        $base['complaint_garda_members'] = 'max_len,4000';
        $base['complaint_details'] = 'required|min_len,5|max_len,4000';
        $base['complaint_lead_up'] = 'max_len,4000';
        $base['complaint_motive'] = 'max_len,4000';
        $has_witnesses_check = ($this->attributes['has_witnesses_check'] ?? null) == "1";
        if($has_witnesses_check){
            $base['complaint_witnesses'] = 'required|max_len,4000';
        }
        $has_evidence_check = ($this->attributes['has_evidence_check'] ?? null) == "1";
        if($has_evidence_check){
            $base['complaint_evidence'] = 'required|max_len,4000';
        }
        $has_injuries_check = ($this->attributes['has_injuries_check'] ?? null) == "1";
        if($has_injuries_check){
            $base['complaint_injuries'] = 'required|max_len,4000';
        }

        $base['child_protection_declaration'] = 'required';
        $base['declaration'] = 'required';
        $base['information'] = 'required';




        return $base;

    }

    public function setupPost(): void
    {
        $post_attribute_keys = [
            'complainant_forename',
            'complainant_surname',
            'complainant_primary_address_street',
            'complainant_primary_address_street2',
            'complainant_primary_address_street3',
            'complainant_primary_address_town',
            'complainant_primary_address_county',
            'complainant_primary_address_postcode',
            'complainant_primary_address_country',
            'complainant_date_of_birth',
            'complainant_primary_address_mobile',
            'complainant_primary_address_phone',
            'complainant_primary_address_email',
            'complainant_primary_address_email_confirm',
            'representative_required_check',
            'has_consent_form_check',
            'third_party_representative_type',
            'third_party_representative_description_other',
            'third_party_representative_forename',
            'third_party_representative_surname',
            'third_party_representative_date_of_birth',
            'third_party_representative_mobile',
            'third_party_representative_email',
            'third_party_representative_email_confirm',
            'correspondence_check',
            'complainant_secondary_address_street',
            'complainant_secondary_address_town',
            'complainant_secondary_address_county',
            'complainant_secondary_address_postcode',
            'complainant_secondary_address_country',
            'complainant_secondary_address_mobile',
            'complainant_secondary_address_phone',
            'complainant_secondary_address_email',
            'complainant_secondary_address_email_confirm',
            'complaint_date',
            'complaint_time_reason',
            'complaint_time',
            'complaint_location',
            'complaint_garda_members',
            'complaint_details',
            'complaint_lead_up',
            'complaint_motive',
            'has_witnesses_check',
            'complaint_witnesses',
            'has_evidence_check',
            'complaint_evidence',
            'has_injuries_check',
            'complaint_injuries',
            'child_protection_declaration',
            'declaration',
            'information'
        ];

        $this->attributes = [];

        foreach ($post_attribute_keys as $key) {
            $this->attributes[$key]  = isset($_POST[$key]) ? stripslashes(filter_var($_POST[$key], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES)) : null;
        }
        $this->attributes['third_party_representative_type_submit'] = null;

        $representative_required_check = $this->attributes['representative_required_check'] == "1";
        if(!$representative_required_check){
            $representative_required_check_keys = [
                'has_consent_form_check',
                'third_party_representative_type',
                'third_party_representative_description_other',
                'third_party_representative_forename',
                'third_party_representative_surname',
                'third_party_representative_date_of_birth',
                'third_party_representative_mobile',
                'third_party_representative_email',
                'third_party_representative_email_confirm',
                'correspondence_check',
                'complainant_secondary_address_street',
                'complainant_secondary_address_town',
                'complainant_secondary_address_county',
                'complainant_secondary_address_postcode',
                'complainant_secondary_address_country',
                'complainant_secondary_address_mobile',
                'complainant_secondary_address_phone',
                'complainant_secondary_address_email',
                'complainant_secondary_address_email_confirm'
            ];

            foreach ($representative_required_check_keys as $key) {
                $this->attributes[$key]  = null;
            }
        } else {
            if($this->attributes['third_party_representative_type'] !== 'third_party_representative_type_other'){
                $this->attributes['third_party_representative_type_submit'] = $this->attributes['third_party_representative_type'] ? translateComplaintForm($this->attributes['third_party_representative_type'], 'fields') : null;
                $this->attributes['third_party_representative_description_other'] = null;
            } else {
                $this->attributes['third_party_representative_type_submit'] = $this->attributes['third_party_representative_description_other'];
            }
            $correspondence_check = isset($this->attributes['correspondence_check']) && $this->attributes['correspondence_check'] == "1";
            if(!$correspondence_check){
                $correspondence_check_keys = [
                    'complainant_secondary_address_street',
                    'complainant_secondary_address_town',
                    'complainant_secondary_address_county',
                    'complainant_secondary_address_postcode',
                    'complainant_secondary_address_country',
                    'complainant_secondary_address_mobile',
                    'complainant_secondary_address_phone',
                    'complainant_secondary_address_email',
                    'complainant_secondary_address_email_confirm'
                ];

                foreach ($correspondence_check_keys as $key) {
                    $this->attributes[$key]  = null;
                }
            }
        }

        $complaint_date_exceeds_one_year = false;
        if($this->attributes['complaint_date']){
            $one_year_ago = strtotime("-1 year", time());
            $complaint_date = strtotime(str_replace('/', '-', $this->attributes['complaint_date']));
            $complaint_date_exceeds_one_year = $complaint_date < $one_year_ago;
        }
        $this->attributes['complaint_date_exceeds_one_year'] = $complaint_date_exceeds_one_year;
        if(!$this->attributes['complaint_date_exceeds_one_year']){
            $this->attributes['complaint_time_reason'] = null;
        }

        $has_witnesses_check = isset($this->attributes['has_witnesses_check']) && $this->attributes['has_witnesses_check'] == "1";
        if(!$has_witnesses_check){
            $this->attributes['complaint_witnesses']  = null;
        }

        $has_evidence_check = isset($this->attributes['has_evidence_check']) && $this->attributes['has_evidence_check'] == "1";
        if(!$has_evidence_check){
            $this->attributes['complaint_evidence']  = null;
        }
        $has_injuries_check = isset($this->attributes['has_injuries_check']) && $this->attributes['has_injuries_check'] == "1";
        if(!$has_injuries_check){
            $this->attributes['complaint_injuries']  = null;
        }

        $complainant_primary_address_street_combined_values = [$this->attributes['complainant_primary_address_street'], $this->attributes['complainant_primary_address_street2'], $this->attributes['complainant_primary_address_street3']];
        $complainant_primary_address_street_combined = "";
        foreach ($complainant_primary_address_street_combined_values as $value) {
            if($value){
                if($complainant_primary_address_street_combined){
                    $complainant_primary_address_street_combined .= " ";
                }
                $complainant_primary_address_street_combined .= $value;
            }
        }
        $this->attributes['complainant_primary_address_street_combined'] = $complainant_primary_address_street_combined ? : null;
    
    }

}