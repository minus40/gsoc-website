<?php

namespace Hailstone\Core\Forms;

use Illuminate\Container\Container;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Translation\Translator;

class ExampleForm
{
    public function __construct($lang = 'en')
    {
        $loader = new FileLoader(new Filesystem, 'lang');
        $translator = new Translator($loader, $lang);

        $validation = new Factory($translator, new Container);

        $data = ['email' => 'notanemail'];
        $rules = ['email' => 'required|email'];

        $validator = $validation->make($data, $rules);

        if ($validator->fails()) {
            $errors = $validator->errors();
        }
    }
}