<?php

namespace Hailstone\Core\Forms;

use Hailstone\Core\Foundation\Application;

class Validator
{
    /**
     * @var Application
     */
    private $app;

    public function __construct(Application $app, $lang = 'en')
    {
        $this->lang = $lang;
        $this->app = $app;
        $this->session = $app->make('session');

        $this->registerCustomValidators();
        $this->customDateValidator();
        $this->gump = new \GUMP();
    }

    /**
     * @param $input
     * @param $rules
     *
     * @return mixed
     */
    public function validate($input, $rules)
    {
        $this->session->setFlash('oldInput', $input);

        $this->gump->validation_rules($rules);

        $isValid = $this->gump->run($input);

        if(!$isValid) {
            $this->setErrors();

            return false;
        }

        return true;
    }

    /**
     *
     */
    private function setErrors()
    {
        $errors = [];

        $validation_rules = $this->gump->validation_rules();


        foreach($this->gump->errors() as $error) {
            $field_rules_string = $validation_rules[$error['field']] ?? "";
            $field_rules = explode("|", $field_rules_string);
            $rule_params = [];
            for($i = 0; $i < count($field_rules); $i ++){
                $rule = explode(",", $field_rules[$i]);
                if(count($rule) > 1){
                    $key = $rule[0];
                    array_shift($rule);
                    $rule_params[$key] = implode(', ', $rule);
                }

            }
            $errors[$error['field']][] = $this->getMessage($error, $rule_params);
        }

        $this->session->setFlash('errors', new ErrorStore($errors));
    }

    /**
     * @param $error
     * @param $rule_params
     *
     * @return mixed
     * @throws \Exception
     */
    private function getMessage($error, $rule_params)
    {
        $file = $this->app->basePath() . "/resources/lang/{$this->lang}/validation.php";

        if(!is_file($file)) {
            throw new \Exception("Validation file does not exist for language {$this->lang}");
        }

        $messages = include $file;

        $message = '';

        //dd($messages);

        if (is_array($error) && array_key_exists('rule', $error)):

            $txt = $error['rule'];

            if (is_array($messages) && array_key_exists($txt, $messages))
            {
                $message .= $messages[$txt];
            }

            if ($message == "")
            {
                $txt = "validate_" . $txt;
                if (is_array($messages) && array_key_exists($txt, $messages))
                {
                    $message .= $messages[$txt];
                }    
            }

        endif;


        $message = str_replace(":attribute", str_replace("_", " ", $error['field']), $message);
        $rule_param = $rule_params && isset($error['rule']) && isset($rule_params[$error['rule']]) ? $rule_params[$error['rule']] : "";

        if (is_array($error) && $rule_param):

            $message = str_replace(":params", $rule_param, $message);
            $message = str_replace(":param", $rule_param, $message);
        endif;


        return $message;
    }

    private function registerCustomValidators()
    {
        \GUMP::add_validator("required_if", function($field, $input, $param = NULL) {

            $params = explode(";", $param);

            if($input[$params[0]] == $params[1] && isset($input[$field]) && (strlen($input[$field]) == 0 || $input[$field] == false || $input[$field] == 0 || $input[$field] == 0.0)) {
                return false;
            }
        }, 'Is Required');

        /*\GUMP::add_validator("required2", function($field, $input, $param = NULL) {

            if (!isset($input[$field]) || empty($input[$field]) || $input[$field] == "") {
                return false;
            }

        }, 'Is Required');*/

    }

    private function customDateValidator()
    {
        \GUMP::add_validator("custom_date", function($field, $input, $param = NULL) {

            if (!isset($input[$field]) || empty($input[$field])) {
                return;
            }

            try {
                $date = \DateTime::createFromFormat('d/m/Y', $input[$field]);

                if(!$date || $date->format('d/m/Y') != $input[$field]){
                    return false;
                }

            } catch(\Exception $e) {
                return false;
            }

        }, 'Invalid');
    }
}
