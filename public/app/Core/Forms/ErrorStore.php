<?php

namespace Hailstone\Core\Forms;

use AdamWathan\Form\ErrorStore\ErrorStoreInterface;
use Illuminate\Support\Collection;

class ErrorStore implements ErrorStoreInterface
{
    private $errors = [];

    /**
     * ErrorStore constructor.
     *
     * @param array $errors
     */
    public function __construct(array $errors = [])
    {
        $this->errors = new Collection($errors);
    }

    /**
     * @param $key
     *
     * @return bool
     */
    public function hasError($key)
    {
        if(key_exists($key, $this->errors)) return true;

        return false;
    }

    /**
     * @param $key
     *
     * @return mixed
     */
    public function getError($key)
    {
        return $this->errors[$key];
    }

    /**
     * @param $field
     * @param $template
     *
     * @return mixed
     */
    public function first($field, $template) {
        return $this->errors->has($field) ? str_replace(":message", $this->errors->pull($field)[0], $template) : null;
    }

}
