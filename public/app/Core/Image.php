<?php

namespace Hailstone\Core;

class Image extends \Timber\Image
{
    var $_alt;

    /**
     * @return string
     */
    public function alt()
    {
        $this->_alt = parent::title();

        if(strlen($this->caption) > 0) {
            $this->_alt = $this->caption;
        }

        if(strlen(parent::alt()) > 0) {
            $this->_alt = parent::alt();
        }

        return $this->_alt;
    }

    /**
     * @param $image
     *
     * @return static
     */
    public static function make($image)
    {
        if(is_array($image) && array_key_exists('ID'))
        {
            return new static($image['ID']);
        }
    }
}