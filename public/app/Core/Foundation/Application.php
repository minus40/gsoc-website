<?php

namespace Hailstone\Core\Foundation;

use Hailstone\Core\Services\CronErrorHandlerService;
use Minus40\ConfigLoader\Loader;

use Blast\Facades\FacadeFactory;
use Dotenv\Dotenv;
use duncan3dc\Sessions\SessionInstance;
use Hailstone\Core\Forms\ErrorStore;
use Hailstone\Core\Providers\ConfigProvider;
use Illuminate\Config\Repository;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Session\SessionManager;
use League\Container\Container;
use Whoops\Handler\Handler;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Run;

class Application
{
    /**
     * The dependency injection container
     *
     * @var Container
     */
    protected $container;

    /**
     * The base path for installation.
     *
     * @var string
     */
    protected $basePath;

    /**
     * The config path for the site. Defaults to /config
     *
     * @var string
     */
    protected $configPath;

    /**
     * The environment settings file. You shouldn't have to change this.
     *
     * @var string
     */
    protected $environmentFile = '.env';

    /**
     * Registered Service provider
     *
     * @var array
     */
    protected $registeredProviders = [];

    /**
     * Registered Aliases
     *
     * @var array
     */
    protected $registeredAliases = [];

    /**
     * Environment variables that MUST be included in the .env file
     * This is basically to stop silly errors
     *
     * @var array
     */
    protected $requiredEnvironmentVariables = [
        'DB_HOST', 'DB_NAME', 'DB_USER', 'DB_PASSWORD', 'DB_PREFIX',
        'AUTH_KEY', 'SECURE_AUTH_KEY', 'LOGGED_IN_KEY', 'NONCE_KEY',
        'AUTH_SALT', 'SECURE_AUTH_SALT', 'LOGGED_IN_SALT', 'NONCE_SALT'
    ];

    /**
     * Application constructor.
     */
    public function __construct($basePath)
    {
        $this->setContainer();
        $this->setBasePath($basePath);
        $this->bootstrapEnvironment();
        $this->bootstrapConfiguration();
        $this->bootstrapSessionHandler();
        $this->setCommonDirectories();
        $this->setErrorHandler();
        $this->bootstrapWordpress();
        $this->bootstrapDatabase();
        $this->bootstrapSecurityKeys();
        $this->registerServiceProviders();
        $this->registerPostTypes();
        $this->registerFacades();

    }

    /**
     * @param $abstract
     * @param null $concrete
     */
    public function bind($abstract, $concrete = null)
    {
        $this->container->add($abstract, $concrete);
    }

    /**
     * @param $abstract
     *
     * @return mixed
     */
    public function make($abstract)
    {
        return $this->container->get($abstract);
    }

    /**
     * Here we set our dependency injection container.
     * The default, at the moment, is the LeagueOfPHP's container
     * http://container.thephpleague.com/
     */
    private function setContainer()
    {
        $this->container = new Container;
    }

    /**
     * @param $basePath
     */
    private function setBasePath($basePath)
    {
        $this->basePath = rtrim($basePath, '\/');
        $this->configPath = $this->basePath . "/config";
    }

    /**
     * Register the basic bindings into the container.
     *
     * @return void
     */
    protected function registerBaseBindings()
    {
        $this->bind('app', $this);
    }

    /**
     * This loads in the DotEnv condig file and sets the required variables
     */


    private function bootstrapEnvironment()
    {

        $this->env = Dotenv::createImmutable($this->basePath);

        $this->env->load();
        $this->env->required($this->requiredEnvironmentVariables);


        // SSL SUPPORT FOR LOAD BALANCERS
        if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && strpos($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') !== false)
        {
            $_SERVER['HTTPS']='on';
        }

        $this->checkForMaintenanceMode();
    }


    /**
     *
     */
    private function bootstrapConfiguration()
    {

        $loader = new Loader(
            basePath: $this->configPath,
        );

        $loader->loadAll();

        $this->config = $loader->config();
    }


    private function setErrorHandler()
    {

        if($_SERVER["HTTP_HOST"] ?? ""){
            $whoops = new Run();
            $handler = new PrettyPageHandler();
            $handler->addResourcePath($this->basePath());
            $handler->addCustomCss('whoops.css');
            $whoops->pushHandler($handler);


            if(!$this->findValueInConfig('app.debug') || env('APP_ENV') == 'production')
            {
                $whoops->pushHandler(function($exception, $inspector, $run)
                {
                    echo "Something went wrong.";
                    return Handler::QUIT;
                });
            }

            $whoops->register();
        } else {
            CronErrorHandlerService::getInstance()->handleCronError();
        }
    }

    private function bootstrapSessionHandler()
    {
        $this->bind('session', new SessionInstance('hailstone_cookie'));
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return $this->config;
    }

    private function bootstrapWordpress()
    {
        define('WP_ENV', env('APP_ENV', "local"));
        define('WP_DEFAULT_THEME', 'hailstone' );
 
        define('CONTENT_DIR', $this->findValueInConfig('app.directories.content') );
        define('WP_DIR',  $this->findValueInConfig('app.directories.wordpress') );
 
        define('WP_HOME',$this->findValueInConfig('app.url') );
        define('WP_SITEURL', $this->findValueInConfig('app.url') . WP_DIR);
 
        define('WP_CONTENT_DIR', $this->publicPath . CONTENT_DIR);
        define('WP_CONTENT_URL', WP_HOME . CONTENT_DIR);
 

        if ($this->findValueInConfig('app.debug')) // show errors
        {
           
            define('WP_DEBUG', true);
            define('WP_DEBUG_DISPLAY', true );
            define('SAVEQUERIES', true);
            define('SCRIPT_DEBUG', true);
            define('WP_DEBUG_LOG', true);
            ini_set('error_log', WP_DIR . '/debug.txt' );
            define('WP_DISABLE_FATAL_ERROR_HANDLER', true);
            ini_set('error_reporting', E_ALL & ~E_DEPRECATED & ~E_USER_DEPRECATED & ~E_STRICT );
            ini_set( 'display_errors', 1 );

            $GLOBALS['wp_filter'] = [
                'enable_wp_debug_mode_checks' => [
                  10 => [[
                    'accepted_args' => 0,
                    'function'      => function() { return false; },
                  ]],
                ],
            ];

        }
        else // optionally move errors to debug file
        {
            define('WP_DEBUG', false);
            define('WP_DEBUG_DISPLAY', false );
            define('SAVEQUERIES', false); // change here
            define('SCRIPT_DEBUG', false); // here
            define('WP_DEBUG_LOG', false); // and here
            ini_set('error_log', WP_DIR . '/debug.txt' );
            ini_set('error_reporting', 0);
        }

    }

    /**
     *
     */
    private function bootstrapDatabase()
    {
        define('DB_CHARSET', $this->findValueInConfig('database.charset') );
        define('DB_COLLATE', $this->findValueInConfig('database.collation') );

        define('DB_NAME', $this->findValueInConfig('database.database') );
        define('DB_USER', $this->findValueInConfig('database.username') );
        define('DB_PASSWORD', $this->findValueInConfig('database.password'));
        define('DB_HOST', $this->findValueInConfig('database.host'));


    }

    /**
     *
     */
    private function bootstrapSecurityKeys()
    {
        define('AUTH_KEY', env('AUTH_KEY'));
        define('SECURE_AUTH_KEY', env('SECURE_AUTH_KEY'));
        define('LOGGED_IN_KEY', env('LOGGED_IN_KEY'));
        define('NONCE_KEY', env('NONCE_KEY'));
        define('AUTH_SALT', env('AUTH_SALT'));
        define('SECURE_AUTH_SALT', env('SECURE_AUTH_SALT'));
        define('LOGGED_IN_SALT', env('LOGGED_IN_SALT'));
        define('NONCE_SALT', env('NONCE_SALT'));
    }

    /**
     *
     */
    private function setCommonDirectories()
    {
       // $this->publicPath = $this->basePath . $this->config->get('app.directories.public');
        $this->publicPath = $this->basePath . '/public';
    }

    /**
     *
     */
    private function registerServiceProviders()
    {
        foreach($this->findValueInConfig('app.providers') as $provider)
        {
            $this->registeredProviders[] = $provider;
        }
    }

    public function bootServiceProviders()
    {
        foreach($this->registeredProviders as $provider)
        {
            (new $provider($this))->register();
        }
    }


    private function registerPostTypes()
    {
        foreach($this->findValueInConfig('posts')  as $key => $post) {
            if($post['enabled']) {
                $this->registeredProviders[] = $post['provider'];
                $this->registeredAliases = array_merge($this->registeredAliases, $post['alias']);
            }
        }
    }

    private function registerFacades()
    {
        
        //dd($this->container);
        FacadeFactory::setContainer($this->container);

        foreach($this->findValueInConfig('app.aliases')  as $alias => $provider)
        {
            $this->registeredAliases[$alias] = $provider;
        }

        foreach($this->registeredAliases as $alias => $provider)
        {
            class_alias($provider, $alias);
        }
    }

    public function basePath()
    {
        return $this->basePath;
    }


    /**
     *
     */
    private function checkForMaintenanceMode()
    {
        if(file_exists($this->basePath() . "/.maintenance")) {
            header("HTTP/1.1 503 Service Temporarily Unavailable");
            include $this->basePath() . '/resources/views/errors/503.php';
            exit;
        }
    }

    // amn Jan 2023 to allow removal illuminate and aedart from projects
    // simply checks if a key exists and returns value
    public function findValueInConfig($nest)
    {   
        $container = $this->config;

        $nest = explode('.', $nest);

        if ($container && $nest && is_array($nest) && count($nest) > 0)
        {
            $final = count($nest) - 1;
            $lastItem = $nest[$final];
            $steps = 0;
            foreach($nest as $pos => $item)
            {
                if (array_key_exists($item, $container))
                {
                    $steps ++;
                    if ($item == $lastItem && $steps == count($nest) )
                    {
                        return $container[$item];
                    }
                    else
                    {
                        $container = $container[$item];
                    }
                }

            }
        }
        return null;
    }

}