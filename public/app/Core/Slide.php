<?php

namespace Hailstone\Core;

class Slide
{
    var $_heading;
    var $_subheading;
    var $_video = null;
    var $_image = null;
    var $_actions = null;

    public function __construct(array $data = [])
    {
        $this->build($data);
    }

    private function build($data)
    {
        $this->_heading = !empty($data['heading']) ? $data['heading'] : null;
        $this->_subheading = !empty($data['subheading']) ? $data['subheading'] : null;
        $this->_actions = !empty($data['actions']) ? $data['actions'] : null;
        $this->_video = !empty($data['video']) ? new Video($data['video']->ID) : null;
        $this->_image = !empty($data['image']) ? new Image($data['image']['ID']) : null;
    }

    /**
     * @return mixed
     */
    public function heading()
    {
        return $this->_heading;
    }

    /**
     * @return mixed
     */
    public function subheading()
    {
        return $this->_subheading;
    }

    /**
     * @return Video|null
     */
    public function video()
    {
        return $this->_video;
    }

    /**
     * @return Video|null
     */
    public function image()
    {
        return $this->_image;
    }

    /**
     * @return array|null
     */
    public function actions()
    {
        return $this->_actions;
    }

}