<?php
/*
|--------------------------------------------------------------------------
| Using Themes
|--------------------------------------------------------------------------
|
| This would only be set to false, if, for some reason, you're using
| this as a child theme. In 99.99% of cases, leave this alone.
|
*/

define('WP_USE_THEMES', true);

/*
|--------------------------------------------------------------------------
| Bootstrap Wordpress
|--------------------------------------------------------------------------
|
| Load in the Wordpress boostrapper. This automatically looks for
| and loads wp-config.php. More magic happens in there.
|
*/


require(dirname(__FILE__) . '/wp/wp-blog-header.php');